<?php
//Preparamos la conexión:
include("../includes/connection.php");

//Se inicia la sesion del usuario.
session_start();

$ID = $_SESSION['ID_alumno'];

$NOMBRE = $_SESSION['Nombre'];
$APELLIDOS = $_SESSION['Apellidos'];

$RUT = $_SESSION['Rut'];
$FECHA_NACIMIENTO = $_SESSION['Fecha_nacimiento'];

$MAIL = $_SESSION['Mail'];
$TELEFONO = $_SESSION['Telefono'];
$DIRECCION = $_SESSION['Direccion'];

$CINTURON = $_SESSION['Cinturon'];
$GRADO = $_SESSION['Grado'];
$CANTIDAD_CLASES = $_SESSION['Cantidad_clases'];
$FECHA_GRADO = $_SESSION['Fecha_grado'];

$OBSERVACIONES = $_SESSION['Observaciones'];
$EXPERIENCIA = $_SESSION['Experiencia'];

$PROMO_OFERTAS = $_SESSION['Promo_ofertas'];

$ACTIVE = $_SESSION['active'];

$sql = "SELECT Imagen FROM Alumno WHERE ID_alumno = '$ID'";
$result = $connect->query($sql);
$row = $result->fetch_assoc();

if(!isset($ID) OR $ACTIVE == 0){
    session_destroy();
    header("location: ../index.html");
    exit();
}

switch($CINTURON){
    case 1:
        $cinturon = "Ninguno";
        break;
    case 2:
        $cinturon = "Blanco";
        break;
    case 3:
        $cinturon = "Azul";
        break;
    case 4:
        $cinturon = "Morado";
        break;
    case 5:
        $cinturon = "Marron";
        break;
    case 6: 
        $cinturon = "Negro";
        break;
    default:
        break;
}
switch($GRADO){
    case 1:
        $grado = "Ninguno";
        break;
    case 2:
        $grado = "I";
        break;
    case 3:
        $grado = "II";
        break;
    case 4:
        $grado = "III";
        break;
    case 5: 
        $grado = "IV";
        break;
    default:
        break;
}

$verificador_membresia = mysqli_query($connect, "SELECT COUNT(*) AS verificador_membresia FROM Membresia WHERE ID_alumno = '$ID'");
$contador_membresia = mysqli_fetch_array($verificador_membresia);

if($contador_membresia['verificador_membresia'] == 0){
    $existencia_membresia = 0;
}else{
    $existencia_membresia = 1;

    $peticion_membresia = mysqli_query($connect, "SELECT * FROM Membresia WHERE ID_alumno = '$ID'");

    while($tabla_membresia = $peticion_membresia->fetch_assoc()){
        $ID_MEMBRESIA = $tabla_membresia['ID_membresia'];

        $PAGO_MEMBRESIA = $tabla_membresia['Pago'];

        $FECHA_PAGO_MEMBRESIA = $tabla_membresia['Fecha_pago'];
        $FECHA_TERMINO_MEMBRESIA = $tabla_membresia['Fecha_termino'];

        switch($PAGO_MEMBRESIA){
            case 0:
                $pago = "Usted no ha pagado su membresia.";
                break;
            case 1:
                $pago = "Usted pago su membresia.";
                break;
            default:
                break;
        }
    }
}
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="membresia.css">
    <title>Estado de la membresia</title>
</head>
<body>
    <ul>
		<li class="log"> <?php echo "<p>Bienvenido $NOMBRE $APELLIDOS</p>";?> </li>
		<li><a href="../usuario/usuario_php/home.php">Inicio</a></li>
		<li class="active"><a href="../usuario/perfil.php">Perfil</a></li>
		<li><a href="../usuario/horario.php">Horario</a></li>
		<li><a href="../usuario/planes.php">Planes</a></li>
		<li><a href="../general/general_php/logout.php">Cerrar sesion</a></li>
	</ul>

    <div class="tarjeta">
        <h1>Membresia</h1>
        </html>
            <?php
            if($existencia_membresia == 0){
            ?>
                <html>
                    <p>
                        Usted no tiene membresia.
                    </p>
                </html>
            <?php
            }else{
            ?>
                <html>
                    <p>
                        Estado de pago: <?php echo $pago;?>
                        <br>
                        <br>
                        Fecha de pago: <?php echo $FECHA_PAGO_MEMBRESIA;?>
                        <br>
                        Fecha de termino: <?php echo $FECHA_TERMINO_MEMBRESIA;?>
                    </p>
                </html>
            <?php
            }
            ?>
        <html>
    </div>
    
</body>
</html>
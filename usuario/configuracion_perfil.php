<?php
//Preparamos la conexión:

include("../includes/connection.php");

//Se inicia la sesion del usuario.

session_start();

$ID = $_SESSION['ID_alumno'];

$NOMBRE = $_SESSION['Nombre'];
$APELLIDOS = $_SESSION['Apellidos'];

$RUT = $_SESSION['Rut'];
$FECHA_NACIMIENTO = $_SESSION['Fecha_nacimiento'];

$MAIL = $_SESSION['Mail'];
$TELEFONO = $_SESSION['Telefono'];
$DIRECCION = $_SESSION['Direccion'];

$CINTURON = $_SESSION['Cinturon'];
$GRADO = $_SESSION['Grado'];
$CANTIDAD_CLASES = $_SESSION['Cantidad_clases'];
$FECHA_GRADO = $_SESSION['Fecha_grado'];

$OBSERVACIONES = $_SESSION['Observaciones'];
$EXPERIENCIA = $_SESSION['Experiencia'];

$PROMO_OFERTAS = $_SESSION['Promo_ofertas'];

$ACTIVE = $_SESSION['active'];

if(!isset($ID) OR $ACTIVE == 0){
    session_destroy();
    header("location: ../index.html");
    exit();
}

switch($CINTURON){
    case 1:
        $cinturon = "Ninguno";
        break;
    case 2:
        $cinturon = "Blanco";
        break;
    case 3:
        $cinturon = "Azul";
        break;
    case 4:
        $cinturon = "Morado";
        break;
    case 5:
        $cinturon = "Cafe";
        break;
    case 6: 
        $cinturon = "Negro";
        break;
    default:
        break;
}
switch($GRADO){
    case 1:
        $grado = "Ninguno";
        break;
    case 2:
        $grado = "I";
        break;
    case 3:
        $grado = "II";
        break;
    case 4:
        $grado = "III";
        break;
    case 5: 
        $grado = "IV";
        break;
    default:
        break;
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="usuario_css/config.css">
    <title>Configuracion perfil</title>
</head>
<body>
    <ul>
		<li class="log"> <?php echo "<p>Bienvenido $NOMBRE $APELLIDOS</p>";?> </li>
		<li><a href="usuario_php/home.php">Inicio</a></li>
		<li class="active"><a href="perfil.php">Perfil</a></li>
		<li><a href="horario.php">Horario</a></li>
		<li><a href="planes.php">Planes</a></li>
		<li><a href="../general/general_php/logout.php">Cerrar sesion</a></li>
	</ul>

    <div class="editar_perfil">
        <div class="centro">
            <h2>Editar perfil</h2>
            
            <form action="usuario_php/actualizar_perfil.php?opcion=1" method="post">
                <div class="nombre">
                    <label for="nombre"><b>Nombres</b></label>
                    <input class="controls" type="text" placeholder="Ingrese sus nombres" name="nombre" id="nombre" value="<?php echo $NOMBRE;?>">
                </div>
                <div class="apellido">
                    <label for="apellido"><b>Apellidos</b></label>
                    <input class="controls" type="text" placeholder="Ingrese su apellido" name="apellido" id="apellido" value="<?php echo $APELLIDOS;?>">
                </div>
                <div class="rut">
                    <label for="rut"><b>RUT</b></label>
                    <input class="controls" type="text" placeholder="Ingrese su RUT, ej: 11.111.111-1" name="rut" id="rut" value="<?php echo $RUT;?>">
                </div>
                <div class="direccion">
                    <label for="direccion"><b>Dirección</b></label>
                    <input class="controls" type="text" placeholder="Ingrese su dirección" name="direccion" id="direccion" value="<?php echo $DIRECCION;?>">
                </div>
                <div class="telefono">
                    <label for="telefono"><b>Teléfono</b></label>
                    <input class="controls" type="text" placeholder="Ingrese su teléfono" name="telefono" id="telefono" value="<?php echo $TELEFONO;?>">
                </div>
                <div class="fecha">
                    <label for="fecha"><b>Fecha de nacimiento</b></label>
                    <input class="controls" type="date" name="fecha" id="fecha" value="<?php echo $FECHA_NACIMIENTO;?>">
                </div>
               
                <div class="experiencia">
                    <label for="experiencia"><b>Experiencia</b></label>
                    <textarea name="experiencia" id="experiencia" cols="30" rows="7" class="controls" maxlength="255" minlength="1"  placeholder="Escriba su experiencia en las artes marciales"><?php echo $EXPERIENCIA;?></textarea>
                </div>
                <br>
                <div class="enviar">
                    <input type="submit" value="Actualizar perfil" name="actualizar" class="buttons">  
                </div>          
            </form>
        </div>
        <div class="centro">
            <form action="upload.php" method="post" enctype="multipart/form-data">
                <label>Seleccione una foto de perfil:</label>
                <input type="file" name="image">
                <input type="submit" name="submit" value="Confirmar">
            </form>
        </div>
    </div>
    <div class="actualizar_contraseña">
        <div class="centro">
        <h2>Actualizar contraseña</h2>
        <form action="usuario_php/actualizar_perfil.php?opcion=2" method="post">
            <div id="cambiar_password">
            <label for="password"><b>Contraseña</b></label>
            <input class="controls" type="password" alt="strongPass" required="required" placeholder="Ingrese su contraseña" name="psw" id="psw">
    
            <label for="password_repeat"><b>Repita la Contraseña</b></label>
            <input class="controls" type="password" required="required" placeholder="Repita su contraseña" name="psw-repeat" id="psw-repeat">
            </div>
            <div class="enviar_password">
                <input type="submit" value="Actualizar contraseña" name="actualizar" class="buttons" id="send_password">  
            </div>         
        </form>

        <h2>Eliminar cuenta</h2>

        <div id="eliminar_container" class="centro">
            <button id="boton_eliminar" class="buttons">Eliminar cuenta</button>
        </div>
        
        <div id="confirmacion_container" class="centro">
            <label for="buttons">¿Esta seguro de que quiere eliminar su cuenta?</label>
            <br>
            <button  id="boton_eliminar" class="buttons"><a href="usuario_php/actualizar_perfil.php?opcion=3">Eliminar cuenta</a></button>
            <button  id="boton_eliminar_cancelacion" class="buttons">Cancelar</button>
        </div>
    </div>
    </div>
    

    <script>
        document.getElementById("confirmacion_container").style.display = "none";

        document.getElementById("boton_eliminar").onclick = mostrarDiv;
        document.getElementById("boton_eliminar_cancelacion").onclick = MostrarDiv_eliminar;
        
        function mostrarDiv(){
            document.getElementById("eliminar_container").style.display = "none";
            document.getElementById("confirmacion_container").style.display = "block";
        }

        function MostrarDiv_eliminar(){
            document.getElementById("eliminar_container").style.display = "block";
            document.getElementById("confirmacion_container").style.display = "none";
        }

    </script>    
</body>
</html>
<?php
//Preparamos la conexión:
include("../includes/connection.php");
include("../includes/funciones.php");

//Se inicia la sesion del usuario.
session_start();

$ID = $_SESSION['ID_alumno'];

$NOMBRE = $_SESSION['Nombre'];
$APELLIDOS = $_SESSION['Apellidos'];

$RUT = $_SESSION['Rut'];
$FECHA_NACIMIENTO = $_SESSION['Fecha_nacimiento'];

$MAIL = $_SESSION['Mail'];
$TELEFONO = $_SESSION['Telefono'];
$DIRECCION = $_SESSION['Direccion'];

$CINTURON = $_SESSION['Cinturon'];
$GRADO = $_SESSION['Grado'];
$CANTIDAD_CLASES = $_SESSION['Cantidad_clases'];
$FECHA_GRADO = $_SESSION['Fecha_grado'];

$OBSERVACIONES = $_SESSION['Observaciones'];
$EXPERIENCIA = $_SESSION['Experiencia'];

$PROMO_OFERTAS = $_SESSION['Promo_ofertas'];

$ACTIVE = $_SESSION['active'];

$sql = "SELECT Imagen FROM Alumno WHERE ID_alumno = '$ID' AND Imagen != 'NULL'";
$result = $connect->query($sql);
$row = $result->fetch_assoc();

$consulta_burpees = mysqli_query($connect, "SELECT * FROM Alumno WHERE ID_alumno = '$ID'");
if($consulta_burpees){
    while($tabla_alumno = $consulta_burpees->fetch_array()){
        $BURPEES = $tabla_alumno['Burpees'];
    }
}

if(!isset($ID) OR $ACTIVE == 0){
    session_destroy();
    header("location: ../index.html");
    exit();
}

switch($CINTURON){
    case 1:
        $cinturon = "Ninguno";
        break;
    case 2:
        $cinturon = "Blanco";
        break;
    case 3:
        $cinturon = "Azul";
        break;
    case 4:
        $cinturon = "Morado";
        break;
    case 5:
        $cinturon = "Marron";
        break;
    case 6: 
        $cinturon = "Negro";
        break;
    default:
        break;
}
switch($GRADO){
    case 1:
        $grado = "Ninguno";
        break;
    case 2:
        $grado = "I";
        break;
    case 3:
        $grado = "II";
        break;
    case 4:
        $grado = "III";
        break;
    case 5: 
        $grado = "IV";
        break;
    default:
        break;
}

?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta name="viewport" content="width=device-width" />
    <link rel="stylesheet" href="usuario_css/perfil.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Electrolize&display=swap" rel="stylesheet">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title> Perfil de <?php echo "$NOMBRE $APELLIDOS";?></title>
    <ul>
		<li class="log"> <?php echo "<p>Bienvenido $NOMBRE $APELLIDOS</p>";?> </li>
		<li><a href="usuario_php/home.php">Inicio</a></li>
		<li class="active"><a href="#">Perfil</a></li>
		<li><a href="horario.php">Horario</a></li>
		<li><a href="planes.php">Planes</a></li>
		<li><a href="../general/general_php/logout.php">Cerrar sesion</a></li>
	</ul>
</head>

<body>
    <div class="perfil">
        <h1>Perfil de <?php echo "$NOMBRE $APELLIDOS";?> </h1>

        <div class="container-user-info">
            <div class="contenedor_imagen">
                <?php if($result->num_rows > 0){ ?>
                    <img class="imagen" src="data:image/jpg;charset=utf8;base64,<?php echo base64_encode($row['Imagen']); ?>" /> 
                <?php }else{ ?>
                    <img src="https://i.ibb.co/3Tyr88g/no-image.png" alt="Foto perfil d" class="imagen">
                <?php } ?>
            </div>

            <div class="user-nombre">
                <h3 class="nombre"><?php echo "$NOMBRE"." "."$APELLIDOS";?></h3>
            </div>

            <div class="user-cinturon">
                <h3 class="cinturon"><?php echo "$cinturon"." "."$grado";?></h3>
            </div>

            <div class="inscripciones-and-info">
                <div class="Inscripciones">
                    <h2>Clases inscritas: </h2>

                    <div class="scroll">
                        
                    </html>

                    <?php
                    $peticion_verificacion_inscripciones = mysqli_query($connect, "SELECT COUNT(*) AS contador_inscripciones FROM Inscripcion WHERE ID_alumno='$ID'");
                    $contador_inscripciones = mysqli_fetch_array($peticion_verificacion_inscripciones);

                    if($contador_inscripciones['contador_inscripciones'] > 0){
                        $peticion_inscripciones = mysqli_query($connect, "SELECT * FROM Inscripcion WHERE ID_alumno='$ID'");
                        if($peticion_inscripciones){
                            while($tabla = $peticion_inscripciones->fetch_array()){
                                $ID_INSCRIPCION = $tabla['ID_inscripcion'];
                                $ID_CLASE = $tabla['ID_clase'];

                                $peticion_clases = mysqli_query($connect, "SELECT * FROM Clase WHERE ID_clase='$ID_CLASE' AND Visibilidad='1'");
                                if($peticion_clases){
                                    while($tabla = $peticion_clases->fetch_array()){
                                        $TIPO_CLASE = $tabla['Tipo_clase'];
                                        $ID_PROFESOR_CLASE = $tabla['ID_profesor'];
                                        $CUPOS_CLASE = $tabla['Cupos'];

                                        $DIA_CLASE = $tabla['Dia'];
                                        $HORA_CLASE = $tabla['Hora'];

                                        $consulta_cupos = "SELECT COUNT(*) AS contador_cupos FROM Inscripcion WHERE ID_clase='$ID_CLASE'";
                                        $peticion_consulta_cupos = mysqli_query($connect, $consulta_cupos);
                                        $contador_cupos = mysqli_fetch_array($peticion_consulta_cupos);

                                        if($contador_cupos['contador_cupos'] >= 0){
                                            $cupos_clase = $CUPOS_CLASE - $contador_cupos['contador_cupos'];
                                        }

                                        switch($DIA_CLASE){
                                            case 1:
                                                $dia_clase = "Lunes";
                                                break;
                                            case 2:
                                                $dia_clase = "Martes";
                                                break;
                                            case 3:
                                                $dia_clase = "Miércoles";
                                                break;
                                            case 4:
                                                $dia_clase = "Jueves";
                                                break;
                                            case 5:
                                                $dia_clase = "Viernes";
                                                break;
                                            case 6:
                                                $dia_clase = "Sabado";
                                                break;
                                            case 7:
                                                $dia_clase = "Domingo";
                                                break;
                                            default:
                                                break;
                                        }
                    ?>
                    <html>
                        <div>
                            <p>
                                <a href="../clase/Alumnos/vista_clase_alumno.php?id_clase=<?php echo $ID_CLASE;?>"><?php echo switchClases($TIPO_CLASE);?></a>
                                <br>
                                Cupos disponibles: <?php echo $cupos_clase;?>
                                <br>
                                <?php echo "$dia_clase|| ".date('H:i', strtotime($HORA_CLASE));?>

                            </p>
                        </div>
                    
                    </html> 

                    <?php
                                    }
                                }
                            }
                        }
                    }else{
                        echo "<p>Usted no esta inscrito a ninguna clase.</p>";
                    }          
                    ?>
                    <html>
                    </div>
                </div>

                <div class="asistencia_mensualidad_configuracion">
                    <div>
                        <h3 class= "asis">Asistencia Total: <?php echo $CANTIDAD_CLASES;?></h3>
                        <h3 class="asis"> Burpees: <?php echo $BURPEES;?></h3>
                    </div>
                    <div class="centro">
                        <button class="link"><a href="../membresia/membresia_usuario.php">Estado Mensualidad</a></button>
                    </div>
                    <div class="centro">
                        <button><a href="configuracion_perfil.php">Configuracion del perfil</a></button>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
</body>
</html>
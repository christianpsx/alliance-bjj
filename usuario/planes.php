<?php
session_start();

$ID = $_SESSION['ID_alumno'];

$NOMBRE = $_SESSION['Nombre'];
$APELLIDOS = $_SESSION['Apellidos'];

$RUT = $_SESSION['Rut'];
$FECHA_NACIMIENTO = $_SESSION['Fecha_nacimiento'];

$MAIL = $_SESSION['Mail'];
$TELEFONO = $_SESSION['Telefono'];
$DIRECCION = $_SESSION['Direccion'];

$CINTURON = $_SESSION['Cinturon'];
$GRADO = $_SESSION['Grado'];
$CANTIDAD_CLASES = $_SESSION['Cantidad_clases'];
$FECHA_GRADO = $_SESSION['Fecha_grado'];

$OBSERVACIONES = $_SESSION['Observaciones'];
$EXPERIENCIA = $_SESSION['Experiencia'];

$PROMO_OFERTAS = $_SESSION['Promo_ofertas'];

$ACTIVE = $_SESSION['active'];

if(!isset($ID) OR $ACTIVE == 0){
    session_destroy();
    header("location: ../index.html");
    exit();
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Planes Alliance</title>
    <link rel="stylesheet" href="usuario_css/planes.css">
        <ul>
            <li class="log"> <?php echo "<p>Bienvenido $NOMBRE $APELLIDOS</p>";?> </li>
            <li><a href="usuario_php/home.php">Inicio</a></li>
            <li><a href="perfil.php">Perfil</a></li>
            <li><a href="horario.php">Horario</a></li>
            <li class="active"><a href="#">Planes</a></li>
            <li><a href="../general/general_php/logout.php">Cerrar sesion</a></li>
        </ul>

</head>

<body>
    <div class="galeria">
        <div class="contenedor-imagenes">
            <div class="imagen">
                <img src="../imagenes/alliance3.jpg" alt="">
                <div class="overlay">
                    <h2><a href="https://www.flow.cl/btn.php?token=gbot1m6">Plan Mensual $65.000</a></h2> 
                </div>
            </div>
            <div class="imagen">
                <img src="../imagenes/alliance3.jpg" href="https://www.flow.cl/app/web/pagarBtnPago.php?token=0iksrwz" alt="">
                <div class="overlay">
                    <h2><a href="https://www.flow.cl/btn.php?token=2f33ogp">Plan Semestral $245.000</a></h2>
                </div>
            </div>
            <div class="imagen">
                <img src="../imagenes/alliance3.jpg" href="https://www.flow.cl/app/web/pagarBtnPago.php?token=0iksrwz" alt="">
                <div class="overlay">
                    <h2> <a href="https://www.flow.cl/btn.php?token=ujqvqbg">Plan Anual $425.000</a></h2>
                </div>
            </div>
            <div class="imagen">
                <img src="../imagenes/alliance3.jpg" href="https://www.flow.cl/app/web/pagarBtnPago.php?token=0iksrwz" alt="">
                <div class="overlay">
                    <h2><a href="https://www.flow.cl/btn.php?token=iqsxiub">Plan Juvenil $55.000</a></h2> 
                </div>
            </div>
            <div class="imagen">
                <img src="../imagenes/alliance3.jpg" href="https://www.flow.cl/app/web/pagarBtnPago.php?token=0iksrwz" alt="">
                <div class="overlay">
                    <h2>  <a href="https://www.flow.cl/btn.php?token=i7wgap1">Seminario Dimitrius Souza $80.000</a></h2>
                </div>
            </div>
            <div class="imagen">
                <img src="../imagenes/alliance3.jpg" href="https://www.flow.cl/app/web/pagarBtnPago.php?token=0iksrwz" alt="">
                <div class="overlay">
                    <h2><a href="https://www.flow.cl/btn.php?token=clnntm4">Plan Promoción $40.000</a></h2>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
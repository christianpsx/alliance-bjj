<?php
//Conexion a la base de datos
include("../../includes/connection.php");
include("../../includes/funciones.php");

//Recepcion de la id de la clase
$ID_PROFESOR = mysqli_real_escape_string($connect, $_GET['id_profesor']);

//Se comienza la sesion para capturar los datos del profesor

session_start();

$ID = $_SESSION['ID_alumno'];
$ACTIVE = $_SESSION['active'];

$NOMBRE = $_SESSION['Nombre'];
$APELLIDOS = $_SESSION['Apellidos'];

$sql = "SELECT Imagen FROM Profesor WHERE ID_profesor = '$ID_PROFESOR' AND Imagen != 'NULL'";
$result = $connect->query($sql);

//Validacion de que existe una sesion
if(!isset($ID) OR $ACTIVE == 0){
    session_destroy();
    header("location: ../../index.html");
    exit();
}

$informacion_profesor = "SELECT * FROM Profesor WHERE ID_profesor = '$ID_PROFESOR'";
$peticion_informacion_profesor = mysqli_query($connect, $informacion_profesor);

if($peticion_informacion_profesor){
    while($row = $peticion_informacion_profesor->fetch_array()){
        $NOMBRE_PROFESOR = $row['Nombre'];
        $APELLIDOS_PROFESOR = $row['Apellidos'];

        $MAIL_PROFESOR = $row['Mail'];
        $TELEFONO_PROFESOR = $row['Telefono'];

        $CINTURON_PROFESOR = $row['Cinturon'];
        $Imagen = $row['Imagen'];

        switch($CINTURON_PROFESOR){
            case 1:
                $cinturon = "Ninguno";
                break;
            case 2:
                $cinturon = "Blanco";
                break;
            case 3:
                $cinturon = "Azul";
                break;
            case 4:
                $cinturon = "Morado";
                break;
            case 5:
                $cinturon = "Marron";
                break;
            case 6: 
                $cinturon = "Negro";
                break;
            default:
                break;
        }
    }
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../usuario_css/profe.css">
    <title>Perfil de <?php echo $NOMBRE;?></title>
</head>
<body>
    <ul>
		<li class="log"> <?php echo "<p>Bienvenido $NOMBRE $APELLIDOS</p>";?> </li>
		<li><a href="../usuario_php/home.php">Inicio</a></li>
		<li><a href="../perfil.php">Perfil</a></li>
		<li class="active"><a href="../horario.php">Horario</a></li>
		<li><a href="../planes.php">Planes</a></li>
		<li><a href="../../general/general_php/logout.php">Cerrar sesion</a></li>
	</ul>

    <div class="perfil">
        <h1>Perfil de <?php echo "$NOMBRE_PROFESOR $APELLIDOS_PROFESOR";?></h1>

        <div class="container_info_perfil">
            <div class="contenedor_imagen">
                <?php if($result->num_rows > 0){ ?>
                    <img class="imagen" src="data:image/jpg;charset=utf8;base64,<?php echo base64_encode($Imagen); ?>" /> 
                <?php }else{ ?>
                    <img src="https://i.ibb.co/3Tyr88g/no-image.png" alt="Foto perfil d" class="imagen">
                <?php } ?>
            </div>
            <h2>Datos</h2>
            <div class="user-nombre">
                <p>
                    Nombre:
                    <br>
                    <?php echo "$NOMBRE_PROFESOR $APELLIDOS_PROFESOR";?>
                </p>
            </div>
            <div class="direccion_telefono_profesor">
                <p>
                    Contacto:
                    <br>
                    <?php echo $MAIL_PROFESOR;?>
                    <br>
                    <?php echo $TELEFONO_PROFESOR;?>
                </p>
            </div>
            <div class="user-cinturon">
                <p>
                Cinturón:  <?php echo "$cinturon";?>
                </p>
            </div>
        </div>

        <div class="container_clases">
            <h4>Clases del profesor</h4>
            <div class="container_tabla_clases">
                <table border="1">
                    <tr>
                        <th>Nombre Clases</th>
                        <th>Horario</th>
                    </tr>

                    <?php
                        $seleccionar_clase = "SELECT * FROM Clase WHERE ID_profesor = '$ID_PROFESOR' AND Visibilidad = 1 ORDER BY Dia";
                        $peticion_seleccionar_clase = mysqli_query($connect, $seleccionar_clase);
            
                        while($tabla = $peticion_seleccionar_clase->fetch_assoc()){
                            $ID_CLASE = $tabla['ID_clase'];

                            $TIPO_CLASE = $tabla['Tipo_clase'];

                            $DIA_CLASE = $tabla['Dia'];
                            $HORA_CLASE = $tabla['Hora'];

                            $VISIBILIDAD = $tabla['Visibilidad'];
            
                            switch($DIA_CLASE){
                                case 1:
                                    $dia_clase = "Lunes";
                                    break;
                                case 2:
                                    $dia_clase = "Martes";
                                    break;
                                case 3:
                                    $dia_clase = "Miércoles";
                                    break;
                                case 4:
                                    $dia_clase = "Jueves";
                                    break;
                                case 5:
                                    $dia_clase = "Viernes";
                                    break;
                                case 6:
                                    $dia_clase = "Sabado";
                                    break;
                                case 7:
                                    $dia_clase = "Domingo";
                                    break;
                                default:
                                    break;
                            }

                            switch ($VISIBILIDAD) {
                                case 0:
                                    $visibilidad_clase = "No visible en el horario";
                                    break;
                                case 1:
                                    $visibilidad_clase = "Visible en el horario";
                                    break;
                                default:
                                    break;
                            }
                            ?>
                            </html>
                                <tr>
                                    <td> <a href="../../clase/Alumnos/vista_clase_alumno.php?id_clase=<?php echo $ID_CLASE;?>"><?php echo switchClases($TIPO_CLASE);?></a></td>
                                    <td> <?php echo $dia_clase.' '.date('H:i', strtotime("$HORA_CLASE"));?></td>
                                </tr>
                            <html>
                            <?php
                        }
                    ?>
                </table>
            </div>
        </div>
    </div>
    
</body>
</html>
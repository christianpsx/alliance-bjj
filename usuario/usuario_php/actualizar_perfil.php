<?php
//Conexion con la base de datos
include("../../includes/connection.php");

//Iniciar sesion del usuario

session_start();

$ID = $_SESSION['ID_alumno'];
$ACTIVE = $_SESSION['active'];

if(!isset($ID) OR $ACTIVE == 0){
    session_destroy();
    header("location: ../../index.html");
    exit();
}

//Recepcion de variables mediante metodo GET y POST

$OPCION = mysqli_real_escape_string($connect, $_GET['opcion']);

switch($OPCION){
    case 1:
        $NOMBRE = mysqli_real_escape_string($connect, $_POST['nombre']);
        $APELLIDOS = mysqli_real_escape_string($connect, $_POST['apellido']);

        $RUT = mysqli_real_escape_string($connect, $_POST['rut']);
        $FECHA_NACIMIENTO = mysqli_real_escape_string($connect, $_POST['fecha']);

        $DIRECCION = mysqli_real_escape_string($connect, $_POST['direccion']);
        $TELEFONO = mysqli_real_escape_string($connect, $_POST['telefono']);

        $CINTURON = mysqli_real_escape_string($connect, $_POST['cinturon']);
        $GRADO = mysqli_real_escape_string($connect, $_POST['grado']);

        $EXPERIENCIA = mysqli_real_escape_string($connect, $_POST['experiencia']);
        
        $actualizar_informacion_alumno = "UPDATE Alumno SET Nombre = '$NOMBRE', Apellidos = '$APELLIDOS', Rut = '$RUT', Fecha_nacimiento = '$FECHA_NACIMIENTO', Telefono = '$TELEFONO', Direccion = '$DIRECCION', Experiencia = '$EXPERIENCIA' WHERE ID_alumno = '$ID'";
        
        if ($connect->query($actualizar_informacion_alumno) == TRUE) {
            $consulta_alumno = "SELECT * FROM Alumno WHERE ID_alumno='$ID'";
            $peticion_alumno = mysqli_query($connect, $consulta_alumno);
    
            if($peticion_alumno){
                while($row = $peticion_alumno->fetch_array()){
                    $id = $row['ID_alumno'];

                    $nombre = $row['Nombre'];
                    $apellido = $row['Apellidos'];

                    $rut = $row['Rut'];
                    $fecha_n = $row['Fecha_nacimiento'];

                    $telefono = $row['Telefono'];
                    $direccion = $row['Direccion'];

                    $cinturon = $row['Cinturon'];
                    $grado = $row['Grado'];
                    $clases = $row['Cantidad_clases'];
                    $fecha_g = $row['Fecha_grado'];

                    $observaciones = $row['Observaciones'];
                    $experiencia = $row['Experiencia'];

                    $promo = $row['Promo_ofertas'];

                    $active = $row['active'];
                }
            }
            $_SESSION['ID_alumno'] = $id;

            $_SESSION['Nombre'] = $nombre;
            $_SESSION['Apellidos'] = $apellido;

            $_SESSION['Rut'] = $rut;
            $_SESSION['Fecha_nacimiento'] = $fecha_n;

            $_SESSION['Telefono'] = $telefono;
            $_SESSION['Direccion'] = $direccion;
            $_SESSION['Mail'] = $email;

            $_SESSION['Cinturon'] = $cinturon;
            $_SESSION['Grado'] = $grado;
            $_SESSION['Cantidad_clases'] = $clases;
            $_SESSION['Fecha_grado'] = $fecha_g;

            $_SESSION['Observaciones'] = $observaciones;
            $_SESSION['Experiencia'] = $experiencia;

            $_SESSION['Promo_ofertas'] = $promo;

            $_SESSION['active'] = $active;
            
            header('Location: ../perfil.php');
        } else {
            header('Location: ../configuracion_perfil.php');
        }
        break;
    case 2:
        $CONTRASENA = mysqli_real_escape_string($connect, $_POST['psw']);
        $CONTRASENA2 = mysqli_real_escape_string($connect, $_POST['psw-repeat']);
        if($CONTRASENA == $CONTRASENA2){

            $actualizar_contrasena_alumno = "UPDATE Alumno SET Contrasena = MD5('".$CONTRASENA."') WHERE ID_alumno='$ID'";
            $peticion_alumno = mysqli_query($connect, $actualizar_contrasena_alumno);

            if($peticion_alumno){
                header('Location: ../perfil.php');
            }else{
                echo '<script type="text/javascript"> alert("No se actualizo la contraseña."); location="../perfil.php"; </script>';
            }
        }else {
            echo '<script type="text/javascript"> alert("Las contraseñas no son iguales."); location="../configuracion_perfil.php";</script>';
        }
        break;
    case 3:
        $eliminar_alumno = "DELETE FROM Alumno WHERE ID_alumno = '$ID'";
    
        if ($connect->query($eliminar_alumno) === TRUE) {
            session_destroy();
            header('location: ../../index.html');
        } else {
            header('Location: ../perfil.php');
        }
        break;
    default:
    break;
}
mysqli_close($connect);
?>
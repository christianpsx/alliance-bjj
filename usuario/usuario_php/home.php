<?php
session_start();

$ID = $_SESSION['ID_alumno'];

$NOMBRE = $_SESSION['Nombre'];
$APELLIDOS = $_SESSION['Apellidos'];

$RUT = $_SESSION['Rut'];
$FECHA_NACIMIENTO = $_SESSION['Fecha_nacimiento'];

$MAIL = $_SESSION['Mail'];
$TELEFONO = $_SESSION['Telefono'];
$DIRECCION = $_SESSION['Direccion'];

$CINTURON = $_SESSION['Cinturon'];
$GRADO = $_SESSION['Grado'];
$CANTIDAD_CLASES = $_SESSION['Cantidad_clases'];
$FECHA_GRADO = $_SESSION['Fecha_grado'];

$OBSERVACIONES = $_SESSION['Observaciones'];
$EXPERIENCIA = $_SESSION['Experiencia'];

$PROMO_OFERTAS = $_SESSION['Promo_ofertas'];

$ACTIVE = $_SESSION['active'];

if(!isset($ID) OR $ACTIVE == 0){
    session_destroy();
    header("location: ../../index.html");
    exit();
}
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Inicio</title>
    <link rel="stylesheet" href="../usuario_css/home.css">
    <ul>
        <li class="log"><?php echo "<p>Bienvenido $NOMBRE $APELLIDOS</p>";?></li>
        <li class="active"><a href="#">Inicio</a></li>
        <li><a href="../perfil.php">Perfil</a></li>
        <li><a href="../horario.php">Horario</a></li>
        <li><a href="../planes.php">Planes</a></li>
        <li><a href="../../general/general_php/logout.php">Cerrar sesion</a></li>
    </ul>
</head>
<body>
    <div>
        <form action="sugerencias.php" method="post" class="comentarios"> 
            <div class="espacio">
            <label for="comentarios"><b>Enviar comentarios</b></label>
            <select name="sugerencias" id="sugerencias">
            <option value="Felicitacion" selected>Felicitaciones</option>
            <option value="Sugerencia">Sugerencias</option>
            <option value="Reclamo">Reclamos</option>
            </select>
            </div>
            <textarea name="mensaje" id="mensaje" cols="30" rows="7" class="controls" maxlength="255" minlength="1" placeholder="" require></textarea>
            <input type="submit" value="Enviar comentarios" name="comentarios" class="buttons">
        </form>
    </div>
</body>
</html>
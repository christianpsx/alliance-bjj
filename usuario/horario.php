<?php
//Preparamos la conexión:
include("../includes/connection.php");
include("../includes/funciones.php");

//Se inicia la sesion del usuario.
session_start();

$ID = $_SESSION['ID_alumno'];

$NOMBRE = $_SESSION['Nombre'];
$APELLIDOS = $_SESSION['Apellidos'];

$RUT = $_SESSION['Rut'];
$FECHA_NACIMIENTO = $_SESSION['Fecha_nacimiento'];

$MAIL = $_SESSION['Mail'];
$TELEFONO = $_SESSION['Telefono'];
$DIRECCION = $_SESSION['Direccion'];

$CINTURON = $_SESSION['Cinturon'];
$GRADO = $_SESSION['Grado'];
$CANTIDAD_CLASES = $_SESSION['Cantidad_clases'];
$FECHA_GRADO = $_SESSION['Fecha_grado'];

$OBSERVACIONES = $_SESSION['Observaciones'];
$EXPERIENCIA = $_SESSION['Experiencia'];

$PROMO_OFERTAS = $_SESSION['Promo_ofertas'];

$ACTIVE = $_SESSION['active'];

if(!isset($ID) OR $ACTIVE == 0){
    session_destroy();
    header("location: ../index.html");
    exit();
}
?>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" type="text/css" href="usuario_css/horario.css">
	
	
	<title>Horario</title>
</head>

<body>
	<ul>
		<li class="log"> <?php echo "<p>Bienvenido $NOMBRE $APELLIDOS</p>";?> </li>

		<li><a href="usuario_php/home.php">Inicio</a></li>

		<li><a href="perfil.php">Perfil</a></li>

		<li class="active"><a href="#">Horario</a></li>

		<li><a href="planes.php">Planes</a></li>

		<li><a href="../general/general_php/logout.php">Cerrar sesion</a></li>
	</ul>
	<div class="container">
	<table class="cal">
		<caption>Alliance</caption> 
		<thead>
			<tr>
				<th></th>
				<th>Lun</th>
				<th>Mar</th>
				<th>Mié</th>
				<th>Jue</th>
				<th>Vie</th>
                <th>Sáb</th>
                <th>Dom</th>
			</tr>
		</thead>
		<tbody>
			</html>
			<?php
			$consulta_clase = "SELECT * FROM Clase WHERE Visibilidad = 1";
			$peticion_consulta_clase = mysqli_query($connect, $consulta_clase);
			
			while($tabla = $peticion_consulta_clase->fetch_assoc()){
				$ID_CLASE = $tabla['ID_clase'];
				$ID_PROFESOR_CLASE = $tabla['ID_profesor'];
			
				$TIPO_CLASE = $tabla['Tipo_clase'];
				$CUPOS_CLASE = $tabla['Cupos'];
			
				$CINTURON_CLASE = $tabla['Cinturon'];
				$GRADO_CLASE = $tabla['Grado'];
			
				$DIA_CLASE = $tabla['Dia'];
				$HORA_CLASE = $tabla['Hora'];
			
				$VISIBILIDAD_CLASE = $tabla['Visibilidad'];
			
				//Actualizacion automatica de cupos

				$consulta_cupos = "SELECT COUNT(*) AS contador_cupos FROM Inscripcion WHERE ID_clase='$ID_CLASE'";
				$peticion_consulta_cupos = mysqli_query($connect, $consulta_cupos);
				$contador_cupos = mysqli_fetch_array($peticion_consulta_cupos);

				if($contador_cupos['contador_cupos'] >= 0){
					$cupos_clase = $CUPOS_CLASE - $contador_cupos['contador_cupos'];
				}
				
				switch($DIA_CLASE){
					case 1:
						$dia_clase = "Lunes";
						if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("7:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("8:00"))){
							$id_clase_lunes_7 = $ID_CLASE;
							$hora_clase_lunes_7 = $HORA_CLASE;
							$tipo_clase_lunes_7 = switchClases($TIPO_CLASE);
							$cupos_clase_lunes_7 = $cupos_clase;
						}
						if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("8:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("9:00"))){
							$id_clase_lunes_8 = $ID_CLASE;
							$hora_clase_lunes_8 = $HORA_CLASE;
							$tipo_clase_lunes_8 = switchClases($TIPO_CLASE);
							$cupos_clase_lunes_8 = $cupos_clase;
						}
						if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime('09:00')) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime('10:00'))){
							$id_clase_lunes_9 = $ID_CLASE;
							$hora_clase_lunes_9 = $HORA_CLASE;
							$tipo_clase_lunes_9 = switchClases($TIPO_CLASE);
							$cupos_clase_lunes_9 = $cupos_clase;
						}
						if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("10:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("11:00"))){
							$id_clase_lunes_10 = $ID_CLASE;
							$hora_clase_lunes_10 = $HORA_CLASE;
							$tipo_clase_lunes_10 = switchClases($TIPO_CLASE);
							$cupos_clase_lunes_10 = $cupos_clase;
						}
						if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("11:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("12:00"))){
							$id_clase_lunes_11 = $ID_CLASE;
							$hora_clase_lunes_11 = $HORA_CLASE;
							$tipo_clase_lunes_11 = switchClases($TIPO_CLASE);
							$cupos_clase_lunes_11 = $cupos_clase;
						}
						if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("12:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("13:00"))){
							$id_clase_lunes_12 = $ID_CLASE;
							$hora_clase_lunes_12 = $HORA_CLASE;
							$tipo_clase_lunes_12 = switchClases($TIPO_CLASE);
							$cupos_clase_lunes_12 = $cupos_clase;
						}
						if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("13:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("14:00"))){
							$id_clase_lunes_13 = $ID_CLASE;
							$hora_clase_lunes_13 = $HORA_CLASE;
							$tipo_clase_lunes_13 = switchClases($TIPO_CLASE);
							$cupos_clase_lunes_13 = $cupos_clase;
						}
						if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("14:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("15:00"))){
							$id_clase_lunes_14 = $ID_CLASE;
							$hora_clase_lunes_14 = $HORA_CLASE;
							$tipo_clase_lunes_14 = switchClases($TIPO_CLASE);
							$cupos_clase_lunes_14 = $cupos_clase;
						}
						if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("15:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("16:00"))){
							$id_clase_lunes_15 = $ID_CLASE;
							$hora_clase_lunes_15 = $HORA_CLASE;
							$tipo_clase_lunes_15 = switchClases($TIPO_CLASE);
							$cupos_clase_lunes_15 = $cupos_clase;
						}
						if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("16:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("17:00"))){
							$id_clase_lunes_16 = $ID_CLASE;
							$hora_clase_lunes_16 = $HORA_CLASE;
							$tipo_clase_lunes_16 = switchClases($TIPO_CLASE);
							$cupos_clase_lunes_16 = $cupos_clase;
						}
						if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("17:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("18:00"))){
							$id_clase_lunes_17 = $ID_CLASE;
							$hora_clase_lunes_17 = $HORA_CLASE;
							$tipo_clase_lunes_17 = switchClases($TIPO_CLASE);
							$cupos_clase_lunes_17 = $cupos_clase;
						}
						if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("18:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("19:00"))){
							$id_clase_lunes_18 = $ID_CLASE;
							$hora_clase_lunes_18 = $HORA_CLASE;
							$tipo_clase_lunes_18 = switchClases($TIPO_CLASE);
							$cupos_clase_lunes_18 = $cupos_clase;
						}
						if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("19:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("20:00"))){
							$id_clase_lunes_19 = $ID_CLASE;
							$hora_clase_lunes_19 = $HORA_CLASE;
							$tipo_clase_lunes_19 = switchClases($TIPO_CLASE);
							$cupos_clase_lunes_19 = $cupos_clase;
						}
						if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("20:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("21:00"))){
							$id_clase_lunes_20 = $ID_CLASE;
							$hora_clase_lunes_20 = $HORA_CLASE;
							$tipo_clase_lunes_20 = switchClases($TIPO_CLASE);
							$cupos_clase_lunes_20 = $cupos_clase;
						}
						if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("21:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("22:00"))){
							$id_clase_lunes_21 = $ID_CLASE;
							$hora_clase_lunes_21 = $HORA_CLASE;
							$tipo_clase_lunes_21 = switchClases($TIPO_CLASE);
							$cupos_clase_lunes_21 = $cupos_clase;
						}
						break;
					case 2:
						$dia_clase = "Martes";
						if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("7:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("8:00"))){
							$id_clase_martes_7 = $ID_CLASE;
							$hora_clase_martes_7 = $HORA_CLASE;
							$tipo_clase_martes_7 = switchClases($TIPO_CLASE);
							$cupos_clase_martes_7 = $cupos_clase;
						}
						if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("8:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("9:00"))){
							$id_clase_martes_8 = $ID_CLASE;
							$hora_clase_martes_8 = $HORA_CLASE;
							$tipo_clase_martes_8 = switchClases($TIPO_CLASE);
							$cupos_clase_martes_8 = $cupos_clase;
						}
						if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime('09:00')) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime('10:00'))){
							$id_clase_martes_9 = $ID_CLASE;
							$hora_clase_martes_9 = $HORA_CLASE;
							$tipo_clase_martes_9 = switchClases($TIPO_CLASE);
							$cupos_clase_martes_9 = $cupos_clase;
						}
						if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("10:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("11:00"))){
							$id_clase_martes_10 = $ID_CLASE;
							$hora_clase_martes_10 = $HORA_CLASE;
							$tipo_clase_martes_10 = switchClases($TIPO_CLASE);
							$cupos_clase_martes_10 = $cupos_clase;
						}
						if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("11:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("12:00"))){
							$id_clase_martes_11 = $ID_CLASE;
							$hora_clase_martes_11 = $HORA_CLASE;
							$tipo_clase_martes_11 = switchClases($TIPO_CLASE);
							$cupos_clase_martes_11 = $cupos_clase;
						}
						if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("12:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("13:00"))){
							$id_clase_martes_12 = $ID_CLASE;
							$hora_clase_martes_12 = $HORA_CLASE;
							$tipo_clase_martes_12 = switchClases($TIPO_CLASE);
							$cupos_clase_martes_12 = $cupos_clase;
						}
						if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("13:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("14:00"))){
							$id_clase_martes_13 = $ID_CLASE;
							$hora_clase_martes_13 = $HORA_CLASE;
							$tipo_clase_martes_13 = switchClases($TIPO_CLASE);
							$cupos_clase_martes_13 = $cupos_clase;
						}
						if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("14:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("15:00"))){
							$id_clase_martes_14 = $ID_CLASE;
							$hora_clase_martes_14 = $HORA_CLASE;
							$tipo_clase_martes_14 = switchClases($TIPO_CLASE);
							$cupos_clase_martes_14 = $cupos_clase;
						}
						if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("15:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("16:00"))){
							$id_clase_martes_15 = $ID_CLASE;
							$hora_clase_martes_15 = $HORA_CLASE;
							$tipo_clase_martes_15 = switchClases($TIPO_CLASE);
							$cupos_clase_martes_15 = $cupos_clase;
						}
						if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("16:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("17:00"))){
							$id_clase_martes_16 = $ID_CLASE;
							$hora_clase_martes_16 = $HORA_CLASE;
							$tipo_clase_martes_16 = switchClases($TIPO_CLASE);
							$cupos_clase_martes_16 = $cupos_clase;
						}
						if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("17:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("18:00"))){
							$id_clase_martes_17 = $ID_CLASE;
							$hora_clase_martes_17 = $HORA_CLASE;
							$tipo_clase_martes_17 = switchClases($TIPO_CLASE);
							$cupos_clase_martes_17 = $cupos_clase;
						}
						if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("18:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("19:00"))){
							$id_clase_martes_18 = $ID_CLASE;
							$hora_clase_martes_18 = $HORA_CLASE;
							$tipo_clase_martes_18 = switchClases($TIPO_CLASE);
							$cupos_clase_martes_18 = $cupos_clase;
						}
						if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("19:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("20:00"))){
							$id_clase_martes_19 = $ID_CLASE;
							$hora_clase_martes_19 = $HORA_CLASE;
							$tipo_clase_martes_19 = switchClases($TIPO_CLASE);
							$cupos_clase_martes_19 = $cupos_clase;
						}
						if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("20:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("21:00"))){
							$id_clase_martes_20 = $ID_CLASE;
							$hora_clase_martes_20 = $HORA_CLASE;
							$tipo_clase_martes_20 = switchClases($TIPO_CLASE);
							$cupos_clase_martes_20 = $cupos_clase;
						}
						if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("21:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("22:00"))){
							$id_clase_martes_21 = $ID_CLASE;
							$hora_clase_martes_21 = $HORA_CLASE;
							$tipo_clase_martes_21 = switchClases($TIPO_CLASE);
							$cupos_clase_martes_21 = $cupos_clase;
						}
						break;
					case 3:
						$dia_clase = "Miércoles";
						if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("7:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("8:00"))){
							$id_clase_miercoles_7 = $ID_CLASE;
							$hora_clase_miercoles_7 = $HORA_CLASE;
							$tipo_clase_miercoles_7 = switchClases($TIPO_CLASE);
							$cupos_clase_miercoles_7 = $cupos_clase;
						}
						if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("8:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("9:00"))){
							$id_clase_miercoles_8 = $ID_CLASE;
							$hora_clase_miercoles_8 = $HORA_CLASE;
							$tipo_clase_miercoles_8 = switchClases($TIPO_CLASE);
							$cupos_clase_miercoles_8 = $cupos_clase;
						}
						if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime('09:00')) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime('10:00'))){
							$id_clase_miercoles_9 = $ID_CLASE;
							$hora_clase_miercoles_9 = $HORA_CLASE;
							$tipo_clase_miercoles_9 = switchClases($TIPO_CLASE);
							$cupos_clase_miercoles_9 = $cupos_clase;
						}
						if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("10:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("11:00"))){
							$id_clase_miercoles_10 = $ID_CLASE;
							$hora_clase_miercoles_10 = $HORA_CLASE;
							$tipo_clase_miercoles_10 = switchClases($TIPO_CLASE);
							$cupos_clase_miercoles_10 = $cupos_clase;
						}
						if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("11:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("12:00"))){
							$id_clase_miercoles_11 = $ID_CLASE;
							$hora_clase_miercoles_11 = $HORA_CLASE;
							$tipo_clase_miercoles_11 = switchClases($TIPO_CLASE);
							$cupos_clase_miercoles_11 = $cupos_clase;
						}
						if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("12:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("13:00"))){
							$id_clase_miercoles_12 = $ID_CLASE;
							$hora_clase_miercoles_12 = $HORA_CLASE;
							$tipo_clase_miercoles_12 = switchClases($TIPO_CLASE);
							$cupos_clase_miercoles_12 = $cupos_clase;
						}
						if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("13:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("14:00"))){
							$id_clase_miercoles_13 = $ID_CLASE;
							$hora_clase_miercoles_13 = $HORA_CLASE;
							$tipo_clase_miercoles_13 = switchClases($TIPO_CLASE);
							$cupos_clase_miercoles_13 = $cupos_clase;
						}
						if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("14:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("15:00"))){
							$id_clase_miercoles_14 = $ID_CLASE;
							$hora_clase_miercoles_14 = $HORA_CLASE;
							$tipo_clase_miercoles_14 = switchClases($TIPO_CLASE);
							$cupos_clase_miercoles_14 = $cupos_clase;
						}
						if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("15:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("16:00"))){
							$id_clase_miercoles_15 = $ID_CLASE;
							$hora_clase_miercoles_15 = $HORA_CLASE;
							$tipo_clase_miercoles_15 = switchClases($TIPO_CLASE);
							$cupos_clase_miercoles_15 = $cupos_clase;
						}
						if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("16:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("17:00"))){
							$id_clase_miercoles_16 = $ID_CLASE;
							$hora_clase_miercoles_16 = $HORA_CLASE;
							$tipo_clase_miercoles_16 = switchClases($TIPO_CLASE);
							$cupos_clase_miercoles_16 = $CUPOS_CLASE;
						}
						if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("17:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("18:00"))){
							$id_clase_miercoles_17 = $ID_CLASE;
							$hora_clase_miercoles_17 = $HORA_CLASE;
							$tipo_clase_miercoles_17 = switchClases($TIPO_CLASE);
							$cupos_clase_miercoles_17 = $CUPOS_CLASE;
						}
						if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("18:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("19:00"))){
							$id_clase_miercoles_18 = $ID_CLASE;
							$hora_clase_miercoles_18 = $HORA_CLASE;
							$tipo_clase_miercoles_18 = switchClases($TIPO_CLASE);
							$cupos_clase_miercoles_18 = $cupos_clase;
						}
						if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("19:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("20:00"))){
							$id_clase_miercoles_19 = $ID_CLASE;
							$hora_clase_miercoles_19 = $HORA_CLASE;
							$tipo_clase_miercoles_19 = switchClases($TIPO_CLASE);
							$cupos_clase_miercoles_19 = $cupos_clase;
						}
						if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("20:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("21:00"))){
							$id_clase_miercoles_20 = $ID_CLASE;
							$hora_clase_miercoles_20 = $HORA_CLASE;
							$tipo_clase_miercoles_20 = switchClases($TIPO_CLASE);
							$cupos_clase_miercoles_20 = $cupos_clase;
						}
						if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("21:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("22:00"))){
							$id_clase_miercoles_21 = $ID_CLASE;
							$hora_clase_miercoles_21 = $HORA_CLASE;
							$tipo_clase_miercoles_21 = switchClases($TIPO_CLASE);
							$cupos_clase_miercoles_21 = $cupos_clase;
						}
						break;
					case 4:
						$dia_clase = "Jueves";
						if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("7:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("8:00"))){
							$id_clase_jueves_7 = $ID_CLASE;
							$hora_clase_jueves_7 = $HORA_CLASE;
							$tipo_clase_jueves_7 = switchClases($TIPO_CLASE);
							$cupos_clase_jueves_7 = $cupos_clase;
						}
						if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("8:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("9:00"))){
							$id_clase_jueves_8 = $ID_CLASE;
							$hora_clase_jueves_8 = $HORA_CLASE;
							$tipo_clase_jueves_8 = switchClases($TIPO_CLASE);
							$cupos_clase_jueves_8 = $cupos_clase;
						}
						if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime('09:00')) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime('10:00'))){
							$id_clase_jueves_9 = $ID_CLASE;
							$hora_clase_jueves_9 = $HORA_CLASE;
							$tipo_clase_jueves_9 = switchClases($TIPO_CLASE);
							$cupos_clase_jueves_9 = $cupos_clase;
						}
						if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("10:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("11:00"))){
							$id_clase_jueves_10 = $ID_CLASE;
							$hora_clase_jueves_10 = $HORA_CLASE;
							$tipo_clase_jueves_10 = switchClases($TIPO_CLASE);
							$cupos_clase_jueves_10 = $cupos_clase;
						}
						if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("11:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("12:00"))){
							$id_clase_jueves_11 = $ID_CLASE;
							$hora_clase_jueves_11 = $HORA_CLASE;
							$tipo_clase_jueves_11 = switchClases($TIPO_CLASE);
							$cupos_clase_jueves_11 = $cupos_clase;
						}
						if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("12:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("13:00"))){
							$id_clase_jueves_12 = $ID_CLASE;
							$hora_clase_jueves_12 = $HORA_CLASE;
							$tipo_clase_jueves_12 = switchClases($TIPO_CLASE);
							$cupos_clase_jueves_12 = $cupos_clase;
						}
						if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("13:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("14:00"))){
							$id_clase_jueves_13 = $ID_CLASE;
							$hora_clase_jueves_13 = $HORA_CLASE;
							$tipo_clase_jueves_13 = switchClases($TIPO_CLASE);
							$cupos_clase_jueves_13 = $cupos_clase;
						}
						if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("14:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("15:00"))){
							$id_clase_jueves_14 = $ID_CLASE;
							$hora_clase_jueves_14 = $HORA_CLASE;
							$tipo_clase_jueves_14 = switchClases($TIPO_CLASE);
							$cupos_clase_jueves_14 = $cupos_clase;
						}
						if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("15:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("16:00"))){
							$id_clase_jueves_15 = $ID_CLASE;
							$hora_clase_jueves_15 = $HORA_CLASE;
							$tipo_clase_jueves_15 = switchClases($TIPO_CLASE);
							$cupos_clase_jueves_15 = $cupos_clase;
						}
						if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("16:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("17:00"))){
							$id_clase_jueves_16 = $ID_CLASE;
							$hora_clase_jueves_16 = $HORA_CLASE;
							$tipo_clase_jueves_16 = switchClases($TIPO_CLASE);
							$cupos_clase_jueves_16 = $cupos_clase;
						}
						if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("17:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("18:00"))){
							$id_clase_jueves_17 = $ID_CLASE;
							$hora_clase_jueves_17 = $HORA_CLASE;
							$tipo_clase_jueves_17 = switchClases($TIPO_CLASE);
							$cupos_clase_jueves_17 = $cupos_clase;
						}
						if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("18:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("19:00"))){
							$id_clase_jueves_18 = $ID_CLASE;
							$hora_clase_jueves_18 = $HORA_CLASE;
							$tipo_clase_jueves_18 = switchClases($TIPO_CLASE);
							$cupos_clase_jueves_18 = $cupos_clase;
						}
						if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("19:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("20:00"))){
							$id_clase_jueves_19 = $ID_CLASE;
							$hora_clase_jueves_19 = $HORA_CLASE;
							$tipo_clase_jueves_19 = switchClases($TIPO_CLASE);
							$cupos_clase_jueves_19 = $cupos_clase;
						}
						if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("20:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("21:00"))){
							$id_clase_jueves_20 = $ID_CLASE;
							$hora_clase_jueves_20 = $HORA_CLASE;
							$tipo_clase_jueves_20 = switchClases($TIPO_CLASE);
							$cupos_clase_jueves_20 = $cupos_clase;
						}
						if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("21:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("22:00"))){
							$id_clase_jueves_21 = $ID_CLASE;
							$hora_clase_jueves_21 = $HORA_CLASE;
							$tipo_clase_jueves_21 = switchClases($TIPO_CLASE);
							$cupos_clase_jueves_21 = $cupos_clase;
						}
						break;
					case 5:
						$dia_clase = "Viernes";
						if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("7:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("8:00"))){
							$id_clase_viernes_7 = $ID_CLASE;
							$hora_clase_viernes_7 = $HORA_CLASE;
							$tipo_clase_viernes_7 = switchClases($TIPO_CLASE);
							$cupos_clase_viernes_7 = $cupos_clase;
						}
						if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("8:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("9:00"))){
							$id_clase_viernes_8 = $ID_CLASE;
							$hora_clase_viernes_8 = $HORA_CLASE;
							$tipo_clase_viernes_8 = switchClases($TIPO_CLASE);
							$cupos_clase_viernes_8 = $cupos_clase;
						}
						if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime('09:00')) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime('10:00'))){
							$id_clase_viernes_9 = $ID_CLASE;
							$hora_clase_viernes_9 = $HORA_CLASE;
							$tipo_clase_viernes_9 = switchClases($TIPO_CLASE);
							$cupos_clase_viernes_9 = $cupos_clase;
						}
						if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("10:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("11:00"))){
							$id_clase_viernes_10 = $ID_CLASE;
							$hora_clase_viernes_10 = $HORA_CLASE;
							$tipo_clase_viernes_10 = switchClases($TIPO_CLASE);
							$cupos_clase_viernes_10 = $cupos_clase;
						}
						if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("11:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("12:00"))){
							$id_clase_viernes_11 = $ID_CLASE;
							$hora_clase_viernes_11 = $HORA_CLASE;
							$tipo_clase_viernes_11 = switchClases($TIPO_CLASE);
							$cupos_clase_viernes_11 = $cupos_clase;
						}
						if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("12:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("13:00"))){
							$id_clase_viernes_12 = $ID_CLASE;
							$hora_clase_viernes_12 = $HORA_CLASE;
							$tipo_clase_viernes_12 = switchClases($TIPO_CLASE);
							$cupos_clase_viernes_12 = $cupos_clase;
						}
						if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("13:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("14:00"))){
							$id_clase_viernes_13 = $ID_CLASE;
							$hora_clase_viernes_13 = $HORA_CLASE;
							$tipo_clase_viernes_13 = switchClases($TIPO_CLASE);
							$cupos_clase_viernes_13 = $cupos_clase;
						}
						if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("14:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("15:00"))){
							$id_clase_viernes_14 = $ID_CLASE;
							$hora_clase_viernes_14 = $HORA_CLASE;
							$tipo_clase_viernes_14 = switchClases($TIPO_CLASE);
							$cupos_clase_viernes_14 = $cupos_clase;
						}
						if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("15:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("16:00"))){
							$id_clase_viernes_15 = $ID_CLASE;
							$hora_clase_viernes_15 = $HORA_CLASE;
							$tipo_clase_viernes_15 = switchClases($TIPO_CLASE);
							$cupos_clase_viernes_15 = $cupos_clase;
						}
						if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("16:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("17:00"))){
							$id_clase_viernes_16 = $ID_CLASE;
							$hora_clase_viernes_16 = $HORA_CLASE;
							$tipo_clase_viernes_16 = switchClases($TIPO_CLASE);
							$cupos_clase_viernes_16 = $cupos_clase;
						}
						if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("17:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("18:00"))){
							$id_clase_viernes_17 = $ID_CLASE;
							$hora_clase_viernes_17 = $HORA_CLASE;
							$tipo_clase_viernes_17 = switchClases($TIPO_CLASE);
							$cupos_clase_viernes_17 = $cupos_clase;
						}
						if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("18:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("19:00"))){
							$id_clase_viernes_18 = $ID_CLASE;
							$hora_clase_viernes_18 = $HORA_CLASE;
							$tipo_clase_viernes_18 = switchClases($TIPO_CLASE);
							$cupos_clase_viernes_18 = $cupos_clase;
						}
						if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("19:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("20:00"))){
							$id_clase_viernes_19 = $ID_CLASE;
							$hora_clase_viernes_19 = $HORA_CLASE;
							$tipo_clase_viernes_19 = switchClases($TIPO_CLASE);
							$cupos_clase_viernes_19 = $cupos_clase;
						}
						if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("20:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("21:00"))){
							$id_clase_viernes_20 = $ID_CLASE;
							$hora_clase_viernes_20 = $HORA_CLASE;
							$tipo_clase_viernes_20 = switchClases($TIPO_CLASE);
							$cupos_clase_viernes_20 = $cupos_clase;
						}
						if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("21:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("22:00"))){
							$id_clase_viernes_21 = $ID_CLASE;
							$hora_clase_viernes_21 = $HORA_CLASE;
							$tipo_clase_viernes_21 = switchClases($TIPO_CLASE);
							$cupos_clase_viernes_21 = $cupos_clase;
						}
						break;
					case 6:
						$dia_clase = "Sabado";
						if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("7:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("8:00"))){
							$id_clase_sabado_7 = $ID_CLASE;
							$hora_clase_sabado_7 = $HORA_CLASE;
							$tipo_clase_sabado_7 = switchClases($TIPO_CLASE);
							$cupos_clase_sabado_7 = $cupos_clase;
						}
						if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("8:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("9:00"))){
							$id_clase_sabado_8 = $ID_CLASE;
							$hora_clase_sabado_8 = $HORA_CLASE;
							$tipo_clase_sabado_8 = switchClases($TIPO_CLASE);
							$cupos_clase_sabado_8 = $cupos_clase;
						}
						if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime('09:00')) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime('10:00'))){
							$id_clase_sabado_9 = $ID_CLASE;
							$hora_clase_sabado_9 = $HORA_CLASE;
							$tipo_clase_sabado_9 = switchClases($TIPO_CLASE);
							$cupos_clase_sabado_9 = $cupos_clase;
						}
						if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("10:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("11:00"))){
							$id_clase_sabado_10 = $ID_CLASE;
							$hora_clase_sabado_10 = $HORA_CLASE;
							$tipo_clase_sabado_10 = switchClases($TIPO_CLASE);
							$cupos_clase_sabado_10 = $cupos_clase;
						}
						if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("11:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("12:00"))){
							$id_clase_sabado_11 = $ID_CLASE;
							$hora_clase_sabado_11 = $HORA_CLASE;
							$tipo_clase_sabado_11 = switchClases($TIPO_CLASE);
							$cupos_clase_sabado_11 = $cupos_clase;
						}
						if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("12:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("13:00"))){
							$id_clase_sabado_12 = $ID_CLASE;
							$hora_clase_sabado_12 = $HORA_CLASE;
							$tipo_clase_sabado_12 = switchClases($TIPO_CLASE);
							$cupos_clase_sabado_12 = $cupos_clase;
						}
						if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("13:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("14:00"))){
							$id_clase_sabado_13 = $ID_CLASE;
							$hora_clase_sabado_13 = $HORA_CLASE;
							$tipo_clase_sabado_13 = switchClases($TIPO_CLASE);
							$cupos_clase_sabado_13 = $cupos_clase;
						}
						if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("14:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("15:00"))){
							$id_clase_sabado_14 = $ID_CLASE;
							$hora_clase_sabado_14 = $HORA_CLASE;
							$tipo_clase_sabado_14 = switchClases($TIPO_CLASE);
							$cupos_clase_sabado_14 = $cupos_clase;
						}
						if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("15:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("16:00"))){
							$id_clase_sabado_15 = $ID_CLASE;
							$hora_clase_sabado_15 = $HORA_CLASE;
							$tipo_clase_sabado_15 = switchClases($TIPO_CLASE);
							$cupos_clase_sabado_15 = $cupos_clase;
						}
						if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("16:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("17:00"))){
							$id_clase_sabado_16 = $ID_CLASE;
							$hora_clase_sabado_16 = $HORA_CLASE;
							$tipo_clase_sabado_16 = switchClases($TIPO_CLASE);
							$cupos_clase_sabado_16 = $cupos_clase;
						}
						if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("17:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("18:00"))){
							$id_clase_sabado_17 = $ID_CLASE;
							$hora_clase_sabado_17 = $HORA_CLASE;
							$tipo_clase_sabado_17 = switchClases($TIPO_CLASE);
							$cupos_clase_sabado_17 = $cupos_clase;
						}
						if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("18:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("19:00"))){
							$id_clase_sabado_18 = $ID_CLASE;
							$hora_clase_sabado_18 = $HORA_CLASE;
							$tipo_clase_sabado_18 = switchClases($TIPO_CLASE);
							$cupos_clase_sabado_18 = $cupos_clase;
						}
						if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("19:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("20:00"))){
							$id_clase_sabado_19 = $ID_CLASE;
							$hora_clase_sabado_19 = $HORA_CLASE;
							$tipo_clase_sabado_19 = switchClases($TIPO_CLASE);
							$cupos_clase_sabado_19 = $cupos_clase;
						}
						if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("20:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("21:00"))){
							$id_clase_sabado_20 = $ID_CLASE;
							$hora_clase_sabado_20 = $HORA_CLASE;
							$tipo_clase_sabado_20 = switchClases($TIPO_CLASE);
							$cupos_clase_sabado_20 = $cupos_clase;
						}
						if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("21:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("22:00"))){
							$id_clase_sabado_21 = $ID_CLASE;
							$hora_clase_sabado_21 = $HORA_CLASE;
							$tipo_clase_sabado_21 = switchClases($TIPO_CLASE);
							$cupos_clase_sabado_21 = $cupos_clase;
						}
						break;
					case 7:
						$dia_clase = "Domingo";
						if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("7:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("8:00"))){
							$id_clase_domingo_7 = $ID_CLASE;
							$hora_clase_domingo_7 = $HORA_CLASE;
							$tipo_clase_domingo_7 = switchClases($TIPO_CLASE);
							$cupos_clase_domingo_7 = $cupos_clase;
						}
						if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("8:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("9:00"))){
							$id_clase_domingo_8 = $ID_CLASE;
							$hora_clase_domingo_8 = $HORA_CLASE;
							$tipo_clase_domingo_8 = switchClases($TIPO_CLASE);
							$cupos_clase_domingo_8 = $cupos_clase;
						}
						if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime('09:00')) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime('10:00'))){
							$id_clase_domingo_9 = $ID_CLASE;
							$hora_clase_domingo_9 = $HORA_CLASE;
							$tipo_clase_domingo_9 = switchClases($TIPO_CLASE);
							$cupos_clase_domingo_9 = $cupos_clase;
						}
						if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("10:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("11:00"))){
							$id_clase_domingo_10 = $ID_CLASE;
							$hora_clase_domingo_10 = $HORA_CLASE;
							$tipo_clase_domingo_10 = switchClases($TIPO_CLASE);
							$cupos_clase_domingo_10 = $cupos_clase;
						}
						if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("11:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("12:00"))){
							$id_clase_domingo_11 = $ID_CLASE;
							$hora_clase_domingo_11 = $HORA_CLASE;
							$tipo_clase_domingo_11 = switchClases($TIPO_CLASE);
							$cupos_clase_domingo_11 = $cupos_clase;
						}
						if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("12:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("13:00"))){
							$id_clase_domingo_12 = $ID_CLASE;
							$hora_clase_domingo_12 = $HORA_CLASE;
							$tipo_clase_domingo_12 = switchClases($TIPO_CLASE);
							$cupos_clase_domingo_12 = $cupos_clase;
						}
						if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("13:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("14:00"))){
							$id_clase_domingo_13 = $ID_CLASE;
							$hora_clase_domingo_13 = $HORA_CLASE;
							$tipo_clase_domingo_13 = switchClases($TIPO_CLASE);
							$cupos_clase_domingo_13 = $cupos_clase;
						}
						if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("14:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("15:00"))){
							$id_clase_domingo_14 = $ID_CLASE;
							$hora_clase_domingo_14 = $HORA_CLASE;
							$tipo_clase_domingo_14 = switchClases($TIPO_CLASE);
							$cupos_clase_domingo_14 = $cupos_clase;
						}
						if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("15:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("16:00"))){
							$id_clase_domingo_15 = $ID_CLASE;
							$hora_clase_domingo_15 = $HORA_CLASE;
							$tipo_clase_domingo_15 = switchClases($TIPO_CLASE);
							$cupos_clase_domingo_15 = $cupos_clase;
						}
						if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("16:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("17:00"))){
							$id_clase_domingo_16 = $ID_CLASE;
							$hora_clase_domingo_16 = $HORA_CLASE;
							$tipo_clase_domingo_16 = switchClases($TIPO_CLASE);
							$cupos_clase_domingo_16 = $cupos_clase;
						}
						if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("17:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("18:00"))){
							$id_clase_domingo_17 = $ID_CLASE;
							$hora_clase_domingo_17 = $HORA_CLASE;
							$tipo_clase_domingo_17 = switchClases($TIPO_CLASE);
							$cupos_clase_domingo_17 = $cupos_clase;
						}
						if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("18:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("19:00"))){
							$id_clase_domingo_18 = $ID_CLASE;
							$hora_clase_domingo_18 = $HORA_CLASE;
							$tipo_clase_domingo_18 = switchClases($TIPO_CLASE);
							$cupos_clase_domingo_18 = $cupos_clase;
						}
						if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("19:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("20:00"))){
							$id_clase_domingo_19 = $ID_CLASE;
							$hora_clase_domingo_19 = $HORA_CLASE;
							$tipo_clase_domingo_19 = switchClases($TIPO_CLASE);
							$cupos_clase_domingo_19 = $cupos_clase;
						}
						if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("20:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("21:00"))){
							$id_clase_domingo_20 = $ID_CLASE;
							$hora_clase_domingo_20 = $HORA_CLASE;
							$tipo_clase_domingo_20 = switchClases($TIPO_CLASE);
							$cupos_clase_domingo_20 = $cupos_clase;
						}
						if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("21:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("22:00"))){
							$id_clase_domingo_21 = $ID_CLASE;
							$hora_clase_domingo_21 = $HORA_CLASE;
							$tipo_clase_domingo_21 = switchClases($TIPO_CLASE);
							$cupos_clase_domingo_21 = $cupos_clase;
						}
						break;
					default:
						break;
				}
				
			}
			?>
			<html>
			<?php
			?>
			<tr>
				<td class="horas">7:00</td>
				<td class="black" id="lunes_7">
					<p>
						<?php 
						if(isset($hora_clase_lunes_7)){
							echo '<a href="../clase/Alumnos/vista_clase_alumno.php?id_clase='.$id_clase_lunes_7.'">'.$tipo_clase_lunes_7.'</a>';
							echo "<br>";
							echo "<br>";
							echo "Hora: ".date('H:i', strtotime($hora_clase_lunes_7));
							echo "<br>";
							echo "<br>";
							echo "Cupos: ".$cupos_clase_lunes_7;
						}
						?>
					</p>
				</td>
				<td class="black" id="martes_7">
					<p>
						<?php 
						if(isset($hora_clase_martes_7)){
							echo '<a href="../clase/Alumnos/vista_clase_alumno.php?id_clase='.$id_clase_martes_7.'">'.$tipo_clase_martes_7.'</a>';
							echo "<br>";
							echo "<br>";
							echo "Hora: ".date('H:i', strtotime($hora_clase_martes_7));
							echo "<br>";
							echo "<br>";
							echo "Cupos: ".$cupos_clase_martes_7;
						}
						?>
					</p>
				</td>
				<td class="black" id="miercoles_7">
					<p>
						<?php 
						if(isset($hora_clase_miercoles_7)){
							echo '<a href="../clase/Alumnos/vista_clase_alumno.php?id_clase='.$id_clase_miercoles_7.'">'.$tipo_clase_miercoles_7.'</a>';
							echo "<br>";
							echo "<br>";
							echo "Hora: ".date('H:i', strtotime($hora_clase_miercoles_7));
							echo "<br>";
							echo "<br>";
							echo "Cupos: ".$cupos_clase_miercoles_7;
						}
						?>
					</p>
				</td>
				<td class="black" id="jueves_7">
					<p>
						<?php 
						if(isset($hora_clase_jueves_7)){
							echo '<a href="../clase/Alumnos/vista_clase_alumno.php?id_clase='.$id_clase_jueves_7.'">'.$tipo_clase_jueves_7.'</a>';
							echo "<br>";
							echo "<br>";
							echo "Hora: ".date('H:i', strtotime($hora_clase_jueves_7));
							echo "<br>";
							echo "<br>";
							echo "Cupos: ".$cupos_clase_jueves_7;
						}
						?>
					</p>
				</td>
				<td class="black" id="viernes_7">
					<p>
						<?php 
						if(isset($hora_clase_viernes_7)){
							echo '<a href="../clase/Alumnos/vista_clase_alumno.php?id_clase='.$id_clase_viernes_7.'">'.$tipo_clase_viernes_7.'</a>';
							echo "<br>";
							echo "<br>";
							echo "Hora: ".date('H:i', strtotime($hora_clase_viernes_7));
							echo "<br>";
							echo "<br>";
							echo "Cupos: ".$cupos_clase_viernes_7;
						}
						?>
					</p>
				</td>
                <td class="black" id="sabado_7">
					<p>
						<?php 
						if(isset($hora_clase_sabado_7)){
							echo '<a href="../clase/Alumnos/vista_clase_alumno.php?id_clase='.$id_clase_sabado_7.'">'.$tipo_clase_sabado_7.'</a>';
							echo "<br>";
							echo "<br>";
							echo "Hora: ".date('H:i', strtotime($hora_clase_sabado_7));
							echo "<br>";
							echo "<br>";
							echo "Cupos: ".$cupos_clase_sabado_7;
						}
						?>
					</p>
				</td>
                <td class="black" id="domingo_7">
					<p>
						<?php 
						if(isset($hora_clase_domingo_7)){
							echo '<a href="../clase/Alumnos/vista_clase_alumno.php?id_clase='.$id_clase_domingo_7.'">'.$tipo_clase_domingo_7.'</a>';
							echo "<br>";
							echo "<br>";
							echo "Hora: ".date('H:i', strtotime($hora_clase_domingo_7));
							echo "<br>";
							echo "<br>";
							echo "Cupos: ".$cupos_clase_domingo_7;
						}
						?>
					</p>
				</td>
			</tr>
			<tr>
				<td class="horas">8:00</td>
				<td class="black" id="lunes_8">
					<p>
						<?php 
						if(isset($hora_clase_lunes_8)){
							echo '<a href="../clase/Alumnos/vista_clase_alumno.php?id_clase='.$id_clase_lunes_8.'">'.$tipo_clase_lunes_8.'</a>';
							echo "<br>";
							echo "<br>";
							echo "Hora: ".date('H:i', strtotime($hora_clase_lunes_8));
							echo "<br>";
							echo "<br>";
							echo "Cupos: ".$cupos_clase_lunes_8;
						}
						?>
					</p>
				</td>
				<td class="black" id="martes_8">
					<p>
						<?php 
						if(isset($hora_clase_martes_8)){
							echo '<a href="../clase/Alumnos/vista_clase_alumno.php?id_clase='.$id_clase_martes_8.'">'.$tipo_clase_martes_8.'</a>';
							echo "<br>";
							echo "<br>";
							echo "Hora: ".date('H:i', strtotime($hora_clase_martes_8));
							echo "<br>";
							echo "<br>";
							echo "Cupos: ".$cupos_clase_martes_8;
						}
						?>
					</p>
				</td>
				<td class="black" id="miercoles_8">
					<p>
						<?php 
						if(isset($hora_clase_miercoles_8)){
							echo '<a href="../clase/Alumnos/vista_clase_alumno.php?id_clase='.$id_clase_miercoles_8.'">'.$tipo_clase_miercoles_8.'</a>';
							echo "<br>";
							echo "<br>";
							echo "Hora: ".date('H:i', strtotime($hora_clase_miercoles_8));
							echo "<br>";
							echo "<br>";
							echo "Cupos: ".$cupos_clase_miercoles_8;
						}
						?>
					</p>
				</td>
				<td class="black" id="jueves_8">
					<p>
						<?php 
						if(isset($hora_clase_jueves_8)){
							echo '<a href="../clase/Alumnos/vista_clase_alumno.php?id_clase='.$id_clase_jueves_8.'">'.$tipo_clase_jueves_8.'</a>';
							echo "<br>";
							echo "<br>";
							echo "Hora: ".date('H:i', strtotime($hora_clase_jueves_8));
							echo "<br>";
							echo "<br>";
							echo "Cupos: ".$cupos_clase_jueves_8;
						}
						?>
					</p>
				</td>
				<td class="black" id="viernes_8">
					<p>
						<?php 
						if(isset($hora_clase_viernes_8)){
							echo '<a href="../clase/Alumnos/vista_clase_alumno.php?id_clase='.$id_clase_viernes_8.'">'.$tipo_clase_viernes_8.'</a>';
							echo "<br>";
							echo "<br>";
							echo "Hora: ".date('H:i', strtotime($hora_clase_viernes_8));
							echo "<br>";
							echo "<br>";
							echo "Cupos: ".$cupos_clase_viernes_8;
						}
						?>
					</p>
				</td>
                <td class="black" id="sabado_8">
					<p>
						<?php 
						if(isset($hora_clase_sabado_8)){
							echo '<a href="../clase/Alumnos/vista_clase_alumno.php?id_clase='.$id_clase_sabado_8.'">'.$tipo_clase_sabado_8.'</a>';
							echo "<br>";
							echo "<br>";
							echo "Hora: ".date('H:i', strtotime($hora_clase_sabado_8));
							echo "<br>";
							echo "<br>";
							echo "Cupos: ".$cupos_clase_sabado_8;
						}
						?>
					</p>
				</td>
                <td class="black" id="domingo_8">
					<p>
						<?php 
						if(isset($hora_clase_domingo_8)){
							echo '<a href="../clase/Alumnos/vista_clase_alumno.php?id_clase='.$id_clase_domingo_8.'">'.$tipo_clase_domingo_8.'</a>';
							echo "<br>";
							echo "<br>";
							echo "Hora: ".date('H:i', strtotime($hora_clase_domingo_8));
							echo "<br>";
							echo "<br>";
							echo "Cupos: ".$cupos_clase_domingo_8;
						}
						?>
					</p>
				</td>
			</tr>
			<tr>
				<td class="horas">9:00</td>
				<td class="black" id="lunes_9">
					<p>
						<?php 
						if(isset($hora_clase_lunes_9)){
							echo '<a href="../clase/Alumnos/vista_clase_alumno.php?id_clase='.$id_clase_lunes_9.'">'.$tipo_clase_lunes_9.'</a>';
							echo "<br>";
							echo "<br>";
							echo "Hora: ".date('H:i', strtotime($hora_clase_lunes_9));
							echo "<br>";
							echo "<br>";
							echo "Cupos: ".$cupos_clase_lunes_9;
						}
						?>
					</p>
				</td>
				<td class="black" id="martes_9">
					<p>
						<?php 
						if(isset($hora_clase_martes_9)){
							echo '<a href="../clase/Alumnos/vista_clase_alumno.php?id_clase='.$id_clase_martes_9.'">'.$tipo_clase_martes_9.'</a>';
							echo "<br>";
							echo "<br>";
							echo "Hora: ".date('H:i', strtotime($hora_clase_martes_9));
							echo "<br>";
							echo "<br>";
							echo "Cupos: ".$cupos_clase_martes_9;
						}
						?>
					</p>
				</td>
				<td class="black" id="miercoles_9">
					<p>
						<?php 
						if(isset($hora_clase_miercoles_9)){
							echo '<a href="../clase/Alumnos/vista_clase_alumno.php?id_clase='.$id_clase_miercoles_9.'">'.$tipo_clase_miercoles_9.'</a>';
							echo "<br>";
							echo "<br>";
							echo "Hora: ".date('H:i', strtotime($hora_clase_miercoles_9));
							echo "<br>";
							echo "<br>";
							echo "Cupos: ".$cupos_clase_miercoles_9;
						}
						?>
					</p>
				</td>
				<td class="black" id="jueves_9">
					<p>
						<?php 
						if(isset($hora_clase_jueves_9)){
							echo '<a href="../clase/Alumnos/vista_clase_alumno.php?id_clase='.$id_clase_jueves_9.'">'.$tipo_clase_jueves_9.'</a>';
							echo "<br>";
							echo "<br>";
							echo "Hora: ".date('H:i', strtotime($hora_clase_jueves_9));
							echo "<br>";
							echo "<br>";
							echo "Cupos: ".$cupos_clase_jueves_9;
						}
						?>
					</p>
				</td>
				<td class="black" id="viernes_9">
					<p>
						<?php 
						if(isset($hora_clase_viernes_9)){
							echo '<a href="../clase/Alumnos/vista_clase_alumno.php?id_clase='.$id_clase_viernes_9.'">'.$tipo_clase_viernes_9.'</a>';
							echo "<br>";
							echo "<br>";
							echo "Hora: ".date('H:i', strtotime($hora_clase_viernes_9));
							echo "<br>";
							echo "<br>";
							echo "Cupos: ".$cupos_clase_viernes_9;
						}
						?>
					</p>
				</td>
                <td class="black" id="sabado_9">
					<p>
						<?php 
						if(isset($hora_clase_sabado_9)){
							echo '<a href="../clase/Alumnos/vista_clase_alumno.php?id_clase='.$id_clase_sabado_9.'">'.$tipo_clase_sabado_9.'</a>';
							echo "<br>";
							echo "<br>";
							echo "Hora: ".date('H:i', strtotime($hora_clase_sabado_9));
							echo "<br>";
							echo "<br>";
							echo "Cupos: ".$cupos_clase_sabado_9;
						}
						?>
					</p>
				</td>
                <td class="black" id="domingo_9">
					<p>
						<?php 
						if(isset($hora_clase_domingo_9)){
							echo '<a href="../clase/Alumnos/vista_clase_alumno.php?id_clase='.$id_clase_domingo_9.'">'.$tipo_clase_domingo_9.'</a>';
							echo "<br>";
							echo "<br>";
							echo "Hora: ".date('H:i', strtotime($hora_clase_domingo_9));
							echo "<br>";
							echo "<br>";
							echo "Cupos: ".$cupos_clase_domingo_9;
						}
						?>
					</p>
				</td>
			</tr>
			<tr>
				<td class="horas">10:00</td>
				<td class="black" id="lunes_10">
					<p>
						<?php 
						if(isset($hora_clase_lunes_10)){
							echo '<a href="../clase/Alumnos/vista_clase_alumno.php?id_clase='.$id_clase_lunes_10.'">'.$tipo_clase_lunes_10.'</a>';
							echo "<br>";
							echo "<br>";
							echo "Hora: ".date('H:i', strtotime($hora_clase_lunes_10));
							echo "<br>";
							echo "<br>";
							echo "Cupos: ".$cupos_clase_lunes_10;
						}
						?>	
					</p>
				</td>
				<td class="black" id="martes_10">
					<p>
						<?php 
						if(isset($hora_clase_martes_10)){
							echo '<a href="../clase/Alumnos/vista_clase_alumno.php?id_clase='.$id_clase_martes_10.'">'.$tipo_clase_martes_10.'</a>';
							echo "<br>";
							echo "<br>";
							echo "Hora: ".date('H:i', strtotime($hora_clase_martes_10));
							echo "<br>";
							echo "<br>";
							echo "Cupos: ".$cupos_clase_martes_10;
						}
						?>
					</p>
				</td>
				<td class="black" id="miercoles_10">
					<p>
						<?php 
						if(isset($hora_clase_miercoles_10)){
							echo '<a href="../clase/Alumnos/vista_clase_alumno.php?id_clase='.$id_clase_miercoles_10.'">'.$tipo_clase_miercoles_10.'</a>';
							echo "<br>";
							echo "<br>";
							echo "Hora: ".date('H:i', strtotime($hora_clase_miercoles_10));
							echo "<br>";
							echo "<br>";
							echo "Cupos: ".$cupos_clase_miercoles_10;
						}
						?>
					</p>
				</td>
				<td class="black" id="jueves_10">
					<p>
						<?php 
						if(isset($hora_clase_jueves_10)){
							echo '<a href="../clase/Alumnos/vista_clase_alumno.php?id_clase='.$id_clase_jueves_10.'">'.$tipo_clase_jueves_10.'</a>';
							echo "<br>";
							echo "<br>";
							echo "Hora: ".date('H:i', strtotime($hora_clase_jueves_10));
							echo "<br>";
							echo "<br>";
							echo "Cupos: ".$cupos_clase_jueves_10;
						}
						?>
					</p>
				</td>
				<td class="black" id="viernes_10">
					<p>
						<?php 
						if(isset($hora_clase_viernes_10)){
							echo '<a href="../clase/Alumnos/vista_clase_alumno.php?id_clase='.$id_clase_viernes_10.'">'.$tipo_clase_viernes_10.'</a>';
							echo "<br>";
							echo "<br>";
							echo "Hora: ".date('H:i', strtotime($hora_clase_viernes_10));
							echo "<br>";
							echo "<br>";
							echo "Cupos: ".$cupos_clase_viernes_10;
						}
						?>
					</p>
				</td>
                <td class="black" id="sabado_10">
					<p>
						<?php 
						if(isset($hora_clase_sabado_10)){
							echo '<a href="../clase/Alumnos/vista_clase_alumno.php?id_clase='.$id_clase_sabado_10.'">'.$tipo_clase_sabado_10.'</a>';
							echo "<br>";
							echo "<br>";
							echo "Hora: ".date('H:i', strtotime($hora_clase_sabado_10));
							echo "<br>";
							echo "<br>";
							echo "Cupos: ".$cupos_clase_sabado_10;
						}
						?>
					</p>
				</td>
                <td class="black" id="domingo_10">
					<p>
						<?php 
						if(isset($hora_clase_domingo_10)){
							echo '<a href="../clase/Alumnos/vista_clase_alumno.php?id_clase='.$id_clase_domingo_10.'">'.$tipo_clase_domingo_10.'</a>';
							echo "<br>";
							echo "<br>";
							echo "Hora: ".date('H:i', strtotime($hora_clase_domingo_10));
							echo "<br>";
							echo "<br>";
							echo "Cupos: ".$cupos_clase_domingo_10;
						}
						?>
					</p>
				</td>
			</tr>
			<tr>
				<td class="horas">11:00</td>
				<td class="black" id="lunes_11">
					<p>
						<?php 
						if(isset($hora_clase_lunes_11)){
							echo '<a href="../clase/Alumnos/vista_clase_alumno.php?id_clase='.$id_clase_lunes_11.'">'.$tipo_clase_lunes_11.'</a>';
							echo "<br>";
							echo "<br>";
							echo "Hora: ".date('H:i', strtotime($hora_clase_lunes_11));
							echo "<br>";
							echo "<br>";
							echo "Cupos: ".$cupos_clase_lunes_11;
						}
						?>
					</p>
				</td>
				<td class="black" id="martes_11">
					<p>
						<?php 
						if(isset($hora_clase_martes_11)){
							echo '<a href="../clase/Alumnos/vista_clase_alumno.php?id_clase='.$id_clase_martes_11.'">'.$tipo_clase_martes_11.'</a>';
							echo "<br>";
							echo "<br>";
							echo "Hora: ".date('H:i', strtotime($hora_clase_martes_11));
							echo "<br>";
							echo "<br>";
							echo "Cupos: ".$cupos_clase_martes_11;
						}
						?>
					</p>
				</td>
				<td class="black" id="miercoles_11">
					<p>
						<?php 
						if(isset($hora_clase_miercoles_11)){
							echo '<a href="../clase/Alumnos/vista_clase_alumno.php?id_clase='.$id_clase_miercoles_11.'">'.$tipo_clase_miercoles_11.'</a>';
							echo "<br>";
							echo "<br>";
							echo "Hora: ".date('H:i', strtotime($hora_clase_miercoles_11));
							echo "<br>";
							echo "<br>";
							echo "Cupos: ".$cupos_clase_miercoles_11;
						}
						?>
					</p>
				</td>
				<td class="black" id="jueves_11">
					<p>
					<?php 
						if(isset($hora_clase_jueves_11)){
							echo '<a href="../clase/Alumnos/vista_clase_alumno.php?id_clase='.$id_clase_jueves_11.'">'.$tipo_clase_jueves_11.'</a>';
							echo "<br>";
							echo "<br>";
							echo "Hora: ".date('H:i', strtotime($hora_clase_jueves_11));
							echo "<br>";
							echo "<br>";
							echo "Cupos: ".$cupos_clase_jueves_11;
						}
						?>
					</p>
				</td>
				<td class="black" id="viernes_11">
					<p>
						<?php 
						if(isset($hora_clase_viernes_11)){
							echo '<a href="../clase/Alumnos/vista_clase_alumno.php?id_clase='.$id_clase_viernes_11.'">'.$tipo_clase_viernes_11.'</a>';
							echo "<br>";
							echo "<br>";
							echo "Hora: ".date('H:i', strtotime($hora_clase_viernes_11));
							echo "<br>";
							echo "<br>";
							echo "Cupos: ".$cupos_clase_viernes_11;
						}
						?>
					</p>
				</td>
                <td class="black" id="sabado_11">
					<p>
						<?php 
						if(isset($hora_clase_sabado_11)){
							echo '<a href="../clase/Alumnos/vista_clase_alumno.php?id_clase='.$id_clase_sabado_11.'">'.$tipo_clase_sabado_11.'</a>';
							echo "<br>";
							echo "<br>";
							echo "Hora: ".date('H:i', strtotime($hora_clase_sabado_11));
							echo "<br>";
							echo "<br>";
							echo "Cupos: ".$cupos_clase_sabado_11;
						}
						?>
					</p>
				</td>
                <td class="black" id="domingo_11">
					<p>
						<?php 
						if(isset($hora_clase_domingo_11)){
							echo '<a href="../clase/Alumnos/vista_clase_alumno.php?id_clase='.$id_clase_domingo_11.'">'.$tipo_clase_domingo_11.'</a>';
							echo "<br>";
							echo "<br>";
							echo "Hora: ".date('H:i', strtotime($hora_clase_domingo_11));
							echo "<br>";
							echo "<br>";
							echo "Cupos: ".$cupos_clase_domingo_11;
						}
						?>
					</p>
				</td>
			</tr>
			<tr>
				<td class="horas">12:00</td>
				<td class="black" id="lunes_12">
					<p>
						<?php 
						if(isset($hora_clase_lunes_12)){
							echo '<a href="../clase/Alumnos/vista_clase_alumno.php?id_clase='.$id_clase_lunes_12.'">'.$tipo_clase_lunes_12.'</a>';
							echo "<br>";
							echo "<br>";
							echo "Hora: ".date('H:i', strtotime($hora_clase_lunes_12));
							echo "<br>";
							echo "<br>";
							echo "Cupos: ".$cupos_clase_lunes_12;
						}
						?>
					</p>
				</td>
				<td class="black" id="martes_12">
					<p>
						<?php 
						if(isset($hora_clase_martes_12)){
							echo '<a href="../clase/Alumnos/vista_clase_alumno.php?id_clase='.$id_clase_martes_12.'">'.$tipo_clase_martes_12.'</a>';
							echo "<br>";
							echo "<br>";
							echo "Hora: ".date('H:i', strtotime($hora_clase_martes_12));
							echo "<br>";
							echo "<br>";
							echo "Cupos: ".$cupos_clase_martes_12;
						}
						?>
					</p>
				</td>
				<td class="black" id="miercoles_12">
					<p>
						<?php 
						if(isset($hora_clase_miercoles_12)){
							echo '<a href="../clase/Alumnos/vista_clase_alumno.php?id_clase='.$id_clase_miercoles_12.'">'.$tipo_clase_miercoles_12.'</a>';
							echo "<br>";
							echo "<br>";
							echo "Hora: ".date('H:i', strtotime($hora_clase_miercoles_12));
							echo "<br>";
							echo "<br>";
							echo "Cupos: ".$cupos_clase_miercoles_12;
						}
						?>
					</p>
				</td>
				<td class="black" id="jueves_12">
					<p>
					<?php 
						if(isset($hora_clase_jueves_12)){
							echo '<a href="../clase/Alumnos/vista_clase_alumno.php?id_clase='.$id_clase_jueves_12.'">'.$tipo_clase_jueves_12.'</a>';
							echo "<br>";
							echo "<br>";
							echo "Hora: ".date('H:i', strtotime($hora_clase_jueves_12));
							echo "<br>";
							echo "<br>";
							echo "Cupos: ".$cupos_clase_jueves_12;
						}
						?>
					</p>
				</td>
				<td class="black" id="viernes_12">
					<p>
						<?php 
						if(isset($hora_clase_viernes_12)){
							echo '<a href="../clase/Alumnos/vista_clase_alumno.php?id_clase='.$id_clase_viernes_12.'">'.$tipo_clase_viernes_12.'</a>';
							echo "<br>";
							echo "<br>";
							echo "Hora: ".date('H:i', strtotime($hora_clase_viernes_12));
							echo "<br>";
							echo "<br>";
							echo "Cupos: ".$cupos_clase_viernes_12;
						}
						?>
					</p>
				</td>
                <td class="black" id="sabado_12">
					<p>
						<?php 
						if(isset($hora_clase_sabado_12)){
							echo '<a href="../clase/Alumnos/vista_clase_alumno.php?id_clase='.$id_clase_sabado_12.'">'.$tipo_clase_sabado_12.'</a>';
							echo "<br>";
							echo "<br>";
							echo "Hora: ".date('H:i', strtotime($hora_clase_sabado_12));
							echo "<br>";
							echo "<br>";
							echo "Cupos: ".$cupos_clase_sabado_12;
						}
						?>
					</p>
				</td>
                <td class="black" id="domingo_12">
					<p>
						<?php 
						if(isset($hora_clase_domingo_12)){
							echo '<a href="../clase/Alumnos/vista_clase_alumno.php?id_clase='.$id_clase_domingo_12.'">'.$tipo_clase_domingo_12.'</a>';
							echo "<br>";
							echo "<br>";
							echo "Hora: ".date('H:i', strtotime($hora_clase_domingo_12));
							echo "<br>";
							echo "<br>";
							echo "Cupos: ".$cupos_clase_domingo_12;
						}
						?>
					</p>
				</td>
			</tr>
			<tr>
				<td class="horas">13:00</td>
				<td class="black" id="lunes_13">
					<p>
						<?php 
						if(isset($hora_clase_lunes_13)){
							echo '<a href="../clase/Alumnos/vista_clase_alumno.php?id_clase='.$id_clase_lunes_13.'">'.$tipo_clase_lunes_13.'</a>';
							echo "<br>";
							echo "<br>";
							echo "Hora: ".date('H:i', strtotime($hora_clase_lunes_13));
							echo "<br>";
							echo "<br>";
							echo "Cupos: ".$cupos_clase_lunes_13;
						}
						?>
					</p>
				</td>
				<td class="black" id="martes_13">
					<p>
						<?php 
						if(isset($hora_clase_martes_13)){
							echo '<a href="../clase/Alumnos/vista_clase_alumno.php?id_clase='.$id_clase_martes_13.'">'.$tipo_clase_martes_13.'</a>';
							echo "<br>";
							echo "<br>";
							echo "Hora: ".date('H:i', strtotime($hora_clase_martes_13));
							echo "<br>";
							echo "<br>";
							echo "Cupos: ".$cupos_clase_martes_13;
						}
						?>
					</p>
				</td>
				<td class="black" id="miercoles_13">
					<p>
						<?php 
						if(isset($hora_clase_miercoles_13)){
							echo '<a href="../clase/Alumnos/vista_clase_alumno.php?id_clase='.$id_clase_miercoles_13.'">'.$tipo_clase_miercoles_13.'</a>';
							echo "<br>";
							echo "<br>";
							echo "Hora: ".date('H:i', strtotime($hora_clase_miercoles_13));
							echo "<br>";
							echo "<br>";
							echo "Cupos: ".$cupos_clase_miercoles_13;
						}
						?>
					</p>
				</td>
				<td class="black" id="jueves_13">
					<p>
					<?php 
						if(isset($hora_clase_jueves_13)){
							echo '<a href="../clase/Alumnos/vista_clase_alumno.php?id_clase='.$id_clase_jueves_13.'">'.$tipo_clase_jueves_13.'</a>';
							echo "<br>";
							echo "<br>";
							echo "Hora: ".date('H:i', strtotime($hora_clase_jueves_13));
							echo "<br>";
							echo "<br>";
							echo "Cupos: ".$cupos_clase_jueves_13;
						}
						?>
					</p>
				</td>
				<td class="black" id="viernes_13">
					<p>
						<?php 
						if(isset($hora_clase_viernes_13)){
							echo '<a href="../clase/Alumnos/vista_clase_alumno.php?id_clase='.$id_clase_viernes_13.'">'.$tipo_clase_viernes_13.'</a>';
							echo "<br>";
							echo "<br>";
							echo "Hora: ".date('H:i', strtotime($hora_clase_viernes_13));
							echo "<br>";
							echo "<br>";
							echo "Cupos: ".$cupos_clase_viernes_13;
						}
						?>
					</p>
				</td>
                <td class="black" id="sabado_13">
					<p>
						<?php 
						if(isset($hora_clase_sabado_13)){
							echo '<a href="../clase/Alumnos/vista_clase_alumno.php?id_clase='.$id_clase_sabado_13.'">'.$tipo_clase_sabado_13.'</a>';
							echo "<br>";
							echo "<br>";
							echo "Hora: ".date('H:i', strtotime($hora_clase_sabado_13));
							echo "<br>";
							echo "<br>";
							echo "Cupos: ".$cupos_clase_sabado_13;
						}
						?>
					</p>
				</td>
                <td class="black" id="domingo_13">
					<p>
						<?php 
						if(isset($hora_clase_domingo_13)){
							echo '<a href="../clase/Alumnos/vista_clase_alumno.php?id_clase='.$id_clase_domingo_13.'">'.$tipo_clase_domingo_13.'</a>';
							echo "<br>";
							echo "<br>";
							echo "Hora: ".date('H:i', strtotime($hora_clase_domingo_13));
							echo "<br>";
							echo "<br>";
							echo "Cupos: ".$cupos_clase_domingo_13;
						}
						?>
					</p>
				</td>
			</tr>
			<tr>
				<td class="horas">14:00</td>
				<td class="black" id="lunes_14">
					<p>
						<?php 
						if(isset($hora_clase_lunes_14)){
							echo '<a href="../clase/Alumnos/vista_clase_alumno.php?id_clase='.$id_clase_lunes_14.'">'.$tipo_clase_lunes_14.'</a>';
							echo "<br>";
							echo "<br>";
							echo "Hora: ".date('H:i', strtotime($hora_clase_lunes_14));
							echo "<br>";
							echo "<br>";
							echo "Cupos: ".$cupos_clase_lunes_14;
						}
						?>
					</p>
				</td>
				<td class="black" id="martes_14">
					<p>
						<?php 
						if(isset($hora_clase_martes_14)){
							echo '<a href="../clase/Alumnos/vista_clase_alumno.php?id_clase='.$id_clase_martes_14.'">'.$tipo_clase_martes_14.'</a>';
							echo "<br>";
							echo "<br>";
							echo "Hora: ".date('H:i', strtotime($hora_clase_martes_14));
							echo "<br>";
							echo "<br>";
							echo "Cupos: ".$cupos_clase_martes_14;
						}
						?>
					</p>
				</td>
				<td class="black" id="miercoles_14">
					<p>
						<?php 
						if(isset($hora_clase_miercoles_14)){
							echo '<a href="../clase/Alumnos/vista_clase_alumno.php?id_clase='.$id_clase_miercoles_14.'">'.$tipo_clase_miercoles_14.'</a>';
							echo "<br>";
							echo "<br>";
							echo "Hora: ".date('H:i', strtotime($hora_clase_miercoles_14));
							echo "<br>";
							echo "<br>";
							echo "Cupos: ".$cupos_clase_miercoles_14;
						}
						?>
					</p>
				</td>
				<td class="black" id="jueves_14">
					<p>
					<?php 
						if(isset($hora_clase_jueves_14)){
							echo '<a href="../clase/Alumnos/vista_clase_alumno.php?id_clase='.$id_clase_jueves_14.'">'.$tipo_clase_jueves_14.'</a>';
							echo "<br>";
							echo "<br>";
							echo "Hora: ".date('H:i', strtotime($hora_clase_jueves_14));
							echo "<br>";
							echo "<br>";
							echo "Cupos: ".$cupos_clase_jueves_14;
						}
						?>
					</p>
				</td>
				<td class="black" id="viernes_14">
					<p>
						<?php 
						if(isset($hora_clase_viernes_14)){
							echo '<a href="../clase/Alumnos/vista_clase_alumno.php?id_clase='.$id_clase_viernes_14.'">'.$tipo_clase_viernes_14.'</a>';
							echo "<br>";
							echo "<br>";
							echo "Hora: ".date('H:i', strtotime($hora_clase_viernes_14));
							echo "<br>";
							echo "<br>";
							echo "Cupos: ".$cupos_clase_viernes_14;
						}
						?>
					</p>
				</td>
                <td class="black" id="sabado_14">
					<p>
						<?php 
						if(isset($hora_clase_sabado_14)){
							echo '<a href="../clase/Alumnos/vista_clase_alumno.php?id_clase='.$id_clase_sabado_14.'">'.$tipo_clase_sabado_14.'</a>';
							echo "<br>";
							echo "<br>";
							echo "Hora: ".date('H:i', strtotime($hora_clase_sabado_14));
							echo "<br>";
							echo "<br>";
							echo "Cupos: ".$cupos_clase_sabado_14;
						}
						?>
					</p>
				</td>
                <td class="black" id="domingo_14">
					<p>
						<?php 
						if(isset($hora_clase_domingo_14)){
							echo '<a href="../clase/Alumnos/vista_clase_alumno.php?id_clase='.$id_clase_domingo_14.'">'.$tipo_clase_domingo_14.'</a>';
							echo "<br>";
							echo "<br>";
							echo "Hora: ".date('H:i', strtotime($hora_clase_domingo_14));
							echo "<br>";
							echo "<br>";
							echo "Cupos: ".$cupos_clase_domingo_14;
						}
						?>
					</p>
				</td>
			</tr>
			<tr>
				<td class="horas">15:00</td>
				<td class="black" id="lunes_15">
					<p>
						<?php 
						if(isset($hora_clase_lunes_15)){
							echo '<a href="../clase/Alumnos/vista_clase_alumno.php?id_clase='.$id_clase_lunes_15.'">'.$tipo_clase_lunes_15.'</a>';
							echo "<br>";
							echo "<br>";
							echo "Hora: ".date('H:i', strtotime($hora_clase_lunes_15));
							echo "<br>";
							echo "<br>";
							echo "Cupos: ".$cupos_clase_lunes_15;
						}
						?>
					</p>
				</td>
				<td class="black" id="martes_15">
					<p>
						<?php 
						if(isset($hora_clase_martes_15)){
							echo '<a href="../clase/Alumnos/vista_clase_alumno.php?id_clase='.$id_clase_martes_15.'">'.$tipo_clase_martes_15.'</a>';
							echo "<br>";
							echo "<br>";
							echo "Hora: ".date('H:i', strtotime($hora_clase_martes_15));
							echo "<br>";
							echo "<br>";
							echo "Cupos: ".$cupos_clase_martes_15;
						}
						?>
					</p>
				</td>
				<td class="black" id="miercoles_15">
					<p>
						<?php 
						if(isset($hora_clase_miercoles_15)){
							echo '<a href="../clase/Alumnos/vista_clase_alumno.php?id_clase='.$id_clase_miercoles_15.'">'.$tipo_clase_miercoles_15.'</a>';
							echo "<br>";
							echo "<br>";
							echo "Hora: ".date('H:i', strtotime($hora_clase_miercoles_15));
							echo "<br>";
							echo "<br>";
							echo "Cupos: ".$cupos_clase_miercoles_15;
						}
						?>
					</p>
				</td>
				<td class="black" id="jueves_15">
					<p>
					<?php 
						if(isset($hora_clase_jueves_15)){
							echo '<a href="../clase/Alumnos/vista_clase_alumno.php?id_clase='.$id_clase_jueves_15.'">'.$tipo_clase_jueves_15.'</a>';
							echo "<br>";
							echo "<br>";
							echo "Hora: ".date('H:i', strtotime($hora_clase_jueves_15));
							echo "<br>";
							echo "<br>";
							echo "Cupos: ".$cupos_clase_jueves_15;
						}
						?>
					</p>
				</td>
				<td class="black" id="viernes_15">
					<p>
						<?php 
						if(isset($hora_clase_viernes_15)){
							echo '<a href="../clase/Alumnos/vista_clase_alumno.php?id_clase='.$id_clase_viernes_15.'">'.$tipo_clase_viernes_15.'</a>';
							echo "<br>";
							echo "<br>";
							echo "Hora: ".date('H:i', strtotime($hora_clase_viernes_15));
							echo "<br>";
							echo "<br>";
							echo "Cupos: ".$cupos_clase_viernes_15;
						}
						?>
					</p>
				</td>
                <td class="black" id="sabado_15">
					<p>
						<?php 
						if(isset($hora_clase_sabado_15)){
							echo '<a href="../clase/Alumnos/vista_clase_alumno.php?id_clase='.$id_clase_sabado_15.'">'.$tipo_clase_sabado_15.'</a>';
							echo "<br>";
							echo "<br>";
							echo "Hora: ".date('H:i', strtotime($hora_clase_sabado_15));
							echo "<br>";
							echo "<br>";
							echo "Cupos: ".$cupos_clase_sabado_15;
						}
						?>
					</p>
				</td>
                <td class="black" id="domingo_15">
					<p>
						<?php 
						if(isset($hora_clase_domingo_15)){
							echo '<a href="../clase/Alumnos/vista_clase_alumno.php?id_clase='.$id_clase_domingo_15.'">'.$tipo_clase_domingo_15.'</a>';
							echo "<br>";
							echo "<br>";
							echo "Hora: ".date('H:i', strtotime($hora_clase_domingo_15));
							echo "<br>";
							echo "<br>";
							echo "Cupos: ".$cupos_clase_domingo_15;
						}
						?>
					</p>
				</td>
			</tr>
			<tr>
				<td class="horas">16:00</td>
				<td class="black" id="lunes_16">
					<p>
						<?php 
						if(isset($hora_clase_lunes_16)){
							echo '<a href="../clase/Alumnos/vista_clase_alumno.php?id_clase='.$id_clase_lunes_16.'">'.$tipo_clase_lunes_16.'</a>';
							echo "<br>";
							echo "<br>";
							echo "Hora: ".date('H:i', strtotime($hora_clase_lunes_16));
							echo "<br>";
							echo "<br>";
							echo "Cupos: ".$cupos_clase_lunes_16;
						}
						?>
					</p>
				</td>
				<td class="black" id="martes_16">
					<p>
						<?php 
						if(isset($hora_clase_martes_16)){
							echo '<a href="../clase/Alumnos/vista_clase_alumno.php?id_clase='.$id_clase_martes_16.'">'.$tipo_clase_martes_16.'</a>';
							echo "<br>";
							echo "<br>";
							echo "Hora: ".date('H:i', strtotime($hora_clase_martes_16));
							echo "<br>";
							echo "<br>";
							echo "Cupos: ".$cupos_clase_martes_16;
						}
						?>
					</p>
				</td>
				<td class="black" id="miercoles_16">
					<p>
						<?php 
						if(isset($hora_clase_miercoles_16)){
							echo '<a href="../clase/Alumnos/vista_clase_alumno.php?id_clase='.$id_clase_miercoles_16.'">'.$tipo_clase_miercoles_16.'</a>';
							echo "<br>";
							echo "<br>";
							echo "Hora: ".date('H:i', strtotime($hora_clase_miercoles_16));
							echo "<br>";
							echo "<br>";
							echo "Cupos: ".$cupos_clase_miercoles_16;
						}
						?>
					</p>
				</td>
				<td class="black" id="jueves_16">
					<p>
					<?php 
						if(isset($hora_clase_jueves_16)){
							echo '<a href="../clase/Alumnos/vista_clase_alumno.php?id_clase='.$id_clase_jueves_16.'">'.$tipo_clase_jueves_16.'</a>';
							echo "<br>";
							echo "<br>";
							echo "Hora: ".date('H:i', strtotime($hora_clase_jueves_16));
							echo "<br>";
							echo "<br>";
							echo "Cupos: ".$cupos_clase_jueves_16;
						}
						?>
					</p>
				</td>
				<td class="black" id="viernes_16">
					<p>
						<?php 
						if(isset($hora_clase_viernes_16)){
							echo '<a href="../clase/Alumnos/vista_clase_alumno.php?id_clase='.$id_clase_viernes_16.'">'.$tipo_clase_viernes_16.'</a>';
							echo "<br>";
							echo "<br>";
							echo "Hora: ".date('H:i', strtotime($hora_clase_viernes_16));
							echo "<br>";
							echo "<br>";
							echo "Cupos: ".$cupos_clase_viernes_16;
						}
						?>
					</p>
				</td>
                <td class="black" id="sabado_16">
					<p>
						<?php 
						if(isset($hora_clase_sabado_16)){
							echo '<a href="../clase/Alumnos/vista_clase_alumno.php?id_clase='.$id_clase_sabado_16.'">'.$tipo_clase_sabado_16.'</a>';
							echo "<br>";
							echo "<br>";
							echo "Hora: ".date('H:i', strtotime($hora_clase_sabado_16));
							echo "<br>";
							echo "<br>";
							echo "Cupos: ".$cupos_clase_sabado_16;
						}
						?>
					</p>
				</td>
                <td class="black" id="domingo_16">
					<p>
						<?php 
						if(isset($hora_clase_domingo_16)){
							echo '<a href="../clase/Alumnos/vista_clase_alumno.php?id_clase='.$id_clase_domingo_16.'">'.$tipo_clase_domingo_16.'</a>';
							echo "<br>";
							echo "<br>";
							echo "Hora: ".date('H:i', strtotime($hora_clase_domingo_16));
							echo "<br>";
							echo "<br>";
							echo "Cupos: ".$cupos_clase_domingo_16;
						}
						?>
					</p>
				</td>
			</tr>
			<tr>
				<td class="horas">17:00</td>
				<td class="black" id="lunes_17">
					<p>
						<?php 
						if(isset($hora_clase_lunes_17)){
							echo '<a href="../clase/Alumnos/vista_clase_alumno.php?id_clase='.$id_clase_lunes_17.'">'.$tipo_clase_lunes_17.'</a>';
							echo "<br>";
							echo "<br>";
							echo "Hora: ".date('H:i', strtotime($hora_clase_lunes_17));
							echo "<br>";
							echo "<br>";
							echo "Cupos: ".$cupos_clase_lunes_17;
						}
						?>
					</p>
				</td>
				<td class="black" id="martes_17">
					<p>
						<?php 
						if(isset($hora_clase_martes_17)){
							echo '<a href="../clase/Alumnos/vista_clase_alumno.php?id_clase='.$id_clase_martes_17.'">'.$tipo_clase_martes_17.'</a>';
							echo "<br>";
							echo "<br>";
							echo "Hora: ".date('H:i', strtotime($hora_clase_martes_17));
							echo "<br>";
							echo "<br>";
							echo "Cupos: ".$cupos_clase_martes_17;
						}
						?>
					</p>
				</td>
				<td class="black" id="miercoles_17">
					<p>
						<?php 
						if(isset($hora_clase_miercoles_17)){
							echo '<a href="../clase/Alumnos/vista_clase_alumno.php?id_clase='.$id_clase_miercoles_17.'">'.$tipo_clase_miercoles_17.'</a>';
							echo "<br>";
							echo "<br>";
							echo "Hora: ".date('H:i', strtotime($hora_clase_miercoles_17));
							echo "<br>";
							echo "<br>";
							echo "Cupos: ".$cupos_clase_miercoles_17;
						}
						?>
					</p>
				</td>
				<td class="black" id="jueves_17">
					<p>
					<?php 
						if(isset($hora_clase_jueves_17)){
							echo '<a href="../clase/Alumnos/vista_clase_alumno.php?id_clase='.$id_clase_jueves_17.'">'.$tipo_clase_jueves_17.'</a>';
							echo "<br>";
							echo "<br>";
							echo "Hora: ".date('H:i', strtotime($hora_clase_jueves_17));
							echo "<br>";
							echo "<br>";
							echo "Cupos: ".$cupos_clase_jueves_17;
						}
						?>
					</p>
				</td>
				<td class="black" id="viernes_17">
					<p>
						<?php 
						if(isset($hora_clase_viernes_17)){
							echo '<a href="../clase/Alumnos/vista_clase_alumno.php?id_clase='.$id_clase_viernes_17.'">'.$tipo_clase_viernes_17.'</a>';
							echo "<br>";
							echo "<br>";
							echo "Hora: ".date('H:i', strtotime($hora_clase_viernes_17));
							echo "<br>";
							echo "<br>";
							echo "Cupos: ".$cupos_clase_viernes_17;
						}
						?>
					</p>
				</td>
                <td class="black" id="sabado_17">
					<p>
						<?php 
						if(isset($hora_clase_sabado_17)){
							echo '<a href="../clase/Alumnos/vista_clase_alumno.php?id_clase='.$id_clase_sabado_17.'">'.$tipo_clase_sabado_17.'</a>';
							echo "<br>";
							echo "<br>";
							echo "Hora: ".date('H:i', strtotime($hora_clase_sabado_17));
							echo "<br>";
							echo "<br>";
							echo "Cupos: ".$cupos_clase_sabado_17;
						}
						?>
					</p>
				</td>
                <td class="black" id="domingo_17">
					<p>
						<?php 
						if(isset($hora_clase_domingo_17)){
							echo '<a href="../clase/Alumnos/vista_clase_alumno.php?id_clase='.$id_clase_domingo_17.'">'.$tipo_clase_domingo_17.'</a>';
							echo "<br>";
							echo "<br>";
							echo "Hora: ".date('H:i', strtotime($hora_clase_domingo_17));
							echo "<br>";
							echo "<br>";
							echo "Cupos: ".$cupos_clase_domingo_17;
						}
						?>
					</p>
				</td>
			</tr>
			<tr>
				<td class="horas">18:00</td>
				<td class="black" id="lunes_18">
					<p>
						<?php 
						if(isset($hora_clase_lunes_18)){
							echo '<a href="../clase/Alumnos/vista_clase_alumno.php?id_clase='.$id_clase_lunes_18.'">'.$tipo_clase_lunes_18.'</a>';
							echo "<br>";
							echo "<br>";
							echo "Hora: ".date('H:i', strtotime($hora_clase_lunes_18));
							echo "<br>";
							echo "<br>";
							echo "Cupos: ".$cupos_clase_lunes_18;
						}
						?>
					</p>
				</td>
				<td class="black" id="martes_18">
					<p>
						<?php 
						if(isset($hora_clase_martes_18)){
							echo '<a href="../clase/Alumnos/vista_clase_alumno.php?id_clase='.$id_clase_martes_18.'">'.$tipo_clase_martes_18.'</a>';
							echo "<br>";
							echo "<br>";
							echo "Hora: ".date('H:i', strtotime($hora_clase_martes_18));
							echo "<br>";
							echo "<br>";
							echo "Cupos: ".$cupos_clase_martes_18;
						}
						?>
					</p>
				</td>
				<td class="black" id="miercoles_18">
					<p>
						<?php 
						if(isset($hora_clase_miercoles_18)){
							echo '<a href="../clase/Alumnos/vista_clase_alumno.php?id_clase='.$id_clase_miercoles_18.'">'.$tipo_clase_miercoles_18.'</a>';
							echo "<br>";
							echo "<br>";
							echo "Hora: ".date('H:i', strtotime($hora_clase_miercoles_18));
							echo "<br>";
							echo "<br>";
							echo "Cupos: ".$cupos_clase_miercoles_18;
						}
						?>
					</p>
				</td>
				<td class="black" id="jueves_18">
					<p>
					<?php 
						if(isset($hora_clase_jueves_18)){
							echo '<a href="../clase/Alumnos/vista_clase_alumno.php?id_clase='.$id_clase_jueves_18.'">'.$tipo_clase_jueves_18.'</a>';
							echo "<br>";
							echo "<br>";
							echo "Hora: ".date('H:i', strtotime($hora_clase_jueves_18));
							echo "<br>";
							echo "<br>";
							echo "Cupos: ".$cupos_clase_jueves_18;
						}
						?>
					</p>
				</td>
				<td class="black" id="viernes_18">
					<p>
						<?php 
						if(isset($hora_clase_viernes_18)){
							echo '<a href="../clase/Alumnos/vista_clase_alumno.php?id_clase='.$id_clase_viernes_18.'">'.$tipo_clase_viernes_18.'</a>';
							echo "<br>";
							echo "<br>";
							echo "Hora: ".date('H:i', strtotime($hora_clase_viernes_18));
							echo "<br>";
							echo "<br>";
							echo "Cupos: ".$cupos_clase_viernes_18;
						}
						?>
					</p>
				</td>
                <td class="black" id="sabado_18">
					<p>
						<?php 
						if(isset($hora_clase_sabado_18)){
							echo '<a href="../clase/Alumnos/vista_clase_alumno.php?id_clase='.$id_clase_sabado_18.'">'.$tipo_clase_sabado_18.'</a>';
							echo "<br>";
							echo "<br>";
							echo "Hora: ".date('H:i', strtotime($hora_clase_sabado_18));
							echo "<br>";
							echo "<br>";
							echo "Cupos: ".$cupos_clase_sabado_18;
						}
						?>
					</p>
				</td>
                <td class="black" id="domingo_18">
					<p>
						<?php 
						if(isset($hora_clase_domingo_18)){
							echo '<a href="../clase/Alumnos/vista_clase_alumno.php?id_clase='.$id_clase_domingo_18.'">'.$tipo_clase_domingo_18.'</a>';
							echo "<br>";
							echo "<br>";
							echo "Hora: ".date('H:i', strtotime($hora_clase_domingo_18));
							echo "<br>";
							echo "<br>";
							echo "Cupos: ".$cupos_clase_domingo_18;
						}
						?>
					</p>
				</td>
			</tr>
			<tr>
				<td class="horas">19:00</td>
				<td class="black" id="lunes_19">
					<p>
						<?php 
						if(isset($hora_clase_lunes_19)){
							echo '<a href="../clase/Alumnos/vista_clase_alumno.php?id_clase='.$id_clase_lunes_19.'">'.$tipo_clase_lunes_19.'</a>';
							echo "<br>";
							echo "<br>";
							echo "Hora: ".date('H:i', strtotime($hora_clase_lunes_19));
							echo "<br>";
							echo "<br>";
							echo "Cupos: ".$cupos_clase_lunes_19;
						}
						?>
					</p>
				</td>
				<td class="black" id="martes_19">
					<p>
						<?php 
						if(isset($hora_clase_martes_19)){
							echo '<a href="../clase/Alumnos/vista_clase_alumno.php?id_clase='.$id_clase_martes_19.'">'.$tipo_clase_martes_19.'</a>';
							echo "<br>";
							echo "<br>";
							echo "Hora: ".date('H:i', strtotime($hora_clase_martes_19));
							echo "<br>";
							echo "<br>";
							echo "Cupos: ".$cupos_clase_martes_19;
						}
						?>
					</p>
				</td>
				<td class="black" id="miercoles_19">
					<p>
						<?php 
						if(isset($hora_clase_miercoles_19)){
							echo '<a href="../clase/Alumnos/vista_clase_alumno.php?id_clase='.$id_clase_miercoles_19.'">'.$tipo_clase_miercoles_19.'</a>';
							echo "<br>";
							echo "<br>";
							echo "Hora: ".date('H:i', strtotime($hora_clase_miercoles_19));
							echo "<br>";
							echo "<br>";
							echo "Cupos: ".$cupos_clase_miercoles_19;
						}
						?>
					</p>
				</td>
				<td class="black" id="jueves_19">
					<p>
					<?php 
						if(isset($hora_clase_jueves_19)){
							echo '<a href="../clase/Alumnos/vista_clase_alumno.php?id_clase='.$id_clase_jueves_19.'">'.$tipo_clase_jueves_19.'</a>';
							echo "<br>";
							echo "<br>";
							echo "Hora: ".date('H:i', strtotime($hora_clase_jueves_19));
							echo "<br>";
							echo "<br>";
							echo "Cupos: ".$cupos_clase_jueves_19;
						}
						?>
					</p>
				</td>
				<td class="black" id="viernes_19">
					<p>
						<?php 
						if(isset($hora_clase_viernes_19)){
							echo '<a href="../clase/Alumnos/vista_clase_alumno.php?id_clase='.$id_clase_viernes_19.'">'.$tipo_clase_viernes_19.'</a>';
							echo "<br>";
							echo "<br>";
							echo "Hora: ".date('H:i', strtotime($hora_clase_viernes_19));
							echo "<br>";
							echo "<br>";
							echo "Cupos: ".$cupos_clase_viernes_19;
						}
						?>
					</p>
				</td>
                <td class="black" id="sabado_19">
					<p>
						<?php 
						if(isset($hora_clase_sabado_19)){
							echo '<a href="../clase/Alumnos/vista_clase_alumno.php?id_clase='.$id_clase_sabado_19.'">'.$tipo_clase_sabado_19.'</a>';
							echo "<br>";
							echo "<br>";
							echo "Hora: ".date('H:i', strtotime($hora_clase_sabado_19));
							echo "<br>";
							echo "<br>";
							echo "Cupos: ".$cupos_clase_sabado_19;
						}
						?>
					</p>
				</td>
                <td class="black" id="domingo_19">
					<p>
						<?php 
						if(isset($hora_clase_domingo_19)){
							echo '<a href="../clase/Alumnos/vista_clase_alumno.php?id_clase='.$id_clase_domingo_19.'">'.$tipo_clase_domingo_19.'</a>';
							echo "<br>";
							echo "<br>";
							echo "Hora: ".date('H:i', strtotime($hora_clase_domingo_19));
							echo "<br>";
							echo "<br>";
							echo "Cupos: ".$cupos_clase_domingo_19;
						}
						?>
					</p>
				</td>
			</tr>
			<tr>
				<td class="horas">20:00</td>
				<td class="black" id="lunes_20">
					<p>
						<?php 
						if(isset($hora_clase_lunes_20)){
							echo '<a href="../clase/Alumnos/vista_clase_alumno.php?id_clase='.$id_clase_lunes_20.'">'.$tipo_clase_lunes_20.'</a>';
							echo "<br>";
							echo "<br>";
							echo "Hora: ".date('H:i', strtotime($hora_clase_lunes_20));
							echo "<br>";
							echo "<br>";
							echo "Cupos: ".$cupos_clase_lunes_20;
						}
						?>
					</p>
				</td>
				<td class="black" id="martes_20">
					<p>
						<?php 
						if(isset($hora_clase_martes_20)){
							echo '<a href="../clase/Alumnos/vista_clase_alumno.php?id_clase='.$id_clase_martes_20.'">'.$tipo_clase_martes_20.'</a>';
							echo "<br>";
							echo "<br>";
							echo "Hora: ".date('H:i', strtotime($hora_clase_martes_20));
							echo "<br>";
							echo "<br>";
							echo "Cupos: ".$cupos_clase_martes_20;
						}
						?>
					</p>
				</td>
				<td class="black" id="miercoles_20">
					<p>
						<?php 
						if(isset($hora_clase_miercoles_20)){
							echo '<a href="../clase/Alumnos/vista_clase_alumno.php?id_clase='.$id_clase_miercoles_20.'">'.$tipo_clase_miercoles_20.'</a>';
							echo "<br>";
							echo "<br>";
							echo "Hora: ".date('H:i', strtotime($hora_clase_miercoles_20));
							echo "<br>";
							echo "<br>";
							echo "Cupos: ".$cupos_clase_miercoles_20;
						}
						?>
					</p>
				</td>
				<td class="black" id="jueves_20">
					<p>
					<?php 
						if(isset($hora_clase_jueves_20)){
							echo '<a href="../clase/Alumnos/vista_clase_alumno.php?id_clase='.$id_clase_jueves_20.'">'.$tipo_clase_jueves_20.'</a>';
							echo "<br>";
							echo "<br>";
							echo "Hora: ".date('H:i', strtotime($hora_clase_jueves_20));
							echo "<br>";
							echo "<br>";
							echo "Cupos: ".$cupos_clase_jueves_20;
						}
						?>
					</p>
				</td>
				<td class="black" id="viernes_20">
					<p>
						<?php 
						if(isset($hora_clase_viernes_20)){
							echo '<a href="../clase/Alumnos/vista_clase_alumno.php?id_clase='.$id_clase_viernes_20.'">'.$tipo_clase_viernes_20.'</a>';
							echo "<br>";
							echo "<br>";
							echo "Hora: ".date('H:i', strtotime($hora_clase_viernes_20));
							echo "<br>";
							echo "<br>";
							echo "Cupos: ".$cupos_clase_viernes_20;
						}
						?>
					</p>
				</td>
                <td class="black" id="sabado_20">
					<p>
						<?php 
						if(isset($hora_clase_sabado_20)){
							echo '<a href="../clase/Alumnos/vista_clase_alumno.php?id_clase='.$id_clase_sabado_20.'">'.$tipo_clase_sabado_20.'</a>';
							echo "<br>";
							echo "<br>";
							echo "Hora: ".date('H:i', strtotime($hora_clase_sabado_20));
							echo "<br>";
							echo "<br>";
							echo "Cupos: ".$cupos_clase_sabado_20;
						}
						?>
					</p>
				</td>
                <td class="black" id="domingo_20">
					<p>
						<?php 
						if(isset($hora_clase_domingo_20)){
							echo '<a href="../clase/Alumnos/vista_clase_alumno.php?id_clase='.$id_clase_domingo_20.'">'.$tipo_clase_domingo_20.'</a>';
							echo "<br>";
							echo "<br>";
							echo "Hora: ".date('H:i', strtotime($hora_clase_domingo_20));
							echo "<br>";
							echo "<br>";
							echo "Cupos: ".$cupos_clase_domingo_20;
						}
						?>
					</p>
				</td>
			</tr>
			<tr>
				<td class="horas">21:00</td>
				<td class="black" id="lunes_21">
					<p>
						<?php 
						if(isset($hora_clase_lunes_21)){
							echo '<a href="../clase/Alumnos/vista_clase_alumno.php?id_clase='.$id_clase_lunes_21.'">'.$tipo_clase_lunes_21.'</a>';
							echo "<br>";
							echo "<br>";
							echo "Hora: ".date('H:i', strtotime($hora_clase_lunes_21));
							echo "<br>";
							echo "<br>";
							echo "Cupos: ".$cupos_clase_lunes_21;
						}
						?>
					</p>
				</td>
				<td class="black" id="martes_21">
					<p>
						<?php 
						if(isset($hora_clase_martes_21)){
							echo '<a href="../clase/Alumnos/vista_clase_alumno.php?id_clase='.$id_clase_martes_21.'">'.$tipo_clase_martes_21.'</a>';
							echo "<br>";
							echo "<br>";
							echo "Hora: ".date('H:i', strtotime($hora_clase_martes_21));
							echo "<br>";
							echo "<br>";
							echo "Cupos: ".$cupos_clase_martes_21;
						}
						?>
					</p>
				</td>
				<td class="black" id="miercoles_21">
					<p>
						<?php 
						if(isset($hora_clase_miercoles_21)){
							echo '<a href="../clase/Alumnos/vista_clase_alumno.php?id_clase='.$id_clase_miercoles_21.'">'.$tipo_clase_miercoles_21.'</a>';
							echo "<br>";
							echo "<br>";
							echo "Hora: ".date('H:i', strtotime($hora_clase_miercoles_21));
							echo "<br>";
							echo "<br>";
							echo "Cupos: ".$cupos_clase_miercoles_21;
						}
						?>
					</p>
				</td>
				<td class="black" id="jueves_21">
					<p>
					<?php 
						if(isset($hora_clase_jueves_21)){
							echo '<a href="../clase/Alumnos/vista_clase_alumno.php?id_clase='.$id_clase_jueves_21.'">'.$tipo_clase_jueves_21.'</a>';
							echo "<br>";
							echo "<br>";
							echo "Hora: ".date('H:i', strtotime($hora_clase_jueves_21));
							echo "<br>";
							echo "<br>";
							echo "Cupos: ".$cupos_clase_jueves_21;
						}
						?>
					</p>
				</td>
				<td class="black" id="viernes_21">
					<p>
						<?php 
						if(isset($hora_clase_viernes_21)){
							echo '<a href="../clase/Alumnos/vista_clase_alumno.php?id_clase='.$id_clase_viernes_21.'">'.$tipo_clase_viernes_21.'</a>';
							echo "<br>";
							echo "<br>";
							echo "Hora: ".date('H:i', strtotime($hora_clase_viernes_21));
							echo "<br>";
							echo "<br>";
							echo "Cupos: ".$cupos_clase_viernes_21;
						}
						?>
					</p>
				</td>
                <td class="black" id="sabado_21">
					<p>
						<?php 
						if(isset($hora_clase_sabado_21)){
							echo '<a href="../clase/Alumnos/vista_clase_alumno.php?id_clase='.$id_clase_sabado_21.'">'.$tipo_clase_sabado_21.'</a>';
							echo "<br>";
							echo "<br>";
							echo "Hora: ".date('H:i', strtotime($hora_clase_sabado_21));
							echo "<br>";
							echo "<br>";
							echo "Cupos: ".$cupos_clase_sabado_21;
						}
						?>
					</p>
				</td>
                <td class="black" id="domingo_21">
					<p>
						<?php 
						if(isset($hora_clase_domingo_21)){
							echo '<a href="../clase/Alumnos/vista_clase_alumno.php?id_clase='.$id_clase_domingo_21.'">'.$tipo_clase_domingo_21.'</a>';
							echo "<br>";
							echo "<br>";
							echo "Hora: ".date('H:i', strtotime($hora_clase_domingo_21));
							echo "<br>";
							echo "<br>";
							echo "Cupos: ".$cupos_clase_domingo_21;
						}
						?>
					</p>
				</td>
			</tr>
		</tbody>
	</table>
</div>
</body>
</html>
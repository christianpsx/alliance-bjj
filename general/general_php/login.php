<?php
include("../../includes/connection.php");

session_start();

//Codigo para alumnos: 
$email = mysqli_real_escape_string($connect, $_POST['mail']);
$contrasena = mysqli_real_escape_string($connect, $_POST['contrasena']);

$validacion_alumno = "SELECT COUNT(*) AS verificador_alumno FROM Alumno WHERE Mail = '$email' AND Contrasena = MD5('".$contrasena."')";

$consulta_estudiante = mysqli_query($connect, $validacion_alumno);
$arreglo_estudiante = mysqli_fetch_array($consulta_estudiante);

if($arreglo_estudiante['verificador_alumno'] == 1){
    $consulta_alumno = "SELECT * FROM Alumno WHERE Mail = '$email' AND Contrasena = MD5('".$contrasena."')";
    $peticion_alumno = mysqli_query($connect, $consulta_alumno);

    if($peticion_alumno){
        while($row = $peticion_alumno->fetch_array()){
            $id = $row['ID_alumno'];

            $nombre = $row['Nombre'];
            $apellido = $row['Apellidos'];

            $rut = $row['Rut'];
            $fecha_n = $row['Fecha_nacimiento'];

            $telefono = $row['Telefono'];
            $direccion = $row['Direccion'];

            $cinturon = $row['Cinturon'];
            $grado = $row['Grado'];
            $clases = $row['Cantidad_clases'];
            $burpees = $row['Burpees'];
            $fecha_g = $row['Fecha_grado'];

            $observaciones = $row['Observaciones'];
            $experiencia = $row['Experiencia'];

            $promo = $row['Promo_ofertas'];

            $active = $row['active'];
        }
    }
    if($active == 1){
        $_SESSION['ID_alumno'] = $id;

        $_SESSION['Nombre'] = $nombre;
        $_SESSION['Apellidos'] = $apellido;
        
        $_SESSION['Rut'] = $rut;
        $_SESSION['Fecha_nacimiento'] = $fecha_n;

        $_SESSION['Mail'] = $email;
        $_SESSION['Telefono'] = $telefono;
        $_SESSION['Direccion'] = $direccion;

        $_SESSION['Cinturon'] = $cinturon;
        $_SESSION['Grado'] = $grado;
        $_SESSION['Cantidad_clases'] = $clases;
        $_SESSION['Burpees'] = $burpees;
        $_SESSION['Fecha_grado'] = $fecha_g;

        $_SESSION['Observaciones'] = $observaciones;
        $_SESSION['Experiencia'] = $experiencia;

        $_SESSION['Promo_ofertas'] = $promo;

        $_SESSION['active'] = $active;
    
        header("location: ../../usuario/usuario_php/home.php");
    }else{
        echo '<script type="text/javascript"> alert("Debe activar su cuenta."); location="../../index.html"; </script>';
    }    
}else{
    $validacion_profesor = "SELECT COUNT(*) AS verificador_profesor FROM Profesor WHERE Mail = '$email' AND Contrasena = MD5('".$contrasena."')";

    $consulta_maestro = mysqli_query($connect, $validacion_profesor);
    $arreglo_profesor = mysqli_fetch_array($consulta_maestro);

    if($arreglo_profesor['verificador_profesor'] == 1){
        $consulta_profesor = "SELECT * FROM Profesor WHERE Mail = '$email' AND Contrasena = MD5('".$contrasena."')";
        $peticion_profesor = mysqli_query($connect, $consulta_profesor);

        if($peticion_profesor){
            while($row = $peticion_profesor->fetch_array()){
                $id = $row['ID_profesor'];

                $nombre = $row['Nombre'];
                $apellido = $row['Apellidos'];

                $rut = $row['Rut'];
                $fecha_n = $row['Fecha_nacimiento'];

                $telefono = $row['Telefono'];
                $direccion = $row['Direccion'];

                $cinturon = $row['Cinturon'];

                $poder = $row['Poder'];
                $active = $row['active'];
            }
        }
        if($active == 1){
            $_SESSION['ID_profesor'] = $id;

            $_SESSION['Nombre'] = $nombre;
            $_SESSION['Apellidos'] = $apellido;
            
            $_SESSION['Rut'] = $rut;
            $_SESSION['Fecha_nacimiento'] = $fecha_n;

            $_SESSION['Mail'] = $email;
            $_SESSION['Telefono'] = $telefono;
            $_SESSION['Direccion'] = $direccion;

            $_SESSION['Cinturon'] = $cinturon;

            $_SESSION['Poder'] = $poder;
            $_SESSION['active'] = $active;
    
            header("location: ../../profesor/profesor_php/home_profesor.php");
        }else{
            echo '<script type="text/javascript"> alert("Debe activar su cuenta."); location="../../index.html"; </script>';
        }   
    }else{
        echo '<script type="text/javascript"> alert("Datos incorrectos."); location="../../index.html"; </script>';
    }
}

mysqli_close($connect);
?>
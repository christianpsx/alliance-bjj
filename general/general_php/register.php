<?php
include("../../includes/connection.php");

if(isset($_POST['nombre']) && !empty($_POST['nombre']) AND isset($_POST['email']) && !empty($_POST['email'])){
    
    //Limpiamos la entrada de posibles injections:

    $nombre = mysqli_real_escape_string($connect, $_POST['nombre']);
    $apellidos = mysqli_real_escape_string($connect, $_POST['apellido']);

    $fecha_nacimiento = mysqli_real_escape_string($connect, $_POST['fecha']);
    $rut = mysqli_real_escape_string($connect, $_POST['rut']);

    $email = mysqli_real_escape_string($connect, $_POST['email']);

    $contrasena = mysqli_real_escape_string($connect, $_POST['psw']);
    $contrasena2 = mysqli_real_escape_string($connect,$_POST['psw2']);

    $telefono = mysqli_real_escape_string($connect, $_POST['telefono']);
    $direccion = mysqli_real_escape_string($connect, $_POST['direccion']);

    $experiencia = mysqli_real_escape_string($connect, $_POST['experiencia']);
    $cinturon = mysqli_real_escape_string($connect, $_POST['cinturon']);
    $grado = mysqli_real_escape_string($connect, $_POST['grado']);

    $validacion_mail_profesor = "SELECT COUNT(*) AS verificador_mail_profesor FROM Profesor WHERE Mail = '$email'";
    $consulta_mail_profesor = mysqli_query($connect, $validacion_mail_profesor);
    $arreglo_mail_profesor = mysqli_fetch_array($consulta_mail_profesor);
    if ($contrasena != $contrasena2) {
        echo '<script type="text/javascript"> alert("Verificar que las contraseñas sean iguales."); location="../register.html"; </script>';
        
    }else if($arreglo_mail_profesor['verificador_mail_profesor'] == 1){
        echo '<script type="text/javascript"> alert("El e-mail proporcionado ya ha sido registrado."); location="../register.html"; </script>';
        mysqli_close($connect);
    }else{
        $validacion_mail_alumno = "SELECT COUNT(*) AS verificador_mail_alumno FROM Alumno WHERE Mail = '$email'";
        $consulta_mail_alumno = mysqli_query($connect, $validacion_mail_alumno);
        $arreglo_mail_alumno = mysqli_fetch_array($consulta_mail_alumno);
    
        if($arreglo_mail_alumno['verificador_mail_alumno'] == 0){
            $msg = 'Your account has been made, <br /> please verify it by clicking the activation link that has been send to your email.';
    
            $hash = md5(rand(0,1000)); // Generate random 32 character hash and assign it to a local variable.
            $INSERTAR_ALUMNO = "INSERT INTO Alumno (Nombre, Apellidos, Rut, Mail, Contrasena, Fecha_nacimiento, Telefono, Direccion, Cinturon, Grado, Experiencia, Hash_alumno) VALUES ('$nombre','$apellidos','$rut','$email', MD5('".$contrasena."'),'$fecha_nacimiento','$telefono','$direccion','$cinturon','$grado','$experiencia','$hash')";
            if ($connect->query($INSERTAR_ALUMNO) === TRUE) {

                $to      = $email; //Send email to our user
                $subject = 'Verificación'; //// Give the email a subject 
                $message = '

                Gracias por registrarte como alumno!
                Su cuenta ha sido creada, puede iniciar sesión con su correo y la siguiente contraseña después de haber activado su cuenta pulsando la url de abajo.

                ------------------------
                Password: '.$contrasena.'
                ------------------------

                Haga clic en este enlace para activar su cuenta:
                https://alliancebjj.cl/login/general/verify.php?email='.$email.'&hash='.$hash.'

                Para cualquier consulta
                contacto@alliancebjj.cl

                '; // INCLUIR LINK BIEN

                $headers = 'From:noreply@alliance.cl' . "\r\n"; // Set from headers
                mail($to, $subject, $message, $headers); // Enviar el email
                header('Location: ../../index.html');

            } 
            else {
                echo "Error: " . $INSERTAR_ALUMNO . "<br>" . $connect->error;
            }
        }else{
            echo '<script type="text/javascript"> alert("El e-mail proporcionado ya tiene una cuenta asociada."); location="../register.html";</script>';
            
        }
    }
}

mysqli_close($connect);
?>


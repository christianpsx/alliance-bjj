<?php
//Conexion a la base de datos
include("../../includes/connection.php");
include_once("../../includes/funciones.php");

//Recepcion de la id de la clase desde vista_clase_alumno.php
$ID_CLASE = mysqli_real_escape_string($connect, $_REQUEST['id_clase']);

//Se comienza la sesion para capturar los datos del profesor

session_start();

$ID = $_SESSION['ID_profesor'];
$NOMBRE = $_SESSION['Nombre'];
$APELLIDOS = $_SESSION['Apellidos'];

$RUT = $_SESSION['Rut'];
$FECHA_N = $_SESSION['Fecha_nacimiento'];

$DIRECCION = $_SESSION['Direccion'];
$MAIL = $_SESSION['Mail'];
$TELEFONO = $_SESSION['Telefono'];

$CINTURON = $_SESSION['Cinturon'];

$PODER = $_SESSION['Poder'];
$ACTIVE = $_SESSION['active'];

//Validacion de que existe una sesion
if(!isset($ID) OR $ACTIVE == 0){
    session_destroy();
    header("location: ../../index.html");
    exit();
}

//Consultar datos de la clase asociada a la ID obtenida

$informacion_clase = "SELECT * FROM Clase WHERE ID_clase = '$ID_CLASE'";
$peticion_informacion_clase = mysqli_query($connect, $informacion_clase);

if($peticion_informacion_clase){
    while($tabla = $peticion_informacion_clase->fetch_array()){
        $ID_PROFESOR_CLASE = $tabla['ID_profesor'];

        $TIPO_CLASE = $tabla['Tipo_clase'];
        $CUPOS_CLASE = $tabla['Cupos'];

        $CINTURON_CLASE = $tabla['Cinturon'];
        $GRADO_CLASE = $tabla['Grado'];

        $DIA_CLASE = $tabla['Dia'];
        $HORA_CLASE = $tabla['Hora'];

        $VISIBILIDAD_CLASE = $tabla['Visibilidad'];
    }
}

//Los datos de tipo enum se convertiran en su string asociado

switch($CINTURON_CLASE){
    case 1:
        $cinturon_clase = "Ninguno";
        break;
    case 2:
        $cinturon_clase = "Blanco";
        break;
    case 3:
        $cinturon_clase = "Azul";
        break;
    case 4:
        $cinturon_clase = "Morado";
        break;
    case 5:
        $cinturon_clase = "Cafe";
        break;
    case 6: 
        $cinturon_clase = "Negro";
        break;
    default:
        break;
}
switch($GRADO_CLASE){
    case 1:
        $grado_clase = "Ninguno";
        break;
    case 2:
        $grado_clase = "I";
        break;
    case 3:
        $grado_clase = "II";
        break;
    case 4:
        $grado_clase = "III";
        break;
    case 5: 
        $grado_clase = "IV";
        break;
    default:
        break;
}
switch($DIA_CLASE){
    case 1:
        $dia_clase = "Lunes";
        break;
    case 2:
        $dia_clase = "Martes";
        break;
    case 3:
        $dia_clase = "Miércoles";
        break;
    case 4:
        $dia_clase = "Jueves";
        break;
    case 5:
        $dia_clase = "Viernes";
        break;
    case 6:
        $dia_clase = "Sabado";
        break;
    case 7:
        $dia_clase = "Domingo";
        break;
    default:
        break;
}
switch ($VISIBILIDAD_CLASE) {
    case 0:
        $visibilidad_clase = "No visible en el horario";
        break;
    case 1:
        $visibilidad_clase = "Visible en el horario";
        break;
    default:
        break;
}
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="/clase/Profesor/css/config.css">

    <ul>
        <li class="log"> <?php echo "<p>Bienvenido $NOMBRE $APELLIDOS</p>";?> </li>
        <li><a href="../../profesor/profesor_php/home_profesor.php">Inicio</a></li>
        <li><a href="../../profesor/perfil_profesor.php">Perfil</a></li>
        <li class="active"><a href="../../profesor/horario_profesor.php">Horario</a></li>
        <li><a href="../../profesor/vista_clase.php" class="ac">Clases</a></li>
        <li><a href="../../general/general_php/logout.php">Cerrar sesion</a></li>
    </ul>

    <link rel="stylesheet" type="text/css" href="css/vista_config.css">

    <title>Configuracion de <?php echo switchClases($TIPO_CLASE);?></title>
</head>
<body>
    <div class ="head">
        <h1><?php echo switchClases($TIPO_CLASE)." || $dia_clase, Hora: ".date('g:ia', strtotime($HORA_CLASE));?></h1>
    </div>

    <form action="php/configuracion.php?id_clase=<?php echo $ID_CLASE;?>&eliminar_clase=0" method="post" name="formulario" class="formulario">
        <label for="titulo"><b>Configuracion: </b></label>

        <div class="Cupos">
            <label for="Cupos"><b>Cupos</b></label>
            <input class="controls" type="number" placeholder="<?php echo $CUPOS_CLASE;?>" name="Cupos" id="Cupos" min="1" max="100" value="<?php echo $CUPOS_CLASE;?>">
        </div>

        <div class="Dia">
            <label for="Dia"><b>Dia</b></label>
            <select class="controls" name="Dia" id="Dia">
                <option value="<?php echo $DIA_CLASE;?>" selected hidden><?php echo $dia_clase;?></option>
                <option value="1">Lunes</option>
                <option value="2">Martes</option>
                <option value="3">Miércoles</option>
                <option value="4">Jueves</option>
                <option value="5">Viernes</option>
                <option value="6">Sábado</option>
                <option value="7">Domingo</option>
            </select>
        </div>

        <div>
            <label for="Hora"><b>Hora</b></label>
            <input class="controls" type="time" name="Hora" min="7:00" max="22:00" step="60" value="<?php echo $HORA_CLASE;?>"/>
        </div>

        <div class="Cinturon">
            <label for="Cinturon"><b>Cinturon</b></label>
            <select  class="controls" name="Cinturon" id="Cinturon">
                <option value="<?php echo $CINTURON_CLASE;?>" selected hidden><?php echo $cinturon_clase;?></option>
                <option value="1">Ninguno</option>
                <option value="2">Blanco</option>
                <option value="3">Azul</option>
                <option value="4">Morado</option>
                <option value="5">Cafe</option>
                <option value="6">Negro</option>
            </select>
        </div>   

        <div class="Grado">
            <label for="Grado"><b>Grado</b></label>
            <select  class="controls" name="Grado" id="Grado" >
                <option value="<?php echo $GRADO_CLASE;?>" selected hidden><?php echo $grado_clase;?></option>
                <option value="1">Ninguno</option>
                <option value="2">I</option>
                <option value="3">II</option>
                <option value="4">III</option>
                <option value="5">IV</option>
            </select>
        </div>

        <div>
            <label for="Visibilidad"><b>Visibilidad de la clase</b></label>
            <select  class="controls" name="Visibilidad" id="Visibilidad">
                <option value="<?php echo $VISIBILIDAD_CLASE;?>" selected hidden><?php echo $visibilidad_clase;?></option>
                <option value="0">No visible en el horario</option>
                <option value="1">Visible en el horario</option>
            </select>
        </div>

        <input type="submit" value="Actualizar" class="button">
    </form>
        <br>

    <div class="eliminar_clase">
        <h2>Eliminar clase</h2>
        <div id="eliminar_container">
            <button id="boton_eliminar">Eliminar clase</button>
        </div>
        <div id="confirmacion_container">
            <label for="boton_eliminar_confirmacion">¿Esta seguro de que quiere eliminar su clase?</label>
            <br>
            <button id="boton_eliminar_confirmacion"><a href="php/configuracion.php?id_clase=<?php echo $ID_CLASE;?>&eliminar_clase=1">Eliminar clase</a></button>
            <button id="boton_eliminar_cancelacion">Cancelar</button>
        </div>
    </div>
    

    <script>
        document.getElementById("confirmacion_container").style.display = "none";

        document.getElementById("boton_eliminar").onclick = mostrarDiv;
        document.getElementById("boton_eliminar_cancelacion").onclick = MostrarDiv_eliminar;
        
        function mostrarDiv(){
            document.getElementById("eliminar_container").style.display = "none";
            document.getElementById("confirmacion_container").style.display = "block";
        }

        function MostrarDiv_eliminar(){
            document.getElementById("eliminar_container").style.display = "block";
            document.getElementById("confirmacion_container").style.display = "none";
        }

        //Verificador de contraseñas.
    </script> 
</body>
</html>
<?php
//Conexion a la base de datos
include("../../includes/connection.php");
include_once("../../includes/funciones.php");

//Recepcion de la id de la clase
$ID_CLASE = mysqli_real_escape_string($connect, $_REQUEST['id_clase']);

//Se comienza la sesion para capturar los datos del profesor
session_start();

$ID = $_SESSION['ID_profesor'];
$NOMBRE = $_SESSION['Nombre'];
$APELLIDOS = $_SESSION['Apellidos'];

$RUT = $_SESSION['Rut'];
$FECHA_N = $_SESSION['Fecha_nacimiento'];

$DIRECCION = $_SESSION['Direccion'];
$MAIL = $_SESSION['Mail'];
$TELEFONO = $_SESSION['Telefono'];

$CINTURON = $_SESSION['Cinturon'];

$PODER = $_SESSION['Poder'];
$ACTIVE = $_SESSION['active'];

//Validacion de que existe una sesion
if(!isset($ID) OR $ACTIVE == 0){
    session_destroy();
    header("location: ../../index.html");
    exit();
}

//Consultar datos de la clase asociada a la ID obtenida

$informacion_clase = "SELECT * FROM Clase WHERE ID_clase = '$ID_CLASE'";
$peticion_informacion_clase = mysqli_query($connect, $informacion_clase);

if($peticion_informacion_clase){
    while($tabla = $peticion_informacion_clase->fetch_array()){
        $ID_PROFESOR_CLASE = $tabla['ID_profesor'];
        $TIPO_CLASE = $tabla['Tipo_clase'];
        $CUPOS_CLASE = $tabla['Cupos'];

        $CINTURON_CLASE = $tabla['Cinturon'];
        $GRADO_CLASE = $tabla['Grado'];

        $DIA_CLASE = $tabla['Dia'];
        $HORA_CLASE = $tabla['Hora'];

        $VISIBILIDAD_CLASE = $tabla['Visibilidad'];
    }
}

//Consultar datos del profesor encargado de la clase

$informacion_profesor = "SELECT * FROM Profesor WHERE ID_profesor = '$ID_PROFESOR_CLASE'";
$peticion_informacion_profesor = mysqli_query($connect, $informacion_profesor);

if($peticion_informacion_profesor){
    while($row = $peticion_informacion_profesor->fetch_array()){
        $ID_PROFESOR = $row['ID_profesor'];

        $NOMBRE_PROFESOR = $row['Nombre'];
        $APELLIDOS_PROFESOR = $row['Apellidos'];
    }
}

//Actualizacion automatica de cupos

$consulta_cupos = "SELECT COUNT(*) AS contador_cupos FROM Inscripcion WHERE ID_clase='$ID_CLASE'";
$peticion_consulta_cupos = mysqli_query($connect, $consulta_cupos);
$contador_cupos = mysqli_fetch_array($peticion_consulta_cupos);

if($contador_cupos['contador_cupos'] >= 0){
    $cupos_clase = $CUPOS_CLASE - $contador_cupos['contador_cupos'];
}


//Los datos de tipo enum se convertiran en su string asociado

switch($CINTURON_CLASE){
    case 1:
        $cinturon_clase = "Ninguno";
        break;
    case 2:
        $cinturon_clase = "Blanco";
        break;
    case 3:
        $cinturon_clase = "Azul";
        break;
    case 4:
        $cinturon_clase = "Morado";
        break;
    case 5:
        $cinturon_clase = "Marron";
        break;
    case 6: 
        $cinturon_clase = "Negro";
        break;
    default:
        break;
}
switch($GRADO_CLASE){
    case 1:
        $grado_clase = "Ninguno";
        break;
    case 2:
        $grado_clase = "I";
        break;
    case 3:
        $grado_clase = "II";
        break;
    case 4:
        $grado_clase = "III";
        break;
    case 5: 
        $grado_clase = "IV";
        break;
    default:
        break;
}
switch($DIA_CLASE){
    case 1:
        $dia_clase = "Lunes";
        break;
    case 2:
        $dia_clase = "Martes";
        break;
    case 3:
        $dia_clase = "Miércoles";
        break;
    case 4:
        $dia_clase = "Jueves";
        break;
    case 5:
        $dia_clase = "Viernes";
        break;
    case 6:
        $dia_clase = "Sabado";
        break;
    case 7:
        $dia_clase = "Domingo";
        break;
    default:
        break;
}
switch ($VISIBILIDAD_CLASE) {
    case 0:
        $visibilidad_clase = "No visible en el horario";
        break;
    case 1:
        $visibilidad_clase = "Visible en el horario";
        break;
    default:
        break;
}

?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" type="text/css" href="css/vista_clase.css">

    <title>Clase <?php echo switchClases($TIPO_CLASE)?></title>

    <ul>
        <li class="log"> <?php echo "<p>Bienvenido $NOMBRE $APELLIDOS</p>";?> </li>

        <li><a href="../../profesor/profesor_php/home_profesor.php">Inicio</a></li>

        <li><a href="../../profesor/perfil_profesor.php">Perfil</a></li>

        <li class="active"><a href="../../profesor/horario_profesor.php">Horario</a></li>

        <li><a href="../../profesor/vista_clase.php">Clases</a></li>

        <li><a href="../../general/general_php/logout.php">Cerrar sesion</a></li>
    </ul>
</head>
<body>
    <div class ="head">
        <h1><?php echo switchClases($TIPO_CLASE)." || $dia_clase, Hora ".date('H:i', strtotime($HORA_CLASE));?></h1>
    </div>
    <div class = "centro">
            <div class= "espacio">
            </html>
            <?php
            if($ID == $ID_PROFESOR){
            ?>
            <html>
                <button><a href="vista_configuracion_profesor.php?id_clase=<?php echo $ID_CLASE;?>">Configuración</a></button>
                <br>
                <div id="eliminar_container">
                    <button id="boton_eliminar">Reiniciar inscripciones</button>
                </div>
            
                <div id="confirmacion_container">
                    <label for="buttons">¿Esta seguro?</label>
                    <br>
                    <br>
                    <button  id="boton_eliminar"><a href="php/configuracion_inscripciones.php?id_clase=<?php echo $ID_CLASE;?>&opcion=1">Reiniciar inscripciones</a></button>
                    <button  id="boton_eliminar_cancelacion" class="buttons">Cancelar</button>
                </div>
                <br>
            </html>
                
            <?php
                if($TIPO_CLASE == 17){
                    ?>
                        <html>
                        <form action="php/inscribir.php?id_clase=<?php echo $ID_CLASE;?>" method="POST">
                            <select name="alumno" id="alumno">
                            </html>
                            <?php
                            
                            $alumnos_disponibles = mysqli_query($connect, "SELECT * FROM Alumno WHERE Cinturon >= '$CINTURON_CLASE'");
                            while($alumno = $alumnos_disponibles->fetch_assoc()){
                                $id_alumno_inscribir = $alumno['ID_alumno'];
                                $nombre_alumno_inscribir = $alumno['Nombre'];
                                $apellidos_alumno_inscribir = $alumno['Apellidos'];
                                $cinturon_alumno_inscribir = $alumno['Cinturon'];
                                $grado_alumno_inscribir = $alumno['Grado'];

                                switch($cinturon_alumno_inscribir){
                                    case 1:
                                        $cinturon = "Ninguno";
                                        break;
                                    case 2:
                                        $cinturon = "Blanco";
                                        break;
                                    case 3:
                                        $cinturon = "Azul";
                                        break;
                                    case 4:
                                        $cinturon = "Morado";
                                        break;
                                    case 5:
                                        $cinturon = "Marron";
                                        break;
                                    case 6: 
                                        $cinturon = "Negro";
                                        break;
                                    default:
                                        break;
                                }
                                switch($grado_alumno_inscribir){
                                    case 1:
                                        $grado = "Ninguno";
                                        break;
                                    case 2:
                                        $grado = "I";
                                        break;
                                    case 3:
                                        $grado = "II";
                                        break;
                                    case 4:
                                        $grado = "III";
                                        break;
                                    case 5: 
                                        $grado = "IV";
                                        break;
                                    default:
                                        break;
                                }

                                $existencia_alumno = mysqli_fetch_array(mysqli_query($connect, "SELECT COUNT(*) AS coincidencia FROM Inscripcion WHERE ID_alumno = '$id_alumno_inscribir' AND ID_clase = '$ID_CLASE'"));
                                if($existencia_alumno['coincidencia'] == 0 AND $grado_alumno_inscribir >= $GRADO_CLASE){
                                    ?>
                                    <html>
                                        <option value="<?php echo $id_alumno_inscribir?>"><?php echo "$nombre_alumno_inscribir $apellidos_alumno_inscribir, $cinturon $grado";?></option>
                                    </html>
                                    <?php
                                }
                            }
                            ?>
                            <html>
                            </select>

                            <input type="submit" value="Inscribir">
                        </form>
                        </html>
                    <?php
                }
            }
            ?>
            <html>
                
            </div>
        <div class="informacion_clase">
            <h2>Informacion de la clase: </h2>
            <p>
                Profesor: 
                <?php 
                    if($ID == $ID_PROFESOR){
                        echo "$NOMBRE_PROFESOR $APELLIDOS_PROFESOR";
                    }else{
                        echo "<a href='../../perfil_ajeno/profesor/perfil_profesor.php?id_profesor=$ID_PROFESOR'>$NOMBRE_PROFESOR $APELLIDOS_PROFESOR</a>";
                    }
                
                ?>
                <br>
                Cupos: <?php echo $CUPOS_CLASE;?>
                <br>
                Cupos disponibles: <?php echo $cupos_clase?>
                <br>
                Cinturon: <?php echo $cinturon_clase;?>
                <br>
                Grado: <?php echo $grado_clase;?>
                <br>
                Estado: <?php echo $visibilidad_clase;?>
            </p>
        </div>
    </div>


    <div class= "inscritos">
        <h2>Inscritos: </h2>

        <div class ="inscritos2">
        </html>
        <?php
            $alumnos_inscritos = mysqli_query($connect, "SELECT * FROM Inscripcion WHERE ID_clase = '$ID_CLASE'");

            while($tabla = $alumnos_inscritos->fetch_assoc()){
                $ID_ALUMNO_INSCRIPCION = $tabla['ID_alumno'];
                $ASISTENCIA_INSCRIPCION = $tabla['Asistencia'];

                $consulta_alumno = mysqli_query($connect, "SELECT * FROM Alumno WHERE ID_alumno = '$ID_ALUMNO_INSCRIPCION'");
                if($consulta_alumno){
                    while($tabla_alumno = $consulta_alumno->fetch_array()){
                        $ID_ALUMNO = $tabla_alumno['ID_alumno'];
                        $NOMBRE_ALUMNO = $tabla_alumno['Nombre'];
                        $APELLIDOS_ALUMNO =  $tabla_alumno['Apellidos'];
                        $CINTURON_ALUMNO = $tabla_alumno['Cinturon'];
                        $GRADO_ALUMNO = $tabla_alumno['Grado'];

                        switch($CINTURON_ALUMNO){
                            case 1:
                                $cinturon_alumno = "Ninguno";
                                break;
                            case 2:
                                $cinturon_alumno = "Blanco";
                                break;
                            case 3:
                                $cinturon_alumno = "Azul";
                                break;
                            case 4:
                                $cinturon_alumno = "Morado";
                                break;
                            case 5:
                                $cinturon_alumno = "Marron";
                                break;
                            case 6: 
                                $cinturon_alumno = "Negro";
                                break;
                            default:
                                break;
                        }
                        
                        switch($GRADO_ALUMNO){
                            case 1:
                                $grado_alumno = "Ninguno";
                                break;
                            case 2:
                                $grado_alumno = "I";
                                break;
                            case 3:
                                $grado_alumno = "II";
                                break;
                            case 4:
                                $grado_alumno = "III";
                                break;
                            case 5: 
                                $grado_alumno = "IV";
                                break;
                            default:
                                break;
                        }
        ?>
        <html>   
            <div class ="inscritoss">
                <p>
                    </html>
                    <?php 
                        if($ID == $ID_PROFESOR_CLASE){
                            echo "<a href='perfil_alumnos/vista_perfil_alumno.php?id_alumno=$ID_ALUMNO'>$NOMBRE_ALUMNO $APELLIDOS_ALUMNO</a>";
                        }else{
                            echo "$NOMBRE_ALUMNO $APELLIDOS_ALUMNO";
                        }
                
                        if($CINTURON_ALUMNO == 1 AND $GRADO_ALUMNO == 1){
                            echo " | ";
                        }else{
                            echo " | ";
                            echo " $cinturon_alumno $grado_alumno ";
                            
                        }

                        if($ID == $ID_PROFESOR_CLASE){
                            echo " | ";
                        ?>
                        <html>
                            <a class="bold" href="php/configuracion_inscripciones.php?id_alumno=<?php echo $ID_ALUMNO;?>&id_clase=<?php echo $ID_CLASE;?>&opcion=2">Desinscribir</a>
                        </html>
                        <?php
                        }
                        if($ID == $ID_PROFESOR_CLASE){
                            if(isset($ASISTENCIA_INSCRIPCION)){
                                switch($ASISTENCIA_INSCRIPCION){
                                    case 0:
                                        ?>
                                        <html>
                                            <form action="php/asistencia.php?id_alumno=<?php echo $ID_ALUMNO;?>&id_clase=<?php echo $ID_CLASE;?>&caso=2" method="post">
                                                <label for="si">Asistió</label>
                                                <input type="radio" name="asistencia" id="asistencia" value="1" required>
    
                                                <label for="no">No Asistió</label>
                                                <input type="radio" name="asistencia" id="asistencia" value="0" checked>

                                                <input type="submit" value="Actualizar">
                                            </form>
                                        </html>
                                        <?php
                                        break;
                                    case 1:
                                        ?>
                                        <html>
                                            <form action="php/asistencia.php?id_alumno=<?php echo $ID_ALUMNO;?>&id_clase=<?php echo $ID_CLASE;?>&caso=1" method="post">
                                                <label for="si">Asistió</label>
                                                <input type="radio" name="asistencia" id="asistencia" value="1" checked required>
    
                                                <label for="no">No Asistió</label>
                                                <input type="radio" name="asistencia" id="asistencia" value="0">

                                                <input type="submit" value="Actualizar">
                                            </form>
                                        </html>
                                        <?php
                                        break;
                                    default:
                                        break;
                                }
                            }else{
                                ?>
                                <html>
                                    <form action="php/asistencia.php?id_alumno=<?php echo $ID_ALUMNO;?>&id_clase=<?php echo $ID_CLASE;?>&caso=0" method="post">
                                        <label for="si">Asistió</label>
                                        <input type="radio" name="asistencia" id="asistencia" value="1" required>

                                        <label for="no">No Asistió</label>
                                        <input type="radio" name="asistencia" id="asistencia" value="0">

                                        <input type="submit" value="Actualizar">
                                    </form>
                                </html>
                                <?php
                            }      
                        }
                    ?>
                    <html>        
                </p>
            </div>
        </html>
        <?php
                    }
                }    
            }
        ?>
        <html>
        </div>
    </div> 
    <script>
        document.getElementById("confirmacion_container").style.display = "none";

        document.getElementById("boton_eliminar").onclick = mostrarDiv;
        document.getElementById("boton_eliminar_cancelacion").onclick = MostrarDiv_eliminar;
        
        function mostrarDiv(){
            document.getElementById("eliminar_container").style.display = "none";
            document.getElementById("confirmacion_container").style.display = "block";
        }

        function MostrarDiv_eliminar(){
            document.getElementById("eliminar_container").style.display = "block";
            document.getElementById("confirmacion_container").style.display = "none";
        }

    </script> 
</body>
</html>

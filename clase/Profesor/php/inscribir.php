<?php
include("../../../includes/connection.php");
session_start();

$ID = $_SESSION['ID_profesor'];
$ACTIVE = $_SESSION['active'];

if(!isset($ID) OR $ACTIVE == 0){
    session_destroy();
    header("location: ../../../index.html");
    exit();
}

$ID_CLASE = mysqli_real_escape_string($connect, $_GET['id_clase']);
$ID_ALUMNO = mysqli_real_escape_string($connect, $_POST['alumno']);

$informacion_clase = mysqli_query($connect, "SELECT * FROM Clase WHERE ID_clase = '$ID_CLASE'");
while($tabla_clase = $informacion_clase->fetch_array()){
    $CUPOS_CLASE = $tabla_clase['Cupos'];
}

$contador_membresia = mysqli_fetch_array(mysqli_query($connect, "SELECT COUNT(*) AS contador_membresia FROM Membresia WHERE ID_alumno = '$ID_ALUMNO'"));

if($contador_membresia['contador_membresia'] >= 1){
    $MEMBRESIA = 1;

    $consulta_membresia = mysqli_query($connect, "SELECT * FROM Membresia WHERE ID_alumno = '$ID_ALUMNO'");
    while($tabla_membresia = $consulta_membresia->fetch_array()){
        $PAGO = $tabla_membresia['Pago'];
    }
}else{
    $MEMBRESIA = 0;
}

$consulta_cupos = mysqli_query($connect, "SELECT COUNT(*) AS contador_cupos FROM Inscripcion WHERE ID_clase='$ID_CLASE'");
$contador_cupos = mysqli_fetch_array($consulta_cupos);

if($contador_cupos['contador_cupos'] >= 0){
    $cupos_clase = $CUPOS_CLASE - $contador_cupos['contador_cupos'];
}

if($MEMBRESIA == 1){
    if($cupos_clase == 0){
        echo '<script type="text/javascript"> alert("Ya se llenaron todos los cupos."); location="../vista_clase_profesor.php?id_clase='.$ID_CLASE.'"; </script>';
    }else if($PAGO == 1){
        $inscribir_alumno = mysqli_query($connect, "INSERT INTO Inscripcion (ID_clase, ID_alumno) VALUES ('$ID_CLASE','$ID_ALUMNO')");
        if($inscribir_alumno){
            header("location: ../vista_clase_profesor.php?id_clase=".$ID_CLASE);
        }else{
            header("location: ../vista_clase_profesor.php?id_clase=".$ID_CLASE);
        }
    }else{
        echo '<script type="text/javascript"> alert("El alumno no ha pagado la membresia"); location="../vista_clase_profesor.php?id_clase='.$ID_CLASE.'"; </script>';
    }
}else{
    echo '<script type="text/javascript"> alert("El alumno no tiene membresia"); location="../vista_clase_profesor.php?id_clase='.$ID_CLASE.'"; </script>';
}



mysqli_close($connect);
?>
<?php
    include("../../../includes/connection.php");
    
    //Datos del profesor:
    session_start();

    $ID = $_SESSION['ID_profesor'];
    $ACTIVE = $_SESSION['active'];

    if(!isset($ID) OR $ACTIVE == 0){
        session_destroy();
        header("location: ../../../index.html");
        exit();
    }

    //Pedir datos por metodo POST y GET

    $id = mysqli_real_escape_string($connect, $_GET['id_clase']);
    $ELIMINAR_CLASE = mysqli_real_escape_string($connect, $_GET['eliminar_clase']);

    $cupos = mysqli_real_escape_string($connect, $_POST['Cupos']);
    $cinturon = mysqli_real_escape_string($connect, $_POST['Cinturon']);
    $grado = mysqli_real_escape_string($connect, $_POST['Grado']);
    $dia = mysqli_real_escape_string($connect, $_POST['Dia']);
    $hora = mysqli_real_escape_string($connect, $_POST['Hora']);
    $visibilidad = mysqli_real_escape_string($connect, $_POST['Visibilidad']);

    $informacion_clase = "SELECT * FROM Clase WHERE ID_clase = '$id'";
    $peticion_informacion_clase = mysqli_query($connect, $informacion_clase);

    if($peticion_informacion_clase){
        while($tabla = $peticion_informacion_clase->fetch_array()){
            $ID_PROFESOR_CLASE = $tabla['ID_profesor'];
        }
    }
    if($ELIMINAR_CLASE == 0){
        if($ID_PROFESOR_CLASE == $ID){
            $actualizar_clase = "UPDATE Clase SET Cupos='$cupos', Cinturon='$cinturon', Grado='$grado', Dia='$dia', Hora='$hora', Visibilidad='$visibilidad' WHERE ID_clase='$id'";
            
            if ($connect->query($actualizar_clase) === TRUE) {
                header('Location: ../vista_clase_profesor.php?id_clase='.$id);
            } else {
                header("Location: ../../../profesor/vista_clase.php");
            }
        }else{
            header("Location: ../../../general/general_php/logout.php");
        }
    }else{
        if($ID_PROFESOR_CLASE == $ID){
            $eliminar_clase = "DELETE FROM Clase WHERE ID_clase='$id'";
            
            if ($connect->query($eliminar_clase) === TRUE) {
                header("Location: ../../../profesor/vista_clase.php");
            } else {
                header("Location: ../../../profesor/profesor_php/home_profesor.php");
            }
        }else{
            header("Location: ../../../general/general_php/logout.php");
        }
    }

    mysqli_close($connect);
?>
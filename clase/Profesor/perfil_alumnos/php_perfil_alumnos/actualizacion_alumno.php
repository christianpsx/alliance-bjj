<?php
//Conexion a la base de datos
include("../../../../includes/connection.php");

//Recepcion de la id de la clase
$ID_ALUMNO = mysqli_real_escape_string($connect, $_REQUEST['id_alumno']);

//Se comienza la sesion para capturar los datos del profesor
session_start();
$ID = $_SESSION['ID_profesor'];
$ACTIVE = $_SESSION['active'];

//Validacion de que existe una sesion
if(!isset($ID) OR $ACTIVE == 0){
    session_destroy();
    header("location: ../../../../index.html");
    exit();
}

$FECHA_GRADO = mysqli_real_escape_string($connect, $_POST['fecha_grado']);

if($FECHA_GRADO == NULL){
    $CINTURON = mysqli_real_escape_string($connect, $_POST['cinturon']);
    $GRADO = mysqli_real_escape_string($connect, $_POST['grado']);

    $OBSERVACIONES = mysqli_real_escape_string($connect, $_POST['observaciones']);
    $CANTIDAD_CLASES = mysqli_real_escape_string($connect, $_POST['cantidad_clases']);
    $BURPEES = mysqli_real_escape_string($connect, $_POST['burpees']);

    $actualizar_alumno = "UPDATE Alumno SET Cinturon = '$CINTURON', Grado = '$GRADO', Observaciones = '$OBSERVACIONES', Cantidad_clases = '$CANTIDAD_CLASES', Burpees = '$BURPEES' WHERE ID_alumno = '$ID_ALUMNO'";
    if ($connect->query($actualizar_alumno) == TRUE){
        header('Location: ../vista_perfil_alumno.php?id_alumno='.$ID_ALUMNO);
    }else{
        header('Location: ../actualizacion_grado_alumno.php?id_alumno='.$ID_ALUMNO);
    }
}else{
    $CINTURON = mysqli_real_escape_string($connect, $_POST['cinturon']);
    $GRADO = mysqli_real_escape_string($connect, $_POST['grado']);

    $OBSERVACIONES = mysqli_real_escape_string($connect, $_POST['observaciones']);
    $CANTIDAD_CLASES = mysqli_real_escape_string($connect, $_POST['cantidad_clases']);
    $BURPEES = mysqli_real_escape_string($connect, $_POST['burpees']);

    $actualizar_alumno = "UPDATE Alumno SET Cinturon = '$CINTURON', Grado = '$GRADO', Observaciones = '$OBSERVACIONES', Fecha_grado = '$FECHA_GRADO', Cantidad_clases = '$CANTIDAD_CLASES', Burpees = '$BURPEES' WHERE ID_alumno = '$ID_ALUMNO'";
    if ($connect->query($actualizar_alumno) == TRUE){
        header('Location: ../vista_perfil_alumno.php?id_alumno='.$ID_ALUMNO);
    }else{
        header('Location: ../actualizacion_grado_alumno.php?id_alumno='.$ID_ALUMNO);
    }
}

mysqli_close($connect);
?>
<?php
//Conexion a la base de datos
include("../../../includes/connection.php");

//Recepcion de la id de la clase
$ID_ALUMNO = mysqli_real_escape_string($connect, $_REQUEST['id_alumno']);

//Se comienza la sesion para capturar los datos del profesor
session_start();

$NOMBRE = $_SESSION['Nombre'];
$APELLIDOS = $_SESSION['Apellidos'];

$ID = $_SESSION['ID_profesor'];
$ACTIVE = $_SESSION['active'];

//Validacion de que existe una sesion
if(!isset($ID) OR $ACTIVE == 0){
    session_destroy();
    header("location: ../../../index.html");
    exit();
}

$alumnos = "SELECT * FROM Alumno WHERE ID_alumno='$ID_ALUMNO'";
$peticion_alumnos = mysqli_query($connect, $alumnos);

while($tabla = $peticion_alumnos->fetch_assoc()){
    $NOMBRE_ALUMNO = $tabla['Nombre'];
    $APELLIDO_ALUMNO = $tabla['Apellidos'];

    $CINTURON_ALUMNO = $tabla['Cinturon'];
    $GRADO_ALUMNO = $tabla['Grado'];
    $FECHA_GRADO_ALUMNO = $tabla['Fecha_grado'];

    $OBSERVACIONES_ALUMNO = $tabla['Observaciones'];
    $CANTIDAD_CLASES = $tabla['Cantidad_clases'];
    $BURPEES = $tabla['Burpees'];

    switch($CINTURON_ALUMNO){
        case 1:
            $cinturon = "Ninguno";
            break;
        case 2:
            $cinturon = "Blanco";
            break;
        case 3:
            $cinturon = "Azul";
            break;
        case 4:
            $cinturon = "Morado";
            break;
        case 5:
            $cinturon = "Cafe";
            break;
        case 6: 
            $cinturon = "Negro";
            break;
        default:
            break;
    }
    switch($GRADO_ALUMNO){
        case 1:
            $grado = "Ninguno";
            break;
        case 2:
            $grado = "I";
            break;
        case 3:
            $grado = "II";
            break;
        case 4:
            $grado = "III";
            break;
        case 5: 
            $grado = "IV";
            break;
        default:
            break;
    }
}
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../css/grado.css">
    <title>Configurar perfil de <?php echo "$NOMBRE_ALUMNO $APELLIDO_ALUMNO";?></title>
</head>
<body>

<div class="editar_perfil">
        <div class="centro">
            <h2>Editar perfil de <?php echo "$NOMBRE_ALUMNO $APELLIDO_ALUMNO";?></h2>
            
            <form action="php_perfil_alumnos/actualizacion_alumno.php?id_alumno=<?php echo $ID_ALUMNO;?>" method="post">
                <div class="fecha_grado">
                    <label for="fecha_grado"><b>Fecha de grado</b></label>
                    <input class="controls" type="date" name="fecha_grado" id="fecha_grado" value="<?php echo $FECHA_GRADO_ALUMNO;?>">
                </div>

                <div class="cinturon">
                    <label for="cinturon"><b>Cinturon</b></label>
                    <select class="select" name="cinturon" id="cinturon">
                        <option value="<?php echo $CINTURON_ALUMNO;?>" selected hidden><?php echo $cinturon;?></option>
                        <option value="1">Ninguno</option>
                        <option value="2">Blanco</option>
                        <option value="3">Azul</option>
                        <option value="4">Morado</option>
                        <option value="5">Cafe</option>
                        <option value="6">Negro</option>
                    </select>
                </div>
                <div class="grado">
                    <label for="grado"><b>Grado</b></label>
                    <select class="select" name="grado" id="grado" >
                        <option value="<?php echo $GRADO_ALUMNO;?>" selected hidden><?php echo $grado;?></option>
                        <option value="1">Ninguno</option>
                        <option value="2">I</option>
                        <option value="3">II</option>
                        <option value="4">III</option>
                        <option value="5">IV</option>
                    </select>
                </div>     
                <div class="observaciones">
                    <label for="observaciones"><b>Observaciones</b></label>
                    <br>
                    <textarea name="observaciones" id="observaciones" cols="30" rows="7" class="controls" minlength="1"  placeholder="Observaciones del alumno"><?php echo $OBSERVACIONES_ALUMNO;?></textarea>
                </div>
                <div>
                    <label for="cantidad_clases"><b>Numero de clases asistidas</b></label>
                    <input type="number" name="cantidad_clases" id="cantidad_clases" value="<?php echo $CANTIDAD_CLASES;?>" min="0">
                </div>
                <div>
                    <label for="burpees"><b>Cantidad de Burpees a dar</b></label>
                    <input type="number" name="burpees" id="burpees" value="<?php echo $BURPEES?>" min="0">
                </div>
                <br>
                <div class="enviar">
                    <input type="submit" value="Actualizar perfil" name="actualizar" class="buttons">  
                </div>   
                <button><a href="vista_perfil_alumno.php?id_alumno=<?php echo $ID_ALUMNO;?>">Volver</a></button>       
            </form>
        </div>
    </div>
</body>
</html>
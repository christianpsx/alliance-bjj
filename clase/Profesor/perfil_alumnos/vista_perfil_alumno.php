<?php
//Conexion a la base de datos
include("../../../includes/connection.php");

//Recepcion de la id de la clase
$ID_ALUMNO = mysqli_real_escape_string($connect, $_REQUEST['id_alumno']);

//Se comienza la sesion para capturar los datos del profesor
session_start();

$NOMBRE = $_SESSION['Nombre'];
$APELLIDOS = $_SESSION['Apellidos'];

$ID = $_SESSION['ID_profesor'];
$ACTIVE = $_SESSION['active'];

$sql = "SELECT Imagen FROM Alumno WHERE ID_alumno = '$ID_ALUMNO' AND Imagen != 'NULL'";
$result = $connect->query($sql);

//Validacion de que existe una sesion
if(!isset($ID) OR $ACTIVE == 0){
    session_destroy();
    header("location: ../../../index.html");
    exit();
}

$alumnos = "SELECT * FROM Alumno WHERE ID_alumno='$ID_ALUMNO'";
$peticion_alumnos = mysqli_query($connect, $alumnos);

while($tabla = $peticion_alumnos->fetch_assoc()){
    $NOMBRE_ALUMNO = $tabla['Nombre'];
    $APELLIDO_ALUMNO = $tabla['Apellidos'];

    $RUT_ALUMNO = $tabla['Rut'];
    $FECHA_NACIMIENTO_ALUMNO = $tabla['Fecha_nacimiento'];

    $TELEFONO_ALUMNO = $tabla['Telefono'];
    $DIRECCION_ALUMNO = $tabla['Direccion'];
    $MAIL_ALUMNO = $tabla['Mail'];

    $CINTURON_ALUMNO = $tabla['Cinturon'];
    $GRADO_ALUMNO = $tabla['Grado'];
    $FECHA_GRADO_ALUMNO = $tabla['Fecha_grado'];

    $CANTIDAD_CLASES = $tabla['Cantidad_clases'];
    $BURPEES = $tabla['Burpees'];

    $OBSERVACIONES_ALUMNO = $tabla['Observaciones'];
    $EXPERIENCIA_ALUMNO = $tabla['Experiencia'];

    $Imagen = $tabla['Imagen'];

    switch($CINTURON_ALUMNO){
        case 1:
            $cinturon = "Ninguno";
            break;
        case 2:
            $cinturon = "Blanco";
            break;
        case 3:
            $cinturon = "Azul";
            break;
        case 4:
            $cinturon = "Morado";
            break;
        case 5:
            $cinturon = "Cafe";
            break;
        case 6: 
            $cinturon = "Negro";
            break;
        default:
            break;
    }
    switch($GRADO_ALUMNO){
        case 1:
            $grado = "Ninguno";
            break;
        case 2:
            $grado = "I";
            break;
        case 3:
            $grado = "II";
            break;
        case 4:
            $grado = "III";
            break;
        case 5: 
            $grado = "IV";
            break;
        default:
            break;
    }
}
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="../css/alumno.css">
    <title>Perfil de <?php echo "$NOMBRE_ALUMNO $APELLIDO_ALUMNO";?></title>
</head>
<body>
    <ul>
        <li class="log"> <?php echo "<p>Bienvenido $NOMBRE $APELLIDOS</p>";?> </li>
        <li><a href="../../../profesor/profesor_php/home_profesor.php">Inicio</a></li>
        <li><a href="../../../profesor/perfil_profesor.php">Perfil</a></li>
        <li><a href="../../../profesor/horario_profesor.php">Horario</a></li>
        <li class="active"><a href="../../../profesor/vista_clase.php">Clases</a></li>
        <li><a href="../../../general/general_php/logout.php">Cerrar sesion</a></li>
    </ul>
    <div class="perfil">
    <h1>Perfil de <?php echo "$NOMBRE_ALUMNO $APELLIDO_ALUMNO";?></h1>

    <div class="container-user-info">
    <div class="contenedor_imagen">
        <?php if($result->num_rows > 0){ ?>
            <img class="imagen" src="data:image/jpg;charset=utf8;base64,<?php echo base64_encode($Imagen); ?>" /> 
        <?php }else{ ?>
            <img src="https://i.ibb.co/3Tyr88g/no-image.png" alt="Foto perfil d" class="imagen">
        <?php } ?>
    </div>
    </div>

    <div class="user-nombre">
        <p>
            Nombre: <?php echo "$NOMBRE_ALUMNO $APELLIDO_ALUMNO";?>
        </p>
        <p>
            RUT: <?php echo "$RUT_ALUMNO";?>
            <br>
            Fecha de nacimiento: <?php echo "$FECHA_NACIMIENTO_ALUMNO";?>
        </p>
    </div>

    <div class="user-nombre">
        <p>
            Cinturon: <?php echo $cinturon;?>
            <br>
            Grado: <?php echo $grado;?>
            <br>
            Fecha de grado: <?php if($FECHA_GRADO_ALUMNO == NULL){ echo "No hay fecha";}else{echo $FECHA_GRADO_ALUMNO;}?>
        </p>
        <p>
            Telefono: <?php echo $TELEFONO_ALUMNO;?>
            <br>
            Direccion: <?php echo $DIRECCION_ALUMNO;?>
            <br>
            Correo electronico: <?php echo $MAIL_ALUMNO;?>
        </p>
    </div>

    <div class="user-nombre">
        <p>
            Experiencia:
            <?php echo $EXPERIENCIA_ALUMNO;?>
            <br>
            Observaciones:
            <?php if($OBSERVACIONES_ALUMNO == NULL){ echo "No hay observaciones";}else{echo $OBSERVACIONES_ALUMNO;}?>
            <br>
            Cantidad de clases asistidas: 
            <?php echo $CANTIDAD_CLASES;?>
            <br>
            Cantidad de Burpees:
            <?php echo $BURPEES;?>
        </p>
    </div>

    <div class="configuracion_alumno">
        <button><a href="actualizacion_grado_alumno.php?id_alumno=<?php echo "$ID_ALUMNO";?>">Configuracion</a></button>
    </div>
        </div> 
    
</body>
</html>
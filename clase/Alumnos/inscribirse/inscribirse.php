<?php
//Conexion a la base de datos
include("../../../includes/connection.php");

//Recepcion mediante metodo get
$ID_CLASE = mysqli_real_escape_string($connect, $_GET['id_clase']);
$ID =  mysqli_real_escape_string($connect, $_GET['id']);

$OPCION = mysqli_real_escape_string($connect, $_GET['opcion']);

//Consultar datos del alumno

$informacion_alumno = mysqli_query($connect, "SELECT * FROM Alumno WHERE ID_alumno = '$ID'");
while($tabla_alumno = $informacion_alumno->fetch_array()){
    $BURPEES = $tabla_alumno['Burpees'];
    $CINTURON_ALUMNO = $tabla_alumno['Cinturon'];
    $GRADO_ALUMNO = $tabla_alumno['Grado']; 
}

//Consultar datos de la clase

$informacion_clase = mysqli_query($connect, "SELECT * FROM Clase WHERE ID_clase = '$ID_CLASE'");
while($tabla_clase = $informacion_clase->fetch_array()){
    $CINTURON_CLASE = $tabla_clase['Cinturon'];
    $GRADO_CLASE = $tabla_clase['Grado'];
    $CUPOS_CLASE = $tabla_clase['Cupos'];
}

//Consultar si el alumno tiene membresia

$informacion_membresia = mysqli_query($connect, "SELECT COUNT(*) AS contador_membresia FROM Membresia WHERE ID_alumno = '$ID'");
$contador_membresia = mysqli_fetch_array($informacion_membresia);

if($contador_membresia['contador_membresia'] >= 1){
    $MEMBRESIA = 1;

    $consulta_membresia = mysqli_query($connect, "SELECT * FROM Membresia WHERE ID_alumno = '$ID'");
    while($tabla_membresia = $consulta_membresia->fetch_array()){
        $PAGO = $tabla_membresia['Pago'];
    }
}else{
    $MEMBRESIA = 0;
}

//Actualizacion automatica de cupos


$consulta_cupos = mysqli_query($connect, "SELECT COUNT(*) AS contador_cupos FROM Inscripcion WHERE ID_clase='$ID_CLASE'");
$contador_cupos = mysqli_fetch_array($consulta_cupos);

if($contador_cupos['contador_cupos'] >= 0){
    $cupos_clase = $CUPOS_CLASE - $contador_cupos['contador_cupos'];
}


switch($OPCION){
    case 0:
        //Eliminar inscripcion
        //Aqui
        echo '<script type="text/javascript">
        function eliminar ()
        {
            var statusConfirm = confirm("¿Realmente desea eliminar esto?");
            if (statusConfirm == true)
            {
                alert ("Eliminas");
            }
            else
            {
                alert("Haces otra cosa");
            }
        }
        </script>';
        $eliminar_inscripcion = mysqli_query($connect, "DELETE FROM Inscripcion WHERE ID_clase='$ID_CLASE' AND ID_alumno='$ID'");
        if($eliminar_inscripcion){
            $BURPEES += 50;

            $actualizar_alumno = mysqli_query($connect, "UPDATE Alumno SET Burpees = '$BURPEES' WHERE ID_alumno = '$ID'");
            if($actualizar_alumno){
                header("Location: ../vista_clase_alumno.php?id_clase=$ID_CLASE");
            }
            header("Location: ../vista_clase_alumno.php?id_clase=$ID_CLASE");
        }

        break;
    case 1:
        //Crear inscripcion

        if($MEMBRESIA == 1){
            if($cupos_clase == 0){
                echo '<script type="text/javascript"> alert("Ya se llenaron todos los cupos."); location="../vista_clase_alumno.php?id_clase='.$ID_CLASE.'"; </script>';
            }else if($PAGO == 1){
                if($CINTURON_ALUMNO == $CINTURON_CLASE){
                    if($GRADO_ALUMNO >= $GRADO_CLASE){
                        $insertar_inscripcion = mysqli_query($connect, "INSERT INTO Inscripcion (ID_clase, ID_alumno) VALUES ('$ID_CLASE','$ID')");
                        if($insertar_inscripcion){
                            header("Location: ../vista_clase_alumno.php?id_clase=$ID_CLASE");
                        }else{
                            header("Location: ../vista_clase_alumno.php?id_clase=$ID_CLASE");
                        }
                    }else{
                        echo '<script type="text/javascript"> alert("Usted no cumple los requisitos para tomar esta clase."); location="../../../usuario/horario.php"; </script>';
                    }
                }else if($CINTURON_ALUMNO > $CINTURON_CLASE){
                    $insertar_inscripcion = mysqli_query($connect, "INSERT INTO Inscripcion (ID_clase, ID_alumno) VALUES ('$ID_CLASE','$ID')");
                    if($insertar_inscripcion){
                        header("Location: ../vista_clase_alumno.php?id_clase=$ID_CLASE");
                    }else{
                        header("Location: ../vista_clase_alumno.php?id_clase=$ID_CLASE");
                    }
                }else{
                    echo '<script type="text/javascript"> alert("Usted no cumple los requisitos para tomar esta clase."); location="../../../usuario/horario.php"; </script>';
                }
            }else{
                echo '<script type="text/javascript"> alert("Usted no ha pagado la membresia"); location="../../../usuario/horario.php"; </script>';
            }
        }else{
            echo '<script type="text/javascript"> alert("Usted no tiene la membresia"); location="../../../usuario/horario.php"; </script>';
        }

        break;
    default:
        header("Location: ../vista_clase_alumno.php?id_clase=$ID_CLASE");
        break;
}

mysqli_close($connect);
?>
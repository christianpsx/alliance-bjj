<script> 
          //Cuadro de diálogo de confirmación en JavaScript
          function confirmarAccesoURL()
          {
	    return confirm("¿Está seguro que desea acceder a la web ProyectoA?");
          }
	</script>
<?php
//Conexion a la base de datos
include("../../includes/connection.php");
include("../../includes/funciones.php");

//Recepcion de la id de la clase
$ID_CLASE = mysqli_real_escape_string($connect, $_REQUEST['id_clase']);

//Se comienza la sesion para capturar los datos del profesor
session_start();

$ID = $_SESSION['ID_alumno'];

$NOMBRE = $_SESSION['Nombre'];
$APELLIDOS = $_SESSION['Apellidos'];

$RUT = $_SESSION['Rut'];
$FECHA_NACIMIENTO = $_SESSION['Fecha_nacimiento'];

$MAIL = $_SESSION['Mail'];
$TELEFONO = $_SESSION['Telefono'];
$DIRECCION = $_SESSION['Direccion'];

$CINTURON = $_SESSION['Cinturon'];
$GRADO = $_SESSION['Grado'];
$CANTIDAD_CLASES = $_SESSION['Cantidad_clases'];
$FECHA_GRADO = $_SESSION['Fecha_grado'];

$OBSERVACIONES = $_SESSION['Observaciones'];
$EXPERIENCIA = $_SESSION['Experiencia'];

$PROMO_OFERTAS = $_SESSION['Promo_ofertas'];

$ACTIVE = $_SESSION['active'];

//Validacion de que existe una sesion
if(!isset($ID) AND $ACTIVE == 0){
    session_destroy();
    header("location: ../../index.html");
    exit();
}

//Consultar datos de la clase asociada a la ID obtenida

$informacion_clase = "SELECT * FROM Clase WHERE ID_clase = '$ID_CLASE'";
$peticion_informacion_clase = mysqli_query($connect, $informacion_clase);

if($peticion_informacion_clase){
    while($tabla = $peticion_informacion_clase->fetch_array()){
        $TIPO_CLASE = $tabla['Tipo_clase'];
        $ID_PROFESOR_CLASE = $tabla['ID_profesor'];
        $CUPOS_CLASE = $tabla['Cupos'];
        $CINTURON_CLASE = $tabla['Cinturon'];
        $GRADO_CLASE = $tabla['Grado'];
        $DIA_CLASE = $tabla['Dia'];
        $HORA_CLASE = $tabla['Hora'];
    }
}

//Consultar datos del profesor encargado de la clase

$informacion_profesor = "SELECT * FROM Profesor WHERE ID_profesor = '$ID_PROFESOR_CLASE'";
$peticion_informacion_profesor = mysqli_query($connect, $informacion_profesor);

if($peticion_informacion_profesor){
    while($row = $peticion_informacion_profesor->fetch_array()){
        $ID_PROFESOR = $row['ID_profesor'];
        $NOMBRE_PROFESOR = $row['Nombre'];
        $APELLIDOS_PROFESOR = $row['Apellidos'];
    }
}

//Actualizacion automatica de cupos

$consulta_cupos = "SELECT COUNT(*) AS contador_cupos FROM Inscripcion WHERE ID_clase='$ID_CLASE'";
$peticion_consulta_cupos = mysqli_query($connect, $consulta_cupos);
$contador_cupos = mysqli_fetch_array($peticion_consulta_cupos);

if($contador_cupos['contador_cupos'] >= 0){
    $cupos_clase = $CUPOS_CLASE - $contador_cupos['contador_cupos'];
}

//Los datos de tipo enum se convertiran en su string asociado

switch($CINTURON_CLASE){
    case 1:
        $cinturon_clase = "Ninguno";
        break;
    case 2:
        $cinturon_clase = "Blanco";
        break;
    case 3:
        $cinturon_clase = "Azul";
        break;
    case 4:
        $cinturon_clase = "Morado";
        break;
    case 5:
        $cinturon_clase = "Marron";
        break;
    case 6: 
        $cinturon_clase = "Negro";
        break;
    default:
        break;
}
switch($GRADO_CLASE){
    case 1:
        $grado_clase = "Ninguno";
        break;
    case 2:
        $grado_clase = "I";
        break;
    case 3:
        $grado_clase = "II";
        break;
    case 4:
        $grado_clase = "III";
        break;
    case 5: 
        $grado_clase = "IV";
        break;
    default:
        break;
}
switch($DIA_CLASE){
    case 1:
        $dia_clase = "Lunes";
        break;
    case 2:
        $dia_clase = "Martes";
        break;
    case 3:
        $dia_clase = "Miércoles";
        break;
    case 4:
        $dia_clase = "Jueves";
        break;
    case 5:
        $dia_clase = "Viernes";
        break;
    case 6:
        $dia_clase = "Sabado";
        break;
    case 7:
        $dia_clase = "Domingo";
        break;
    default:
        break;
}

?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" type="text/css" href="css/vista_clase.css">

    <title>Clase <?php echo switchClases($TIPO_CLASE);?></title>
    <ul>
        <li class="log"> <?php echo "<p>Bienvenido $NOMBRE $APELLIDOS</p>";?> </li>
        <li><a href="../../usuario/usuario_php/home.php">Inicio</a></li>
        <li><a href="../../usuario/perfil.php">Perfil</a></li>
        <li class="active"><a href="../../usuario/horario.php">Horario</a></li>
        <li><a href="../../usuario/planes.php">Planes</a></li>
        <li><a href="../../general/general_php/logout.php">Cerrar sesion</a></li>
    </ul>

</head>
<body>
    <div class ="head">
        <h1><?php echo switchClases($TIPO_CLASE)." $dia_clase "; ?><?php echo date('H:i', strtotime($HORA_CLASE));?></h1>
    </div>
</html>
    <?php
        $verificacion_inscripcion = "SELECT COUNT(*) AS verificador_inscripcion FROM Inscripcion WHERE ID_clase = '$ID_CLASE' AND ID_alumno = '$ID'";
        $peticion_verificacion_inscripcion = mysqli_query($connect, $verificacion_inscripcion);
        $verificador_inscripcion = mysqli_fetch_array($peticion_verificacion_inscripcion);

        if($verificador_inscripcion['verificador_inscripcion'] == 1){
    ?>
<html>
    <div class = "centro">
        <div class="espacio">
            <p>
            Usted ya esta incrito
            </p>
            
            <div id="eliminar_container">
                <button id="boton_eliminar">Desinscribirse</button>
            </div>
        
            <div id="confirmacion_container">
                <label for="buttons">Al desinscribir se añadiran 50 burpees</label>
                <br>
                <br>
                <button  id="boton_eliminar"><a href="inscribirse/inscribirse.php?id_clase=<?php echo $ID_CLASE?>&id=<?php echo $ID?>&opcion=0">Desinscribirse</a></button>
                <button  id="boton_eliminar_cancelacion" class="buttons">Cancelar</button>
            </div>
        </div>
        <div>
            <h2>Información de la clase: </h2>
            <p>
                Profesor: <a href="../../usuario/vista_profesores/vista_perfil_profesor.php?id_profesor=<?php echo $ID_PROFESOR;?>"><?php echo "$NOMBRE_PROFESOR $APELLIDOS_PROFESOR";?></a>
                <br>
                Cupos: <?php echo $CUPOS_CLASE;?>
                <br>
                Cupos disponibles: <?php echo $cupos_clase?>
                <br>
                Cinturon: <?php echo $cinturon_clase;?>
                <br>
                Grado: <?php echo $grado_clase;?>
            </p>
        </div>
    </div>
</html>
    <?php
        }else{
    ?>
<html>
    <div class = "centro">
        <div class= "espacio">
           </html>
           <?php
           if($TIPO_CLASE == 17){
               ?>
               <html>
                   <p>
                       Para inscribir esta clase debe confirmar el pago con el profesor.
                   </p>
               </html>
               <?php
           }else{
            ?>
            <html>
                <p>
                ¿Quiere inscribirse a esta clase?
                </p>
                <button>
                    <a href="inscribirse/inscribirse.php?id_clase=<?php echo $ID_CLASE?>&id=<?php echo $ID?>&opcion=1">Inscribirse</a>
                </button>
            </html>
            <?php
           }
           ?>
           <html>
        </div>
    
        <div>
            <h2>Información de la clase: </h2>
            <p>
                Profesor: <a href="../../usuario/vista_profesores/vista_perfil_profesor.php?id_profesor=<?php echo $ID_PROFESOR;?>"><?php echo "$NOMBRE_PROFESOR $APELLIDOS_PROFESOR";?></a>
                <br>
                Cupos: <?php echo $CUPOS_CLASE;?>
                <br>
                Cupos disponibles: <?php echo $cupos_clase?>
                <br>
                Cinturon: <?php echo $cinturon_clase;?>
                <br>
                Grado: <?php echo $grado_clase;?>
            </p>
        </div>
    </div>
</html>
    <?php
        }
    ?>
<html>
    <div class= "inscritos">
        <h2>Inscritos: </h2>
    <div class ="inscritos2">
</html>
        <?php
            $consulta_alumnos_inscritos = "SELECT * FROM Inscripcion WHERE ID_clase = '$ID_CLASE'";
            $peticion_consulta_alumnos_inscritos = mysqli_query($connect, $consulta_alumnos_inscritos);

            while($tabla = $peticion_consulta_alumnos_inscritos->fetch_assoc()){
                $ID_ALUMNO_INSCRIPCION = $tabla['ID_alumno'];
                $ASISTENCIA_INSCRIPCION = $tabla['Asistencia'];

                $consulta_alumno = "SELECT * FROM Alumno WHERE ID_alumno = '$ID_ALUMNO_INSCRIPCION'";
                $peticion_consulta_alumno = mysqli_query($connect, $consulta_alumno);

                if($peticion_consulta_alumno){
                    while($tabla_alumno = $peticion_consulta_alumno->fetch_array()){
                        $ID_ALUMNO = $tabla_alumno['ID_alumno'];
                        $NOMBRE_ALUMNO = $tabla_alumno['Nombre'];
                        $APELLIDOS_ALUMNO =  $tabla_alumno['Apellidos'];
                        $CINTURON_ALUMNO = $tabla_alumno['Cinturon'];
                        $GRADO_ALUMNO = $tabla_alumno['Grado'];

                        switch($CINTURON_ALUMNO){
                            case 1:
                                $cinturon_alumno = "Ninguno";
                                break;
                            case 2:
                                $cinturon_alumno = "Blanco";
                                break;
                            case 3:
                                $cinturon_alumno = "Azul";
                                break;
                            case 4:
                                $cinturon_alumno = "Morado";
                                break;
                            case 5:
                                $cinturon_alumno = "Marron";
                                break;
                            case 6: 
                                $cinturon_alumno = "Negro";
                                break;
                            default:
                                break;
                        }
                        
                        switch($GRADO_ALUMNO){
                            case 1:
                                $grado_alumno = "Ninguno";
                                break;
                            case 2:
                                $grado_alumno = "I";
                                break;
                            case 3:
                                $grado_alumno = "II";
                                break;
                            case 4:
                                $grado_alumno = "III";
                                break;
                            case 5: 
                                $grado_alumno = "IV";
                                break;
                            default:
                                break;
                        }
        ?>
<html>   
    <div class ="inscritoss">
        <p>
            <?php 
                if($ID == $ID_PROFESOR_CLASE){
                    echo "<a href='perfil_alumnos/vista_perfil_alumno.php?id_alumno=$ID_ALUMNO'>$NOMBRE_ALUMNO $APELLIDOS_ALUMNO</a>";
                }else{
                    echo "$NOMBRE_ALUMNO $APELLIDOS_ALUMNO";
                }
            ?>
            <?php
                if($CINTURON_ALUMNO == 1 AND $GRADO_ALUMNO == 1){
                }else{
                    echo "|";
                    echo " $cinturon_alumno $grado_alumno ";
                }
            
            
            ?>
        </p>
    </div>
</html>
        <?php
                    }
                }    
            }
        ?>
<html>
        </div>
    </div>

    <script>
        document.getElementById("confirmacion_container").style.display = "none";

        document.getElementById("boton_eliminar").onclick = mostrarDiv;
        document.getElementById("boton_eliminar_cancelacion").onclick = MostrarDiv_eliminar;
        
        function mostrarDiv(){
            document.getElementById("eliminar_container").style.display = "none";
            document.getElementById("confirmacion_container").style.display = "block";
        }

        function MostrarDiv_eliminar(){
            document.getElementById("eliminar_container").style.display = "block";
            document.getElementById("confirmacion_container").style.display = "none";
        }

    </script> 
</body>
</html>

<?php
function switchClases ($TIPO_CLASE){
    switch($TIPO_CLASE){
        case 1:
            $tipo_clase = "Gi Iniciantes";
            break;
        case 2:
            $tipo_clase = "NoGi Iniciantes";
            break;
        case 3:
            $tipo_clase = "Gi Principiantes";
            break;
        case 4:
            $tipo_clase = "Nogi Principiantes";
            break;
        case 5:
            $tipo_clase = "Gi Intermedio";
            break;
        case 6:
            $tipo_clase = "NoGi Intermedio";
            break;
        case 7:
            $tipo_clase = "Gi Avanzado";
            break;
        case 8:
            $tipo_clase = "NoGi Avanzado";
            break;
        case 9:
            $tipo_clase = "Gi Competidor";
            break;
        case 10:
            $tipo_clase = "NoGi Competidor";
            break;
        case 11:
            $tipo_clase = "Master +30";
            break;
        case 12:
            $tipo_clase = "Open Mat Gi";
            break;
        case 13:
            $tipo_clase = "Open Mat NoGi";
            break;
        case 14:
            $tipo_clase = "Entrenamiento Funcional";
            break;
        case 15:
            $tipo_clase = "Gi Mujeres";
            break;
        case 16:
            $tipo_clase = "No Gi Mujeres";
            break;
        case 17:
            $tipo_clase = "Clase Personalizada";
            break;
        case 18:
            $tipo_clase = "Gi Juvenil";
            break;
        case 19:
            $tipo_clase = "No Gi Juvenil";
            break;
        default:
            $tipo_clase = "salir";
            break;
    }

    return $tipo_clase;
}

function switchCinturon ($CINTURON){
    switch($CINTURON){
        case 1:
            $cinturon = "Ninguno";
            break;
        case 2:
            $cinturon = "Blanco";
            break;
        case 3:
            $cinturon = "Azul";
            break;
        case 4:
            $cinturon = "Morado";
            break;
        case 5:
            $cinturon = "Marrón";
            break;
        case 6: 
            $cinturon = "Negro";
            break;
        default:
            break;
    }

    return $cinturon;
}

function switchGrado ($GRADO){
    switch($GRADO){
        case 1:
            $grado = "Ninguno";
            break;
        case 2:
            $grado = "I";
            break;
        case 3:
            $grado = "II";
            break;
        case 4:
            $grado = "III";
            break;
        case 5: 
            $grado = "IV";
            break;
        default:
            break;
    }
    
    return $grado;
}

function estadoCuenta ($ESTADO){
    switch($ESTADO){
        case 0:
            $estado = "Cuenta desactivada";
            break;
        case 1:
            $estado = "Cuenta activada";
            break;
        default:
            break;
    }

    return $estado;
}

function pagoMembresia ($PAGO){
    switch($PAGO){
        case 0:
            $pago = "No ha pagado";
            break;
        case 1:
            $pago = "Ha pagado";
        default: break;
    }

    return $pago;
}





?>
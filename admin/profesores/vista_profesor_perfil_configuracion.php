<?php
include("../../includes/connection.php");
session_start();

$ID = $_SESSION['ID_profesor'];
$NOMBRE = $_SESSION['Nombre'];
$APELLIDOS = $_SESSION['Apellidos'];

$PODER = $_SESSION['Poder'];
$ACTIVE = $_SESSION['active'];

if(!isset($ID) OR $PODER == 0 OR $ACTIVE == 0){
    session_destroy();
    header("location: ../../index.html");
    exit();
}

$ID_PROFESOR = mysqli_real_escape_string($connect, $_GET['id_profesor']);

$profesores = "SELECT * FROM Profesor WHERE ID_profesor = '$ID_PROFESOR'";
$peticion_profesores = mysqli_query($connect, $profesores);

while($tabla = $peticion_profesores->fetch_assoc()){
    $NOMBRE_PROFESOR = $tabla['Nombre'];
    $APELLIDO_PROFESOR = $tabla['Apellidos'];

    $RUT_PROFESOR = $tabla['Rut'];
    $FECHA_NACIMIENTO_PROFESOR = $tabla['Fecha_nacimiento'];

    $TELEFONO_PROFESOR = $tabla['Telefono'];
    $DIRECCION_PROFESOR = $tabla['Direccion'];
    $MAIL_PROFESOR = $tabla['Mail'];

    $CINTURON_PROFESOR = $tabla['Cinturon'];

    $PODER_PROFESOR = $tabla['Poder'];

    $ACTIVE_PROFESOR = $tabla['active'];

    switch($CINTURON_PROFESOR){
        case 1:
            $cinturon = "Ninguno";
            break;
        case 2:
            $cinturon = "Blanco";
            break;
        case 3:
            $cinturon = "Azul";
            break;
        case 4:
            $cinturon = "Morado";
            break;
        case 5:
            $cinturon = "Cafe";
            break;
        case 6: 
            $cinturon = "Negro";
            break;
        default:
            break;
    }
    
    switch($ACTIVE_PROFESOR){
        case 0:
            $active = "Desactivar cuenta";
            break;
        case 1:
            $active = "Activar cuenta";
            break;
        default:
            break;
    }

    switch($PODER_PROFESOR){
        case 0:
            $poder = "Quitar poder";
            break;
        case 1:
            $poder = "Otorgar poder";
            break;
        default:
            break;
    }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Configuracion de <?php echo "$NOMBRE_PROFESOR $APELLIDO_PROFESOR";?></title>
</head>
<body>
    <ul>
        <li class="log"> <?php echo "<p>Bienvenido $NOMBRE $APELLIDOS</p>";?> </li>

        <li><a href="../../profesor/profesor_php/home_profesor.php">Inicio</a></li>
        <li class="active"><a href="../../profesor/perfil_profesor.php">Perfil</a></li>
        <li><a href="../../profesor/horario_profesor.php">Horario</a></li>
        <li><a href="../../profesor/vista_clase.php">Clases</a></li>
        <li><a href="../../general/general_php/logout.php">Cerrar sesion</a></li>
    </ul>

    <h1>Editar alumno: <?php echo "$NOMBRE_PROFESOR $APELLIDO_PROFESOR";?></h1>

    <button><a href="vista_profesor_perfil.php?id_profesor=<?php echo $ID_PROFESOR;?>">volver</a></button>

    <div class="contenedor_formulario">
        <form action="profesores_php/actualizar_profesor.php?id_profesor=<?php echo $ID_PROFESOR;?>&eliminar=0" method="post">
            <div class="nombre">
                <label for="nombre"><b>Nombres</b></label>
                <input class="controls" type="text" placeholder="Ingrese sus nombres" name="nombre" id="nombre" value="<?php echo $NOMBRE_PROFESOR;?>">
            </div>
            <div class="apellido">
                <label for="apellido"><b>Apellidos</b></label>
                <input class="controls" type="text" placeholder="Ingrese su apellido" name="apellido" id="apellido" value="<?php echo $APELLIDO_PROFESOR;?>">
            </div>
            <div class="rut">
                <label for="rut"><b>RUT</b></label>
                <input class="controls" type="text" placeholder="Ingrese su RUT, ej: 11.111.111-1" name="rut" id="rut" value="<?php echo $RUT_PROFESOR;?>">
            </div>
            <div class="direccion">
                <label for="direccion"><b>Dirección</b></label>
                <input class="controls" type="text" placeholder="Ingrese su dirección" name="direccion" id="direccion" value="<?php echo $DIRECCION_PROFESOR;?>">
            </div>
            <div class="telefono">
                <label for="telefono"><b>Teléfono</b></label>
                <input class="controls" type="text" placeholder="Ingrese su teléfono" name="telefono" id="telefono" value="<?php echo $TELEFONO_PROFESOR;?>">
            </div>
            <div class="fecha">
                <label for="fecha"><b>Fecha de nacimiento</b></label>
                <input class="controls" type="date" name="fecha" id="fecha" value="<?php echo $FECHA_NACIMIENTO_PROFESOR;?>">
            </div>

            <div class="cinturon">
                <label for="cinturon"><b>Cinturon</b></label>
                <select class="cinturon" name="cinturon" id="cinturon">
                    <option value="<?php echo $CINTURON_PROFESOR;?>" selected hidden ><?php echo $cinturon;?></option>
                    <option value="1">Ninguno</option>
                    <option value="2">Blanco</option>
                    <option value="3">Azul</option>
                    <option value="4">Morado</option>
                    <option value="5">Cafe</option>
                    <option value="6">Negro</option>
                </select>
            </div> 

            <div class="active">
                <label for="active"><b>Estado</b></label>
                <select class="active" name="active" id="active">
                    <option value="<?php echo $ACTIVE_PROFESOR?>" selected hidden><?php echo $active;?></option>
                    <option value="0">Desactivar cuenta</option>
                    <option value="1">Activar cuenta</option>
                </select>
            </div>

            <div class="poder">
                <label for="poder"><b>Poder</b></label>
                <select class="poder" name="poder" id="poder">
                    <option value="<?php echo $PODER_PROFESOR?>" selected hidden><?php echo $poder;?></option>
                    <option value="0">Quitar poder</option>
                    <option value="1">Otorgar poder</option>
                </select>
            </div>
            
            <div class="enviar">
                <input type="submit" value="Actualizar perfil" name="actualizar" class="buttons">  
            </div>   
        </form>
    </div>
   
    <div class="contenedor_eliminar">
        <h2>Eliminar cuenta</h2>
        <div id="eliminar_container" class="centro" >
            <button id="boton_eliminar" class="buttons" >Eliminar cuenta</button>
        </div>
        <div id="confirmacion_container" class="centro">
            <label for="boton_eliminar_confirmacion" class="centro">¿Esta seguro de que quiere eliminar su cuenta?</label>
            <br>
            <button id="boton_eliminar_confirmacion" class="buttons"><a href="profesores_php/actualizar_profesor.php?id_profesor=<?php echo $ID_PROFESOR;?>&eliminar=1">Eliminar cuenta</a></button>
            <button id="boton_eliminar_cancelacion" class="buttons">Cancelar</button>
        </div>
    </div>
    

    <script>
        document.getElementById("confirmacion_container").style.display = "none";

        document.getElementById("boton_eliminar").onclick = mostrarDiv;
        document.getElementById("boton_eliminar_cancelacion").onclick = MostrarDiv_eliminar;
        
        function mostrarDiv(){
            document.getElementById("eliminar_container").style.display = "none";
            document.getElementById("confirmacion_container").style.display = "block";
        }

        function MostrarDiv_eliminar(){
            document.getElementById("eliminar_container").style.display = "block";
            document.getElementById("confirmacion_container").style.display = "none";
        }

        //Verificador de contraseñas.
    </script>  
    
</body>
</html>
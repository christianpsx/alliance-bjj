<?php
include("../../includes/connection.php");
session_start();

$ID = $_SESSION['ID_profesor'];
$NOMBRE = $_SESSION['Nombre'];
$APELLIDOS = $_SESSION['Apellidos'];

$RUT = $_SESSION['Rut'];
$FECHA_N = $_SESSION['Fecha_nacimiento'];

$DIRECCION = $_SESSION['Direccion'];
$MAIL = $_SESSION['Mail'];
$TELEFONO = $_SESSION['Telefono'];

$CINTURON = $_SESSION['Cinturon'];

$PODER = $_SESSION['Poder'];
$ACTIVE = $_SESSION['active'];

if(!isset($ID) OR $PODER == 0 OR $ACTIVE == 0){
    session_destroy();
    header("location: ../../index.html");
    exit();
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="css/vista_profesores.css">
    <title>Profesores</title>
</head>
<body>
    <ul>
        <li class="log"> <?php echo "<p>Bienvenido $NOMBRE $APELLIDOS</p>";?> </li>

        <li><a href="../../profesor/profesor_php/home_profesor.php">Inicio</a></li>
        <li class="active"><a href="../../profesor/perfil_profesor.php">Perfil</a></li>
        <li><a href="../../profesor/horario_profesor.php">Horario</a></li>
        <li><a href="../../profesor/vista_clase.php">Clases</a></li>
        <li><a href="../../general/general_php/logout.php">Cerrar sesion</a></li>
    </ul>

    <h1>Profesores</h1>
    
    <button><a href="../admin_home.php">volver</a></button>

    <table class="alliance" border="1">
        <tr>
            <th>Nombre</th>
            <th>Cinturon</th>
            <th>Estado</th>
        </tr>
        
        <?php
        $profesores = "SELECT * FROM Profesor";
        $peticion_profesores = mysqli_query($connect, $profesores);

        while($tabla = $peticion_profesores->fetch_assoc()){
            $ID_PROFESOR = $tabla['ID_profesor'];
            $NOMBRE_PROFESOR = $tabla['Nombre'];
            $APELLIDO_PROFESOR = $tabla['Apellidos'];
            $CINTURON_PROFESOR = $tabla['Cinturon'];
            $ACTIVE_PROFESOR = $tabla['active'];

            switch($CINTURON_PROFESOR){
                case 1:
                    $cinturon = "Ninguno";
                    break;
                case 2:
                    $cinturon = "Blanco";
                    break;
                case 3:
                    $cinturon = "Azul";
                    break;
                case 4:
                    $cinturon = "Morado";
                    break;
                case 5:
                    $cinturon = "Cafe";
                    break;
                case 6: 
                    $cinturon = "Negro";
                    break;
                default:
                    break;
            }
            switch($ACTIVE_PROFESOR){
                case 0:
                    $active_profesor = "Cuenta desactivada";
                    break;
                case 1:
                    $active_profesor = "Cuenta activada";
                    break;
                default:
                    break;
            }
            ?>
            </html>
            <tr>
                <td><a href="vista_profesor_perfil.php?id_profesor=<?php echo $ID_PROFESOR;?>"><?php echo $NOMBRE_PROFESOR.' '.$APELLIDO_PROFESOR;?></a></td>
                <td><?php echo $cinturon;?></td>
                <td><?php echo $active_profesor;?></td>
            </tr>
            <html>
            <?php
        }
        ?>
        
    </table>
    
</body>
</html>
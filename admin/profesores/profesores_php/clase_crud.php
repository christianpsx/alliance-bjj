<?php
include("../../../includes/connection.php");
session_start();

$ID_ADMIN = $_SESSION['ID_profesor'];

$PODER = $_SESSION['Poder'];
$ACTIVE = $_SESSION['active'];

if(!isset($ID_ADMIN) OR $PODER == 0 OR $ACTIVE == 0){
    session_destroy();
    header("location: ../../../index.html");
    exit();
}


$OPCION = mysqli_real_escape_string($connect, $_GET['opcion']);


switch($OPCION){
    case 0:
        $ID_CLASE = mysqli_real_escape_string($connect, $_GET['id_clase']);
        $ID_PROFESOR_CLASE = mysqli_real_escape_string($connect, $_GET['id_profesor']);

        $eliminar_clase = mysqli_query($connect, "DELETE FROM Clase WHERE ID_clase = '$ID_CLASE' AND ID_profesor = '$ID_PROFESOR_CLASE'");
        if($eliminar_clase){
            header("location: ../vista_profesor_perfil.php?id_profesor=".$ID_PROFESOR_CLASE);
        }else{
            header("location: ../vista_profesor_perfil.php?id_profesor=<".$ID_PROFESOR_CLASE);
        }
        break;
    case 1:
        $ID = mysqli_real_escape_string($connect, $_GET['id_profesor']);

        //Datos de la clase a crear

        $TIPO_CLASE = mysqli_real_escape_string($connect, $_POST['Clase']);
        $CUPOS_CLASE = mysqli_real_escape_string($connect, $_POST['Cupos']);

        $DIA_CLASE = mysqli_real_escape_string($connect, $_POST['Dia']);
        $HORA_CLASE = mysqli_real_escape_string($connect, $_POST['Hora']);

        $CINTURON_CLASE = mysqli_real_escape_string($connect, $_POST['Cinturon']);
        $GRADO_CLASE = mysqli_real_escape_string($connect, $_POST['Grado']);

        switch($DIA_CLASE){
            case 1:
                if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("7:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("8:00"))){
                    $peticion = mysqli_query($connect, "SELECT COUNT(*) AS TOPE FROM Clase WHERE Hora >= '7:00:00' AND Hora < '8:00:00' AND Visibilidad=1 AND Dia='$DIA_CLASE'");
                    $verificador_tope = mysqli_fetch_array($peticion);
        
                    if($verificador_tope['TOPE'] == 0){
                        $insertar_clase = "INSERT INTO Clase (Tipo_clase, ID_profesor, Cupos, Cinturon, Grado, Dia, Hora, Visibilidad) VALUES ('$TIPO_CLASE', '$ID', '$CUPOS_CLASE', '$CINTURON_CLASE', '$GRADO_CLASE', '$DIA_CLASE', '$HORA_CLASE', 1)";
                        if ($connect->query($insertar_clase) === TRUE) {
                            header('Location: ../vista_profesor_perfil.php?id_profesor='.$ID);
                        }
                    }else{
                        echo '<script type="text/javascript"> alert("Existe un tope de horario, usted no puede crear esta clase"); location="../vista_profesor_perfil.php?id_profesor='.$ID.'"; </script>';
                    }
                }
                if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("8:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("9:00"))){
                    $peticion = mysqli_query($connect, "SELECT COUNT(*) AS TOPE FROM Clase WHERE Hora >= '8:00:00' AND Hora < '9:00:00' AND Visibilidad=1 AND Dia='$DIA_CLASE'");
                    $verificador_tope = mysqli_fetch_array($peticion);
        
                    if($verificador_tope['TOPE'] == 0){
                        $insertar_clase = "INSERT INTO Clase (Tipo_clase, ID_profesor, Cupos, Cinturon, Grado, Dia, Hora, Visibilidad) VALUES ('$TIPO_CLASE', '$ID', '$CUPOS_CLASE', '$CINTURON_CLASE', '$GRADO_CLASE', '$DIA_CLASE', '$HORA_CLASE', 1)";
                        if ($connect->query($insertar_clase) === TRUE) {
                            header('Location: ../vista_profesor_perfil.php?id_profesor='.$ID);
                        }
                    }else{
                        echo '<script type="text/javascript"> alert("Existe un tope de horario, usted no puede crear esta clase"); location="../vista_profesor_perfil.php?id_profesor='.$ID.'"; </script>';
                    }
                }
                if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("9:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("10:00"))){
                    $peticion = mysqli_query($connect, "SELECT COUNT(*) AS TOPE FROM Clase WHERE Hora >= '9:00:00' AND Hora < '10:00:00' AND Visibilidad=1 AND Dia='$DIA_CLASE'");
                    $verificador_tope = mysqli_fetch_array($peticion);
        
                    if($verificador_tope['TOPE'] == 0){
                        $insertar_clase = "INSERT INTO Clase (Tipo_clase, ID_profesor, Cupos, Cinturon, Grado, Dia, Hora, Visibilidad) VALUES ('$TIPO_CLASE', '$ID', '$CUPOS_CLASE', '$CINTURON_CLASE', '$GRADO_CLASE', '$DIA_CLASE', '$HORA_CLASE', 1)";
                        if ($connect->query($insertar_clase) === TRUE) {
                            header('Location: ../vista_profesor_perfil.php?id_profesor='.$ID);
                        }
                    }else{
                        echo '<script type="text/javascript"> alert("Existe un tope de horario, usted no puede crear esta clase"); location="../vista_profesor_perfil.php?id_profesor='.$ID.'"; </script>';
                    }
                }
                if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("10:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("11:00"))){
                    $peticion = mysqli_query($connect, "SELECT COUNT(*) AS TOPE FROM Clase WHERE Hora >= '10:00:00' AND Hora < '11:00:00' AND Visibilidad=1 AND Dia='$DIA_CLASE'");
                    $verificador_tope = mysqli_fetch_array($peticion);
        
                    if($verificador_tope['TOPE'] == 0){
                        $insertar_clase = "INSERT INTO Clase (Tipo_clase, ID_profesor, Cupos, Cinturon, Grado, Dia, Hora, Visibilidad) VALUES ('$TIPO_CLASE', '$ID', '$CUPOS_CLASE', '$CINTURON_CLASE', '$GRADO_CLASE', '$DIA_CLASE', '$HORA_CLASE', 1)";
                        if ($connect->query($insertar_clase) === TRUE) {
                            header('Location: ../vista_profesor_perfil.php?id_profesor='.$ID);
                        }
                    }else{
                        echo '<script type="text/javascript"> alert("Existe un tope de horario, usted no puede crear esta clase"); location="../vista_profesor_perfil.php?id_profesor='.$ID.'"; </script>';
                    }
                }
                if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("11:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("12:00"))){
                    $peticion = mysqli_query($connect, "SELECT COUNT(*) AS TOPE FROM Clase WHERE Hora >= '11:00:00' AND Hora < '12:00:00' AND Visibilidad=1 AND Dia='$DIA_CLASE'");
                    $verificador_tope = mysqli_fetch_array($peticion);
        
                    if($verificador_tope['TOPE'] == 0){
                        $insertar_clase = "INSERT INTO Clase (Tipo_clase, ID_profesor, Cupos, Cinturon, Grado, Dia, Hora, Visibilidad) VALUES ('$TIPO_CLASE', '$ID', '$CUPOS_CLASE', '$CINTURON_CLASE', '$GRADO_CLASE', '$DIA_CLASE', '$HORA_CLASE', 1)";
                        if ($connect->query($insertar_clase) === TRUE) {
                            header('Location: ../vista_profesor_perfil.php?id_profesor='.$ID);
                        }
                    }else{
                        echo '<script type="text/javascript"> alert("Existe un tope de horario, usted no puede crear esta clase"); location="../vista_profesor_perfil.php?id_profesor='.$ID.'"; </script>';
                    }
                }
                if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("12:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("13:00"))){
                    $peticion = mysqli_query($connect, "SELECT COUNT(*) AS TOPE FROM Clase WHERE Hora >= '12:00:00' AND Hora < '13:00:00' AND Visibilidad=1 AND Dia='$DIA_CLASE'");
                    $verificador_tope = mysqli_fetch_array($peticion);
        
                    if($verificador_tope['TOPE'] == 0){
                        $insertar_clase = "INSERT INTO Clase (Tipo_clase, ID_profesor, Cupos, Cinturon, Grado, Dia, Hora, Visibilidad) VALUES ('$TIPO_CLASE', '$ID', '$CUPOS_CLASE', '$CINTURON_CLASE', '$GRADO_CLASE', '$DIA_CLASE', '$HORA_CLASE', 1)";
                        if ($connect->query($insertar_clase) === TRUE) {
                            header('Location: ../vista_profesor_perfil.php?id_profesor='.$ID);
                        }
                    }else{
                        echo '<script type="text/javascript"> alert("Existe un tope de horario, usted no puede crear esta clase"); location="../vista_profesor_perfil.php?id_profesor='.$ID.'"; </script>';
                    }
                }
                if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("13:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("14:00"))){
                    $peticion = mysqli_query($connect, "SELECT COUNT(*) AS TOPE FROM Clase WHERE Hora >= '13:00:00' AND Hora < '14:00:00' AND Visibilidad=1 AND Dia='$DIA_CLASE'");
                    $verificador_tope = mysqli_fetch_array($peticion);
        
                    if($verificador_tope['TOPE'] == 0){
                        $insertar_clase = "INSERT INTO Clase (Tipo_clase, ID_profesor, Cupos, Cinturon, Grado, Dia, Hora, Visibilidad) VALUES ('$TIPO_CLASE', '$ID', '$CUPOS_CLASE', '$CINTURON_CLASE', '$GRADO_CLASE', '$DIA_CLASE', '$HORA_CLASE', 1)";
                        if ($connect->query($insertar_clase) === TRUE) {
                            header('Location: ../vista_profesor_perfil.php?id_profesor='.$ID);
                        }
                    }else{
                        echo '<script type="text/javascript"> alert("Existe un tope de horario, usted no puede crear esta clase"); location="../vista_profesor_perfil.php?id_profesor='.$ID.'"; </script>';
                    }
                }
                if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("14:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("15:00"))){
                    $peticion = mysqli_query($connect, "SELECT COUNT(*) AS TOPE FROM Clase WHERE Hora >= '14:00:00' AND Hora < '15:00:00' AND Visibilidad=1 AND Dia='$DIA_CLASE'");
                    $verificador_tope = mysqli_fetch_array($peticion);
        
                    if($verificador_tope['TOPE'] == 0){
                        $insertar_clase = "INSERT INTO Clase (Tipo_clase, ID_profesor, Cupos, Cinturon, Grado, Dia, Hora, Visibilidad) VALUES ('$TIPO_CLASE', '$ID', '$CUPOS_CLASE', '$CINTURON_CLASE', '$GRADO_CLASE', '$DIA_CLASE', '$HORA_CLASE', 1)";
                        if ($connect->query($insertar_clase) === TRUE) {
                            header('Location: ../vista_profesor_perfil.php?id_profesor='.$ID);
                        }
                    }else{
                        echo '<script type="text/javascript"> alert("Existe un tope de horario, usted no puede crear esta clase"); location="../vista_profesor_perfil.php?id_profesor='.$ID.'"; </script>';
                    }
                }
                if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("15:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("16:00"))){
                    $peticion = mysqli_query($connect, "SELECT COUNT(*) AS TOPE FROM Clase WHERE Hora >= '15:00:00' AND Hora < '16:00:00' AND Visibilidad=1 AND Dia='$DIA_CLASE'");
                    $verificador_tope = mysqli_fetch_array($peticion);
        
                    if($verificador_tope['TOPE'] == 0){
                        $insertar_clase = "INSERT INTO Clase (Tipo_clase, ID_profesor, Cupos, Cinturon, Grado, Dia, Hora, Visibilidad) VALUES ('$TIPO_CLASE', '$ID', '$CUPOS_CLASE', '$CINTURON_CLASE', '$GRADO_CLASE', '$DIA_CLASE', '$HORA_CLASE', 1)";
                        if ($connect->query($insertar_clase) === TRUE) {
                            header('Location: ../vista_profesor_perfil.php?id_profesor='.$ID);
                        }
                    }else{
                        echo '<script type="text/javascript"> alert("Existe un tope de horario, usted no puede crear esta clase"); location="../vista_profesor_perfil.php?id_profesor='.$ID.'"; </script>';
                    }
                }
                if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("16:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("17:00"))){
                    $peticion = mysqli_query($connect, "SELECT COUNT(*) AS TOPE FROM Clase WHERE Hora >= '16:00:00' AND Hora < '17:00:00' AND Visibilidad=1 AND Dia='$DIA_CLASE'");
                    $verificador_tope = mysqli_fetch_array($peticion);
        
                    if($verificador_tope['TOPE'] == 0){
                        $insertar_clase = "INSERT INTO Clase (Tipo_clase, ID_profesor, Cupos, Cinturon, Grado, Dia, Hora, Visibilidad) VALUES ('$TIPO_CLASE', '$ID', '$CUPOS_CLASE', '$CINTURON_CLASE', '$GRADO_CLASE', '$DIA_CLASE', '$HORA_CLASE', 1)";
                        if ($connect->query($insertar_clase) === TRUE) {
                            header('Location: ../vista_profesor_perfil.php?id_profesor='.$ID);
                        }
                    }else{
                        echo '<script type="text/javascript"> alert("Existe un tope de horario, usted no puede crear esta clase"); location="../vista_profesor_perfil.php?id_profesor='.$ID.'"; </script>';
                    }
                }
                if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("17:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("18:00"))){
                    $peticion = mysqli_query($connect, "SELECT COUNT(*) AS TOPE FROM Clase WHERE Hora >= '17:00:00' AND Hora < '18:00:00' AND Visibilidad=1 AND Dia='$DIA_CLASE'");
                    $verificador_tope = mysqli_fetch_array($peticion);
        
                    if($verificador_tope['TOPE'] == 0){
                        $insertar_clase = "INSERT INTO Clase (Tipo_clase, ID_profesor, Cupos, Cinturon, Grado, Dia, Hora, Visibilidad) VALUES ('$TIPO_CLASE', '$ID', '$CUPOS_CLASE', '$CINTURON_CLASE', '$GRADO_CLASE', '$DIA_CLASE', '$HORA_CLASE', 1)";
                        if ($connect->query($insertar_clase) === TRUE) {
                            header('Location: ../vista_profesor_perfil.php?id_profesor='.$ID);
                        }
                    }else{
                        echo '<script type="text/javascript"> alert("Existe un tope de horario, usted no puede crear esta clase"); location="../vista_profesor_perfil.php?id_profesor='.$ID.'"; </script>';
                    }
                }
                if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("18:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("19:00"))){
                    $peticion = mysqli_query($connect, "SELECT COUNT(*) AS TOPE FROM Clase WHERE Hora >= '18:00:00' AND Hora < '19:00:00' AND Visibilidad=1 AND Dia='$DIA_CLASE'");
                    $verificador_tope = mysqli_fetch_array($peticion);
        
                    if($verificador_tope['TOPE'] == 0){
                        $insertar_clase = "INSERT INTO Clase (Tipo_clase, ID_profesor, Cupos, Cinturon, Grado, Dia, Hora, Visibilidad) VALUES ('$TIPO_CLASE', '$ID', '$CUPOS_CLASE', '$CINTURON_CLASE', '$GRADO_CLASE', '$DIA_CLASE', '$HORA_CLASE', 1)";
                        if ($connect->query($insertar_clase) === TRUE) {
                            header('Location: ../vista_profesor_perfil.php?id_profesor='.$ID);
                        }
                    }else{
                        echo '<script type="text/javascript"> alert("Existe un tope de horario, usted no puede crear esta clase"); location="../vista_profesor_perfil.php?id_profesor='.$ID.'"; </script>';
                    }
                }
                if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("19:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("20:00"))){
                    $peticion = mysqli_query($connect, "SELECT COUNT(*) AS TOPE FROM Clase WHERE Hora >= '19:00:00' AND Hora < '20:00:00' AND Visibilidad=1 AND Dia='$DIA_CLASE'");
                    $verificador_tope = mysqli_fetch_array($peticion);
        
                    if($verificador_tope['TOPE'] == 0){
                        $insertar_clase = "INSERT INTO Clase (Tipo_clase, ID_profesor, Cupos, Cinturon, Grado, Dia, Hora, Visibilidad) VALUES ('$TIPO_CLASE', '$ID', '$CUPOS_CLASE', '$CINTURON_CLASE', '$GRADO_CLASE', '$DIA_CLASE', '$HORA_CLASE', 1)";
                        if ($connect->query($insertar_clase) === TRUE) {
                            header('Location: ../vista_profesor_perfil.php?id_profesor='.$ID);
                        }
                    }else{
                        echo '<script type="text/javascript"> alert("Existe un tope de horario, usted no puede crear esta clase"); location="../vista_profesor_perfil.php?id_profesor='.$ID.'"; </script>';
                    }
                }
                if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("20:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("21:00"))){
                    $peticion = mysqli_query($connect, "SELECT COUNT(*) AS TOPE FROM Clase WHERE Hora >= '20:00:00' AND Hora < '21:00:00' AND Visibilidad=1 AND Dia='$DIA_CLASE'");
                    $verificador_tope = mysqli_fetch_array($peticion);
        
                    if($verificador_tope['TOPE'] == 0){
                        $insertar_clase = "INSERT INTO Clase (Tipo_clase, ID_profesor, Cupos, Cinturon, Grado, Dia, Hora, Visibilidad) VALUES ('$TIPO_CLASE', '$ID', '$CUPOS_CLASE', '$CINTURON_CLASE', '$GRADO_CLASE', '$DIA_CLASE', '$HORA_CLASE', 1)";
                        if ($connect->query($insertar_clase) === TRUE) {
                            header('Location: ../vista_profesor_perfil.php?id_profesor='.$ID);
                        }
                    }else{
                        echo '<script type="text/javascript"> alert("Existe un tope de horario, usted no puede crear esta clase"); location="../vista_profesor_perfil.php?id_profesor='.$ID.'"; </script>';
                    }
                }
                if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("21:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("22:00"))){
                    $peticion = mysqli_query($connect, "SELECT COUNT(*) AS TOPE FROM Clase WHERE Hora >= '21:00:00' AND Hora < '22:00:00' AND Visibilidad=1 AND Dia='$DIA_CLASE'");
                    $verificador_tope = mysqli_fetch_array($peticion);
        
                    if($verificador_tope['TOPE'] == 0){
                        $insertar_clase = "INSERT INTO Clase (Tipo_clase, ID_profesor, Cupos, Cinturon, Grado, Dia, Hora, Visibilidad) VALUES ('$TIPO_CLASE', '$ID', '$CUPOS_CLASE', '$CINTURON_CLASE', '$GRADO_CLASE', '$DIA_CLASE', '$HORA_CLASE', 1)";
                        if ($connect->query($insertar_clase) === TRUE) {
                            header('Location: ../vista_profesor_perfil.php?id_profesor='.$ID);
                        }
                    }else{
                        echo '<script type="text/javascript"> alert("Existe un tope de horario, usted no puede crear esta clase"); location="../vista_profesor_perfil.php?id_profesor='.$ID.'"; </script>';
                    }
                }
                break;
            case 2:
                if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("7:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("8:00"))){
                    $peticion = mysqli_query($connect, "SELECT COUNT(*) AS TOPE FROM Clase WHERE Hora >= '7:00:00' AND Hora < '8:00:00' AND Visibilidad=1 AND Dia='$DIA_CLASE'");
                    $verificador_tope = mysqli_fetch_array($peticion);
        
                    if($verificador_tope['TOPE'] == 0){
                        $insertar_clase = "INSERT INTO Clase (Tipo_clase, ID_profesor, Cupos, Cinturon, Grado, Dia, Hora, Visibilidad) VALUES ('$TIPO_CLASE', '$ID', '$CUPOS_CLASE', '$CINTURON_CLASE', '$GRADO_CLASE', '$DIA_CLASE', '$HORA_CLASE', 1)";
                        if ($connect->query($insertar_clase) === TRUE) {
                            header('Location: ../vista_profesor_perfil.php?id_profesor='.$ID);
                        }
                    }else{
                        echo '<script type="text/javascript"> alert("Existe un tope de horario, usted no puede crear esta clase"); location="../vista_profesor_perfil.php?id_profesor='.$ID.'"; </script>';
                    }
                }
                if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("8:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("9:00"))){
                    $peticion = mysqli_query($connect, "SELECT COUNT(*) AS TOPE FROM Clase WHERE Hora >= '8:00:00' AND Hora < '9:00:00' AND Visibilidad=1 AND Dia='$DIA_CLASE'");
                    $verificador_tope = mysqli_fetch_array($peticion);
        
                    if($verificador_tope['TOPE'] == 0){
                        $insertar_clase = "INSERT INTO Clase (Tipo_clase, ID_profesor, Cupos, Cinturon, Grado, Dia, Hora, Visibilidad) VALUES ('$TIPO_CLASE', '$ID', '$CUPOS_CLASE', '$CINTURON_CLASE', '$GRADO_CLASE', '$DIA_CLASE', '$HORA_CLASE', 1)";
                        if ($connect->query($insertar_clase) === TRUE) {
                            header('Location: ../vista_profesor_perfil.php?id_profesor='.$ID);
                        }
                    }else{
                        echo '<script type="text/javascript"> alert("Existe un tope de horario, usted no puede crear esta clase"); location="../vista_profesor_perfil.php?id_profesor='.$ID.'"; </script>';
                    }
                }
                if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("9:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("10:00"))){
                    $peticion = mysqli_query($connect, "SELECT COUNT(*) AS TOPE FROM Clase WHERE Hora >= '9:00:00' AND Hora < '10:00:00' AND Visibilidad=1 AND Dia='$DIA_CLASE'");
                    $verificador_tope = mysqli_fetch_array($peticion);
        
                    if($verificador_tope['TOPE'] == 0){
                        $insertar_clase = "INSERT INTO Clase (Tipo_clase, ID_profesor, Cupos, Cinturon, Grado, Dia, Hora, Visibilidad) VALUES ('$TIPO_CLASE', '$ID', '$CUPOS_CLASE', '$CINTURON_CLASE', '$GRADO_CLASE', '$DIA_CLASE', '$HORA_CLASE', 1)";
                        if ($connect->query($insertar_clase) === TRUE) {
                            header('Location: ../vista_profesor_perfil.php?id_profesor='.$ID);
                        }
                    }else{
                        echo '<script type="text/javascript"> alert("Existe un tope de horario, usted no puede crear esta clase"); location="../vista_profesor_perfil.php?id_profesor='.$ID.'"; </script>';
                    }
                }
                if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("10:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("11:00"))){
                    $peticion = mysqli_query($connect, "SELECT COUNT(*) AS TOPE FROM Clase WHERE Hora >= '10:00:00' AND Hora < '11:00:00' AND Visibilidad=1 AND Dia='$DIA_CLASE'");
                    $verificador_tope = mysqli_fetch_array($peticion);
        
                    if($verificador_tope['TOPE'] == 0){
                        $insertar_clase = "INSERT INTO Clase (Tipo_clase, ID_profesor, Cupos, Cinturon, Grado, Dia, Hora, Visibilidad) VALUES ('$TIPO_CLASE', '$ID', '$CUPOS_CLASE', '$CINTURON_CLASE', '$GRADO_CLASE', '$DIA_CLASE', '$HORA_CLASE', 1)";
                        if ($connect->query($insertar_clase) === TRUE) {
                            header('Location: ../vista_profesor_perfil.php?id_profesor='.$ID);
                        }
                    }else{
                        echo '<script type="text/javascript"> alert("Existe un tope de horario, usted no puede crear esta clase"); location="../vista_profesor_perfil.php?id_profesor='.$ID.'"; </script>';
                    }
                }
                if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("11:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("12:00"))){
                    $peticion = mysqli_query($connect, "SELECT COUNT(*) AS TOPE FROM Clase WHERE Hora >= '11:00:00' AND Hora < '12:00:00' AND Visibilidad=1 AND Dia='$DIA_CLASE'");
                    $verificador_tope = mysqli_fetch_array($peticion);
        
                    if($verificador_tope['TOPE'] == 0){
                        $insertar_clase = "INSERT INTO Clase (Tipo_clase, ID_profesor, Cupos, Cinturon, Grado, Dia, Hora, Visibilidad) VALUES ('$TIPO_CLASE', '$ID', '$CUPOS_CLASE', '$CINTURON_CLASE', '$GRADO_CLASE', '$DIA_CLASE', '$HORA_CLASE', 1)";
                        if ($connect->query($insertar_clase) === TRUE) {
                            header('Location: ../vista_profesor_perfil.php?id_profesor='.$ID);
                        }
                    }else{
                        echo '<script type="text/javascript"> alert("Existe un tope de horario, usted no puede crear esta clase"); location="../vista_profesor_perfil.php?id_profesor='.$ID.'"; </script>';
                    }
                }
                if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("12:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("13:00"))){
                    $peticion = mysqli_query($connect, "SELECT COUNT(*) AS TOPE FROM Clase WHERE Hora >= '12:00:00' AND Hora < '13:00:00' AND Visibilidad=1 AND Dia='$DIA_CLASE'");
                    $verificador_tope = mysqli_fetch_array($peticion);
        
                    if($verificador_tope['TOPE'] == 0){
                        $insertar_clase = "INSERT INTO Clase (Tipo_clase, ID_profesor, Cupos, Cinturon, Grado, Dia, Hora, Visibilidad) VALUES ('$TIPO_CLASE', '$ID', '$CUPOS_CLASE', '$CINTURON_CLASE', '$GRADO_CLASE', '$DIA_CLASE', '$HORA_CLASE', 1)";
                        if ($connect->query($insertar_clase) === TRUE) {
                            header('Location: ../vista_profesor_perfil.php?id_profesor='.$ID);
                        }
                    }else{
                        echo '<script type="text/javascript"> alert("Existe un tope de horario, usted no puede crear esta clase"); location="../vista_profesor_perfil.php?id_profesor='.$ID.'"; </script>';
                    }
                }
                if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("13:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("14:00"))){
                    $peticion = mysqli_query($connect, "SELECT COUNT(*) AS TOPE FROM Clase WHERE Hora >= '13:00:00' AND Hora < '14:00:00' AND Visibilidad=1 AND Dia='$DIA_CLASE'");
                    $verificador_tope = mysqli_fetch_array($peticion);
        
                    if($verificador_tope['TOPE'] == 0){
                        $insertar_clase = "INSERT INTO Clase (Tipo_clase, ID_profesor, Cupos, Cinturon, Grado, Dia, Hora, Visibilidad) VALUES ('$TIPO_CLASE', '$ID', '$CUPOS_CLASE', '$CINTURON_CLASE', '$GRADO_CLASE', '$DIA_CLASE', '$HORA_CLASE', 1)";
                        if ($connect->query($insertar_clase) === TRUE) {
                            header('Location: ../vista_profesor_perfil.php?id_profesor='.$ID);
                        }
                    }else{
                        echo '<script type="text/javascript"> alert("Existe un tope de horario, usted no puede crear esta clase"); location="../vista_profesor_perfil.php?id_profesor='.$ID.'"; </script>';
                    }
                }
                if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("14:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("15:00"))){
                    $peticion = mysqli_query($connect, "SELECT COUNT(*) AS TOPE FROM Clase WHERE Hora >= '14:00:00' AND Hora < '15:00:00' AND Visibilidad=1 AND Dia='$DIA_CLASE'");
                    $verificador_tope = mysqli_fetch_array($peticion);
        
                    if($verificador_tope['TOPE'] == 0){
                        $insertar_clase = "INSERT INTO Clase (Tipo_clase, ID_profesor, Cupos, Cinturon, Grado, Dia, Hora, Visibilidad) VALUES ('$TIPO_CLASE', '$ID', '$CUPOS_CLASE', '$CINTURON_CLASE', '$GRADO_CLASE', '$DIA_CLASE', '$HORA_CLASE', 1)";
                        if ($connect->query($insertar_clase) === TRUE) {
                            header('Location: ../vista_profesor_perfil.php?id_profesor='.$ID);
                        }
                    }else{
                        echo '<script type="text/javascript"> alert("Existe un tope de horario, usted no puede crear esta clase"); location="../vista_profesor_perfil.php?id_profesor='.$ID.'"; </script>';
                    }
                }
                if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("15:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("16:00"))){
                    $peticion = mysqli_query($connect, "SELECT COUNT(*) AS TOPE FROM Clase WHERE Hora >= '15:00:00' AND Hora < '16:00:00' AND Visibilidad=1 AND Dia='$DIA_CLASE'");
                    $verificador_tope = mysqli_fetch_array($peticion);
        
                    if($verificador_tope['TOPE'] == 0){
                        $insertar_clase = "INSERT INTO Clase (Tipo_clase, ID_profesor, Cupos, Cinturon, Grado, Dia, Hora, Visibilidad) VALUES ('$TIPO_CLASE', '$ID', '$CUPOS_CLASE', '$CINTURON_CLASE', '$GRADO_CLASE', '$DIA_CLASE', '$HORA_CLASE', 1)";
                        if ($connect->query($insertar_clase) === TRUE) {
                            header('Location: ../vista_profesor_perfil.php?id_profesor='.$ID);
                        }
                    }else{
                        echo '<script type="text/javascript"> alert("Existe un tope de horario, usted no puede crear esta clase"); location="../vista_profesor_perfil.php?id_profesor='.$ID.'"; </script>';
                    }
                }
                if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("16:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("17:00"))){
                    $peticion = mysqli_query($connect, "SELECT COUNT(*) AS TOPE FROM Clase WHERE Hora >= '16:00:00' AND Hora < '17:00:00' AND Visibilidad=1 AND Dia='$DIA_CLASE'");
                    $verificador_tope = mysqli_fetch_array($peticion);
        
                    if($verificador_tope['TOPE'] == 0){
                        $insertar_clase = "INSERT INTO Clase (Tipo_clase, ID_profesor, Cupos, Cinturon, Grado, Dia, Hora, Visibilidad) VALUES ('$TIPO_CLASE', '$ID', '$CUPOS_CLASE', '$CINTURON_CLASE', '$GRADO_CLASE', '$DIA_CLASE', '$HORA_CLASE', 1)";
                        if ($connect->query($insertar_clase) === TRUE) {
                            header('Location: ../vista_profesor_perfil.php?id_profesor='.$ID);
                        }
                    }else{
                        echo '<script type="text/javascript"> alert("Existe un tope de horario, usted no puede crear esta clase"); location="../vista_profesor_perfil.php?id_profesor='.$ID.'"; </script>';
                    }
                }
                if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("17:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("18:00"))){
                    $peticion = mysqli_query($connect, "SELECT COUNT(*) AS TOPE FROM Clase WHERE Hora >= '17:00:00' AND Hora < '18:00:00' AND Visibilidad=1 AND Dia='$DIA_CLASE'");
                    $verificador_tope = mysqli_fetch_array($peticion);
        
                    if($verificador_tope['TOPE'] == 0){
                        $insertar_clase = "INSERT INTO Clase (Tipo_clase, ID_profesor, Cupos, Cinturon, Grado, Dia, Hora, Visibilidad) VALUES ('$TIPO_CLASE', '$ID', '$CUPOS_CLASE', '$CINTURON_CLASE', '$GRADO_CLASE', '$DIA_CLASE', '$HORA_CLASE', 1)";
                        if ($connect->query($insertar_clase) === TRUE) {
                            header('Location: ../vista_profesor_perfil.php?id_profesor='.$ID);
                        }
                    }else{
                        echo '<script type="text/javascript"> alert("Existe un tope de horario, usted no puede crear esta clase"); location="../vista_profesor_perfil.php?id_profesor='.$ID.'"; </script>';
                    }
                }
                if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("18:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("19:00"))){
                    $peticion = mysqli_query($connect, "SELECT COUNT(*) AS TOPE FROM Clase WHERE Hora >= '18:00:00' AND Hora < '19:00:00' AND Visibilidad=1 AND Dia='$DIA_CLASE'");
                    $verificador_tope = mysqli_fetch_array($peticion);
        
                    if($verificador_tope['TOPE'] == 0){
                        $insertar_clase = "INSERT INTO Clase (Tipo_clase, ID_profesor, Cupos, Cinturon, Grado, Dia, Hora, Visibilidad) VALUES ('$TIPO_CLASE', '$ID', '$CUPOS_CLASE', '$CINTURON_CLASE', '$GRADO_CLASE', '$DIA_CLASE', '$HORA_CLASE', 1)";
                        if ($connect->query($insertar_clase) === TRUE) {
                            header('Location: ../vista_profesor_perfil.php?id_profesor='.$ID);
                        }
                    }else{
                        echo '<script type="text/javascript"> alert("Existe un tope de horario, usted no puede crear esta clase"); location="../vista_profesor_perfil.php?id_profesor='.$ID.'"; </script>';
                    }
                }
                if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("19:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("20:00"))){
                    $peticion = mysqli_query($connect, "SELECT COUNT(*) AS TOPE FROM Clase WHERE Hora >= '19:00:00' AND Hora < '20:00:00' AND Visibilidad=1 AND Dia='$DIA_CLASE'");
                    $verificador_tope = mysqli_fetch_array($peticion);
        
                    if($verificador_tope['TOPE'] == 0){
                        $insertar_clase = "INSERT INTO Clase (Tipo_clase, ID_profesor, Cupos, Cinturon, Grado, Dia, Hora, Visibilidad) VALUES ('$TIPO_CLASE', '$ID', '$CUPOS_CLASE', '$CINTURON_CLASE', '$GRADO_CLASE', '$DIA_CLASE', '$HORA_CLASE', 1)";
                        if ($connect->query($insertar_clase) === TRUE) {
                            header('Location: ../vista_profesor_perfil.php?id_profesor='.$ID);
                        }
                    }else{
                        echo '<script type="text/javascript"> alert("Existe un tope de horario, usted no puede crear esta clase"); location="../vista_profesor_perfil.php?id_profesor='.$ID.'"; </script>';
                    }
                }
                if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("20:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("21:00"))){
                    $peticion = mysqli_query($connect, "SELECT COUNT(*) AS TOPE FROM Clase WHERE Hora >= '20:00:00' AND Hora < '21:00:00' AND Visibilidad=1 AND Dia='$DIA_CLASE'");
                    $verificador_tope = mysqli_fetch_array($peticion);
        
                    if($verificador_tope['TOPE'] == 0){
                        $insertar_clase = "INSERT INTO Clase (Tipo_clase, ID_profesor, Cupos, Cinturon, Grado, Dia, Hora, Visibilidad) VALUES ('$TIPO_CLASE', '$ID', '$CUPOS_CLASE', '$CINTURON_CLASE', '$GRADO_CLASE', '$DIA_CLASE', '$HORA_CLASE', 1)";
                        if ($connect->query($insertar_clase) === TRUE) {
                            header('Location: ../vista_profesor_perfil.php?id_profesor='.$ID);
                        }
                    }else{
                        echo '<script type="text/javascript"> alert("Existe un tope de horario, usted no puede crear esta clase"); location="../vista_profesor_perfil.php?id_profesor='.$ID.'"; </script>';
                    }
                }
                if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("21:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("22:00"))){
                    $peticion = mysqli_query($connect, "SELECT COUNT(*) AS TOPE FROM Clase WHERE Hora >= '21:00:00' AND Hora < '22:00:00' AND Visibilidad=1 AND Dia='$DIA_CLASE'");
                    $verificador_tope = mysqli_fetch_array($peticion);
        
                    if($verificador_tope['TOPE'] == 0){
                        $insertar_clase = "INSERT INTO Clase (Tipo_clase, ID_profesor, Cupos, Cinturon, Grado, Dia, Hora, Visibilidad) VALUES ('$TIPO_CLASE', '$ID', '$CUPOS_CLASE', '$CINTURON_CLASE', '$GRADO_CLASE', '$DIA_CLASE', '$HORA_CLASE', 1)";
                        if ($connect->query($insertar_clase) === TRUE) {
                            header('Location: ../vista_profesor_perfil.php?id_profesor='.$ID);
                        }
                    }else{
                        echo '<script type="text/javascript"> alert("Existe un tope de horario, usted no puede crear esta clase"); location="../vista_profesor_perfil.php?id_profesor='.$ID.'"; </script>';
                    }
                }
                break;
            case 3:
                if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("7:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("8:00"))){
                    $peticion = mysqli_query($connect, "SELECT COUNT(*) AS TOPE FROM Clase WHERE Hora >= '7:00:00' AND Hora < '8:00:00' AND Visibilidad=1 AND Dia='$DIA_CLASE'");
                    $verificador_tope = mysqli_fetch_array($peticion);
        
                    if($verificador_tope['TOPE'] == 0){
                        $insertar_clase = "INSERT INTO Clase (Tipo_clase, ID_profesor, Cupos, Cinturon, Grado, Dia, Hora, Visibilidad) VALUES ('$TIPO_CLASE', '$ID', '$CUPOS_CLASE', '$CINTURON_CLASE', '$GRADO_CLASE', '$DIA_CLASE', '$HORA_CLASE', 1)";
                        if ($connect->query($insertar_clase) === TRUE) {
                            header('Location: ../vista_profesor_perfil.php?id_profesor='.$ID);
                        }
                    }else{
                        echo '<script type="text/javascript"> alert("Existe un tope de horario, usted no puede crear esta clase"); location="../vista_profesor_perfil.php?id_profesor='.$ID.'"; </script>';
                    }
                }
                if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("8:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("9:00"))){
                    $peticion = mysqli_query($connect, "SELECT COUNT(*) AS TOPE FROM Clase WHERE Hora >= '8:00:00' AND Hora < '9:00:00' AND Visibilidad=1 AND Dia='$DIA_CLASE'");
                    $verificador_tope = mysqli_fetch_array($peticion);
        
                    if($verificador_tope['TOPE'] == 0){
                        $insertar_clase = "INSERT INTO Clase (Tipo_clase, ID_profesor, Cupos, Cinturon, Grado, Dia, Hora, Visibilidad) VALUES ('$TIPO_CLASE', '$ID', '$CUPOS_CLASE', '$CINTURON_CLASE', '$GRADO_CLASE', '$DIA_CLASE', '$HORA_CLASE', 1)";
                        if ($connect->query($insertar_clase) === TRUE) {
                            header('Location: ../vista_profesor_perfil.php?id_profesor='.$ID);
                        }
                    }else{
                        echo '<script type="text/javascript"> alert("Existe un tope de horario, usted no puede crear esta clase"); location="../vista_profesor_perfil.php?id_profesor='.$ID.'"; </script>';
                    }
                }
                if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("9:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("10:00"))){
                    $peticion = mysqli_query($connect, "SELECT COUNT(*) AS TOPE FROM Clase WHERE Hora >= '9:00:00' AND Hora < '10:00:00' AND Visibilidad=1 AND Dia='$DIA_CLASE'");
                    $verificador_tope = mysqli_fetch_array($peticion);
        
                    if($verificador_tope['TOPE'] == 0){
                        $insertar_clase = "INSERT INTO Clase (Tipo_clase, ID_profesor, Cupos, Cinturon, Grado, Dia, Hora, Visibilidad) VALUES ('$TIPO_CLASE', '$ID', '$CUPOS_CLASE', '$CINTURON_CLASE', '$GRADO_CLASE', '$DIA_CLASE', '$HORA_CLASE', 1)";
                        if ($connect->query($insertar_clase) === TRUE) {
                            header('Location: ../vista_profesor_perfil.php?id_profesor='.$ID);
                        }
                    }else{
                        echo '<script type="text/javascript"> alert("Existe un tope de horario, usted no puede crear esta clase"); location="../vista_profesor_perfil.php?id_profesor='.$ID.'"; </script>';
                    }
                }
                if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("10:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("11:00"))){
                    $peticion = mysqli_query($connect, "SELECT COUNT(*) AS TOPE FROM Clase WHERE Hora >= '10:00:00' AND Hora < '11:00:00' AND Visibilidad=1 AND Dia='$DIA_CLASE'");
                    $verificador_tope = mysqli_fetch_array($peticion);
        
                    if($verificador_tope['TOPE'] == 0){
                        $insertar_clase = "INSERT INTO Clase (Tipo_clase, ID_profesor, Cupos, Cinturon, Grado, Dia, Hora, Visibilidad) VALUES ('$TIPO_CLASE', '$ID', '$CUPOS_CLASE', '$CINTURON_CLASE', '$GRADO_CLASE', '$DIA_CLASE', '$HORA_CLASE', 1)";
                        if ($connect->query($insertar_clase) === TRUE) {
                            header('Location: ../vista_profesor_perfil.php?id_profesor='.$ID);
                        }
                    }else{
                        echo '<script type="text/javascript"> alert("Existe un tope de horario, usted no puede crear esta clase"); location="../vista_profesor_perfil.php?id_profesor='.$ID.'"; </script>';
                    }
                }
                if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("11:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("12:00"))){
                    $peticion = mysqli_query($connect, "SELECT COUNT(*) AS TOPE FROM Clase WHERE Hora >= '11:00:00' AND Hora < '12:00:00' AND Visibilidad=1 AND Dia='$DIA_CLASE'");
                    $verificador_tope = mysqli_fetch_array($peticion);
        
                    if($verificador_tope['TOPE'] == 0){
                        $insertar_clase = "INSERT INTO Clase (Tipo_clase, ID_profesor, Cupos, Cinturon, Grado, Dia, Hora, Visibilidad) VALUES ('$TIPO_CLASE', '$ID', '$CUPOS_CLASE', '$CINTURON_CLASE', '$GRADO_CLASE', '$DIA_CLASE', '$HORA_CLASE', 1)";
                        if ($connect->query($insertar_clase) === TRUE) {
                            header('Location: ../vista_profesor_perfil.php?id_profesor='.$ID);
                        }
                    }else{
                        echo '<script type="text/javascript"> alert("Existe un tope de horario, usted no puede crear esta clase"); location="../vista_profesor_perfil.php?id_profesor='.$ID.'"; </script>';
                    }
                }
                if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("12:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("13:00"))){
                    $peticion = mysqli_query($connect, "SELECT COUNT(*) AS TOPE FROM Clase WHERE Hora >= '12:00:00' AND Hora < '13:00:00' AND Visibilidad=1 AND Dia='$DIA_CLASE'");
                    $verificador_tope = mysqli_fetch_array($peticion);
        
                    if($verificador_tope['TOPE'] == 0){
                        $insertar_clase = "INSERT INTO Clase (Tipo_clase, ID_profesor, Cupos, Cinturon, Grado, Dia, Hora, Visibilidad) VALUES ('$TIPO_CLASE', '$ID', '$CUPOS_CLASE', '$CINTURON_CLASE', '$GRADO_CLASE', '$DIA_CLASE', '$HORA_CLASE', 1)";
                        if ($connect->query($insertar_clase) === TRUE) {
                            header('Location: ../vista_profesor_perfil.php?id_profesor='.$ID);
                        }
                    }else{
                        echo '<script type="text/javascript"> alert("Existe un tope de horario, usted no puede crear esta clase"); location="../vista_profesor_perfil.php?id_profesor='.$ID.'"; </script>';
                    }
                }
                if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("13:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("14:00"))){
                    $peticion = mysqli_query($connect, "SELECT COUNT(*) AS TOPE FROM Clase WHERE Hora >= '13:00:00' AND Hora < '14:00:00' AND Visibilidad=1 AND Dia='$DIA_CLASE'");
                    $verificador_tope = mysqli_fetch_array($peticion);
        
                    if($verificador_tope['TOPE'] == 0){
                        $insertar_clase = "INSERT INTO Clase (Tipo_clase, ID_profesor, Cupos, Cinturon, Grado, Dia, Hora, Visibilidad) VALUES ('$TIPO_CLASE', '$ID', '$CUPOS_CLASE', '$CINTURON_CLASE', '$GRADO_CLASE', '$DIA_CLASE', '$HORA_CLASE', 1)";
                        if ($connect->query($insertar_clase) === TRUE) {
                            header('Location: ../vista_profesor_perfil.php?id_profesor='.$ID);
                        }
                    }else{
                        echo '<script type="text/javascript"> alert("Existe un tope de horario, usted no puede crear esta clase"); location="../vista_profesor_perfil.php?id_profesor='.$ID.'"; </script>';
                    }
                }
                if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("14:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("15:00"))){
                    $peticion = mysqli_query($connect, "SELECT COUNT(*) AS TOPE FROM Clase WHERE Hora >= '14:00:00' AND Hora < '15:00:00' AND Visibilidad=1 AND Dia='$DIA_CLASE'");
                    $verificador_tope = mysqli_fetch_array($peticion);
        
                    if($verificador_tope['TOPE'] == 0){
                        $insertar_clase = "INSERT INTO Clase (Tipo_clase, ID_profesor, Cupos, Cinturon, Grado, Dia, Hora, Visibilidad) VALUES ('$TIPO_CLASE', '$ID', '$CUPOS_CLASE', '$CINTURON_CLASE', '$GRADO_CLASE', '$DIA_CLASE', '$HORA_CLASE', 1)";
                        if ($connect->query($insertar_clase) === TRUE) {
                            header('Location: ../vista_profesor_perfil.php?id_profesor='.$ID);
                        }
                    }else{
                        echo '<script type="text/javascript"> alert("Existe un tope de horario, usted no puede crear esta clase"); location="../vista_profesor_perfil.php?id_profesor='.$ID.'"; </script>';
                    }
                }
                if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("15:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("16:00"))){
                    $peticion = mysqli_query($connect, "SELECT COUNT(*) AS TOPE FROM Clase WHERE Hora >= '15:00:00' AND Hora < '16:00:00' AND Visibilidad=1 AND Dia='$DIA_CLASE'");
                    $verificador_tope = mysqli_fetch_array($peticion);
        
                    if($verificador_tope['TOPE'] == 0){
                        $insertar_clase = "INSERT INTO Clase (Tipo_clase, ID_profesor, Cupos, Cinturon, Grado, Dia, Hora, Visibilidad) VALUES ('$TIPO_CLASE', '$ID', '$CUPOS_CLASE', '$CINTURON_CLASE', '$GRADO_CLASE', '$DIA_CLASE', '$HORA_CLASE', 1)";
                        if ($connect->query($insertar_clase) === TRUE) {
                            header('Location: ../vista_profesor_perfil.php?id_profesor='.$ID);
                        }
                    }else{
                        echo '<script type="text/javascript"> alert("Existe un tope de horario, usted no puede crear esta clase"); location="../vista_profesor_perfil.php?id_profesor='.$ID.'"; </script>';
                    }
                }
                if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("16:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("17:00"))){
                    $peticion = mysqli_query($connect, "SELECT COUNT(*) AS TOPE FROM Clase WHERE Hora >= '16:00:00' AND Hora < '17:00:00' AND Visibilidad=1 AND Dia='$DIA_CLASE'");
                    $verificador_tope = mysqli_fetch_array($peticion);
        
                    if($verificador_tope['TOPE'] == 0){
                        $insertar_clase = "INSERT INTO Clase (Tipo_clase, ID_profesor, Cupos, Cinturon, Grado, Dia, Hora, Visibilidad) VALUES ('$TIPO_CLASE', '$ID', '$CUPOS_CLASE', '$CINTURON_CLASE', '$GRADO_CLASE', '$DIA_CLASE', '$HORA_CLASE', 1)";
                        if ($connect->query($insertar_clase) === TRUE) {
                            header('Location: ../vista_profesor_perfil.php?id_profesor='.$ID);
                        }
                    }else{
                        echo '<script type="text/javascript"> alert("Existe un tope de horario, usted no puede crear esta clase"); location="../vista_profesor_perfil.php?id_profesor='.$ID.'"; </script>';
                    }
                }
                if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("17:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("18:00"))){
                    $peticion = mysqli_query($connect, "SELECT COUNT(*) AS TOPE FROM Clase WHERE Hora >= '17:00:00' AND Hora < '18:00:00' AND Visibilidad=1 AND Dia='$DIA_CLASE'");
                    $verificador_tope = mysqli_fetch_array($peticion);
        
                    if($verificador_tope['TOPE'] == 0){
                        $insertar_clase = "INSERT INTO Clase (Tipo_clase, ID_profesor, Cupos, Cinturon, Grado, Dia, Hora, Visibilidad) VALUES ('$TIPO_CLASE', '$ID', '$CUPOS_CLASE', '$CINTURON_CLASE', '$GRADO_CLASE', '$DIA_CLASE', '$HORA_CLASE', 1)";
                        if ($connect->query($insertar_clase) === TRUE) {
                            header('Location: ../vista_profesor_perfil.php?id_profesor='.$ID);
                        }
                    }else{
                        echo '<script type="text/javascript"> alert("Existe un tope de horario, usted no puede crear esta clase"); location="../vista_profesor_perfil.php?id_profesor='.$ID.'"; </script>';
                    }
                }
                if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("18:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("19:00"))){
                    $peticion = mysqli_query($connect, "SELECT COUNT(*) AS TOPE FROM Clase WHERE Hora >= '18:00:00' AND Hora < '19:00:00' AND Visibilidad=1 AND Dia='$DIA_CLASE'");
                    $verificador_tope = mysqli_fetch_array($peticion);
        
                    if($verificador_tope['TOPE'] == 0){
                        $insertar_clase = "INSERT INTO Clase (Tipo_clase, ID_profesor, Cupos, Cinturon, Grado, Dia, Hora, Visibilidad) VALUES ('$TIPO_CLASE', '$ID', '$CUPOS_CLASE', '$CINTURON_CLASE', '$GRADO_CLASE', '$DIA_CLASE', '$HORA_CLASE', 1)";
                        if ($connect->query($insertar_clase) === TRUE) {
                            header('Location: ../vista_profesor_perfil.php?id_profesor='.$ID);
                        }
                    }else{
                        echo '<script type="text/javascript"> alert("Existe un tope de horario, usted no puede crear esta clase"); location="../vista_profesor_perfil.php?id_profesor='.$ID.'"; </script>';
                    }
                }
                if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("19:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("20:00"))){
                    $peticion = mysqli_query($connect, "SELECT COUNT(*) AS TOPE FROM Clase WHERE Hora >= '19:00:00' AND Hora < '20:00:00' AND Visibilidad=1 AND Dia='$DIA_CLASE'");
                    $verificador_tope = mysqli_fetch_array($peticion);
        
                    if($verificador_tope['TOPE'] == 0){
                        $insertar_clase = "INSERT INTO Clase (Tipo_clase, ID_profesor, Cupos, Cinturon, Grado, Dia, Hora, Visibilidad) VALUES ('$TIPO_CLASE', '$ID', '$CUPOS_CLASE', '$CINTURON_CLASE', '$GRADO_CLASE', '$DIA_CLASE', '$HORA_CLASE', 1)";
                        if ($connect->query($insertar_clase) === TRUE) {
                            header('Location: ../vista_profesor_perfil.php?id_profesor='.$ID);
                        }
                    }else{
                        echo '<script type="text/javascript"> alert("Existe un tope de horario, usted no puede crear esta clase"); location="../vista_profesor_perfil.php?id_profesor='.$ID.'"; </script>';
                    }
                }
                if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("20:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("21:00"))){
                    $peticion = mysqli_query($connect, "SELECT COUNT(*) AS TOPE FROM Clase WHERE Hora >= '20:00:00' AND Hora < '21:00:00' AND Visibilidad=1 AND Dia='$DIA_CLASE'");
                    $verificador_tope = mysqli_fetch_array($peticion);
        
                    if($verificador_tope['TOPE'] == 0){
                        $insertar_clase = "INSERT INTO Clase (Tipo_clase, ID_profesor, Cupos, Cinturon, Grado, Dia, Hora, Visibilidad) VALUES ('$TIPO_CLASE', '$ID', '$CUPOS_CLASE', '$CINTURON_CLASE', '$GRADO_CLASE', '$DIA_CLASE', '$HORA_CLASE', 1)";
                        if ($connect->query($insertar_clase) === TRUE) {
                            header('Location: ../vista_profesor_perfil.php?id_profesor='.$ID);
                        }
                    }else{
                        echo '<script type="text/javascript"> alert("Existe un tope de horario, usted no puede crear esta clase"); location="../vista_profesor_perfil.php?id_profesor='.$ID.'"; </script>';
                    }
                }
                if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("21:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("22:00"))){
                    $peticion = mysqli_query($connect, "SELECT COUNT(*) AS TOPE FROM Clase WHERE Hora >= '21:00:00' AND Hora < '22:00:00' AND Visibilidad=1 AND Dia='$DIA_CLASE'");
                    $verificador_tope = mysqli_fetch_array($peticion);
        
                    if($verificador_tope['TOPE'] == 0){
                        $insertar_clase = "INSERT INTO Clase (Tipo_clase, ID_profesor, Cupos, Cinturon, Grado, Dia, Hora, Visibilidad) VALUES ('$TIPO_CLASE', '$ID', '$CUPOS_CLASE', '$CINTURON_CLASE', '$GRADO_CLASE', '$DIA_CLASE', '$HORA_CLASE', 1)";
                        if ($connect->query($insertar_clase) === TRUE) {
                            header('Location: ../vista_profesor_perfil.php?id_profesor='.$ID);
                        }
                    }else{
                        echo '<script type="text/javascript"> alert("Existe un tope de horario, usted no puede crear esta clase"); location="../vista_profesor_perfil.php?id_profesor='.$ID.'"; </script>';
                    }
                }
                break;
            case 4:
                if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("7:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("8:00"))){
                    $peticion = mysqli_query($connect, "SELECT COUNT(*) AS TOPE FROM Clase WHERE Hora >= '7:00:00' AND Hora < '8:00:00' AND Visibilidad=1 AND Dia='$DIA_CLASE'");
                    $verificador_tope = mysqli_fetch_array($peticion);
        
                    if($verificador_tope['TOPE'] == 0){
                        $insertar_clase = "INSERT INTO Clase (Tipo_clase, ID_profesor, Cupos, Cinturon, Grado, Dia, Hora, Visibilidad) VALUES ('$TIPO_CLASE', '$ID', '$CUPOS_CLASE', '$CINTURON_CLASE', '$GRADO_CLASE', '$DIA_CLASE', '$HORA_CLASE', 1)";
                        if ($connect->query($insertar_clase) === TRUE) {
                            header('Location: ../vista_profesor_perfil.php?id_profesor='.$ID);
                        }
                    }else{
                        echo '<script type="text/javascript"> alert("Existe un tope de horario, usted no puede crear esta clase"); location="../vista_profesor_perfil.php?id_profesor='.$ID.'"; </script>';
                    }
                }
                if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("8:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("9:00"))){
                    $peticion = mysqli_query($connect, "SELECT COUNT(*) AS TOPE FROM Clase WHERE Hora >= '8:00:00' AND Hora < '9:00:00' AND Visibilidad=1 AND Dia='$DIA_CLASE'");
                    $verificador_tope = mysqli_fetch_array($peticion);
        
                    if($verificador_tope['TOPE'] == 0){
                        $insertar_clase = "INSERT INTO Clase (Tipo_clase, ID_profesor, Cupos, Cinturon, Grado, Dia, Hora, Visibilidad) VALUES ('$TIPO_CLASE', '$ID', '$CUPOS_CLASE', '$CINTURON_CLASE', '$GRADO_CLASE', '$DIA_CLASE', '$HORA_CLASE', 1)";
                        if ($connect->query($insertar_clase) === TRUE) {
                            header('Location: ../vista_profesor_perfil.php?id_profesor='.$ID);
                        }
                    }else{
                        echo '<script type="text/javascript"> alert("Existe un tope de horario, usted no puede crear esta clase"); location="../vista_profesor_perfil.php?id_profesor='.$ID.'"; </script>';
                    }
                }
                if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("9:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("10:00"))){
                    $peticion = mysqli_query($connect, "SELECT COUNT(*) AS TOPE FROM Clase WHERE Hora >= '9:00:00' AND Hora < '10:00:00' AND Visibilidad=1 AND Dia='$DIA_CLASE'");
                    $verificador_tope = mysqli_fetch_array($peticion);
        
                    if($verificador_tope['TOPE'] == 0){
                        $insertar_clase = "INSERT INTO Clase (Tipo_clase, ID_profesor, Cupos, Cinturon, Grado, Dia, Hora, Visibilidad) VALUES ('$TIPO_CLASE', '$ID', '$CUPOS_CLASE', '$CINTURON_CLASE', '$GRADO_CLASE', '$DIA_CLASE', '$HORA_CLASE', 1)";
                        if ($connect->query($insertar_clase) === TRUE) {
                            header('Location: ../vista_profesor_perfil.php?id_profesor='.$ID);
                        }
                    }else{
                        echo '<script type="text/javascript"> alert("Existe un tope de horario, usted no puede crear esta clase"); location="../vista_profesor_perfil.php?id_profesor='.$ID.'"; </script>';
                    }
                }
                if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("10:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("11:00"))){
                    $peticion = mysqli_query($connect, "SELECT COUNT(*) AS TOPE FROM Clase WHERE Hora >= '10:00:00' AND Hora < '11:00:00' AND Visibilidad=1 AND Dia='$DIA_CLASE'");
                    $verificador_tope = mysqli_fetch_array($peticion);
        
                    if($verificador_tope['TOPE'] == 0){
                        $insertar_clase = "INSERT INTO Clase (Tipo_clase, ID_profesor, Cupos, Cinturon, Grado, Dia, Hora, Visibilidad) VALUES ('$TIPO_CLASE', '$ID', '$CUPOS_CLASE', '$CINTURON_CLASE', '$GRADO_CLASE', '$DIA_CLASE', '$HORA_CLASE', 1)";
                        if ($connect->query($insertar_clase) === TRUE) {
                            header('Location: ../vista_profesor_perfil.php?id_profesor='.$ID);
                        }
                    }else{
                        echo '<script type="text/javascript"> alert("Existe un tope de horario, usted no puede crear esta clase"); location="../vista_profesor_perfil.php?id_profesor='.$ID.'"; </script>';
                    }
                }
                if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("11:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("12:00"))){
                    $peticion = mysqli_query($connect, "SELECT COUNT(*) AS TOPE FROM Clase WHERE Hora >= '11:00:00' AND Hora < '12:00:00' AND Visibilidad=1 AND Dia='$DIA_CLASE'");
                    $verificador_tope = mysqli_fetch_array($peticion);
        
                    if($verificador_tope['TOPE'] == 0){
                        $insertar_clase = "INSERT INTO Clase (Tipo_clase, ID_profesor, Cupos, Cinturon, Grado, Dia, Hora, Visibilidad) VALUES ('$TIPO_CLASE', '$ID', '$CUPOS_CLASE', '$CINTURON_CLASE', '$GRADO_CLASE', '$DIA_CLASE', '$HORA_CLASE', 1)";
                        if ($connect->query($insertar_clase) === TRUE) {
                            header('Location: ../vista_profesor_perfil.php?id_profesor='.$ID);
                        }
                    }else{
                        echo '<script type="text/javascript"> alert("Existe un tope de horario, usted no puede crear esta clase"); location="../vista_profesor_perfil.php?id_profesor='.$ID.'"; </script>';
                    }
                }
                if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("12:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("13:00"))){
                    $peticion = mysqli_query($connect, "SELECT COUNT(*) AS TOPE FROM Clase WHERE Hora >= '12:00:00' AND Hora < '13:00:00' AND Visibilidad=1 AND Dia='$DIA_CLASE'");
                    $verificador_tope = mysqli_fetch_array($peticion);
        
                    if($verificador_tope['TOPE'] == 0){
                        $insertar_clase = "INSERT INTO Clase (Tipo_clase, ID_profesor, Cupos, Cinturon, Grado, Dia, Hora, Visibilidad) VALUES ('$TIPO_CLASE', '$ID', '$CUPOS_CLASE', '$CINTURON_CLASE', '$GRADO_CLASE', '$DIA_CLASE', '$HORA_CLASE', 1)";
                        if ($connect->query($insertar_clase) === TRUE) {
                            header('Location: ../vista_profesor_perfil.php?id_profesor='.$ID);
                        }
                    }else{
                        echo '<script type="text/javascript"> alert("Existe un tope de horario, usted no puede crear esta clase"); location="../vista_profesor_perfil.php?id_profesor='.$ID.'"; </script>';
                    }
                }
                if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("13:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("14:00"))){
                    $peticion = mysqli_query($connect, "SELECT COUNT(*) AS TOPE FROM Clase WHERE Hora >= '13:00:00' AND Hora < '14:00:00' AND Visibilidad=1 AND Dia='$DIA_CLASE'");
                    $verificador_tope = mysqli_fetch_array($peticion);
        
                    if($verificador_tope['TOPE'] == 0){
                        $insertar_clase = "INSERT INTO Clase (Tipo_clase, ID_profesor, Cupos, Cinturon, Grado, Dia, Hora, Visibilidad) VALUES ('$TIPO_CLASE', '$ID', '$CUPOS_CLASE', '$CINTURON_CLASE', '$GRADO_CLASE', '$DIA_CLASE', '$HORA_CLASE', 1)";
                        if ($connect->query($insertar_clase) === TRUE) {
                            header('Location: ../vista_profesor_perfil.php?id_profesor='.$ID);
                        }
                    }else{
                        echo '<script type="text/javascript"> alert("Existe un tope de horario, usted no puede crear esta clase"); location="../vista_profesor_perfil.php?id_profesor='.$ID.'"; </script>';
                    }
                }
                if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("14:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("15:00"))){
                    $peticion = mysqli_query($connect, "SELECT COUNT(*) AS TOPE FROM Clase WHERE Hora >= '14:00:00' AND Hora < '15:00:00' AND Visibilidad=1 AND Dia='$DIA_CLASE'");
                    $verificador_tope = mysqli_fetch_array($peticion);
        
                    if($verificador_tope['TOPE'] == 0){
                        $insertar_clase = "INSERT INTO Clase (Tipo_clase, ID_profesor, Cupos, Cinturon, Grado, Dia, Hora, Visibilidad) VALUES ('$TIPO_CLASE', '$ID', '$CUPOS_CLASE', '$CINTURON_CLASE', '$GRADO_CLASE', '$DIA_CLASE', '$HORA_CLASE', 1)";
                        if ($connect->query($insertar_clase) === TRUE) {
                            header('Location: ../vista_profesor_perfil.php?id_profesor='.$ID);
                        }
                    }else{
                        echo '<script type="text/javascript"> alert("Existe un tope de horario, usted no puede crear esta clase"); location="../vista_profesor_perfil.php?id_profesor='.$ID.'"; </script>';
                    }
                }
                if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("15:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("16:00"))){
                    $peticion = mysqli_query($connect, "SELECT COUNT(*) AS TOPE FROM Clase WHERE Hora >= '15:00:00' AND Hora < '16:00:00' AND Visibilidad=1 AND Dia='$DIA_CLASE'");
                    $verificador_tope = mysqli_fetch_array($peticion);
        
                    if($verificador_tope['TOPE'] == 0){
                        $insertar_clase = "INSERT INTO Clase (Tipo_clase, ID_profesor, Cupos, Cinturon, Grado, Dia, Hora, Visibilidad) VALUES ('$TIPO_CLASE', '$ID', '$CUPOS_CLASE', '$CINTURON_CLASE', '$GRADO_CLASE', '$DIA_CLASE', '$HORA_CLASE', 1)";
                        if ($connect->query($insertar_clase) === TRUE) {
                            header('Location: ../vista_profesor_perfil.php?id_profesor='.$ID);
                        }
                    }else{
                        echo '<script type="text/javascript"> alert("Existe un tope de horario, usted no puede crear esta clase"); location="../vista_profesor_perfil.php?id_profesor='.$ID.'"; </script>';
                    }
                }
                if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("16:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("17:00"))){
                    $peticion = mysqli_query($connect, "SELECT COUNT(*) AS TOPE FROM Clase WHERE Hora >= '16:00:00' AND Hora < '17:00:00' AND Visibilidad=1 AND Dia='$DIA_CLASE'");
                    $verificador_tope = mysqli_fetch_array($peticion);
        
                    if($verificador_tope['TOPE'] == 0){
                        $insertar_clase = "INSERT INTO Clase (Tipo_clase, ID_profesor, Cupos, Cinturon, Grado, Dia, Hora, Visibilidad) VALUES ('$TIPO_CLASE', '$ID', '$CUPOS_CLASE', '$CINTURON_CLASE', '$GRADO_CLASE', '$DIA_CLASE', '$HORA_CLASE', 1)";
                        if ($connect->query($insertar_clase) === TRUE) {
                            header('Location: ../vista_profesor_perfil.php?id_profesor='.$ID);
                        }
                    }else{
                        echo '<script type="text/javascript"> alert("Existe un tope de horario, usted no puede crear esta clase"); location="../vista_profesor_perfil.php?id_profesor='.$ID.'"; </script>';
                    }
                }
                if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("17:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("18:00"))){
                    $peticion = mysqli_query($connect, "SELECT COUNT(*) AS TOPE FROM Clase WHERE Hora >= '17:00:00' AND Hora < '18:00:00' AND Visibilidad=1 AND Dia='$DIA_CLASE'");
                    $verificador_tope = mysqli_fetch_array($peticion);
        
                    if($verificador_tope['TOPE'] == 0){
                        $insertar_clase = "INSERT INTO Clase (Tipo_clase, ID_profesor, Cupos, Cinturon, Grado, Dia, Hora, Visibilidad) VALUES ('$TIPO_CLASE', '$ID', '$CUPOS_CLASE', '$CINTURON_CLASE', '$GRADO_CLASE', '$DIA_CLASE', '$HORA_CLASE', 1)";
                        if ($connect->query($insertar_clase) === TRUE) {
                            header('Location: ../vista_profesor_perfil.php?id_profesor='.$ID);
                        }
                    }else{
                        echo '<script type="text/javascript"> alert("Existe un tope de horario, usted no puede crear esta clase"); location="../vista_profesor_perfil.php?id_profesor='.$ID.'"; </script>';
                    }
                }
                if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("18:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("19:00"))){
                    $peticion = mysqli_query($connect, "SELECT COUNT(*) AS TOPE FROM Clase WHERE Hora >= '18:00:00' AND Hora < '19:00:00' AND Visibilidad=1 AND Dia='$DIA_CLASE'");
                    $verificador_tope = mysqli_fetch_array($peticion);
        
                    if($verificador_tope['TOPE'] == 0){
                        $insertar_clase = "INSERT INTO Clase (Tipo_clase, ID_profesor, Cupos, Cinturon, Grado, Dia, Hora, Visibilidad) VALUES ('$TIPO_CLASE', '$ID', '$CUPOS_CLASE', '$CINTURON_CLASE', '$GRADO_CLASE', '$DIA_CLASE', '$HORA_CLASE', 1)";
                        if ($connect->query($insertar_clase) === TRUE) {
                            header('Location: ../vista_profesor_perfil.php?id_profesor='.$ID);
                        }
                    }else{
                        echo '<script type="text/javascript"> alert("Existe un tope de horario, usted no puede crear esta clase"); location="../vista_profesor_perfil.php?id_profesor='.$ID.'"; </script>';
                    }
                }
                if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("19:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("20:00"))){
                    $peticion = mysqli_query($connect, "SELECT COUNT(*) AS TOPE FROM Clase WHERE Hora >= '19:00:00' AND Hora < '20:00:00' AND Visibilidad=1 AND Dia='$DIA_CLASE'");
                    $verificador_tope = mysqli_fetch_array($peticion);
        
                    if($verificador_tope['TOPE'] == 0){
                        $insertar_clase = "INSERT INTO Clase (Tipo_clase, ID_profesor, Cupos, Cinturon, Grado, Dia, Hora, Visibilidad) VALUES ('$TIPO_CLASE', '$ID', '$CUPOS_CLASE', '$CINTURON_CLASE', '$GRADO_CLASE', '$DIA_CLASE', '$HORA_CLASE', 1)";
                        if ($connect->query($insertar_clase) === TRUE) {
                            header('Location: ../vista_profesor_perfil.php?id_profesor='.$ID);
                        }
                    }else{
                        echo '<script type="text/javascript"> alert("Existe un tope de horario, usted no puede crear esta clase"); location="../vista_profesor_perfil.php?id_profesor='.$ID.'"; </script>';
                    }
                }
                if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("20:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("21:00"))){
                    $peticion = mysqli_query($connect, "SELECT COUNT(*) AS TOPE FROM Clase WHERE Hora >= '20:00:00' AND Hora < '21:00:00' AND Visibilidad=1 AND Dia='$DIA_CLASE'");
                    $verificador_tope = mysqli_fetch_array($peticion);
        
                    if($verificador_tope['TOPE'] == 0){
                        $insertar_clase = "INSERT INTO Clase (Tipo_clase, ID_profesor, Cupos, Cinturon, Grado, Dia, Hora, Visibilidad) VALUES ('$TIPO_CLASE', '$ID', '$CUPOS_CLASE', '$CINTURON_CLASE', '$GRADO_CLASE', '$DIA_CLASE', '$HORA_CLASE', 1)";
                        if ($connect->query($insertar_clase) === TRUE) {
                            header('Location: ../vista_profesor_perfil.php?id_profesor='.$ID);
                        }
                    }else{
                        echo '<script type="text/javascript"> alert("Existe un tope de horario, usted no puede crear esta clase"); location="../vista_profesor_perfil.php?id_profesor='.$ID.'"; </script>';
                    }
                }
                if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("21:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("22:00"))){
                    $peticion = mysqli_query($connect, "SELECT COUNT(*) AS TOPE FROM Clase WHERE Hora >= '21:00:00' AND Hora < '22:00:00' AND Visibilidad=1 AND Dia='$DIA_CLASE'");
                    $verificador_tope = mysqli_fetch_array($peticion);
        
                    if($verificador_tope['TOPE'] == 0){
                        $insertar_clase = "INSERT INTO Clase (Tipo_clase, ID_profesor, Cupos, Cinturon, Grado, Dia, Hora, Visibilidad) VALUES ('$TIPO_CLASE', '$ID', '$CUPOS_CLASE', '$CINTURON_CLASE', '$GRADO_CLASE', '$DIA_CLASE', '$HORA_CLASE', 1)";
                        if ($connect->query($insertar_clase) === TRUE) {
                            header('Location: ../vista_profesor_perfil.php?id_profesor='.$ID);
                        }
                    }else{
                        echo '<script type="text/javascript"> alert("Existe un tope de horario, usted no puede crear esta clase"); location="../vista_profesor_perfil.php?id_profesor='.$ID.'"; </script>';
                    }
                }
                break;
            case 5:
                if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("7:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("8:00"))){
                    $peticion = mysqli_query($connect, "SELECT COUNT(*) AS TOPE FROM Clase WHERE Hora >= '7:00:00' AND Hora < '8:00:00' AND Visibilidad=1 AND Dia='$DIA_CLASE'");
                    $verificador_tope = mysqli_fetch_array($peticion);
        
                    if($verificador_tope['TOPE'] == 0){
                        $insertar_clase = "INSERT INTO Clase (Tipo_clase, ID_profesor, Cupos, Cinturon, Grado, Dia, Hora, Visibilidad) VALUES ('$TIPO_CLASE', '$ID', '$CUPOS_CLASE', '$CINTURON_CLASE', '$GRADO_CLASE', '$DIA_CLASE', '$HORA_CLASE', 1)";
                        if ($connect->query($insertar_clase) === TRUE) {
                            header('Location: ../vista_profesor_perfil.php?id_profesor='.$ID);
                        }
                    }else{
                        echo '<script type="text/javascript"> alert("Existe un tope de horario, usted no puede crear esta clase"); location="../vista_profesor_perfil.php?id_profesor='.$ID.'"; </script>';
                    }
                }
                if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("8:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("9:00"))){
                    $peticion = mysqli_query($connect, "SELECT COUNT(*) AS TOPE FROM Clase WHERE Hora >= '8:00:00' AND Hora < '9:00:00' AND Visibilidad=1 AND Dia='$DIA_CLASE'");
                    $verificador_tope = mysqli_fetch_array($peticion);
        
                    if($verificador_tope['TOPE'] == 0){
                        $insertar_clase = "INSERT INTO Clase (Tipo_clase, ID_profesor, Cupos, Cinturon, Grado, Dia, Hora, Visibilidad) VALUES ('$TIPO_CLASE', '$ID', '$CUPOS_CLASE', '$CINTURON_CLASE', '$GRADO_CLASE', '$DIA_CLASE', '$HORA_CLASE', 1)";
                        if ($connect->query($insertar_clase) === TRUE) {
                            header('Location: ../vista_profesor_perfil.php?id_profesor='.$ID);
                        }
                    }else{
                        echo '<script type="text/javascript"> alert("Existe un tope de horario, usted no puede crear esta clase"); location="../vista_profesor_perfil.php?id_profesor='.$ID.'"; </script>';
                    }
                }
                if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("9:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("10:00"))){
                    $peticion = mysqli_query($connect, "SELECT COUNT(*) AS TOPE FROM Clase WHERE Hora >= '9:00:00' AND Hora < '10:00:00' AND Visibilidad=1 AND Dia='$DIA_CLASE'");
                    $verificador_tope = mysqli_fetch_array($peticion);
        
                    if($verificador_tope['TOPE'] == 0){
                        $insertar_clase = "INSERT INTO Clase (Tipo_clase, ID_profesor, Cupos, Cinturon, Grado, Dia, Hora, Visibilidad) VALUES ('$TIPO_CLASE', '$ID', '$CUPOS_CLASE', '$CINTURON_CLASE', '$GRADO_CLASE', '$DIA_CLASE', '$HORA_CLASE', 1)";
                        if ($connect->query($insertar_clase) === TRUE) {
                            header('Location: ../vista_profesor_perfil.php?id_profesor='.$ID);
                        }
                    }else{
                        echo '<script type="text/javascript"> alert("Existe un tope de horario, usted no puede crear esta clase"); location="../vista_profesor_perfil.php?id_profesor='.$ID.'"; </script>';
                    }
                }
                if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("10:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("11:00"))){
                    $peticion = mysqli_query($connect, "SELECT COUNT(*) AS TOPE FROM Clase WHERE Hora >= '10:00:00' AND Hora < '11:00:00' AND Visibilidad=1 AND Dia='$DIA_CLASE'");
                    $verificador_tope = mysqli_fetch_array($peticion);
        
                    if($verificador_tope['TOPE'] == 0){
                        $insertar_clase = "INSERT INTO Clase (Tipo_clase, ID_profesor, Cupos, Cinturon, Grado, Dia, Hora, Visibilidad) VALUES ('$TIPO_CLASE', '$ID', '$CUPOS_CLASE', '$CINTURON_CLASE', '$GRADO_CLASE', '$DIA_CLASE', '$HORA_CLASE', 1)";
                        if ($connect->query($insertar_clase) === TRUE) {
                            header('Location: ../vista_profesor_perfil.php?id_profesor='.$ID);
                        }
                    }else{
                        echo '<script type="text/javascript"> alert("Existe un tope de horario, usted no puede crear esta clase"); location="../vista_profesor_perfil.php?id_profesor='.$ID.'"; </script>';
                    }
                }
                if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("11:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("12:00"))){
                    $peticion = mysqli_query($connect, "SELECT COUNT(*) AS TOPE FROM Clase WHERE Hora >= '11:00:00' AND Hora < '12:00:00' AND Visibilidad=1 AND Dia='$DIA_CLASE'");
                    $verificador_tope = mysqli_fetch_array($peticion);
        
                    if($verificador_tope['TOPE'] == 0){
                        $insertar_clase = "INSERT INTO Clase (Tipo_clase, ID_profesor, Cupos, Cinturon, Grado, Dia, Hora, Visibilidad) VALUES ('$TIPO_CLASE', '$ID', '$CUPOS_CLASE', '$CINTURON_CLASE', '$GRADO_CLASE', '$DIA_CLASE', '$HORA_CLASE', 1)";
                        if ($connect->query($insertar_clase) === TRUE) {
                            header('Location: ../vista_profesor_perfil.php?id_profesor='.$ID);
                        }
                    }else{
                        echo '<script type="text/javascript"> alert("Existe un tope de horario, usted no puede crear esta clase"); location="../vista_profesor_perfil.php?id_profesor='.$ID.'"; </script>';
                    }
                }
                if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("12:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("13:00"))){
                    $peticion = mysqli_query($connect, "SELECT COUNT(*) AS TOPE FROM Clase WHERE Hora >= '12:00:00' AND Hora < '13:00:00' AND Visibilidad=1 AND Dia='$DIA_CLASE'");
                    $verificador_tope = mysqli_fetch_array($peticion);
        
                    if($verificador_tope['TOPE'] == 0){
                        $insertar_clase = "INSERT INTO Clase (Tipo_clase, ID_profesor, Cupos, Cinturon, Grado, Dia, Hora, Visibilidad) VALUES ('$TIPO_CLASE', '$ID', '$CUPOS_CLASE', '$CINTURON_CLASE', '$GRADO_CLASE', '$DIA_CLASE', '$HORA_CLASE', 1)";
                        if ($connect->query($insertar_clase) === TRUE) {
                            header('Location: ../vista_profesor_perfil.php?id_profesor='.$ID);
                        }
                    }else{
                        echo '<script type="text/javascript"> alert("Existe un tope de horario, usted no puede crear esta clase"); location="../vista_profesor_perfil.php?id_profesor='.$ID.'"; </script>';
                    }
                }
                if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("13:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("14:00"))){
                    $peticion = mysqli_query($connect, "SELECT COUNT(*) AS TOPE FROM Clase WHERE Hora >= '13:00:00' AND Hora < '14:00:00' AND Visibilidad=1 AND Dia='$DIA_CLASE'");
                    $verificador_tope = mysqli_fetch_array($peticion);
        
                    if($verificador_tope['TOPE'] == 0){
                        $insertar_clase = "INSERT INTO Clase (Tipo_clase, ID_profesor, Cupos, Cinturon, Grado, Dia, Hora, Visibilidad) VALUES ('$TIPO_CLASE', '$ID', '$CUPOS_CLASE', '$CINTURON_CLASE', '$GRADO_CLASE', '$DIA_CLASE', '$HORA_CLASE', 1)";
                        if ($connect->query($insertar_clase) === TRUE) {
                            header('Location: ../vista_profesor_perfil.php?id_profesor='.$ID);
                        }
                    }else{
                        echo '<script type="text/javascript"> alert("Existe un tope de horario, usted no puede crear esta clase"); location="../vista_profesor_perfil.php?id_profesor='.$ID.'"; </script>';
                    }
                }
                if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("14:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("15:00"))){
                    $peticion = mysqli_query($connect, "SELECT COUNT(*) AS TOPE FROM Clase WHERE Hora >= '14:00:00' AND Hora < '15:00:00' AND Visibilidad=1 AND Dia='$DIA_CLASE'");
                    $verificador_tope = mysqli_fetch_array($peticion);
        
                    if($verificador_tope['TOPE'] == 0){
                        $insertar_clase = "INSERT INTO Clase (Tipo_clase, ID_profesor, Cupos, Cinturon, Grado, Dia, Hora, Visibilidad) VALUES ('$TIPO_CLASE', '$ID', '$CUPOS_CLASE', '$CINTURON_CLASE', '$GRADO_CLASE', '$DIA_CLASE', '$HORA_CLASE', 1)";
                        if ($connect->query($insertar_clase) === TRUE) {
                            header('Location: ../vista_profesor_perfil.php?id_profesor='.$ID);
                        }
                    }else{
                        echo '<script type="text/javascript"> alert("Existe un tope de horario, usted no puede crear esta clase"); location="../vista_profesor_perfil.php?id_profesor='.$ID.'"; </script>';
                    }
                }
                if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("15:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("16:00"))){
                    $peticion = mysqli_query($connect, "SELECT COUNT(*) AS TOPE FROM Clase WHERE Hora >= '15:00:00' AND Hora < '16:00:00' AND Visibilidad=1 AND Dia='$DIA_CLASE'");
                    $verificador_tope = mysqli_fetch_array($peticion);
        
                    if($verificador_tope['TOPE'] == 0){
                        $insertar_clase = "INSERT INTO Clase (Tipo_clase, ID_profesor, Cupos, Cinturon, Grado, Dia, Hora, Visibilidad) VALUES ('$TIPO_CLASE', '$ID', '$CUPOS_CLASE', '$CINTURON_CLASE', '$GRADO_CLASE', '$DIA_CLASE', '$HORA_CLASE', 1)";
                        if ($connect->query($insertar_clase) === TRUE) {
                            header('Location: ../vista_profesor_perfil.php?id_profesor='.$ID);
                        }
                    }else{
                        echo '<script type="text/javascript"> alert("Existe un tope de horario, usted no puede crear esta clase"); location="../vista_profesor_perfil.php?id_profesor='.$ID.'"; </script>';
                    }
                }
                if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("16:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("17:00"))){
                    $peticion = mysqli_query($connect, "SELECT COUNT(*) AS TOPE FROM Clase WHERE Hora >= '16:00:00' AND Hora < '17:00:00' AND Visibilidad=1 AND Dia='$DIA_CLASE'");
                    $verificador_tope = mysqli_fetch_array($peticion);
        
                    if($verificador_tope['TOPE'] == 0){
                        $insertar_clase = "INSERT INTO Clase (Tipo_clase, ID_profesor, Cupos, Cinturon, Grado, Dia, Hora, Visibilidad) VALUES ('$TIPO_CLASE', '$ID', '$CUPOS_CLASE', '$CINTURON_CLASE', '$GRADO_CLASE', '$DIA_CLASE', '$HORA_CLASE', 1)";
                        if ($connect->query($insertar_clase) === TRUE) {
                            header('Location: ../vista_profesor_perfil.php?id_profesor='.$ID);
                        }
                    }else{
                        echo '<script type="text/javascript"> alert("Existe un tope de horario, usted no puede crear esta clase"); location="../vista_profesor_perfil.php?id_profesor='.$ID.'"; </script>';
                    }
                }
                if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("17:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("18:00"))){
                    $peticion = mysqli_query($connect, "SELECT COUNT(*) AS TOPE FROM Clase WHERE Hora >= '17:00:00' AND Hora < '18:00:00' AND Visibilidad=1 AND Dia='$DIA_CLASE'");
                    $verificador_tope = mysqli_fetch_array($peticion);
        
                    if($verificador_tope['TOPE'] == 0){
                        $insertar_clase = "INSERT INTO Clase (Tipo_clase, ID_profesor, Cupos, Cinturon, Grado, Dia, Hora, Visibilidad) VALUES ('$TIPO_CLASE', '$ID', '$CUPOS_CLASE', '$CINTURON_CLASE', '$GRADO_CLASE', '$DIA_CLASE', '$HORA_CLASE', 1)";
                        if ($connect->query($insertar_clase) === TRUE) {
                            header('Location: ../vista_profesor_perfil.php?id_profesor='.$ID);
                        }
                    }else{
                        echo '<script type="text/javascript"> alert("Existe un tope de horario, usted no puede crear esta clase"); location="../vista_profesor_perfil.php?id_profesor='.$ID.'"; </script>';
                    }
                }
                if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("18:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("19:00"))){
                    $peticion = mysqli_query($connect, "SELECT COUNT(*) AS TOPE FROM Clase WHERE Hora >= '18:00:00' AND Hora < '19:00:00' AND Visibilidad=1 AND Dia='$DIA_CLASE'");
                    $verificador_tope = mysqli_fetch_array($peticion);
        
                    if($verificador_tope['TOPE'] == 0){
                        $insertar_clase = "INSERT INTO Clase (Tipo_clase, ID_profesor, Cupos, Cinturon, Grado, Dia, Hora, Visibilidad) VALUES ('$TIPO_CLASE', '$ID', '$CUPOS_CLASE', '$CINTURON_CLASE', '$GRADO_CLASE', '$DIA_CLASE', '$HORA_CLASE', 1)";
                        if ($connect->query($insertar_clase) === TRUE) {
                            header('Location: ../vista_profesor_perfil.php?id_profesor='.$ID);
                        }
                    }else{
                        echo '<script type="text/javascript"> alert("Existe un tope de horario, usted no puede crear esta clase"); location="../vista_profesor_perfil.php?id_profesor='.$ID.'"; </script>';
                    }
                }
                if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("19:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("20:00"))){
                    $peticion = mysqli_query($connect, "SELECT COUNT(*) AS TOPE FROM Clase WHERE Hora >= '19:00:00' AND Hora < '20:00:00' AND Visibilidad=1 AND Dia='$DIA_CLASE'");
                    $verificador_tope = mysqli_fetch_array($peticion);
        
                    if($verificador_tope['TOPE'] == 0){
                        $insertar_clase = "INSERT INTO Clase (Tipo_clase, ID_profesor, Cupos, Cinturon, Grado, Dia, Hora, Visibilidad) VALUES ('$TIPO_CLASE', '$ID', '$CUPOS_CLASE', '$CINTURON_CLASE', '$GRADO_CLASE', '$DIA_CLASE', '$HORA_CLASE', 1)";
                        if ($connect->query($insertar_clase) === TRUE) {
                            header('Location: ../vista_profesor_perfil.php?id_profesor='.$ID);
                        }
                    }else{
                        echo '<script type="text/javascript"> alert("Existe un tope de horario, usted no puede crear esta clase"); location="../vista_profesor_perfil.php?id_profesor='.$ID.'"; </script>';
                    }
                }
                if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("20:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("21:00"))){
                    $peticion = mysqli_query($connect, "SELECT COUNT(*) AS TOPE FROM Clase WHERE Hora >= '20:00:00' AND Hora < '21:00:00' AND Visibilidad=1 AND Dia='$DIA_CLASE'");
                    $verificador_tope = mysqli_fetch_array($peticion);
        
                    if($verificador_tope['TOPE'] == 0){
                        $insertar_clase = "INSERT INTO Clase (Tipo_clase, ID_profesor, Cupos, Cinturon, Grado, Dia, Hora, Visibilidad) VALUES ('$TIPO_CLASE', '$ID', '$CUPOS_CLASE', '$CINTURON_CLASE', '$GRADO_CLASE', '$DIA_CLASE', '$HORA_CLASE', 1)";
                        if ($connect->query($insertar_clase) === TRUE) {
                            header('Location: ../vista_profesor_perfil.php?id_profesor='.$ID);
                        }
                    }else{
                        echo '<script type="text/javascript"> alert("Existe un tope de horario, usted no puede crear esta clase"); location="../vista_profesor_perfil.php?id_profesor='.$ID.'"; </script>';
                    }
                }
                if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("21:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("22:00"))){
                    $peticion = mysqli_query($connect, "SELECT COUNT(*) AS TOPE FROM Clase WHERE Hora >= '21:00:00' AND Hora < '22:00:00' AND Visibilidad=1 AND Dia='$DIA_CLASE'");
                    $verificador_tope = mysqli_fetch_array($peticion);
        
                    if($verificador_tope['TOPE'] == 0){
                        $insertar_clase = "INSERT INTO Clase (Tipo_clase, ID_profesor, Cupos, Cinturon, Grado, Dia, Hora, Visibilidad) VALUES ('$TIPO_CLASE', '$ID', '$CUPOS_CLASE', '$CINTURON_CLASE', '$GRADO_CLASE', '$DIA_CLASE', '$HORA_CLASE', 1)";
                        if ($connect->query($insertar_clase) === TRUE) {
                            header('Location: ../vista_profesor_perfil.php?id_profesor='.$ID);
                        }
                    }else{
                        echo '<script type="text/javascript"> alert("Existe un tope de horario, usted no puede crear esta clase"); location="../vista_profesor_perfil.php?id_profesor='.$ID.'"; </script>';
                    }
                }
                break;
            case 6:
                if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("7:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("8:00"))){
                    $peticion = mysqli_query($connect, "SELECT COUNT(*) AS TOPE FROM Clase WHERE Hora >= '7:00:00' AND Hora < '8:00:00' AND Visibilidad=1 AND Dia='$DIA_CLASE'");
                    $verificador_tope = mysqli_fetch_array($peticion);
        
                    if($verificador_tope['TOPE'] == 0){
                        $insertar_clase = "INSERT INTO Clase (Tipo_clase, ID_profesor, Cupos, Cinturon, Grado, Dia, Hora, Visibilidad) VALUES ('$TIPO_CLASE', '$ID', '$CUPOS_CLASE', '$CINTURON_CLASE', '$GRADO_CLASE', '$DIA_CLASE', '$HORA_CLASE', 1)";
                        if ($connect->query($insertar_clase) === TRUE) {
                            header('Location: ../vista_profesor_perfil.php?id_profesor='.$ID);
                        }
                    }else{
                        echo '<script type="text/javascript"> alert("Existe un tope de horario, usted no puede crear esta clase"); location="../vista_profesor_perfil.php?id_profesor='.$ID.'"; </script>';
                    }
                }
                if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("8:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("9:00"))){
                    $peticion = mysqli_query($connect, "SELECT COUNT(*) AS TOPE FROM Clase WHERE Hora >= '8:00:00' AND Hora < '9:00:00' AND Visibilidad=1 AND Dia='$DIA_CLASE'");
                    $verificador_tope = mysqli_fetch_array($peticion);
        
                    if($verificador_tope['TOPE'] == 0){
                        $insertar_clase = "INSERT INTO Clase (Tipo_clase, ID_profesor, Cupos, Cinturon, Grado, Dia, Hora, Visibilidad) VALUES ('$TIPO_CLASE', '$ID', '$CUPOS_CLASE', '$CINTURON_CLASE', '$GRADO_CLASE', '$DIA_CLASE', '$HORA_CLASE', 1)";
                        if ($connect->query($insertar_clase) === TRUE) {
                            header('Location: ../vista_profesor_perfil.php?id_profesor='.$ID);
                        }
                    }else{
                        echo '<script type="text/javascript"> alert("Existe un tope de horario, usted no puede crear esta clase"); location="../vista_profesor_perfil.php?id_profesor='.$ID.'"; </script>';
                    }
                }
                if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("9:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("10:00"))){
                    $peticion = mysqli_query($connect, "SELECT COUNT(*) AS TOPE FROM Clase WHERE Hora >= '9:00:00' AND Hora < '10:00:00' AND Visibilidad=1 AND Dia='$DIA_CLASE'");
                    $verificador_tope = mysqli_fetch_array($peticion);
        
                    if($verificador_tope['TOPE'] == 0){
                        $insertar_clase = "INSERT INTO Clase (Tipo_clase, ID_profesor, Cupos, Cinturon, Grado, Dia, Hora, Visibilidad) VALUES ('$TIPO_CLASE', '$ID', '$CUPOS_CLASE', '$CINTURON_CLASE', '$GRADO_CLASE', '$DIA_CLASE', '$HORA_CLASE', 1)";
                        if ($connect->query($insertar_clase) === TRUE) {
                            header('Location: ../vista_profesor_perfil.php?id_profesor='.$ID);
                        }
                    }else{
                        echo '<script type="text/javascript"> alert("Existe un tope de horario, usted no puede crear esta clase"); location="../vista_profesor_perfil.php?id_profesor='.$ID.'"; </script>';
                    }
                }
                if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("10:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("11:00"))){
                    $peticion = mysqli_query($connect, "SELECT COUNT(*) AS TOPE FROM Clase WHERE Hora >= '10:00:00' AND Hora < '11:00:00' AND Visibilidad=1 AND Dia='$DIA_CLASE'");
                    $verificador_tope = mysqli_fetch_array($peticion);
        
                    if($verificador_tope['TOPE'] == 0){
                        $insertar_clase = "INSERT INTO Clase (Tipo_clase, ID_profesor, Cupos, Cinturon, Grado, Dia, Hora, Visibilidad) VALUES ('$TIPO_CLASE', '$ID', '$CUPOS_CLASE', '$CINTURON_CLASE', '$GRADO_CLASE', '$DIA_CLASE', '$HORA_CLASE', 1)";
                        if ($connect->query($insertar_clase) === TRUE) {
                            header('Location: ../vista_profesor_perfil.php?id_profesor='.$ID);
                        }
                    }else{
                        echo '<script type="text/javascript"> alert("Existe un tope de horario, usted no puede crear esta clase"); location="../vista_profesor_perfil.php?id_profesor='.$ID.'"; </script>';
                    }
                }
                if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("11:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("12:00"))){
                    $peticion = mysqli_query($connect, "SELECT COUNT(*) AS TOPE FROM Clase WHERE Hora >= '11:00:00' AND Hora < '12:00:00' AND Visibilidad=1 AND Dia='$DIA_CLASE'");
                    $verificador_tope = mysqli_fetch_array($peticion);
        
                    if($verificador_tope['TOPE'] == 0){
                        $insertar_clase = "INSERT INTO Clase (Tipo_clase, ID_profesor, Cupos, Cinturon, Grado, Dia, Hora, Visibilidad) VALUES ('$TIPO_CLASE', '$ID', '$CUPOS_CLASE', '$CINTURON_CLASE', '$GRADO_CLASE', '$DIA_CLASE', '$HORA_CLASE', 1)";
                        if ($connect->query($insertar_clase) === TRUE) {
                            header('Location: ../vista_profesor_perfil.php?id_profesor='.$ID);
                        }
                    }else{
                        echo '<script type="text/javascript"> alert("Existe un tope de horario, usted no puede crear esta clase"); location="../vista_profesor_perfil.php?id_profesor='.$ID.'"; </script>';
                    }
                }
                if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("12:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("13:00"))){
                    $peticion = mysqli_query($connect, "SELECT COUNT(*) AS TOPE FROM Clase WHERE Hora >= '12:00:00' AND Hora < '13:00:00' AND Visibilidad=1 AND Dia='$DIA_CLASE'");
                    $verificador_tope = mysqli_fetch_array($peticion);
        
                    if($verificador_tope['TOPE'] == 0){
                        $insertar_clase = "INSERT INTO Clase (Tipo_clase, ID_profesor, Cupos, Cinturon, Grado, Dia, Hora, Visibilidad) VALUES ('$TIPO_CLASE', '$ID', '$CUPOS_CLASE', '$CINTURON_CLASE', '$GRADO_CLASE', '$DIA_CLASE', '$HORA_CLASE', 1)";
                        if ($connect->query($insertar_clase) === TRUE) {
                            header('Location: ../vista_profesor_perfil.php?id_profesor='.$ID);
                        }
                    }else{
                        echo '<script type="text/javascript"> alert("Existe un tope de horario, usted no puede crear esta clase"); location="../vista_profesor_perfil.php?id_profesor='.$ID.'"; </script>';
                    }
                }
                if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("13:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("14:00"))){
                    $peticion = mysqli_query($connect, "SELECT COUNT(*) AS TOPE FROM Clase WHERE Hora >= '13:00:00' AND Hora < '14:00:00' AND Visibilidad=1 AND Dia='$DIA_CLASE'");
                    $verificador_tope = mysqli_fetch_array($peticion);
        
                    if($verificador_tope['TOPE'] == 0){
                        $insertar_clase = "INSERT INTO Clase (Tipo_clase, ID_profesor, Cupos, Cinturon, Grado, Dia, Hora, Visibilidad) VALUES ('$TIPO_CLASE', '$ID', '$CUPOS_CLASE', '$CINTURON_CLASE', '$GRADO_CLASE', '$DIA_CLASE', '$HORA_CLASE', 1)";
                        if ($connect->query($insertar_clase) === TRUE) {
                            header('Location: ../vista_profesor_perfil.php?id_profesor='.$ID);
                        }
                    }else{
                        echo '<script type="text/javascript"> alert("Existe un tope de horario, usted no puede crear esta clase"); location="../vista_profesor_perfil.php?id_profesor='.$ID.'"; </script>';
                    }
                }
                if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("14:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("15:00"))){
                    $peticion = mysqli_query($connect, "SELECT COUNT(*) AS TOPE FROM Clase WHERE Hora >= '14:00:00' AND Hora < '15:00:00' AND Visibilidad=1 AND Dia='$DIA_CLASE'");
                    $verificador_tope = mysqli_fetch_array($peticion);
        
                    if($verificador_tope['TOPE'] == 0){
                        $insertar_clase = "INSERT INTO Clase (Tipo_clase, ID_profesor, Cupos, Cinturon, Grado, Dia, Hora, Visibilidad) VALUES ('$TIPO_CLASE', '$ID', '$CUPOS_CLASE', '$CINTURON_CLASE', '$GRADO_CLASE', '$DIA_CLASE', '$HORA_CLASE', 1)";
                        if ($connect->query($insertar_clase) === TRUE) {
                            header('Location: ../vista_profesor_perfil.php?id_profesor='.$ID);
                        }
                    }else{
                        echo '<script type="text/javascript"> alert("Existe un tope de horario, usted no puede crear esta clase"); location="../vista_profesor_perfil.php?id_profesor='.$ID.'"; </script>';
                    }
                }
                if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("15:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("16:00"))){
                    $peticion = mysqli_query($connect, "SELECT COUNT(*) AS TOPE FROM Clase WHERE Hora >= '15:00:00' AND Hora < '16:00:00' AND Visibilidad=1 AND Dia='$DIA_CLASE'");
                    $verificador_tope = mysqli_fetch_array($peticion);
        
                    if($verificador_tope['TOPE'] == 0){
                        $insertar_clase = "INSERT INTO Clase (Tipo_clase, ID_profesor, Cupos, Cinturon, Grado, Dia, Hora, Visibilidad) VALUES ('$TIPO_CLASE', '$ID', '$CUPOS_CLASE', '$CINTURON_CLASE', '$GRADO_CLASE', '$DIA_CLASE', '$HORA_CLASE', 1)";
                        if ($connect->query($insertar_clase) === TRUE) {
                            header('Location: ../vista_profesor_perfil.php?id_profesor='.$ID);
                        }
                    }else{
                        echo '<script type="text/javascript"> alert("Existe un tope de horario, usted no puede crear esta clase"); location="../vista_profesor_perfil.php?id_profesor='.$ID.'"; </script>';
                    }
                }
                if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("16:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("17:00"))){
                    $peticion = mysqli_query($connect, "SELECT COUNT(*) AS TOPE FROM Clase WHERE Hora >= '16:00:00' AND Hora < '17:00:00' AND Visibilidad=1 AND Dia='$DIA_CLASE'");
                    $verificador_tope = mysqli_fetch_array($peticion);
        
                    if($verificador_tope['TOPE'] == 0){
                        $insertar_clase = "INSERT INTO Clase (Tipo_clase, ID_profesor, Cupos, Cinturon, Grado, Dia, Hora, Visibilidad) VALUES ('$TIPO_CLASE', '$ID', '$CUPOS_CLASE', '$CINTURON_CLASE', '$GRADO_CLASE', '$DIA_CLASE', '$HORA_CLASE', 1)";
                        if ($connect->query($insertar_clase) === TRUE) {
                            header('Location: ../vista_profesor_perfil.php?id_profesor='.$ID);
                        }
                    }else{
                        echo '<script type="text/javascript"> alert("Existe un tope de horario, usted no puede crear esta clase"); location="../vista_profesor_perfil.php?id_profesor='.$ID.'"; </script>';
                    }
                }
                if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("17:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("18:00"))){
                    $peticion = mysqli_query($connect, "SELECT COUNT(*) AS TOPE FROM Clase WHERE Hora >= '17:00:00' AND Hora < '18:00:00' AND Visibilidad=1 AND Dia='$DIA_CLASE'");
                    $verificador_tope = mysqli_fetch_array($peticion);
        
                    if($verificador_tope['TOPE'] == 0){
                        $insertar_clase = "INSERT INTO Clase (Tipo_clase, ID_profesor, Cupos, Cinturon, Grado, Dia, Hora, Visibilidad) VALUES ('$TIPO_CLASE', '$ID', '$CUPOS_CLASE', '$CINTURON_CLASE', '$GRADO_CLASE', '$DIA_CLASE', '$HORA_CLASE', 1)";
                        if ($connect->query($insertar_clase) === TRUE) {
                            header('Location: ../vista_profesor_perfil.php?id_profesor='.$ID);
                        }
                    }else{
                        echo '<script type="text/javascript"> alert("Existe un tope de horario, usted no puede crear esta clase"); location="../vista_profesor_perfil.php?id_profesor='.$ID.'"; </script>';
                    }
                }
                if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("18:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("19:00"))){
                    $peticion = mysqli_query($connect, "SELECT COUNT(*) AS TOPE FROM Clase WHERE Hora >= '18:00:00' AND Hora < '19:00:00' AND Visibilidad=1 AND Dia='$DIA_CLASE'");
                    $verificador_tope = mysqli_fetch_array($peticion);
        
                    if($verificador_tope['TOPE'] == 0){
                        $insertar_clase = "INSERT INTO Clase (Tipo_clase, ID_profesor, Cupos, Cinturon, Grado, Dia, Hora, Visibilidad) VALUES ('$TIPO_CLASE', '$ID', '$CUPOS_CLASE', '$CINTURON_CLASE', '$GRADO_CLASE', '$DIA_CLASE', '$HORA_CLASE', 1)";
                        if ($connect->query($insertar_clase) === TRUE) {
                            header('Location: ../vista_profesor_perfil.php?id_profesor='.$ID);
                        }
                    }else{
                        echo '<script type="text/javascript"> alert("Existe un tope de horario, usted no puede crear esta clase"); location="../vista_profesor_perfil.php?id_profesor='.$ID.'"; </script>';
                    }
                }
                if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("19:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("20:00"))){
                    $peticion = mysqli_query($connect, "SELECT COUNT(*) AS TOPE FROM Clase WHERE Hora >= '19:00:00' AND Hora < '20:00:00' AND Visibilidad=1 AND Dia='$DIA_CLASE'");
                    $verificador_tope = mysqli_fetch_array($peticion);
        
                    if($verificador_tope['TOPE'] == 0){
                        $insertar_clase = "INSERT INTO Clase (Tipo_clase, ID_profesor, Cupos, Cinturon, Grado, Dia, Hora, Visibilidad) VALUES ('$TIPO_CLASE', '$ID', '$CUPOS_CLASE', '$CINTURON_CLASE', '$GRADO_CLASE', '$DIA_CLASE', '$HORA_CLASE', 1)";
                        if ($connect->query($insertar_clase) === TRUE) {
                            header('Location: ../vista_profesor_perfil.php?id_profesor='.$ID);
                        }
                    }else{
                        echo '<script type="text/javascript"> alert("Existe un tope de horario, usted no puede crear esta clase"); location="../vista_profesor_perfil.php?id_profesor='.$ID.'"; </script>';
                    }
                }
                if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("20:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("21:00"))){
                    $peticion = mysqli_query($connect, "SELECT COUNT(*) AS TOPE FROM Clase WHERE Hora >= '20:00:00' AND Hora < '21:00:00' AND Visibilidad=1 AND Dia='$DIA_CLASE'");
                    $verificador_tope = mysqli_fetch_array($peticion);
        
                    if($verificador_tope['TOPE'] == 0){
                        $insertar_clase = "INSERT INTO Clase (Tipo_clase, ID_profesor, Cupos, Cinturon, Grado, Dia, Hora, Visibilidad) VALUES ('$TIPO_CLASE', '$ID', '$CUPOS_CLASE', '$CINTURON_CLASE', '$GRADO_CLASE', '$DIA_CLASE', '$HORA_CLASE', 1)";
                        if ($connect->query($insertar_clase) === TRUE) {
                            header('Location: ../vista_profesor_perfil.php?id_profesor='.$ID);
                        }
                    }else{
                        echo '<script type="text/javascript"> alert("Existe un tope de horario, usted no puede crear esta clase"); location="../vista_profesor_perfil.php?id_profesor='.$ID.'"; </script>';
                    }
                }
                if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("21:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("22:00"))){
                    $peticion = mysqli_query($connect, "SELECT COUNT(*) AS TOPE FROM Clase WHERE Hora >= '21:00:00' AND Hora < '22:00:00' AND Visibilidad=1 AND Dia='$DIA_CLASE'");
                    $verificador_tope = mysqli_fetch_array($peticion);
        
                    if($verificador_tope['TOPE'] == 0){
                        $insertar_clase = "INSERT INTO Clase (Tipo_clase, ID_profesor, Cupos, Cinturon, Grado, Dia, Hora, Visibilidad) VALUES ('$TIPO_CLASE', '$ID', '$CUPOS_CLASE', '$CINTURON_CLASE', '$GRADO_CLASE', '$DIA_CLASE', '$HORA_CLASE', 1)";
                        if ($connect->query($insertar_clase) === TRUE) {
                            header('Location: ../vista_profesor_perfil.php?id_profesor='.$ID);
                        }
                    }else{
                        echo '<script type="text/javascript"> alert("Existe un tope de horario, usted no puede crear esta clase"); location="../vista_profesor_perfil.php?id_profesor='.$ID.'"; </script>';
                    }
                }
                break;
            case 7:
                if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("7:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("8:00"))){
                    $peticion = mysqli_query($connect, "SELECT COUNT(*) AS TOPE FROM Clase WHERE Hora >= '7:00:00' AND Hora < '8:00:00' AND Visibilidad=1 AND Dia='$DIA_CLASE'");
                    $verificador_tope = mysqli_fetch_array($peticion);
        
                    if($verificador_tope['TOPE'] == 0){
                        $insertar_clase = "INSERT INTO Clase (Tipo_clase, ID_profesor, Cupos, Cinturon, Grado, Dia, Hora, Visibilidad) VALUES ('$TIPO_CLASE', '$ID', '$CUPOS_CLASE', '$CINTURON_CLASE', '$GRADO_CLASE', '$DIA_CLASE', '$HORA_CLASE', 1)";
                        if ($connect->query($insertar_clase) === TRUE) {
                            header('Location: ../vista_profesor_perfil.php?id_profesor='.$ID);
                        }
                    }else{
                        echo '<script type="text/javascript"> alert("Existe un tope de horario, usted no puede crear esta clase"); location="../vista_profesor_perfil.php?id_profesor='.$ID.'"; </script>';
                    }
                }
                if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("8:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("9:00"))){
                    $peticion = mysqli_query($connect, "SELECT COUNT(*) AS TOPE FROM Clase WHERE Hora >= '8:00:00' AND Hora < '9:00:00' AND Visibilidad=1 AND Dia='$DIA_CLASE'");
                    $verificador_tope = mysqli_fetch_array($peticion);
        
                    if($verificador_tope['TOPE'] == 0){
                        $insertar_clase = "INSERT INTO Clase (Tipo_clase, ID_profesor, Cupos, Cinturon, Grado, Dia, Hora, Visibilidad) VALUES ('$TIPO_CLASE', '$ID', '$CUPOS_CLASE', '$CINTURON_CLASE', '$GRADO_CLASE', '$DIA_CLASE', '$HORA_CLASE', 1)";
                        if ($connect->query($insertar_clase) === TRUE) {
                            header('Location: ../vista_profesor_perfil.php?id_profesor='.$ID);
                        }
                    }else{
                        echo '<script type="text/javascript"> alert("Existe un tope de horario, usted no puede crear esta clase"); location="../vista_profesor_perfil.php?id_profesor='.$ID.'"; </script>';
                    }
                }
                if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("9:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("10:00"))){
                    $peticion = mysqli_query($connect, "SELECT COUNT(*) AS TOPE FROM Clase WHERE Hora >= '9:00:00' AND Hora < '10:00:00' AND Visibilidad=1 AND Dia='$DIA_CLASE'");
                    $verificador_tope = mysqli_fetch_array($peticion);
        
                    if($verificador_tope['TOPE'] == 0){
                        $insertar_clase = "INSERT INTO Clase (Tipo_clase, ID_profesor, Cupos, Cinturon, Grado, Dia, Hora, Visibilidad) VALUES ('$TIPO_CLASE', '$ID', '$CUPOS_CLASE', '$CINTURON_CLASE', '$GRADO_CLASE', '$DIA_CLASE', '$HORA_CLASE', 1)";
                        if ($connect->query($insertar_clase) === TRUE) {
                            header('Location: ../vista_profesor_perfil.php?id_profesor='.$ID);
                        }
                    }else{
                        echo '<script type="text/javascript"> alert("Existe un tope de horario, usted no puede crear esta clase"); location="../vista_profesor_perfil.php?id_profesor='.$ID.'"; </script>';
                    }
                }
                if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("10:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("11:00"))){
                    $peticion = mysqli_query($connect, "SELECT COUNT(*) AS TOPE FROM Clase WHERE Hora >= '10:00:00' AND Hora < '11:00:00' AND Visibilidad=1 AND Dia='$DIA_CLASE'");
                    $verificador_tope = mysqli_fetch_array($peticion);
        
                    if($verificador_tope['TOPE'] == 0){
                        $insertar_clase = "INSERT INTO Clase (Tipo_clase, ID_profesor, Cupos, Cinturon, Grado, Dia, Hora, Visibilidad) VALUES ('$TIPO_CLASE', '$ID', '$CUPOS_CLASE', '$CINTURON_CLASE', '$GRADO_CLASE', '$DIA_CLASE', '$HORA_CLASE', 1)";
                        if ($connect->query($insertar_clase) === TRUE) {
                            header('Location: ../vista_profesor_perfil.php?id_profesor='.$ID);
                        }
                    }else{
                        echo '<script type="text/javascript"> alert("Existe un tope de horario, usted no puede crear esta clase"); location="../vista_profesor_perfil.php?id_profesor='.$ID.'"; </script>';
                    }
                }
                if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("11:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("12:00"))){
                    $peticion = mysqli_query($connect, "SELECT COUNT(*) AS TOPE FROM Clase WHERE Hora >= '11:00:00' AND Hora < '12:00:00' AND Visibilidad=1 AND Dia='$DIA_CLASE'");
                    $verificador_tope = mysqli_fetch_array($peticion);
        
                    if($verificador_tope['TOPE'] == 0){
                        $insertar_clase = "INSERT INTO Clase (Tipo_clase, ID_profesor, Cupos, Cinturon, Grado, Dia, Hora, Visibilidad) VALUES ('$TIPO_CLASE', '$ID', '$CUPOS_CLASE', '$CINTURON_CLASE', '$GRADO_CLASE', '$DIA_CLASE', '$HORA_CLASE', 1)";
                        if ($connect->query($insertar_clase) === TRUE) {
                            header('Location: ../vista_profesor_perfil.php?id_profesor='.$ID);
                        }
                    }else{
                        echo '<script type="text/javascript"> alert("Existe un tope de horario, usted no puede crear esta clase"); location="../vista_profesor_perfil.php?id_profesor='.$ID.'"; </script>';
                    }
                }
                if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("12:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("13:00"))){
                    $peticion = mysqli_query($connect, "SELECT COUNT(*) AS TOPE FROM Clase WHERE Hora >= '12:00:00' AND Hora < '13:00:00' AND Visibilidad=1 AND Dia='$DIA_CLASE'");
                    $verificador_tope = mysqli_fetch_array($peticion);
        
                    if($verificador_tope['TOPE'] == 0){
                        $insertar_clase = "INSERT INTO Clase (Tipo_clase, ID_profesor, Cupos, Cinturon, Grado, Dia, Hora, Visibilidad) VALUES ('$TIPO_CLASE', '$ID', '$CUPOS_CLASE', '$CINTURON_CLASE', '$GRADO_CLASE', '$DIA_CLASE', '$HORA_CLASE', 1)";
                        if ($connect->query($insertar_clase) === TRUE) {
                            header('Location: ../vista_profesor_perfil.php?id_profesor='.$ID);
                        }
                    }else{
                        echo '<script type="text/javascript"> alert("Existe un tope de horario, usted no puede crear esta clase"); location="../vista_profesor_perfil.php?id_profesor='.$ID.'"; </script>';
                    }
                }
                if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("13:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("14:00"))){
                    $peticion = mysqli_query($connect, "SELECT COUNT(*) AS TOPE FROM Clase WHERE Hora >= '13:00:00' AND Hora < '14:00:00' AND Visibilidad=1 AND Dia='$DIA_CLASE'");
                    $verificador_tope = mysqli_fetch_array($peticion);
        
                    if($verificador_tope['TOPE'] == 0){
                        $insertar_clase = "INSERT INTO Clase (Tipo_clase, ID_profesor, Cupos, Cinturon, Grado, Dia, Hora, Visibilidad) VALUES ('$TIPO_CLASE', '$ID', '$CUPOS_CLASE', '$CINTURON_CLASE', '$GRADO_CLASE', '$DIA_CLASE', '$HORA_CLASE', 1)";
                        if ($connect->query($insertar_clase) === TRUE) {
                            header('Location: ../vista_profesor_perfil.php?id_profesor='.$ID);
                        }
                    }else{
                        echo '<script type="text/javascript"> alert("Existe un tope de horario, usted no puede crear esta clase"); location="../vista_profesor_perfil.php?id_profesor='.$ID.'"; </script>';
                    }
                }
                if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("14:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("15:00"))){
                    $peticion = mysqli_query($connect, "SELECT COUNT(*) AS TOPE FROM Clase WHERE Hora >= '14:00:00' AND Hora < '15:00:00' AND Visibilidad=1 AND Dia='$DIA_CLASE'");
                    $verificador_tope = mysqli_fetch_array($peticion);
        
                    if($verificador_tope['TOPE'] == 0){
                        $insertar_clase = "INSERT INTO Clase (Tipo_clase, ID_profesor, Cupos, Cinturon, Grado, Dia, Hora, Visibilidad) VALUES ('$TIPO_CLASE', '$ID', '$CUPOS_CLASE', '$CINTURON_CLASE', '$GRADO_CLASE', '$DIA_CLASE', '$HORA_CLASE', 1)";
                        if ($connect->query($insertar_clase) === TRUE) {
                            header('Location: ../vista_profesor_perfil.php?id_profesor='.$ID);
                        }
                    }else{
                        echo '<script type="text/javascript"> alert("Existe un tope de horario, usted no puede crear esta clase"); location="../vista_profesor_perfil.php?id_profesor='.$ID.'"; </script>';
                    }
                }
                if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("15:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("16:00"))){
                    $peticion = mysqli_query($connect, "SELECT COUNT(*) AS TOPE FROM Clase WHERE Hora >= '15:00:00' AND Hora < '16:00:00' AND Visibilidad=1 AND Dia='$DIA_CLASE'");
                    $verificador_tope = mysqli_fetch_array($peticion);
        
                    if($verificador_tope['TOPE'] == 0){
                        $insertar_clase = "INSERT INTO Clase (Tipo_clase, ID_profesor, Cupos, Cinturon, Grado, Dia, Hora, Visibilidad) VALUES ('$TIPO_CLASE', '$ID', '$CUPOS_CLASE', '$CINTURON_CLASE', '$GRADO_CLASE', '$DIA_CLASE', '$HORA_CLASE', 1)";
                        if ($connect->query($insertar_clase) === TRUE) {
                            header('Location: ../vista_profesor_perfil.php?id_profesor='.$ID);
                        }
                    }else{
                        echo '<script type="text/javascript"> alert("Existe un tope de horario, usted no puede crear esta clase"); location="../vista_profesor_perfil.php?id_profesor='.$ID.'"; </script>';
                    }
                }
                if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("16:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("17:00"))){
                    $peticion = mysqli_query($connect, "SELECT COUNT(*) AS TOPE FROM Clase WHERE Hora >= '16:00:00' AND Hora < '17:00:00' AND Visibilidad=1 AND Dia='$DIA_CLASE'");
                    $verificador_tope = mysqli_fetch_array($peticion);
        
                    if($verificador_tope['TOPE'] == 0){
                        $insertar_clase = "INSERT INTO Clase (Tipo_clase, ID_profesor, Cupos, Cinturon, Grado, Dia, Hora, Visibilidad) VALUES ('$TIPO_CLASE', '$ID', '$CUPOS_CLASE', '$CINTURON_CLASE', '$GRADO_CLASE', '$DIA_CLASE', '$HORA_CLASE', 1)";
                        if ($connect->query($insertar_clase) === TRUE) {
                            header('Location: ../vista_profesor_perfil.php?id_profesor='.$ID);
                        }
                    }else{
                        echo '<script type="text/javascript"> alert("Existe un tope de horario, usted no puede crear esta clase"); location="../vista_profesor_perfil.php?id_profesor='.$ID.'"; </script>';
                    }
                }
                if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("17:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("18:00"))){
                    $peticion = mysqli_query($connect, "SELECT COUNT(*) AS TOPE FROM Clase WHERE Hora >= '17:00:00' AND Hora < '18:00:00' AND Visibilidad=1 AND Dia='$DIA_CLASE'");
                    $verificador_tope = mysqli_fetch_array($peticion);
        
                    if($verificador_tope['TOPE'] == 0){
                        $insertar_clase = "INSERT INTO Clase (Tipo_clase, ID_profesor, Cupos, Cinturon, Grado, Dia, Hora, Visibilidad) VALUES ('$TIPO_CLASE', '$ID', '$CUPOS_CLASE', '$CINTURON_CLASE', '$GRADO_CLASE', '$DIA_CLASE', '$HORA_CLASE', 1)";
                        if ($connect->query($insertar_clase) === TRUE) {
                            header('Location: ../vista_profesor_perfil.php?id_profesor='.$ID);
                        }
                    }else{
                        echo '<script type="text/javascript"> alert("Existe un tope de horario, usted no puede crear esta clase"); location="../vista_profesor_perfil.php?id_profesor='.$ID.'"; </script>';
                    }
                }
                if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("18:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("19:00"))){
                    $peticion = mysqli_query($connect, "SELECT COUNT(*) AS TOPE FROM Clase WHERE Hora >= '18:00:00' AND Hora < '19:00:00' AND Visibilidad=1 AND Dia='$DIA_CLASE'");
                    $verificador_tope = mysqli_fetch_array($peticion);
        
                    if($verificador_tope['TOPE'] == 0){
                        $insertar_clase = "INSERT INTO Clase (Tipo_clase, ID_profesor, Cupos, Cinturon, Grado, Dia, Hora, Visibilidad) VALUES ('$TIPO_CLASE', '$ID', '$CUPOS_CLASE', '$CINTURON_CLASE', '$GRADO_CLASE', '$DIA_CLASE', '$HORA_CLASE', 1)";
                        if ($connect->query($insertar_clase) === TRUE) {
                            header('Location: ../vista_profesor_perfil.php?id_profesor='.$ID);
                        }
                    }else{
                        echo '<script type="text/javascript"> alert("Existe un tope de horario, usted no puede crear esta clase"); location="../vista_profesor_perfil.php?id_profesor='.$ID.'"; </script>';
                    }
                }
                if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("19:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("20:00"))){
                    $peticion = mysqli_query($connect, "SELECT COUNT(*) AS TOPE FROM Clase WHERE Hora >= '19:00:00' AND Hora < '20:00:00' AND Visibilidad=1 AND Dia='$DIA_CLASE'");
                    $verificador_tope = mysqli_fetch_array($peticion);
        
                    if($verificador_tope['TOPE'] == 0){
                        $insertar_clase = "INSERT INTO Clase (Tipo_clase, ID_profesor, Cupos, Cinturon, Grado, Dia, Hora, Visibilidad) VALUES ('$TIPO_CLASE', '$ID', '$CUPOS_CLASE', '$CINTURON_CLASE', '$GRADO_CLASE', '$DIA_CLASE', '$HORA_CLASE', 1)";
                        if ($connect->query($insertar_clase) === TRUE) {
                            header('Location: ../vista_profesor_perfil.php?id_profesor='.$ID);
                        }
                    }else{
                        echo '<script type="text/javascript"> alert("Existe un tope de horario, usted no puede crear esta clase"); location="../vista_profesor_perfil.php?id_profesor='.$ID.'"; </script>';
                    }
                }
                if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("20:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("21:00"))){
                    $peticion = mysqli_query($connect, "SELECT COUNT(*) AS TOPE FROM Clase WHERE Hora >= '20:00:00' AND Hora < '21:00:00' AND Visibilidad=1 AND Dia='$DIA_CLASE'");
                    $verificador_tope = mysqli_fetch_array($peticion);
        
                    if($verificador_tope['TOPE'] == 0){
                        $insertar_clase = "INSERT INTO Clase (Tipo_clase, ID_profesor, Cupos, Cinturon, Grado, Dia, Hora, Visibilidad) VALUES ('$TIPO_CLASE', '$ID', '$CUPOS_CLASE', '$CINTURON_CLASE', '$GRADO_CLASE', '$DIA_CLASE', '$HORA_CLASE', 1)";
                        if ($connect->query($insertar_clase) === TRUE) {
                            header('Location: ../vista_profesor_perfil.php?id_profesor='.$ID);
                        }
                    }else{
                        echo '<script type="text/javascript"> alert("Existe un tope de horario, usted no puede crear esta clase"); location="../vista_profesor_perfil.php?id_profesor='.$ID.'"; </script>';
                    }
                }
                if(date('H:i', strtotime($HORA_CLASE)) >= date('H:i', strtotime("21:00")) and date('H:i', strtotime($HORA_CLASE)) < date('H:i', strtotime("22:00"))){
                    $peticion = mysqli_query($connect, "SELECT COUNT(*) AS TOPE FROM Clase WHERE Hora >= '21:00:00' AND Hora < '22:00:00' AND Visibilidad=1 AND Dia='$DIA_CLASE'");
                    $verificador_tope = mysqli_fetch_array($peticion);
        
                    if($verificador_tope['TOPE'] == 0){
                        $insertar_clase = "INSERT INTO Clase (Tipo_clase, ID_profesor, Cupos, Cinturon, Grado, Dia, Hora, Visibilidad) VALUES ('$TIPO_CLASE', '$ID', '$CUPOS_CLASE', '$CINTURON_CLASE', '$GRADO_CLASE', '$DIA_CLASE', '$HORA_CLASE', 1)";
                        if ($connect->query($insertar_clase) === TRUE) {
                            header('Location: ../vista_profesor_perfil.php?id_profesor='.$ID);
                        }
                    }else{
                        echo '<script type="text/javascript"> alert("Existe un tope de horario, usted no puede crear esta clase"); location="../vista_profesor_perfil.php?id_profesor='.$ID.'"; </script>';
                    }
                }
                break;
            default:
                break;
        }
        break;
    default: 
        $ID_PROFESOR_CLASE = mysqli_real_escape_string($connect, $_GET['id_profesor']);
        header("location: ../vista_profesor_perfil.php?id_profesor=".$ID_PROFESOR_CLASE);
        break;
}















?>
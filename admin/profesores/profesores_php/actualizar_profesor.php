<?php
    include("../../../includes/connection.php");
    session_start();
    
    $ID = $_SESSION['ID_profesor'];
    $PODER = $_SESSION['Poder'];
    $ACTIVE = $_SESSION['active'];
    
    if(!isset($ID) OR $PODER == 0 OR $ACTIVE == 0){
        session_destroy();
        header("location: ../../../index.html");
        exit();
    }

    $ELIMINAR_PROFESOR = mysqli_real_escape_string($connect, $_GET['eliminar']);
    $ID_PROFESOR = mysqli_real_escape_string($connect, $_GET['id_profesor']);

    if($ELIMINAR_PROFESOR == 0){
        $NOMBRE_PROFESOR = mysqli_real_escape_string($connect, $_POST['nombre']);
        $APELLIDOS_PROFESOR = mysqli_real_escape_string($connect, $_POST['apellido']);

        $RUT_PROFESOR = mysqli_real_escape_string($connect, $_POST['rut']);
        $FECHA_NACIMIENTO_PROFESOR = mysqli_real_escape_string($connect, $_POST['fecha']);

        $DIRECCION_PROFESOR = mysqli_real_escape_string($connect, $_POST['direccion']);
        $TELEFONO_PROFESOR = mysqli_real_escape_string($connect, $_POST['telefono']);
        
        $CINTURON_PROFESOR = mysqli_real_escape_string($connect, $_POST['cinturon']);
        
        $ACTIVE_PROFESOR = mysqli_real_escape_string($connect, $_POST['active']);
        $PODER_PROFESOR = mysqli_real_escape_string($connect, $_POST['poder']);

        $actualizar_profesor = "UPDATE Profesor SET Nombre = '$NOMBRE_PROFESOR', Apellidos = '$APELLIDOS_PROFESOR', Rut = '$RUT_PROFESOR', Fecha_nacimiento = '$FECHA_NACIMIENTO_PROFESOR', Telefono = '$TELEFONO_PROFESOR', Direccion = '$DIRECCION_PROFESOR', Cinturon = '$CINTURON_PROFESOR', active = '$ACTIVE_PROFESOR', Poder = '$PODER_PROFESOR' WHERE ID_profesor = '$ID_PROFESOR'";
        if ($connect->query($actualizar_profesor) == TRUE){
            $consulta_profesor = "SELECT * FROM Profesor WHERE ID_profesor='$ID'";
            $peticion_profesor = mysqli_query($connect, $consulta_profesor);
    
            if($peticion_profesor){
                while($row = $peticion_profesor->fetch_array()){
                    $id = $row['ID_profesor'];

                    $nombre = $row['Nombre'];
                    $apellido = $row['Apellidos'];
                    $rut = $row['Rut'];
                    $direccion = $row['Direccion'];
                    $telefono = $row['Telefono'];

                    $fecha_n = $row['Fecha_nacimiento'];
                    $cinturon = $row['Cinturon'];

                    $poder = $row['Poder'];
                    $active = $row['active'];
                }
            }
            $_SESSION['ID_profesor'] = $id;

            $_SESSION['Nombre'] = $nombre;
            $_SESSION['Apellidos'] = $apellido;
            $_SESSION['Rut'] = $rut;
            $_SESSION['Direccion'] = $direccion;
            $_SESSION['Telefono'] = $telefono;

            $_SESSION['Fecha_nacimiento'] = $fecha_n;    
            $_SESSION['Cinturon'] = $cinturon;
            
            $_SESSION['Poder'] = $poder;
            $_SESSION['active'] = $active;
            header('Location: ../vista_profesor_perfil.php?id_profesor='.$ID_PROFESOR);
        }else{
            header('Location: ../vista_profesor_perfil_configuracion.php?id_profesor='.$ID_PROFESOR);
        }
        
    }else{
        $eliminar_profesor = "DELETE FROM Profesor WHERE ID_profesor = '$ID_PROFESOR'";
    
        if ($connect->query($eliminar_profesor) === TRUE) {
            header('location: ../vista_profesores.php');
        } else {
            header('Location: ../vista_profesor_perfil_configuracion.php?id_profesor='.$ID_PROFESOR);
        }
    }











?>
<?php
include("../../includes/connection.php");
include("../../includes/funciones.php");
session_start();

$ID = $_SESSION['ID_profesor'];
$NOMBRE = $_SESSION['Nombre'];
$APELLIDOS = $_SESSION['Apellidos'];

$PODER = $_SESSION['Poder'];
$ACTIVE = $_SESSION['active'];

if(!isset($ID) OR $PODER == 0 OR $ACTIVE == 0){
    session_destroy();
    header("location: ../../index.html");
    exit();
}

$ID_PROFESOR = mysqli_real_escape_string($connect, $_GET['id_profesor']);

$profesores = "SELECT * FROM Profesor WHERE ID_profesor = '$ID_PROFESOR'";
$peticion_profesores = mysqli_query($connect, $profesores);

while($tabla = $peticion_profesores->fetch_assoc()){
    $NOMBRE_PROFESOR = $tabla['Nombre'];
    $APELLIDO_PROFESOR = $tabla['Apellidos'];

    $RUT_PROFESOR = $tabla['Rut'];
    $FECHA_NACIMIENTO_PROFESOR = $tabla['Fecha_nacimiento'];

    $TELEFONO_PROFESOR = $tabla['Telefono'];
    $DIRECCION_PROFESOR = $tabla['Direccion'];
    $MAIL_PROFESOR = $tabla['Mail'];

    $CINTURON_PROFESOR = $tabla['Cinturon'];

    $PODER_PROFESOR = $tabla['Poder'];

    $ACTIVE_PROFESOR = $tabla['active'];

    switch($CINTURON_PROFESOR){
        case 1:
            $cinturon = "Ninguno";
            break;
        case 2:
            $cinturon = "Blanco";
            break;
        case 3:
            $cinturon = "Azul";
            break;
        case 4:
            $cinturon = "Morado";
            break;
        case 5:
            $cinturon = "Cafe";
            break;
        case 6: 
            $cinturon = "Negro";
            break;
        default:
            break;
    }
    
    switch($ACTIVE_PROFESOR){
        case 0:
            $active = "El profesor no ha activado su cuenta";
            break;
        case 1:
            $active = "El profesor ha activado su cuenta";
            break;
        default:
            break;
    }

    switch($PODER_PROFESOR){
        case 0:
            $poder = "El profesor no es administrador";
            break;
        case 1:
            $poder = "El profesor es administrador";
            break;
        default:
            break;
    }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Perfil de <?php echo "$NOMBRE_PROFESOR $APELLIDO_PROFESOR";?></title>
</head>
<body>
    <ul>
        <li class="log"> <?php echo "<p>Bienvenido $NOMBRE $APELLIDOS</p>";?> </li>
        <li><a href="../../profesor/profesor_php/home_profesor.php">Inicio</a></li>
        <li><a href="../../profesor/perfil_profesor.php">Perfil</a></li>
        <li><a href="../../profesor/horario_profesor.php">Horario</a></li>
        <li class="active"><a href="../../profesor/vista_clase.php">Clases</a></li>
        <li><a href="../../general/general_php/logout.php">Cerrar sesion</a></li>
    </ul>
    
    <h1>Perfil de <?php echo "$NOMBRE_PROFESOR $APELLIDO_PROFESOR";?></h1>

    <button><a href="vista_profesores.php">volver</a></button>

    <div class="nombre">
        <p>
            Nombre: <?php echo "$NOMBRE_PROFESOR $APELLIDO_PROFESOR";?>
        </p>
    </div>

    <div class="rut_fecha">
        <p>
            RUT: <?php echo "$RUT_PROFESOR";?>
            <br>
            Fecha de nacimiento: <?php echo "$FECHA_NACIMIENTO_PROFESOR";?>
        </p>
    </div>

    <div class="cinturon">
        <p>
            Cinturon: <?php echo $cinturon;?>
            <br>
        </p>
    </div>

    <div class="contacto">
        <p>
            Telefono: <?php echo $TELEFONO_PROFESOR;?>
            <br>
            Direccion: <?php echo $DIRECCION_PROFESOR;?>
            <br>
            Correo electronico: <?php echo $MAIL_PROFESOR;?>
        </p>
    </div>

    <div class="anexo">
        <p>
            Estado: 
            <?php echo $active;?>
            <br>
            Poder:
            <?php echo $poder;?>
        </p>
    </div>

    <div class="configuracion_alumno">
        <button><a href="vista_profesor_perfil_configuracion.php?id_profesor=<?php echo "$ID_PROFESOR";?>">Configuracion</a></button>
    </div>

    <div>
        <h2>Crear clase</h2>

        <div class="form-container">
            <form action="profesores_php/clase_crud.php?id_profesor=<?php echo $ID_PROFESOR;?>&opcion=1" method="post" name="formulario" class="formulario">
                <label for="Cupos"><b>Cupos</b></label>
                <input class="controls" type="number" placeholder="Cupos" name="Cupos" id="Cupos" min="1" max="100" required>

                <div class="Dia">
                    <label for="Dia"><b>Dia</b></label>
                    <select class="select" name="Dia" id="Dia" required>
                        <option value="1" selected>Lunes</option>
                        <option value="2">Martes</option>
                        <option value="3">Miércoles</option>
                        <option value="4">Jueves</option>
                        <option value="5">Viernes</option>
                        <option value="6">Sábado</option>
                        <option value="7">Domingo</option>
                    </select>

                    <input class = "time" type="time" name="Hora" min="7:00" max="22:00" step="60"/>
                </div>

                <div class="Clase">
                    <label for="Clase"><b>Clase</b></label>
                    <select class="select" name="Clase" id="Clase" required>
                        </html>
                        <?php
                        for ($i = 1; $i < 30; $i++) { 
                            if(switchClases($i) != "salir"){
                                ?>
                                <option value="<?php echo $i;?>"><?php echo switchClases($i)?></option>
                                <?php
                            }
                        }                        
                        ?>               
                        <html>
                    </select>
                </div>
                
                <div class="Cinturon_Grado">
                    <label for="Cinturon"><b>Cinturon</b></label>
                    <select class="select" name="Cinturon" id="Cinturon" >
                        <option value="1" selected>Ninguno</option>
                        <option value="2">Blanco</option>
                        <option value="3">Azul</option>
                        <option value="4">Morado</option>
                        <option value="5">Cafe</option>
                        <option value="6">Negro</option>
                    </select>
        
                    <label for="Grado"><b>Grado</b></label>
                    <select class="select" name="Grado" id="Grado" >
                        <option value="1" selected>Ninguno</option>
                        <option value="2">I</option>
                        <option value="3">II</option>
                        <option value="4">III</option>
                        <option value="5">IV</option>
                    </select>
                </div>
                
                <input type="submit" value="Crear clase" class="button">
            </form>
        </div>
    </div>

    <div class="clases_creadas">
        <h2>Clases creadas</h2>

        <div class="tabla-container">
            <table border="1">
                <thead>
                    <tr>
                        <th>Clase</th>
                        <th>Horario</th>
                        <th>Visibilidad</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    </html>
                    <?php
                    $consulta_clases = mysqli_query($connect, "SELECT * FROM Clase WHERE ID_profesor = '$ID_PROFESOR' ORDER BY Dia");
                    while($tabla_clase = $consulta_clases->fetch_assoc()){
                        $ID_CLASE = $tabla_clase['ID_clase'];
                        $TIPO_CLASE = $tabla_clase['Tipo_clase'];
    
                        $VISIBILIDAD_CLASE = $tabla_clase['Visibilidad'];
    
                        $DIA_CLASE = $tabla_clase['Dia'];
                        $HORA_CLASE = $tabla_clase['Hora'];
    
                        $ID_PROFESOR_CLASE = $tabla_clase['ID_profesor'];
    
                        switch($DIA_CLASE){
                            case 1:
                                $dia = "Lunes";
                                break;
                            case 2:
                                $dia = "Martes";
                                break;
                            case 3:
                                $dia = "Miércoles";
                                break;
                            case 4:
                                $dia = "Jueves";
                                break;
                            case 5:
                                $dia = "Viernes";
                                break;
                            case 6:
                                $dia = "Sabado";
                                break;
                            case 7:
                                $dia = "Domingo";
                                break;
                            default:
                                break;
                        }
    
                        switch ($VISIBILIDAD_CLASE) {
                            case 0:
                                $visibilidad = "Deshabilitada";
                                break;
                            case 1:
                                $visibilidad = "Habilitada";
                                break;
                            default:
                                break;
                        }
                        ?>
                        <html>
                            <tr>
                                <td><a href="../clases/vista_clase_perfil.php?id_clase=<?php echo $ID_CLASE;?>"><?php echo switchClases($TIPO_CLASE);?></a></td>
                                <td><?php echo "$dia ".date('H:i', strtotime("$HORA_CLASE"));?></td>
                                <td><?php echo $visibilidad;?></td>
                                <td><a href="profesores_php/clase_crud.php?id_clase=<?php echo $ID_CLASE;?>&id_profesor=<?php echo $ID_PROFESOR_CLASE;?>&opcion=0">Eliminar</a></td>
                            </tr>
                        </html>
                        <?php
                    }
                    ?>
                    <html>
                </tbody>
            </table>
        </div>
    </div>
    
</body>
</html>
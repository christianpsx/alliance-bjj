<?php
include("../../includes/connection.php");
include_once("../../includes/funciones.php");
session_start();

$ID = $_SESSION['ID_profesor'];
$NOMBRE = $_SESSION['Nombre'];
$APELLIDOS = $_SESSION['Apellidos'];

$RUT = $_SESSION['Rut'];
$FECHA_N = $_SESSION['Fecha_nacimiento'];

$DIRECCION = $_SESSION['Direccion'];
$MAIL = $_SESSION['Mail'];
$TELEFONO = $_SESSION['Telefono'];

$CINTURON = $_SESSION['Cinturon'];

$PODER = $_SESSION['Poder'];
$ACTIVE = $_SESSION['active'];

if(!isset($ID) OR $PODER == 0 OR $ACTIVE == 0){
    session_destroy();
    header("location: ../../index.html");
    exit();
}
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="css/vista_clases.css">
    <title>Clases</title>
</head>
<body>
    <ul>
        <li class="log"> <?php echo "<p>Bienvenido $NOMBRE $APELLIDOS</p>";?> </li>

        <li><a href="../../profesor/profesor_php/home_profesor.php">Inicio</a></li>
        <li class="active"><a href="../../profesor/perfil_profesor.php">Perfil</a></li>
        <li><a href="../../profesor/horario_profesor.php">Horario</a></li>
        <li><a href="../../profesor/vista_clase.php">Clases</a></li>
        <li><a href="../../general/general_php/logout.php">Cerrar sesion</a></li>
    </ul>

    <div class="main-container">
        <h1>Clases: </h1>

        <button><a href="../admin_home.php">Volver</a></button>

        <table class="alliance" border=1>
            <thead>
                <tr>
                    <th>Clase</th>
                    <th>Profesor</th>
                    <th>Dia</th>
                    <th>Hora</th>
                    <th>Visibilidad</th>
                </tr>
            </thead>
            <tbody>
                </html>
                <?php
                // $consulta_clases = mysqli_query($connect, "SELECT * FROM Clase ORDER BY Dia");
                $consulta_clases = mysqli_query($connect, "SELECT ID_clase,Tipo_clase,Visibilidad,Dia,Hora,Nombre,Apellidos,P.ID_profesor FROM Clase C LEFT JOIN Profesor P ON C.ID_profesor = P.ID_profesor ORDER BY Dia");
                while($tabla_clase = $consulta_clases->fetch_assoc()){
                    $ID_CLASE = $tabla_clase['ID_clase'];
                    $TIPO_CLASE = $tabla_clase['Tipo_clase'];

                    $VISIBILIDAD_CLASE = $tabla_clase['Visibilidad'];

                    $DIA_CLASE = $tabla_clase['Dia'];
                    $HORA_CLASE = $tabla_clase['Hora'];

                    $NOMBRE_PROFESOR = $tabla_clase['Nombre'];
                    $APELLIDOS_PROFESOR = $tabla_clase['Apellidos'];
                    $ID_PROFESOR_CLASE = $tabla_clase['ID_profesor'];

                    switch($DIA_CLASE){
                        case 1:
                            $dia = "Lunes";
                            break;
                        case 2:
                            $dia = "Martes";
                            break;
                        case 3:
                            $dia = "Miércoles";
                            break;
                        case 4:
                            $dia = "Jueves";
                            break;
                        case 5:
                            $dia = "Viernes";
                            break;
                        case 6:
                            $dia = "Sabado";
                            break;
                        case 7:
                            $dia = "Domingo";
                            break;
                        default:
                            break;
                    }

                    switch ($VISIBILIDAD_CLASE) {
                        case 0:
                            $visibilidad = "Deshabilitada";
                            break;
                        case 1:
                            $visibilidad = "Habilitada";
                            break;
                        default:
                            break;
                    }

                    // $consulta_profesores = mysqli_query($connect, "SELECT Nombre,Apellidos FROM Profesor WHERE ID_profesor = '$ID_PROFESOR_CLASE'");
                    // while($tabla_profesor = $consulta_profesores->fetch_assoc()){
                    //     $NOMBRE_PROFESOR = $tabla_profesor['Nombre'];
                    //     $APELLIDOS_PROFESOR = $tabla_profesor['Apellidos'];
                    // }
                    ?>
                    <html>
                        <tr>
                            <td><a href="vista_clase_perfil.php?id_clase=<?php echo $ID_CLASE;?>"><?php echo switchClases($TIPO_CLASE);?></a></td>
                            <td><a href="../profesores/vista_profesor_perfil.php?id_profesor=<?php echo $ID_PROFESOR_CLASE?>"><?php echo "$NOMBRE_PROFESOR $APELLIDOS_PROFESOR";?></a></td>
                            <td><?php echo $dia;?></td>
                            <td><?php echo date('H:i', strtotime("$HORA_CLASE"));?></td>
                            <td><?php echo $visibilidad;?></td>
                        </tr>
                    </html>   
                    <?php
                }
                ?>
                <html>
            </tbody>
        </table>
    </div>
    
</body>
</html>
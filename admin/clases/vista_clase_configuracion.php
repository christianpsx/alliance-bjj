<?php
include("../../includes/connection.php");
include_once("../../includes/funciones.php");
session_start();

$ID = $_SESSION['ID_profesor'];
$NOMBRE = $_SESSION['Nombre'];
$APELLIDOS = $_SESSION['Apellidos'];

$RUT = $_SESSION['Rut'];
$FECHA_N = $_SESSION['Fecha_nacimiento'];

$DIRECCION = $_SESSION['Direccion'];
$MAIL = $_SESSION['Mail'];
$TELEFONO = $_SESSION['Telefono'];

$CINTURON = $_SESSION['Cinturon'];

$PODER = $_SESSION['Poder'];
$ACTIVE = $_SESSION['active'];

if(!isset($ID) OR $PODER == 0 OR $ACTIVE == 0){
    session_destroy();
    header("location: ../../index.html");
    exit();
}

$ID_CLASE = mysqli_real_escape_string($connect, $_GET['id_clase']);

$consulta_clases = mysqli_query($connect, "SELECT * FROM Clase WHERE ID_clase = '$ID_CLASE'");
while($tabla_clase = $consulta_clases->fetch_assoc()){
    $ID_CLASE = $tabla_clase['ID_clase'];
    $TIPO_CLASE = $tabla_clase['Tipo_clase'];

    $VISIBILIDAD_CLASE = $tabla_clase['Visibilidad'];

    $DIA_CLASE = $tabla_clase['Dia'];
    $HORA_CLASE = $tabla_clase['Hora'];

    $CUPOS_CLASE = $tabla_clase['Cupos'];

    $CINTURON_CLASE = $tabla_clase['Cinturon'];
    $GRADO_CLASE = $tabla_clase['Grado'];

    $ID_PROFESOR_CLASE = $tabla_clase['ID_profesor'];

    switch($CINTURON_CLASE){
        case 1:
            $cinturon = "Ninguno";
            break;
        case 2:
            $cinturon = "Blanco";
            break;
        case 3:
            $cinturon = "Azul";
            break;
        case 4:
            $cinturon = "Morado";
            break;
        case 5:
            $cinturon = "Marron";
            break;
        case 6: 
            $cinturon = "Negro";
            break;
        default:
            break;
    }
    switch($GRADO_CLASE){
        case 1:
            $grado = "Ninguno";
            break;
        case 2:
            $grado = "I";
            break;
        case 3:
            $grado = "II";
            break;
        case 4:
            $grado = "III";
            break;
        case 5: 
            $grado = "IV";
            break;
        default:
            break;
    }
    switch($DIA_CLASE){
        case 1:
            $dia = "Lunes";
            break;
        case 2:
            $dia = "Martes";
            break;
        case 3:
            $dia = "Miércoles";
            break;
        case 4:
            $dia = "Jueves";
            break;
        case 5:
            $dia = "Viernes";
            break;
        case 6:
            $dia = "Sabado";
            break;
        case 7:
            $dia = "Domingo";
            break;
        default:
            break;
    }
    switch ($VISIBILIDAD_CLASE) {
        case 0:
            $visibilidad = "Deshabilitada";
            break;
        case 1:
            $visibilidad = "Habilitada";
            break;
        default:
            break;
    }
}

?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Configuracion</title>
</head>
<body>
    <ul>
        <li class="log"> <?php echo "<p>Bienvenido $NOMBRE $APELLIDOS</p>";?> </li>

        <li><a href="../../profesor/profesor_php/home_profesor.php">Inicio</a></li>
        <li class="active"><a href="../../profesor/perfil_profesor.php">Perfil</a></li>
        <li><a href="../../profesor/horario_profesor.php">Horario</a></li>
        <li><a href="../../profesor/vista_clase.php">Clases</a></li>
        <li><a href="../../general/general_php/logout.php">Cerrar sesion</a></li>
    </ul>

    <div class="main-container">
        <h1>Configuracion de <?php echo switchClases($TIPO_CLASE);?></h1>

        <button><a href="vista_clase_perfil.php?id_clase=<?php echo $ID_CLASE;?>">Volver</a></button>

        <div class="formulario-container">
            <h2>Actualizar clase</h2>
            <form action="clases_php/actualizar_clase.php?id_clase=<?php echo $ID_CLASE;?>&opcion=1" method="post" name="formulario" class="formulario">
                

                <div class="Cupos">
                    <label for="Cupos"><b>Cupos</b></label>
                    <input class="controls" type="number" placeholder="Cupos Disponibles" name="Cupos" id="Cupos" min="1" max="100" value="<?php echo $CUPOS_CLASE;?>">
                </div>

                <div class="Dia">
                    <label for="Dia"><b>Dia</b></label>
                    <select class="select" name="Dia" id="Dia">
                        <option value="<?php echo $DIA_CLASE;?>" selected hidden><?php echo $dia?></option>
                        <option value="1">Lunes</option>
                        <option value="2">Martes</option>
                        <option value="3">Miércoles</option>
                        <option value="4">Jueves</option>
                        <option value="5">Viernes</option>
                        <option value="6">Sábado</option>
                        <option value="7">Domingo</option>
                    </select>

                    <input class = "time" type="time" name="Hora" min="7:00" max="22:00" step="60" value="<?php echo $HORA_CLASE;?>"/>
                </div>
                
                <div class="Cinturon_Grado">
                    <label for="Cinturon"><b>Cinturon</b></label>
                    <select class="select" name="Cinturon" id="Cinturon">
                        <option value="<?php echo $CINTURON_CLASE;?>" selected hidden><?php echo $cinturon;?></option>
                        <option value="1">Ninguno</option>
                        <option value="2">Blanco</option>
                        <option value="3">Azul</option>
                        <option value="4">Morado</option>
                        <option value="5">Cafe</option>
                        <option value="6">Negro</option>
                    </select>
        
                    <label for="Grado"><b>Grado</b></label>
                    <select class="select" name="Grado" id="Grado" >
                        <option value="<?php echo $GRADO_CLASE;?>" selected hidden><?php echo $grado;?></option>
                        <option value="1" selected>Ninguno</option>
                        <option value="2">I</option>
                        <option value="3">II</option>
                        <option value="4">III</option>
                        <option value="5">IV</option>
                    </select>
                </div>

                <div>
                    <label for="visibilidad"><b>Estado</b></label>
                    <select name="visibilidad" id="visibilidad">
                        <option value="<?php echo $VISIBILIDAD_CLASE;?>" selected hidden><?php echo $visibilidad;?></option>
                        <option value="0">Deshabilitar</option>
                        <option value="1">Habilitar</option>
                    </select>
                </div>

                <br>

                <input type="submit" value="Actualizar" class="button">
            </form>
        </div>

        <div class="eliminar-container">
            <h2>Eliminar clase</h2>

            <div id="eliminar_container" class="centro" >
                <button id="boton_eliminar" class="buttons" >Eliminar clase</button>
            </div>

            <div id="confirmacion_container" class="centro">
                <label for="boton_eliminar_confirmacion" class="centro">¿Esta seguro de que quiere eliminar su clase?</label>
                <br>
                <button id="boton_eliminar_confirmacion" class="buttons"><a href="clases_php/actualizar_clase.php?id_clase=<?php echo $ID_CLASE;?>&opcion=2">Eliminar clase</a></button>
                <button id="boton_eliminar_cancelacion" class="buttons">Cancelar</button>
            </div>
        </div>
    </div>

    <script>
        document.getElementById("confirmacion_container").style.display = "none";

        document.getElementById("boton_eliminar").onclick = mostrarDiv;
        document.getElementById("boton_eliminar_cancelacion").onclick = MostrarDiv_eliminar;
        
        function mostrarDiv(){
            document.getElementById("eliminar_container").style.display = "none";
            document.getElementById("confirmacion_container").style.display = "block";
        }

        function MostrarDiv_eliminar(){
            document.getElementById("eliminar_container").style.display = "block";
            document.getElementById("confirmacion_container").style.display = "none";
        }
    </script> 

    
</body>
</html>
<?php
include("../../includes/connection.php");
include_once("../../includes/funciones.php");
session_start();

$ID = $_SESSION['ID_profesor'];
$NOMBRE = $_SESSION['Nombre'];
$APELLIDOS = $_SESSION['Apellidos'];

$RUT = $_SESSION['Rut'];
$FECHA_N = $_SESSION['Fecha_nacimiento'];

$DIRECCION = $_SESSION['Direccion'];
$MAIL = $_SESSION['Mail'];
$TELEFONO = $_SESSION['Telefono'];

$CINTURON = $_SESSION['Cinturon'];

$PODER = $_SESSION['Poder'];
$ACTIVE = $_SESSION['active'];

if(!isset($ID) OR $PODER == 0 OR $ACTIVE == 0){
    session_destroy();
    header("location: ../../index.html");
    exit();
}

$ID_CLASE = mysqli_real_escape_string($connect, $_GET['id_clase']);

$consulta_clases = mysqli_query($connect, "SELECT * FROM Clase WHERE ID_clase = '$ID_CLASE'");
while($tabla_clase = $consulta_clases->fetch_assoc()){
    $ID_CLASE = $tabla_clase['ID_clase'];
    $TIPO_CLASE = $tabla_clase['Tipo_clase'];

    $VISIBILIDAD_CLASE = $tabla_clase['Visibilidad'];

    $DIA_CLASE = $tabla_clase['Dia'];
    $HORA_CLASE = $tabla_clase['Hora'];

    $CUPOS_CLASE = $tabla_clase['Cupos'];

    $CINTURON_CLASE = $tabla_clase['Cinturon'];
    $GRADO_CLASE = $tabla_clase['Grado'];

    $ID_PROFESOR_CLASE = $tabla_clase['ID_profesor'];

    switch($CINTURON_CLASE){
        case 1:
            $cinturon = "Ninguno";
            break;
        case 2:
            $cinturon = "Blanco";
            break;
        case 3:
            $cinturon = "Azul";
            break;
        case 4:
            $cinturon = "Morado";
            break;
        case 5:
            $cinturon = "Marron";
            break;
        case 6: 
            $cinturon = "Negro";
            break;
        default:
            break;
    }
    switch($GRADO_CLASE){
        case 1:
            $grado = "Ninguno";
            break;
        case 2:
            $grado = "I";
            break;
        case 3:
            $grado = "II";
            break;
        case 4:
            $grado = "III";
            break;
        case 5: 
            $grado = "IV";
            break;
        default:
            break;
    }

    switch($DIA_CLASE){
        case 1:
            $dia = "Lunes";
            break;
        case 2:
            $dia = "Martes";
            break;
        case 3:
            $dia = "Miércoles";
            break;
        case 4:
            $dia = "Jueves";
            break;
        case 5:
            $dia = "Viernes";
            break;
        case 6:
            $dia = "Sabado";
            break;
        case 7:
            $dia = "Domingo";
            break;
        default:
            break;
    }

    switch ($VISIBILIDAD_CLASE) {
        case 0:
            $visibilidad = "Deshabilitada";
            break;
        case 1:
            $visibilidad = "Habilitada";
            break;
        default:
            break;
    }

    $consulta_profesores = mysqli_query($connect, "SELECT * FROM Profesor WHERE ID_profesor = '$ID_PROFESOR_CLASE'");
    while($tabla_profesor = $consulta_profesores->fetch_assoc()){
        $NOMBRE_PROFESOR = $tabla_profesor['Nombre'];
        $APELLIDOS_PROFESOR = $tabla_profesor['Apellidos'];
    }

    $consulta_cupos = "SELECT COUNT(*) AS contador_cupos FROM Inscripcion WHERE ID_clase='$ID_CLASE'";
    $peticion_consulta_cupos = mysqli_query($connect, $consulta_cupos);
    $contador_cupos = mysqli_fetch_array($peticion_consulta_cupos);

    if($contador_cupos['contador_cupos'] >= 0){
        $cupos_clase = $CUPOS_CLASE - $contador_cupos['contador_cupos'];
    }
}
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title><?php echo switchClases($TIPO_CLASE);?></title>
</head>
<body>
    <ul>
        <li class="log"> <?php echo "<p>Bienvenido $NOMBRE $APELLIDOS</p>";?> </li>

        <li><a href="../../profesor/profesor_php/home_profesor.php">Inicio</a></li>
        <li class="active"><a href="../../profesor/perfil_profesor.php">Perfil</a></li>
        <li><a href="../../profesor/horario_profesor.php">Horario</a></li>
        <li><a href="../../profesor/vista_clase.php">Clases</a></li>
        <li><a href="../../general/general_php/logout.php">Cerrar sesion</a></li>
    </ul>

    <div class="main-container">
        <h1><?php echo switchClases($TIPO_CLASE);?></h1>

        <button><a href="vista_clases.php">Volver</a></button>

        <div class="info-container">
            <h2>Informacion:</h2>
            <p>
                Profesor: <a href="../profesores/vista_profesor_perfil.php?id_profesor=<?php echo $ID_PROFESOR_CLASE;?>"><?php echo "$NOMBRE_PROFESOR $APELLIDOS_PROFESOR";?></a>
                <br><br>
                Horario: <?php echo "$dia ".date('H:i', strtotime("$HORA_CLASE"));?>
                <br><br>
                Cupos: <?php echo $CUPOS_CLASE;?>
                <br>
                Cupos disponibles: <?php echo $cupos_clase;?>
                <br><br>
                Cinturon: <?php echo "$cinturon";?>
                <br>
                Grado:  <?php echo "$grado";?>
                <br><br>
                Estado: <?php echo "$visibilidad";?>
            </p>

            <button><a href="vista_clase_configuracion.php?id_clase=<?php echo $ID_CLASE;?>">Configuracion</a></button>
        </div>

        
        <div class="inscribir-container">
            <h2>Inscribir</h2>

            <form action="clases_php/inscribir_alumno.php?id_clase=<?php echo $ID_CLASE;?>" method="POST">
                <select name="alumno" id="alumno">
                </html>
                <?php
                
                $alumnos_disponibles = mysqli_query($connect, "SELECT * FROM Alumno WHERE Cinturon >= '$CINTURON_CLASE'");
                while($alumno = $alumnos_disponibles->fetch_assoc()){
                    $id_alumno_inscribir = $alumno['ID_alumno'];
                    $nombre_alumno_inscribir = $alumno['Nombre'];
                    $apellidos_alumno_inscribir = $alumno['Apellidos'];
                    $cinturon_alumno_inscribir = $alumno['Cinturon'];
                    $grado_alumno_inscribir = $alumno['Grado'];

                    switch($cinturon_alumno_inscribir){
                        case 1:
                            $cinturon = "Ninguno";
                            break;
                        case 2:
                            $cinturon = "Blanco";
                            break;
                        case 3:
                            $cinturon = "Azul";
                            break;
                        case 4:
                            $cinturon = "Morado";
                            break;
                        case 5:
                            $cinturon = "Marron";
                            break;
                        case 6: 
                            $cinturon = "Negro";
                            break;
                        default:
                            break;
                    }
                    switch($grado_alumno_inscribir){
                        case 1:
                            $grado = "Ninguno";
                            break;
                        case 2:
                            $grado = "I";
                            break;
                        case 3:
                            $grado = "II";
                            break;
                        case 4:
                            $grado = "III";
                            break;
                        case 5: 
                            $grado = "IV";
                            break;
                        default:
                            break;
                    }

                    $existencia_alumno = mysqli_fetch_array(mysqli_query($connect, "SELECT COUNT(*) AS coincidencia FROM Inscripcion WHERE ID_alumno = '$id_alumno_inscribir'"));
                    if($existencia_alumno['coincidencia'] == 0 AND $grado_alumno_inscribir >= $GRADO_CLASE){
                        ?>
                        <html>
                            <option value="<?php echo $id_alumno_inscribir?>"><?php echo "$nombre_alumno_inscribir $apellidos_alumno_inscribir, $cinturon $grado";?></option>
                        </html>
                        <?php
                    }
                }
                ?>
                <html>
                </select>

                <input type="submit" value="Inscribir">
            </form>
        </div>
        

        <div class="inscritos-container">
            <h2>Inscritos: </h2>
            <button><a href="clases_php/inscripcion_clase.php?id_clase=<?php echo $ID_CLASE;?>&opcion=1">Eliminar todas las inscripciones</a></button>
            <div class="tabla-container">
                <table border="1">
                    <thead>
                        <tr>
                            <th>Nombre</th>
                            <th>Cinturon</th>
                            <th>Grado</th>
                            <th colspan="2">Configuración</th>
                        </tr>
                    </thead>
                    <tbody>
                        </html>
                        <?php
                        $consulta_inscripcion = mysqli_query($connect, "SELECT * FROM Inscripcion WHERE ID_clase = '$ID_CLASE'");
                        while($tabla_inscripcion = $consulta_inscripcion->fetch_assoc()){
                            $ID_INSCRIPCION = $tabla_inscripcion['ID_inscripcion'];
                            $ID_ALUMNO_INSCRIPCION = $tabla_inscripcion['ID_alumno'];
                            $ASISTENCIA_INSCRIPCION = $tabla_inscripcion['Asistencia'];

                            $consulta_alumno = mysqli_query($connect, "SELECT * FROM Alumno WHERE ID_alumno = '$ID_ALUMNO_INSCRIPCION'");
                            while($tabla_alumno = $consulta_alumno->fetch_assoc()){
                                $ID_ALUMNO = $tabla_alumno['ID_alumno'];

                                $NOMBRE_ALUMNO = $tabla_alumno['Nombre'];
                                $APELLIDOS_ALUMNO = $tabla_alumno['Apellidos'];

                                $CINTURON_ALUMNO = $tabla_alumno['Cinturon'];
                                $GRADO_ALUMNO = $tabla_alumno['Grado'];

                                switch($CINTURON_ALUMNO){
                                    case 1:
                                        $cinturon_alumno = "Ninguno";
                                        break;
                                    case 2:
                                        $cinturon_alumno = "Blanco";
                                        break;
                                    case 3:
                                        $cinturon_alumno = "Azul";
                                        break;
                                    case 4:
                                        $cinturon_alumno = "Morado";
                                        break;
                                    case 5:
                                        $cinturon_alumno = "Marron";
                                        break;
                                    case 6: 
                                        $cinturon_alumno = "Negro";
                                        break;
                                    default:
                                        break;
                                }
                                switch($GRADO_ALUMNO){
                                    case 1:
                                        $grado_alumno = "Ninguno";
                                        break;
                                    case 2:
                                        $grado_alumno = "I";
                                        break;
                                    case 3:
                                        $grado_alumno = "II";
                                        break;
                                    case 4:
                                        $grado_alumno = "III";
                                        break;
                                    case 5: 
                                        $grado_alumno = "IV";
                                        break;
                                    default:
                                        break;
                                }
                                ?>
                                <html>
                                    <tr>
                                        <td><a href="../alumnos/vista_alumno_perfil.php?id_alumno=<?php echo $ID_ALUMNO;?>"><?php echo "$NOMBRE_ALUMNO $APELLIDOS_ALUMNO";?></a></td>
                                        <td><?php echo "$cinturon_alumno"?></td>
                                        <td><?php echo "$grado_alumno";?></td>
                                        <td><a href="clases_php/inscripcion_clase.php?id_clase=<?php echo $ID_CLASE;?>&id_alumno=<?php echo $ID_ALUMNO?>&opcion=0">Desinscribir</a></td>
                                        <td>
                                        </html>
                                        <?php
                                        if(isset($ASISTENCIA_INSCRIPCION)){
                                            switch($ASISTENCIA_INSCRIPCION){
                                                case 0:
                                                    ?>
                                                    <html>
                                                        <form action="asistencia_php/asistencia.php?id_alumno=<?php echo $ID_ALUMNO;?>&id_clase=<?php echo $ID_CLASE;?>&caso=2" method="post">
                                                            <label for="si">Asistió</label>
                                                            <input type="radio" name="asistencia" id="asistencia" value="1" required>
                
                                                            <label for="no">No Asistió</label>
                                                            <input type="radio" name="asistencia" id="asistencia" value="0" checked>
            
                                                            <input type="submit" value="Actualizar">
                                                        </form>
                                                    </html>
                                                    <?php
                                                    break;
                                                case 1:
                                                    ?>
                                                    <html>
                                                        <form action="asistencia_php/asistencia.php?id_alumno=<?php echo $ID_ALUMNO;?>&id_clase=<?php echo $ID_CLASE;?>&caso=1" method="post">
                                                            <label for="si">Asistió</label>
                                                            <input type="radio" name="asistencia" id="asistencia" value="1" checked required>
                
                                                            <label for="no">No Asistió</label>
                                                            <input type="radio" name="asistencia" id="asistencia" value="0">
            
                                                            <input type="submit" value="Actualizar">
                                                        </form>
                                                    </html>
                                                    <?php
                                                    break;
                                                default:
                                                    break;
                                            }
                                        }else{
                                            ?>
                                            <html>
                                                <form action="asistencia_php/asistencia.php?id_alumno=<?php echo $ID_ALUMNO;?>&id_clase=<?php echo $ID_CLASE;?>&caso=0" method="post">
                                                    <label for="si">Asistió</label>
                                                    <input type="radio" name="asistencia" id="asistencia" value="1" required>
            
                                                    <label for="no">No Asistió</label>
                                                    <input type="radio" name="asistencia" id="asistencia" value="0">
            
                                                    <input type="submit" value="Actualizar">
                                                </form>
                                            </html>
                                            <?php
                                        }  
                                        ?>
                                        <html>
                                        </td>
                                    </tr>
                                </html>
                                <?php
                            }
                        }
                        ?>
                        <html>
                    </tbody>
                </table>
            </div>

        </div>
    </div>
</body>
</html>
<?php
include("../../../includes/connection.php");
session_start();

$ID = $_SESSION['ID_profesor'];
$PODER = $_SESSION['Poder'];
$ACTIVE = $_SESSION['active'];

if(!isset($ID) OR $PODER == 0 OR $ACTIVE == 0){
    session_destroy();
    header("location: ../../../index.html");
    exit();
}


$OPCION = mysqli_real_escape_string($connect, $_GET['opcion']);

switch($OPCION){
    case 0:
        $ID_CLASE = mysqli_real_escape_string($connect, $_GET['id_clase']);
        $ID_ALUMNO = mysqli_real_escape_string($connect, $_GET['id_alumno']);

        $eliminar_inscripcion = mysqli_query($connect, "DELETE FROM Inscripcion WHERE ID_clase = '$ID_CLASE' AND ID_alumno = '$ID_ALUMNO'");
        if($eliminar_inscripcion){
            header("Location: ../vista_alumno_perfil.php?id_alumno=".$ID_ALUMNO);
        }else{
            header("Location: ../vista_alumno_perfil.php?id_alumno=".$ID_ALUMNO);
        }
        break;
    case 1:
        $ID_ALUMNO = mysqli_real_escape_string($connect, $_GET['id_alumno']);
        $ID_CLASE = mysqli_real_escape_string($connect, $_POST['clase']);

        $informacion_alumno = mysqli_query($connect, "SELECT * FROM Alumno WHERE ID_alumno = '$ID_ALUMNO'");
        while($tabla_alumno = $informacion_alumno->fetch_array()){
            $CINTURON_ALUMNO = $tabla_alumno['Cinturon'];
            $GRADO_ALUMNO = $tabla_alumno['Grado']; 
        }

        $informacion_clase = mysqli_query($connect, "SELECT * FROM Clase WHERE ID_clase = '$ID_CLASE'");
        while($tabla_clase = $informacion_clase->fetch_array()){
            $CINTURON_CLASE = $tabla_clase['Cinturon'];
            $GRADO_CLASE = $tabla_clase['Grado'];
            $CUPOS_CLASE = $tabla_clase['Cupos'];
        }

        $informacion_membresia = mysqli_query($connect, "SELECT COUNT(*) AS contador_membresia FROM Membresia WHERE ID_alumno = '$ID_ALUMNO'");
        $contador_membresia = mysqli_fetch_array($informacion_membresia);

        if($contador_membresia['contador_membresia'] >= 1){
            $MEMBRESIA = 1;

            $consulta_membresia = mysqli_query($connect, "SELECT * FROM Membresia WHERE ID_alumno = '$ID_ALUMNO'");
            while($tabla_membresia = $consulta_membresia->fetch_array()){
                $PAGO = $tabla_membresia['Pago'];
            }
        }else{
            $MEMBRESIA = 0;
        }

        $consulta_cupos = mysqli_query($connect, "SELECT COUNT(*) AS contador_cupos FROM Inscripcion WHERE ID_clase='$ID_CLASE'");
        $contador_cupos = mysqli_fetch_array($consulta_cupos);

        if($contador_cupos['contador_cupos'] >= 0){
            $cupos_clase = $CUPOS_CLASE - $contador_cupos['contador_cupos'];
        }

        if($MEMBRESIA == 1){
            if($cupos_clase == 0){
                echo '<script type="text/javascript"> alert("Ya se llenaron todos los cupos."); location="../vista_alumno_perfil.php?id_alumno='.$ID_ALUMNO.'"; </script>';
            }else if($PAGO == 1){
                if($CINTURON_ALUMNO >= $CINTURON_CLASE AND $GRADO_ALUMNO >= $GRADO_CLASE){
                    $insertar_inscripcion = mysqli_query($connect, "INSERT INTO Inscripcion (ID_clase, ID_alumno) VALUES ('$ID_CLASE','$ID_ALUMNO')");
            
                    if($insertar_inscripcion){
                        header("Location: ../vista_alumno_perfil.php?id_alumno=$ID_ALUMNO");
                    }
                }
                else{
                    echo '<script type="text/javascript"> alert("Usted no cumple los requisitos para tomar esta clase."); location="../vista_alumno_perfil.php?id_alumno='.$ID_ALUMNO.'"; </script>';
                }
            }else{
                echo '<script type="text/javascript"> alert("El alumno no ha pagado la membresia"); location="../vista_alumno_perfil.php?id_alumno='.$ID_ALUMNO.'"; </script>';
            }
        }else{
            echo '<script type="text/javascript"> alert("El alumno no tiene membresia"); location="../vista_alumno_perfil.php?id_alumno='.$ID_ALUMNO.'"; </script>';
        }

        break;
    default:
        $ID_ALUMNO = mysqli_real_escape_string($connect, $_GET['id_alumno']);
        header("Location: ../vista_alumno_perfil.php?id_alumno=".$ID_ALUMNO);

        break;
}

mysqli_close($connect);
?>
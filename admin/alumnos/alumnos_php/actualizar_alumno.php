<?php
    include("../../../includes/connection.php");
    session_start();
    
    $ID = $_SESSION['ID_profesor'];
    $PODER = $_SESSION['Poder'];
    $ACTIVE = $_SESSION['active'];
    
    if(!isset($ID) OR $PODER == 0 OR $ACTIVE == 0){
        session_destroy();
        header("location: ../../../index.html");
        exit();
    }

    $ELIMINAR_ALUMNO = mysqli_real_escape_string($connect, $_GET['eliminar']);
    $ID_ALUMNO = mysqli_real_escape_string($connect, $_GET['id_alumno']);

    if($ELIMINAR_ALUMNO == 0){
        $FECHA_GRADO_ALUMNO = $_POST['fecha_grado'];

        if($FECHA_GRADO_ALUMNO == NULL){
            $NOMBRE_ALUMNO = mysqli_real_escape_string($connect, $_POST['nombre']);
            $APELLIDOS_ALUMNO = mysqli_real_escape_string($connect, $_POST['apellido']);

            $FECHA_NACIMIENTO_ALUMNO = mysqli_real_escape_string($connect, $_POST['fecha']);
            $RUT_ALUMNO = mysqli_real_escape_string($connect, $_POST['rut']);

            $DIRECCION_ALUMNO = mysqli_real_escape_string($connect, $_POST['direccion']);
            $TELEFONO_ALUMNO = mysqli_real_escape_string($connect, $_POST['telefono']);

            $EXPERIENCIA_ALUMNO = mysqli_real_escape_string($connect, $_POST['experiencia']);
            $OBSERVACIONES_ALUMNO = mysqli_real_escape_string($connect, $_POST['observaciones']);
            $CINTURON_ALUMNO = mysqli_real_escape_string($connect, $_POST['cinturon']);
            $GRADO_ALUMNO = mysqli_real_escape_string($connect, $_POST['grado']);

            $BURPEES_ALUMNO = mysqli_real_escape_string($connect, $_POST['burpees']);
            $CANTIDAD_CLASES_ALUMNO = mysqli_real_escape_string($connect, $_POST['cantidadclases']);

            $ACTIVE_ALUMNO = mysqli_real_escape_string($connect, $_POST['active']);

            $actualizar_alumno = "UPDATE Alumno SET Nombre = '$NOMBRE_ALUMNO', Apellidos = '$APELLIDOS_ALUMNO', Rut = '$RUT_ALUMNO', Fecha_nacimiento = '$FECHA_NACIMIENTO_ALUMNO', Telefono = '$TELEFONO_ALUMNO', Direccion = '$DIRECCION_ALUMNO', Experiencia = '$EXPERIENCIA_ALUMNO', Cinturon = '$CINTURON_ALUMNO', Grado = '$GRADO_ALUMNO', Observaciones = '$OBSERVACIONES_ALUMNO', active = '$ACTIVE_ALUMNO', Burpees = '$BURPEES_ALUMNO', Cantidad_clases = '$CANTIDAD_CLASES_ALUMNO' WHERE ID_alumno = '$ID_ALUMNO'";
            if ($connect->query($actualizar_alumno) == TRUE){
                header('Location: ../vista_alumno_perfil.php?id_alumno='.$ID_ALUMNO);
            }else{
                header('Location: ../vista_alumno_perfil_configuracion.php?id_alumno='.$ID_ALUMNO);
            }
        }else{
            $NOMBRE_ALUMNO = $_POST['nombre'];
            $APELLIDOS_ALUMNO = $_POST['apellido'];
            $RUT_ALUMNO = $_POST['rut'];
            $DIRECCION_ALUMNO = $_POST['direccion'];
            $TELEFONO_ALUMNO = $_POST['telefono'];
            $FECHA_NACIMIENTO_ALUMNO = $_POST['fecha'];
            $EXPERIENCIA_ALUMNO = $_POST['experiencia'];
            $OBSERVACIONES_ALUMNO = $_POST['observaciones'];
            $CINTURON_ALUMNO = $_POST['cinturon'];
            $GRADO_ALUMNO = $_POST['grado'];
            $ACTIVE_ALUMNO = $_POST['active'];
            $BURPEES_ALUMNO = mysqli_real_escape_string($connect, $_POST['burpees']);
            $CANTIDAD_CLASES_ALUMNO = mysqli_real_escape_string($connect, $_POST['cantidadclases']);

            $actualizar_alumno = "UPDATE Alumno SET Nombre = '$NOMBRE_ALUMNO', Apellidos = '$APELLIDOS_ALUMNO', Rut = '$RUT_ALUMNO', Fecha_nacimiento = '$FECHA_NACIMIENTO_ALUMNO', Telefono = '$TELEFONO_ALUMNO', Direccion = '$DIRECCION_ALUMNO', Experiencia = '$EXPERIENCIA_ALUMNO', Cinturon = '$CINTURON_ALUMNO', Grado = '$GRADO_ALUMNO', Observaciones = '$OBSERVACIONES_ALUMNO', Fecha_grado = '$FECHA_GRADO_ALUMNO', active = '$ACTIVE_ALUMNO', Burpees = '$BURPEES_ALUMNO', Cantidad_clases = '$CANTIDAD_CLASES_ALUMNO' WHERE ID_alumno = '$ID_ALUMNO'";
            if ($connect->query($actualizar_alumno) == TRUE){
                header('Location: ../vista_alumno_perfil.php?id_alumno='.$ID_ALUMNO);
            }else{
                header('Location: ../vista_alumno_perfil_configuracion.php?id_alumno='.$ID_ALUMNO);
            }
        } 
    }else{
        $eliminar_alumno = "DELETE FROM Alumno WHERE ID_alumno = '$ID_ALUMNO'";
    
        if ($connect->query($eliminar_alumno) === TRUE) {
            header('location: ../vista_alumnos.php');
        } else {
            header('Location: ../vista_alumno_perfil_configuracion.php?id_alumno='.$ID_ALUMNO);
        }
    }

    mysqli_close($connect);
?>
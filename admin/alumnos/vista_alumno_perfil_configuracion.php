<?php
include("../../includes/connection.php");
session_start();

$ID = $_SESSION['ID_profesor'];
$NOMBRE = $_SESSION['Nombre'];
$APELLIDOS = $_SESSION['Apellidos'];

$PODER = $_SESSION['Poder'];
$ACTIVE = $_SESSION['active'];

if(!isset($ID) OR $PODER == 0 OR $ACTIVE == 0){
    session_destroy();
    header("location: ../../index.html");
    exit();
}

$ID_ALUMNO = mysqli_real_escape_string($connect, $_GET['id_alumno']);

$alumnos = "SELECT * FROM Alumno WHERE ID_alumno='$ID_ALUMNO'";
$peticion_alumnos = mysqli_query($connect, $alumnos);

while($tabla = $peticion_alumnos->fetch_assoc()){
    $NOMBRE_ALUMNO = $tabla['Nombre'];
    $APELLIDO_ALUMNO = $tabla['Apellidos'];

    $RUT_ALUMNO = $tabla['Rut'];
    $FECHA_NACIMIENTO_ALUMNO = $tabla['Fecha_nacimiento'];

    $TELEFONO_ALUMNO = $tabla['Telefono'];
    $DIRECCION_ALUMNO = $tabla['Direccion'];
    $MAIL_ALUMNO = $tabla['Mail'];

    $CINTURON_ALUMNO = $tabla['Cinturon'];
    $GRADO_ALUMNO = $tabla['Grado'];
    $FECHA_GRADO_ALUMNO = $tabla['Fecha_grado'];

    $OBSERVACIONES_ALUMNO = $tabla['Observaciones'];
    $EXPERIENCIA_ALUMNO = $tabla['Experiencia'];

    $BURPEES_ALUMNO = $tabla['Burpees'];
    $CANTIDAD_CLASES_ALUMNO = $tabla['Cantidad_clases'];

    $ACTIVE_ALUMNO = $tabla['active'];

    switch($CINTURON_ALUMNO){
        case 1:
            $cinturon = "Ninguno";
            break;
        case 2:
            $cinturon = "Blanco";
            break;
        case 3:
            $cinturon = "Azul";
            break;
        case 4:
            $cinturon = "Morado";
            break;
        case 5:
            $cinturon = "Cafe";
            break;
        case 6: 
            $cinturon = "Negro";
            break;
        default:
            break;
    }
    
    switch($GRADO_ALUMNO){
        case 1:
            $grado = "Ninguno";
            break;
        case 2:
            $grado = "I";
            break;
        case 3:
            $grado = "II";
            break;
        case 4:
            $grado = "III";
            break;
        case 5: 
            $grado = "IV";
            break;
        default:
            break;
    }

    switch($ACTIVE_ALUMNO){
        case 0:
            $active = "Cuenta desactivada";
            break;
        case 1:
            $active = "Cuenta activada";
            break;
        default:
            break;
    }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Configuracion de <?php echo "$NOMBRE_ALUMNO $APELLIDO_ALUMNO";?></title>
</head>
<body>
    <ul>
        <li class="log"> <?php echo "<p>Bienvenido $NOMBRE $APELLIDOS</p>";?> </li>

        <li><a href="../../profesor/profesor_php/home_profesor.php">Inicio</a></li>
        <li class="active"><a href="../../profesor/perfil_profesor.php">Perfil</a></li>
        <li><a href="../../profesor/horario_profesor.php">Horario</a></li>
        <li><a href="../../profesor/vista_clase.php">Clases</a></li>
        <li><a href="../../general/general_php/logout.php">Cerrar sesion</a></li>
    </ul>

    <h1>Editar alumno: <?php echo "$NOMBRE_ALUMNO $APELLIDO_ALUMNO";?></h1>

    <button><a href="vista_alumno_perfil.php?id_alumno=<?php echo $ID_ALUMNO;?>">volver</a></button>

    <div class="contenedor_formulario">
        <form action="alumnos_php/actualizar_alumno.php?id_alumno=<?php echo $ID_ALUMNO;?>&eliminar=0" method="post">
            <div class="nombre">
                <label for="nombre"><b>Nombres</b></label>
                <input class="controls" type="text" placeholder="Ingrese sus nombres" name="nombre" id="nombre" value="<?php echo $NOMBRE_ALUMNO;?>">
            </div>
            <div class="apellido">
                <label for="apellido"><b>Apellidos</b></label>
                <input class="controls" type="text" placeholder="Ingrese su apellido" name="apellido" id="apellido" value="<?php echo $APELLIDO_ALUMNO;?>">
            </div>
            <div class="rut">
                <label for="rut"><b>RUT</b></label>
                <input class="controls" type="text" placeholder="Ingrese su RUT, ej: 11.111.111-1" name="rut" id="rut" value="<?php echo $RUT_ALUMNO;?>">
            </div>
            <div class="direccion">
                <label for="direccion"><b>Dirección</b></label>
                <input class="controls" type="text" placeholder="Ingrese su dirección" name="direccion" id="direccion" value="<?php echo $DIRECCION_ALUMNO;?>">
            </div>
            <div class="telefono">
                <label for="telefono"><b>Teléfono</b></label>
                <input class="controls" type="text" placeholder="Ingrese su teléfono" name="telefono" id="telefono" value="<?php echo $TELEFONO_ALUMNO;?>">
            </div>
            <div class="fecha">
                <label for="fecha"><b>Fecha de nacimiento</b></label>
                <input class="controls" type="date" name="fecha" id="fecha" value="<?php echo $FECHA_NACIMIENTO_ALUMNO;?>">
            </div>
            
            <div class="experiencia">
                <label for="experiencia"><b>Experiencia</b></label>
                <br>
                <textarea name="experiencia" id="experiencia" cols="30" rows="7" class="controls" maxlength="255" minlength="1"  placeholder="Escriba su experiencia en las artes marciales"><?php echo $EXPERIENCIA_ALUMNO;?></textarea>
            </div>

            <div class="observaciones">
                    <label for="observaciones"><b>Observaciones</b></label>
                    <br>
                    <textarea name="observaciones" id="observaciones" cols="30" rows="7" class="controls" minlength="1"  placeholder="Observaciones del alumno"><?php echo $OBSERVACIONES_ALUMNO;?></textarea>
            </div>

            <div class="fecha_grado">
                <label for="fecha_grado"><b>Fecha de grado</b></label>
                <input class="controls" type="date" name="fecha_grado" id="fecha_grado" value="<?php echo $FECHA_GRADO_ALUMNO;?>">
            </div>

            <div class="cinturon">
                <label for="cinturon"><b>Cinturon</b></label>
                <select class="cinturon" name="cinturon" id="cinturon">
                    <option value="<?php echo $CINTURON_ALUMNO;?>" selected hidden ><?php echo $cinturon;?></option>
                    <option value="1">Ninguno</option>
                    <option value="2">Blanco</option>
                    <option value="3">Azul</option>
                    <option value="4">Morado</option>
                    <option value="5">Cafe</option>
                    <option value="6">Negro</option>
                </select>
            </div>

            <div class="grado">
                <label for="grado"><b>Grado</b></label>
                <select class="select" name="grado" id="grado" >
                    <option value="<?php echo $GRADO_ALUMNO;?>" selected hidden><?php echo $grado;?></option>
                    <option value="1">Ninguno</option>
                    <option value="2">I</option>
                    <option value="3">II</option>
                    <option value="4">III</option>
                    <option value="5">IV</option>
                </select>
            </div>    

            <div class="active">
                <label for="active"><b>Estado</b></label>
                <select class="active" name="active" id="active">
                    <option value="<?php echo $ACTIVE_ALUMNO?>" selected hidden><?php echo $active;?></option>
                    <option value="0">Desactivar cuenta</option>
                    <option value="1">Activar cuenta</option>
                </select>
            </div>
            
            <div>
                <label for="burpees"><b>Burpees</b></label>
                <input type="number" name="burpees" id="burpees" value="<?php echo $BURPEES_ALUMNO;?>">
            </div>

            <div>
                <label for="cantidadclases">Cantidad Clases</label>
                <input type="number" name="cantidadclases" id="cantidadclases" value="<?php echo $CANTIDAD_CLASES_ALUMNO;?>">
            </div>

            <br>

            <div class="enviar">
                <input type="submit" value="Actualizar perfil" name="actualizar" class="buttons">  
            </div>   
        </form>
    </div>
   
    <div class="contenedor_eliminar">
        <h2>Eliminar cuenta</h2>
        <div id="eliminar_container" class="centro" >
            <button id="boton_eliminar" class="buttons" >Eliminar cuenta</button>
        </div>
        <div id="confirmacion_container" class="centro">
            <label for="boton_eliminar_confirmacion" class="centro">¿Esta seguro de que quiere eliminar su cuenta?</label>
            <br>
            <button id="boton_eliminar_confirmacion" class="buttons"><a href="alumnos_php/actualizar_alumno.php?id_alumno=<?php echo $ID_ALUMNO;?>&eliminar=1">Eliminar cuenta</a></button>
            <button id="boton_eliminar_cancelacion" class="buttons">Cancelar</button>
        </div>
    </div>
    

    <script>
        document.getElementById("confirmacion_container").style.display = "none";

        document.getElementById("boton_eliminar").onclick = mostrarDiv;
        document.getElementById("boton_eliminar_cancelacion").onclick = MostrarDiv_eliminar;
        
        function mostrarDiv(){
            document.getElementById("eliminar_container").style.display = "none";
            document.getElementById("confirmacion_container").style.display = "block";
        }

        function MostrarDiv_eliminar(){
            document.getElementById("eliminar_container").style.display = "block";
            document.getElementById("confirmacion_container").style.display = "none";
        }

        //Verificador de contraseñas.
    </script>  
    
</body>
</html>
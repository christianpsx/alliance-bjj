<?php
include("../../../../includes/connection.php");
session_start();

$ID = $_SESSION['ID_profesor'];
$PODER = $_SESSION['Poder'];
$ACTIVE = $_SESSION['active'];

if(!isset($ID) OR $PODER == 0 OR $ACTIVE == 0){
    session_destroy();
    header("location: ../../../../general/general_php/logout.php");
    exit();
}

$ID_ALUMNO = mysqli_real_escape_string($connect, $_GET['id_alumno']);
$OPCION = mysqli_real_escape_string($connect, $_GET['opcion']);

switch($OPCION){
    case 0:
        $pago = mysqli_real_escape_string($connect, $_POST['pago']);

        $fecha_pago = mysqli_real_escape_string($connect, $_POST['fecha_pago']);
        $fecha_termino =  mysqli_real_escape_string($connect, $_POST['fecha_termino']);

        $crear_membresia = mysqli_query($connect, "INSERT INTO Membresia (ID_alumno, Pago, Fecha_pago, Fecha_termino) VALUES ('$ID_ALUMNO', '$pago', '$fecha_pago', '$fecha_termino')");
        if ($crear_membresia) {
            header('Location: ../../vista_alumno_perfil.php?id_alumno='.$ID_ALUMNO);
        } else {
            header("Location: ../vista_alumno_crear_membresia.php?id_alumno=".$ID_ALUMNO);
        }

        break;
    case 1:
        $ID_MEMBRESIA = mysqli_real_escape_string($connect, $_GET['id_membresia']);

        $pago = mysqli_real_escape_string($connect, $_POST['pago']);
        $fecha_pago = mysqli_real_escape_string($connect, $_POST['fecha_pago']);
        $fecha_termino =  mysqli_real_escape_string($connect, $_POST['fecha_termino']);

        $actualizar_membresia = mysqli_query($connect, "UPDATE Membresia SET Pago = '$pago', Fecha_pago = '$fecha_pago', Fecha_termino = '$fecha_termino' WHERE ID_membresia = '$ID_MEMBRESIA'");
        if ($actualizar_membresia === TRUE) {
            header('Location: ../../vista_alumno_perfil.php?id_alumno='.$ID_ALUMNO);
        } else {
            header("Location: ../vista_alumno_actualizar_membresia.php?id_alumno=".$ID_ALUMNO);
        }

        break;
    case 2:
        $ID_MEMBRESIA = mysqli_real_escape_string($connect, $_GET['id_membresia']);

        $eliminar_membresia = mysqli_query($connect, "DELETE FROM Membresia WHERE ID_membresia = '$ID_MEMBRESIA'");
        if ($eliminar_membresia === TRUE) {
            header('Location: ../../vista_alumno_perfil.php?id_alumno='.$ID_ALUMNO);
        } else {
            header("Location: ../vista_alumno_actualizar_membresia.php?id_alumno=".$ID_ALUMNO);
        }
        break;
    default: 
        header('Location: ../../vista_alumno_perfil.php?id_alumno='.$ID_ALUMNO);
        break;
}

mysqli_close($connect);
?>
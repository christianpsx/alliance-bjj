<?php
include("../../../../includes/connection.php");
session_start();

$ID = $_SESSION['ID_profesor'];
$PODER = $_SESSION['Poder'];
$ACTIVE = $_SESSION['active'];

if(!isset($ID) OR $PODER == 0 OR $ACTIVE == 0){
    session_destroy();
    header("location: ../../../../general/general_php/logout.php");
    exit();
}

$ID_ALUMNO = mysqli_real_escape_string($connect, $_GET['id_alumno']);
$ACTIVE_ALUMNO = mysqli_real_escape_string($connect, $_POST['active']);

$actualizar_estado = mysqli_query($connect, "UPDATE Alumno SET active = '$ACTIVE_ALUMNO' WHERE ID_alumno = '$ID_ALUMNO'");

if($actualizar_estado){
    header('Location: ../../vista_alumno_perfil.php?id_alumno='.$ID_ALUMNO);
}else{
    header("Location: ../vista_alumno_actualizar_membresia.php?id_alumno=".$ID_ALUMNO);
}
mysqli_close($connect);
?>
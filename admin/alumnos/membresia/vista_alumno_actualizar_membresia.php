<?php
include("../../../includes/connection.php");
session_start();

$ID = $_SESSION['ID_profesor'];

$NOMBRE = $_SESSION['Nombre'];
$APELLIDOS = $_SESSION['Apellidos'];

$PODER = $_SESSION['Poder'];
$ACTIVE = $_SESSION['active'];

if(!isset($ID) OR $PODER == 0 OR $ACTIVE == 0){
    session_destroy();
    header("location: ../../../index.html");
    exit();
}

$ID_ALUMNO = mysqli_real_escape_string($connect, $_GET['id_alumno']);


$peticion_alumnos = mysqli_query($connect, "SELECT * FROM Alumno WHERE ID_alumno='$ID_ALUMNO'");
while($tabla = $peticion_alumnos->fetch_assoc()){
    $ACTIVE_ALUMNO = $tabla['active'];

    switch($ACTIVE_ALUMNO){
        case 0:
            $active_alumno = "Cuenta desactivada";
            break;
        case 1:
            $active_alumno = "Cuenta activada";
            break;
        default:
            break;
    }
}

$peticion_membresia = mysqli_query($connect, "SELECT * FROM Membresia WHERE ID_alumno='$ID_ALUMNO'");

while($tabla = $peticion_membresia->fetch_assoc()){
    $ID_MEMBRESIA = $tabla['ID_membresia'];

    $PAGO_MEMBRESIA = $tabla['Pago'];

    $FECHA_PAGO = $tabla['Fecha_pago'];
    $FECHA_TERMINO = $tabla['Fecha_termino'];

    switch($PAGO_MEMBRESIA){
        case 0:
            $pago_membresia = "No pagado";
            break;
        case 1:
            $pago_membresia = "Pagado";
            break;
        default:
            break;
    }
}
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Actualizar membresia</title>
</head>
<body>
    <ul>
        <li class="log"> <?php echo "<p>Bienvenido $NOMBRE $APELLIDOS</p>";?> </li>

        <li><a href="../../../profesor/profesor_php/home_profesor.php">Inicio</a></li>
        <li class="active"><a href="../../../profesor/perfil_profesor.php">Perfil</a></li>
        <li><a href="../../../profesor/horario_profesor.php">Horario</a></li>
        <li><a href="../../../profesor/vista_clase.php">Clases</a></li>
        <li><a href="../../../general/general_php/logout.php">Cerrar sesion</a></li>
    </ul>


    <h1>Actualizar membresia</h1>

    <button><a href="../vista_alumno_perfil.php?id_alumno=<?php echo $ID_ALUMNO?>">Volver</a></button>

    <div class="membresia_formulario">
        <form action="membresia_php/membresia_crud.php?id_alumno=<?php echo $ID_ALUMNO;?>&id_membresia=<?php echo $ID_MEMBRESIA?>&opcion=1" method="post">
            <div class="pago">
                <label for="pago">Pago:</label>
                <select name="pago" id="pago">
                    <option value="<?php echo $PAGO_MEMBRESIA;?>" selected hidden><?php echo $pago_membresia;?></option>
                    <option value="0">No ha pagado</option>
                    <option value="1">Ha pagado</option>
                </select>
            </div>

            <div class="fecha_pago">
                <label for="fecha_pago">Fecha de pago: </label>
                <input type="date" name="fecha_pago" id="fecha_pago" value="<?php echo $FECHA_PAGO;?>">
            </div>

            <div class="fecha_termino">
                <label for="fecha_termino">Fecha de termino: </label>
                <input type="date" name="fecha_termino" id="fecha_termino" value="<?php echo $FECHA_TERMINO;?>">
            </div>
            <br>
            <div class="actualizar">
            <input type="submit" value="Actualizar membresia">
            </div>
        </form>
    </div>

    <br>

    <div class="estado_cuenta">
        <h2>Opciones de estado de cuenta:</h2>

        <form action="membresia_php/estado.php?id_alumno=<?php echo $ID_ALUMNO;?>" method="post">
            <div class="active">
                <label for="active">Estado de cuenta: </label>
                <select name="active" id="active">
                    <option value="<?php echo $ACTIVE_ALUMNO;?>" selected hidden><?php echo $active_alumno;?></option>
                    <option value="0">Desactivar cuenta</option>
                    <option value="1">Activar cuenta</option>
                </select>
            </div>
            <br>
            <div>
                <input type="submit" value="Actualizar cuenta">
            </div>
        </form>

        <br>
        <h2>Eliminar membresia</h2>

        <div class="eliminar_membresia">
            <button><a href="membresia_php/membresia_crud.php?id_alumno=<?php echo $ID_ALUMNO?>&id_membresia=<?php echo $ID_MEMBRESIA;?>&opcion=2">Eliminar membresia</a></button>
        </div>
    </div>
</body>
</html>
<?php
include("../../../includes/connection.php");
session_start();

$ID = $_SESSION['ID_profesor'];

$NOMBRE = $_SESSION['Nombre'];
$APELLIDOS = $_SESSION['Apellidos'];

$PODER = $_SESSION['Poder'];
$ACTIVE = $_SESSION['active'];

if(!isset($ID) OR $PODER == 0 OR $ACTIVE == 0){
    session_destroy();
    header("location: ../../../index.html");
    exit();
}

$ID_ALUMNO = mysqli_real_escape_string($connect, $_GET['id_alumno']);

$alumnos = "SELECT * FROM Alumno WHERE ID_alumno='$ID_ALUMNO'";
$peticion_alumnos = mysqli_query($connect, $alumnos);

while($tabla = $peticion_alumnos->fetch_assoc()){
    $NOMBRE_ALUMNO = $tabla['Nombre'];
    $APELLIDO_ALUMNO = $tabla['Apellidos'];

    $ACTIVE_ALUMNO = $tabla['active'];
}
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Crear membresia</title>
</head>
<body>
    <ul>
        <li class="log"> <?php echo "<p>Bienvenido $NOMBRE $APELLIDOS</p>";?> </li>

        <li><a href="../../../profesor/profesor_php/home_profesor.php">Inicio</a></li>
        <li class="active"><a href="../../../profesor/perfil_profesor.php">Perfil</a></li>
        <li><a href="../../../profesor/horario_profesor.php">Horario</a></li>
        <li><a href="../../../profesor/vista_clase.php">Clases</a></li>
        <li><a href="../../../general/general_php/logout.php">Cerrar sesion</a></li>
    </ul>
    
    <h1>Crear membresia a <?php echo "$NOMBRE_ALUMNO $APELLIDO_ALUMNO";?></h1>

    <button><a href="../vista_alumno_perfil.php?id_alumno=<?php echo $ID_ALUMNO?>">Volver</a></button>

    <div class="crear-membresia">
        <form action="membresia_php/membresia_crud.php?id_alumno=<?php echo $ID_ALUMNO?>&opcion=0" method="post">
            <div class="pago">
                <label for="pago">Estado de pago: </label>

                <select name="pago" id="pago">
                    <option value="0">El alumno no ha pagado</option>
                    <option value="1">El alumno ha pagado</option>
                </select>
            </div>

            <div class="fecha_pago">
                <label for="fecha_pago">Fecha de pago: </label>
                <input type="date" name="fecha_pago" id="fecha_pago">
            </div>

            <div class="fecha_termino">
                <label for="fecha_termino">Fecha de termino: </label>
                <input type="date" name="fecha_termino" id="fecha_termino">
            </div>

            <div class="enviar">
                <input type="submit" value="Crear membresia" name="crear" class="buttons">  
            </div>  
        </form>
    </div>
    
</body>
</html>
<?php
//Datos del profesor:
session_start();

include("../../../includes/connection.php");
    session_start();
    
    $ID = $_SESSION['ID_profesor'];
    $PODER = $_SESSION['Poder'];
    $ACTIVE = $_SESSION['active'];
    
    if(!isset($ID) OR $PODER == 0 OR $ACTIVE == 0){
        session_destroy();
        header("location: ../../../index.html");
        exit();
    }



$CASO = mysqli_real_escape_string($connect, $_GET['caso']);

switch($CASO){
    case 0:
        //Este caso corresponde cuando el formulario esta Asistio == NULL y No asistio == NULL
        $ID_CLASE = mysqli_real_escape_string($connect, $_GET['id_clase']);
        $ID_ALUMNO = mysqli_real_escape_string($connect, $_GET['id_alumno']);

        $ASISTENCIA = mysqli_real_escape_string($connect, $_POST['asistencia']);

        $informacion_alumno = mysqli_query($connect, "SELECT * FROM Alumno WHERE ID_alumno = '$ID_ALUMNO'");
        if($informacion_alumno){
            while($tabla_alumno = $informacion_alumno->fetch_array()){
                $CANTIDAD_CLASES = $tabla_alumno['Cantidad_clases'];
                $BURPEES = $tabla_alumno['Burpees'];
            }

            if($CANTIDAD_CLASES == 0){
                switch($ASISTENCIA){
                    case 0:
                        $actualizar_inscripcion = mysqli_query($connect, "UPDATE Inscripcion SET Asistencia = '$ASISTENCIA' WHERE ID_clase = '$ID_CLASE' AND ID_alumno = '$ID_ALUMNO'");
                        
                        $BURPEES = $BURPEES + 50;
                        $actualizar_alumno = mysqli_query($connect, "UPDATE Alumno SET Burpees = '$BURPEES' WHERE ID_alumno = '$ID_ALUMNO'");
                        if($actualizar_alumno){
                            header("Location: ../vista_alumno_perfil.php?id_alumno=".$ID_ALUMNO);
                        }
                        break;
                    case 1:
                        $actualizar_inscripcion = mysqli_query($connect, "UPDATE Inscripcion SET Asistencia = '$ASISTENCIA' WHERE ID_clase = '$ID_CLASE' AND ID_alumno = '$ID_ALUMNO'");
                        if($actualizar_inscripcion){
                            $contador_asistencia = mysqli_fetch_array(mysqli_query($connect, "SELECT COUNT(*) AS asistencia FROM Inscripcion WHERE ID_alumno = '$ID_ALUMNO' AND Asistencia = '1'"));
                            $asistencia_total = $contador_asistencia['asistencia'];
        
                            $actualizar_alumno = mysqli_query($connect, "UPDATE Alumno SET Cantidad_clases = '$asistencia_total' WHERE ID_alumno = '$ID_ALUMNO'");
                            if($actualizar_alumno){
                                header("Location: ../vista_alumno_perfil.php?id_alumno=".$ID_ALUMNO);
                            }
                        }
                        break;
                    default:
                        header("Location: ../vista_alumno_perfil.php?id_alumno=".$ID_ALUMNO);
                        break;
                }
            }else{
                switch($ASISTENCIA){
                    case 0:
                        $actualizar_inscripcion = mysqli_query($connect, "UPDATE Inscripcion SET Asistencia = '$ASISTENCIA' WHERE ID_clase = '$ID_CLASE' AND ID_alumno = '$ID_ALUMNO'");
                        
                        $BURPEES = $BURPEES + 50;
        
                        $actualizar_alumno = mysqli_query($connect, "UPDATE Alumno SET Burpees = '$BURPEES' WHERE ID_alumno = '$ID_ALUMNO'");
                        if($actualizar_alumno){
                            header("Location: ../vista_alumno_perfil.php?id_alumno=".$ID_ALUMNO);
                        }
                        break;
                    case 1:
                        $actualizar_inscripcion = mysqli_query($connect, "UPDATE Inscripcion SET Asistencia = '$ASISTENCIA' WHERE ID_clase = '$ID_CLASE' AND ID_alumno = '$ID_ALUMNO'");
                        if($actualizar_inscripcion){
                            $CANTIDAD_CLASES = $CANTIDAD_CLASES + 1;
                            $actualizar_alumno = mysqli_query($connect, "UPDATE Alumno SET Cantidad_clases = '$CANTIDAD_CLASES' WHERE ID_alumno = '$ID_ALUMNO'");
                            if($actualizar_alumno){
                                header("Location: ../vista_alumno_perfil.php?id_alumno=".$ID_ALUMNO);
                            }
                        }
                        break;
                    default:
                        header("Location: ../vista_alumno_perfil.php?id_alumno=".$ID_ALUMNO);
                        break;
                }
            }
        }

        break;    
    case 1:
        //Este caso corresponde cuando el formulario esta Asistio == (1, 0) y No asistio == NULL
        $ID_CLASE = mysqli_real_escape_string($connect, $_GET['id_clase']);
        $ID_ALUMNO = mysqli_real_escape_string($connect, $_GET['id_alumno']);

        $ASISTENCIA = mysqli_real_escape_string($connect, $_POST['asistencia']);

        $informacion_alumno = mysqli_query($connect, "SELECT * FROM Alumno WHERE ID_alumno = '$ID_ALUMNO'");
        if($informacion_alumno){
            while($tabla_alumno = $informacion_alumno->fetch_array()){
                $CANTIDAD_CLASES = $tabla_alumno['Cantidad_clases'];
                $BURPEES = $tabla_alumno['Burpees'];
            }

            if($CANTIDAD_CLASES >= 1){
                switch($ASISTENCIA){
                    case 0:
                        $actualizar_inscripcion = mysqli_query($connect, "UPDATE Inscripcion SET Asistencia = '$ASISTENCIA' WHERE ID_clase = '$ID_CLASE' AND ID_alumno = '$ID_ALUMNO'");
                        if($actualizar_inscripcion){
                            $CANTIDAD_CLASES = $CANTIDAD_CLASES - 1;
                            $BURPEES = $BURPEES + 50;
                            $actualizar_alumno = mysqli_query($connect, "UPDATE Alumno SET Cantidad_clases = '$CANTIDAD_CLASES', Burpees = '$BURPEES' WHERE ID_alumno = '$ID_ALUMNO'");
                            if($actualizar_alumno){
                                header("Location: ../vista_alumno_perfil.php?id_alumno=".$ID_ALUMNO);
                            }
                        }
        
                        break;
                    case 1:
                        header("Location: ../vista_alumno_perfil.php?id_alumno=".$ID_ALUMNO);
                        break;
                    default:
                        header("Location: ../vista_alumno_perfil.php?id_alumno=".$ID_ALUMNO);
                        break;
                }
            }
        }else{
            header("Location: ../vista_alumno_perfil.php?id_alumno=".$ID_ALUMNO);
        }

        break;
    case 2:
        //Este caso corresponde cuando el formulario esta Asistio == NULL y No asistio == (1, 0)
        $ID_CLASE = mysqli_real_escape_string($connect, $_GET['id_clase']);
        $ID_ALUMNO = mysqli_real_escape_string($connect, $_GET['id_alumno']);

        $ASISTENCIA = mysqli_real_escape_string($connect, $_POST['asistencia']);

        $informacion_alumno = mysqli_query($connect, "SELECT * FROM Alumno WHERE ID_alumno = '$ID_ALUMNO'");
        if($informacion_alumno){
            while($tabla_alumno = $informacion_alumno->fetch_array()){
                $CANTIDAD_CLASES = $tabla_alumno['Cantidad_clases'];
                $BURPEES = $tabla_alumno['Burpees'];
            }

            if($CANTIDAD_CLASES == 0){
                switch($ASISTENCIA){
                    case 0:
                        header("Location: ../vista_alumno_perfil.php?id_alumno =".$ID_ALUMNO);
                        break;
                    case 1:
                        $actualizar_inscripcion = mysqli_query($connect, "UPDATE Inscripcion SET Asistencia = '$ASISTENCIA' WHERE ID_clase = '$ID_CLASE' AND ID_alumno = '$ID_ALUMNO'");
                        if($actualizar_inscripcion){
                            $CANTIDAD_CLASES =  $CANTIDAD_CLASES + 1;
                            
                            $actualizar_alumno = mysqli_query($connect, "UPDATE Alumno SET Cantidad_clases = '$CANTIDAD_CLASES' WHERE ID_alumno = '$ID_ALUMNO'");
                            if($actualizar_alumno){
                                header("Location: ../vista_alumno_perfil.php?id_alumno=".$ID_ALUMNO);
                            }
                        }
        
                        break;
                    default:
                        header("Location: ../vista_alumno_perfil.php?id_alumno=".$ID_ALUMNO);
                        break;
                }
            }else{
                switch($ASISTENCIA){
                    case 0:
                        header("Location: ../vista_alumno_perfil.php?id_alumno=".$ID_ALUMNO);
                        break;
                    case 1:
                        $actualizar_inscripcion = mysqli_query($connect, "UPDATE Inscripcion SET Asistencia = '$ASISTENCIA' WHERE ID_clase = '$ID_CLASE' AND ID_alumno = '$ID_ALUMNO'");
                        if($actualizar_inscripcion){
                            $CANTIDAD_CLASES =  $CANTIDAD_CLASES + 1;
                            
                            $actualizar_alumno = mysqli_query($connect, "UPDATE Alumno SET Cantidad_clases = '$CANTIDAD_CLASES' WHERE ID_alumno = '$ID_ALUMNO'");
                            if($actualizar_alumno){
                                header("Location: ../vista_alumno_perfil.php?id_alumno=".$ID_ALUMNO);
                            }
                        }
        
                        break;
                    default:
                        header("Location: ../vista_alumno_perfil.php?id_alumno=".$ID_ALUMNO);
                        break;
                }
            }
        }

        break;
    default:
        header("Location: ../vista_alumno_perfil.php?id_alumno=".$ID_ALUMNO);
        break;

}

mysqli_close($connect);
?>
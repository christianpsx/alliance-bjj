<?php
include("../../includes/connection.php");
session_start();

$ID = $_SESSION['ID_profesor'];
$NOMBRE = $_SESSION['Nombre'];
$APELLIDOS = $_SESSION['Apellidos'];

$RUT = $_SESSION['Rut'];
$FECHA_N = $_SESSION['Fecha_nacimiento'];

$DIRECCION = $_SESSION['Direccion'];
$MAIL = $_SESSION['Mail'];
$TELEFONO = $_SESSION['Telefono'];

$CINTURON = $_SESSION['Cinturon'];

$PODER = $_SESSION['Poder'];
$ACTIVE = $_SESSION['active'];


if(!isset($ID) OR $PODER == 0 OR $ACTIVE == 0){
    session_destroy();
    header("location: ../../index.html");
    exit();
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="css/vista_alumnos.css">
    <title>Alumnos</title>
</head>
<body>
    <ul>
        <li class="log"> <?php echo "<p>Bienvenido $NOMBRE $APELLIDOS</p>";?> </li>

        <li><a href="../../profesor/profesor_php/home_profesor.php">Inicio</a></li>
        <li class="active"><a href="../../profesor/perfil_profesor.php">Perfil</a></li>
        <li><a href="../../profesor/horario_profesor.php">Horario</a></li>
        <li><a href="../../profesor/vista_clase.php">Clases</a></li>
        <li><a href="../../general/general_php/logout.php">Cerrar sesion</a></li>
    </ul>

    <h1>Alumnos</h1>
    
    <button><a href="../admin_home.php">volver</a></button>

    <table class="alliance" border="1">
        <tr>
            <th>Nombre</th>
            <th>Cinturon</th>
            <th>Grado</th>
            <th>Estado</th>
            <th colspan="2">Membresia</th>
        </tr>
        
        <?php
        $alumnos = "SELECT * FROM Alumno";
        $peticion_alumnos = mysqli_query($connect, $alumnos);

        while($tabla = $peticion_alumnos->fetch_assoc()){
            $ID_ALUMNO = $tabla['ID_alumno'];

            $NOMBRE_ALUMNO = $tabla['Nombre'];
            $APELLIDO_ALUMNO = $tabla['Apellidos'];

            $CINTURON_ALUMNO = $tabla['Cinturon'];
            $GRADO_ALUMNO = $tabla['Grado'];

            $ACTIVE_ALUMNO = $tabla['active'];

            switch($CINTURON_ALUMNO){
                case 1:
                    $cinturon = "Ninguno";
                    break;
                case 2:
                    $cinturon = "Blanco";
                    break;
                case 3:
                    $cinturon = "Azul";
                    break;
                case 4:
                    $cinturon = "Morado";
                    break;
                case 5:
                    $cinturon = "Cafe";
                    break;
                case 6: 
                    $cinturon = "Negro";
                    break;
                default:
                    break;
            }
            
            switch($GRADO_ALUMNO){
                case 1:
                    $grado = "Ninguno";
                    break;
                case 2:
                    $grado = "I";
                    break;
                case 3:
                    $grado = "II";
                    break;
                case 4:
                    $grado = "III";
                    break;
                case 5: 
                    $grado = "IV";
                    break;
                default:
                    break;
            }

            switch($ACTIVE_ALUMNO){
                case 0:
                    $active_alumno = "Cuenta desactivada";
                    break;
                case 1:
                    $active_alumno = "Cuenta activada";
                    break;
                default:
                    break;
            }

            
            $consulta_membresia = mysqli_query($connect, "SELECT * FROM Membresia WHERE ID_alumno = '$ID_ALUMNO'");
            while($tabla_membresia = $consulta_membresia->fetch_assoc()){
                $PAGO = $tabla_membresia['Pago'];
                $FECHA_TERMINO = $tabla_membresia['Fecha_termino'];

                switch($PAGO){
                    case 0:
                        $pago = "No ha pagado";
                        break;
                    case 1:
                        $pago = "Ha pagado";
                    default: break;
                }
                $verificador_membresia = mysqli_fetch_array(mysqli_query($connect, "SELECT COUNT(*) AS membresia FROM Membresia WHERE ID_alumno = '$ID_ALUMNO'"));
                ?>
            <html>
                <tr>
                    <td><a href="vista_alumno_perfil.php?id_alumno=<?php echo $ID_ALUMNO;?>"><?php echo $NOMBRE_ALUMNO.' '.$APELLIDO_ALUMNO;?></a></td>
                    <td><?php echo $cinturon;?></td>
                    <td><?php echo $grado;?></td>
                    <td><?php echo $active_alumno;?></td>
                    </html>
                        <?php
                        if($verificador_membresia['membresia'] == 0){
                            ?>
                            <html>
                                <td>No tiene</td>
                                <td>No hay fecha</td>
                            </html>
                            <?php
                        }else{
                            if($PAGO == 0){
                                ?>
                                <html>
                                    <td style="color: red; font-weight: bold;"><?php echo $pago;?></td>
                                    <td><?php echo $FECHA_TERMINO?></td>
                                </html>
                                <?php
                            }else{
                                ?>
                                <html>
                                    <td><?php echo $pago;?></td>
                                    <td><?php echo $FECHA_TERMINO?></td>
                                </html>
                                <?php
                            }
                        
                        }
                        ?>
                    <html>
                </tr>
            </html>
            <?php
                
            }

            
            
        }
        ?>
        <html>
    </table>
    
</body>
</html>
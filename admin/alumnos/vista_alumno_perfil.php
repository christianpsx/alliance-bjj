<?php
include("../../includes/connection.php");
include_once("../../includes/funciones.php");
session_start();

$ID = $_SESSION['ID_profesor'];
$NOMBRE = $_SESSION['Nombre'];
$APELLIDOS = $_SESSION['Apellidos'];

$PODER = $_SESSION['Poder'];
$ACTIVE = $_SESSION['active'];

if(!isset($ID) OR $PODER == 0 OR $ACTIVE == 0){
    session_destroy();
    header("location: ../../index.html");
    exit();
}

$ID_ALUMNO = mysqli_real_escape_string($connect, $_GET['id_alumno']);

$alumnos = "SELECT * FROM Alumno WHERE ID_alumno='$ID_ALUMNO'";
$peticion_alumnos = mysqli_query($connect, $alumnos);

while($tabla = $peticion_alumnos->fetch_assoc()){
    $NOMBRE_ALUMNO = $tabla['Nombre'];
    $APELLIDO_ALUMNO = $tabla['Apellidos'];

    $RUT_ALUMNO = $tabla['Rut'];
    $FECHA_NACIMIENTO_ALUMNO = $tabla['Fecha_nacimiento'];

    $TELEFONO_ALUMNO = $tabla['Telefono'];
    $DIRECCION_ALUMNO = $tabla['Direccion'];
    $MAIL_ALUMNO = $tabla['Mail'];

    $CINTURON_ALUMNO = $tabla['Cinturon'];
    $GRADO_ALUMNO = $tabla['Grado'];
    $FECHA_GRADO_ALUMNO = $tabla['Fecha_grado'];
    $CANTIDAD_CLASES = $tabla['Cantidad_clases'];
    $BURPEES = $tabla['Burpees'];


    $OBSERVACIONES_ALUMNO = $tabla['Observaciones'];
    $EXPERIENCIA_ALUMNO = $tabla['Experiencia'];

    $ACTIVE_ALUMNO = $tabla['active'];

    switch($CINTURON_ALUMNO){
        case 1:
            $cinturon = "Ninguno";
            break;
        case 2:
            $cinturon = "Blanco";
            break;
        case 3:
            $cinturon = "Azul";
            break;
        case 4:
            $cinturon = "Morado";
            break;
        case 5:
            $cinturon = "Cafe";
            break;
        case 6: 
            $cinturon = "Negro";
            break;
        default:
            break;
    }
    
    switch($GRADO_ALUMNO){
        case 1:
            $grado = "Ninguno";
            break;
        case 2:
            $grado = "I";
            break;
        case 3:
            $grado = "II";
            break;
        case 4:
            $grado = "III";
            break;
        case 5: 
            $grado = "IV";
            break;
        default:
            break;
    }

    switch($ACTIVE_ALUMNO){
        case 0:
            $active = "El alumno no ha activado su cuenta";
            break;
        case 1:
            $active = "El alumno ha activado su cuenta";
            break;
        default:
            break;
    }
}

$verificador_membresia = mysqli_query($connect, "SELECT COUNT(*) AS verificador_membresia FROM Membresia WHERE ID_alumno = '$ID_ALUMNO'");
$contador_membresia = mysqli_fetch_array($verificador_membresia);

if($contador_membresia['verificador_membresia'] == 0){
    $existencia_membresia = 0;
}else{
    $existencia_membresia = 1;

    $peticion_membresia = mysqli_query($connect, "SELECT * FROM Membresia WHERE ID_alumno = '$ID_ALUMNO'");

    while($tabla_membresia = $peticion_membresia->fetch_assoc()){
        $ID_MEMBRESIA = $tabla_membresia['ID_membresia'];

        $PAGO_MEMBRESIA = $tabla_membresia['Pago'];

        $FECHA_PAGO_MEMBRESIA = $tabla_membresia['Fecha_pago'];
        $FECHA_TERMINO_MEMBRESIA = $tabla_membresia['Fecha_termino'];



        switch($PAGO_MEMBRESIA){
            case 0:
                $pago = "El alumno no ha pagado su membresia";
                break;
            case 1:
                $pago = "El alumno pago su membresia";
                break;
            default:
                break;
        }
    }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Perfil de <?php echo "$NOMBRE_ALUMNO $APELLIDO_ALUMNO";?></title>
</head>
<body>
    <ul>
        <li class="log"> <?php echo "<p>Bienvenido $NOMBRE $APELLIDOS</p>";?> </li>
        
        <li><a href="../../profesor/profesor_php/home_profesor.php">Inicio</a></li>
        <li class="active"><a href="../../profesor/perfil_profesor.php">Perfil</a></li>
        <li><a href="../../profesor/horario_profesor.php">Horario</a></li>
        <li><a href="../../profesor/vista_clase.php">Clases</a></li>
        <li><a href="../../general/general_php/logout.php">Cerrar sesion</a></li>
    </ul>
    
    <h1>Perfil de <?php echo "$NOMBRE_ALUMNO $APELLIDO_ALUMNO";?></h1>

    <button><a href="vista_alumnos.php">volver</a></button>

    <div class="nombre">
        <p>
            Nombre: <?php echo "$NOMBRE_ALUMNO $APELLIDO_ALUMNO";?>
        </p>
    </div>

    <div class="rut_fecha">
        <p>
            RUT: <?php echo "$RUT_ALUMNO";?>
            <br>
            Fecha de nacimiento: <?php echo "$FECHA_NACIMIENTO_ALUMNO";?>
        </p>
    </div>

    <div class="cinturon_grado">
        <p>
            Cinturon: <?php echo $cinturon;?>
            <br>
            Grado: <?php echo $grado;?>
            <br>
            Fecha de grado: <?php if($FECHA_GRADO_ALUMNO == NULL){ echo "No hay fecha";}else{echo $FECHA_GRADO_ALUMNO;}?>
        </p>
    </div>

    <div class="asistencia">
        <p>
            Cantidad de clases asistidas: <?php echo $CANTIDAD_CLASES;?>
            <br>
            Burpees: <?php echo $BURPEES;?>
        </p>
    </div>

    <div class="contacto">
        <p>
            Telefono: <?php echo $TELEFONO_ALUMNO;?>
            <br>
            Direccion: <?php echo $DIRECCION_ALUMNO;?>
            <br>
            Correo electronico: <?php echo $MAIL_ALUMNO;?>
        </p>
    </div>

    <div class="experiencia_observacion">
        <p>
            Experiencia:
            <?php echo $EXPERIENCIA_ALUMNO;?>
            <br>
            Observaciones:
            <?php if($OBSERVACIONES_ALUMNO == NULL){ echo "No hay observaciones";}else{echo $OBSERVACIONES_ALUMNO;}?>
        </p>
    </div>

    <div>
        <p>
            Estado: 
            <?php echo $active;?>
        </p>
    </div>

    

    <div class="configuracion">
        <button><a href="vista_alumno_perfil_configuracion.php?id_alumno=<?php echo "$ID_ALUMNO";?>">Configuracion</a></button>
    </div>
    
    <div class="membresia">
        <h2>Membresia: </h2>
            </html>
            <?php
            if($existencia_membresia == 1){
                ?>
                <html>
                <p>
                    Pago: <?php echo $pago;?>
                    <br>
                    Fecha de pago: <?php echo $FECHA_PAGO_MEMBRESIA;?>
                    <br>
                    Fecha de termino: <?php echo $FECHA_TERMINO_MEMBRESIA;?>
                </p>

                <div>
                    <button><a href="membresia/vista_alumno_actualizar_membresia.php?id_alumno=<?php echo $ID_ALUMNO;?>">Actualizar membresia</a></button>
                </div>
                </html>
                <?php
            }else{
                ?>
                <p>
                    El alumno no tiene membresia.                    
                </p>
                <div>
                    <button><a href="membresia/vista_alumno_crear_membresia.php?id_alumno=<?php echo $ID_ALUMNO?>">Crear membresia</a></button>
                </div>
                <?php
            }
            ?>
            
    </div>

    <div class="crear-inscripcion">
        <h2>Inscribir a clase</h2>

        <div class="formulario-container">
            <form action="alumnos_php/inscripcion_alumno.php?id_alumno=<?php echo $ID_ALUMNO;?>&opcion=1" method="POST">
                <select name="clase" id="clase">
                    </html>
                    <?php
                        $cantidad_clase = mysqli_fetch_array(mysqli_query($connect, "SELECT COUNT(*) AS contador_clase FROM Clase"));
                        if($cantidad_clase['contador_clase'] > 0){
                            $consulta_clase = mysqli_query($connect, "SELECT * FROM Clase WHERE Cinturon <= '$CINTURON_ALUMNO' AND Grado <= '$GRADO_ALUMNO' AND Visibilidad = 1 ORDER BY Dia");
                            
                            while($tabla_clase = $consulta_clase->fetch_array()){
                                $ID_CLASE = $tabla_clase['ID_clase'];

                                $ID_PROFESOR_CLASE = $tabla_clase['ID_profesor'];
                                $TIPO_CLASE = $tabla_clase['Tipo_clase'];
    
                                $DIA_CLASE = $tabla_clase['Dia'];
                                $HORA_CLASE = $tabla_clase['Hora'];

                                $comprobar_inscripcion = mysqli_fetch_array(mysqli_query($connect, "SELECT COUNT(*) AS contador_inscripcion FROM Inscripcion WHERE ID_alumno = '$ID_ALUMNO' AND ID_clase = '$ID_CLASE'"));
                                if($comprobar_inscripcion['contador_inscripcion'] == 0){
                                    $peticion_profesor = mysqli_query($connect, "SELECT * FROM Profesor WHERE ID_profesor = '$ID_PROFESOR_CLASE'");
                                    while($tabla_profesor = $peticion_profesor->fetch_array()){
                                        $ID_PROFESOR = $tabla_profesor['ID_profesor'];

                                        $NOMBRE_PROFESOR = $tabla_profesor['Nombre'];
                                        $APELLIDOS_PROFESOR = $tabla_profesor['Apellidos'];

                                        switch($DIA_CLASE){
                                            case 1:
                                                $dia_clase = "Lunes";
                                                break;
                                            case 2:
                                                $dia_clase = "Martes";
                                                break;
                                            case 3:
                                                $dia_clase = "Miércoles";
                                                break;
                                            case 4:
                                                $dia_clase = "Jueves";
                                                break;
                                            case 5:
                                                $dia_clase = "Viernes";
                                                break;
                                            case 6:
                                                $dia_clase = "Sabado";
                                                break;
                                            case 7:
                                                $dia_clase = "Domingo";
                                                break;
                                            default:
                                                break;
                                        }

                                        switch ($VISIBILIDAD_CLASE) {
                                            case 0:
                                                $visibilidad = "Deshabilitada";
                                                break;
                                            case 1:
                                                $visibilidad = "Habilitada";
                                                break;
                                            default:
                                                break;
                                        }
                                        ?>
                                        <html>
                                            <option value="<?php echo $ID_CLASE?>"><?php echo switchClases($TIPO_CLASE).", $NOMBRE_PROFESOR $APELLIDOS_PROFESOR, $dia_clase ".date("H:i", strtotime($HORA_CLASE));?></option>
                                        </html>
                                        <?php
                                    }
                                }
                            }
                        }else{
                            ?>
                            <html>
                                <option value="">No hay clases</option>
                            </html>
                            <?php
                        }
                    ?>
                    <html>
                </select>

                <input type="submit" value="Inscribir">
            </form>
        </div>
    </div>

    <div class="clase-container">
        <h2>Clases inscritas</h2>

        <div class="tabla-container">
            </html>
            <?php
            $peticion_verificacion_inscripciones = mysqli_query($connect, "SELECT COUNT(*) AS contador_inscripciones FROM Inscripcion WHERE ID_alumno='$ID_ALUMNO'");
            $contador_inscripciones = mysqli_fetch_array($peticion_verificacion_inscripciones);

            if($contador_inscripciones['contador_inscripciones'] == 0){
                ?>
                <html>
                    <p>El alumno no tiene clases inscritas.</p>
                </html>
                <?php
            }else{
                ?>
                <html>
                <table border="1">
                    <thead>
                        <tr>
                            <th>Clase</th>
                            <th>Profesor</th>
                            <th>Horario</th>
                            <th>Estado</th>
                            <th colspan="2">Opciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        </html>
                        <?php
                        $peticion_inscripciones = mysqli_query($connect, "SELECT * FROM Inscripcion WHERE ID_alumno='$ID_ALUMNO'");
                        while($tabla_inscripcion = $peticion_inscripciones->fetch_array()){
                            $ID_INSCRIPCION = $tabla_inscripcion['ID_inscripcion'];
                            $ID_CLASE = $tabla_inscripcion['ID_clase'];
                            $ASISTENCIA_INSCRIPCION = $tabla_inscripcion['Asistencia'];

                            

                            $peticion_clases = mysqli_query($connect, "SELECT * FROM Clase WHERE ID_clase='$ID_CLASE'");
                            while($tabla_clase = $peticion_clases->fetch_array()){
                                $TIPO_CLASE = $tabla_clase['Tipo_clase'];
                                $ID_PROFESOR_CLASE = $tabla_clase['ID_profesor'];
                                $CUPOS_CLASE = $tabla_clase['Cupos'];

                                $DIA_CLASE = $tabla_clase['Dia'];
                                $HORA_CLASE = $tabla_clase['Hora'];

                                $VISIBILIDAD_CLASE = $tabla_clase['Visibilidad'];

                                $peticion_profesor = mysqli_query($connect, "SELECT * FROM Profesor WHERE ID_profesor = '$ID_PROFESOR_CLASE'");
                                while($tabla_profesor = $peticion_profesor->fetch_array()){
                                    $ID_PROFESOR = $tabla_profesor['ID_profesor'];

                                    $NOMBRE_PROFESOR = $tabla_profesor['Nombre'];
                                    $APELLIDOS_PROFESOR = $tabla_profesor['Apellidos'];

                                    switch($DIA_CLASE){
                                        case 1:
                                            $dia_clase = "Lunes";
                                            break;
                                        case 2:
                                            $dia_clase = "Martes";
                                            break;
                                        case 3:
                                            $dia_clase = "Miércoles";
                                            break;
                                        case 4:
                                            $dia_clase = "Jueves";
                                            break;
                                        case 5:
                                            $dia_clase = "Viernes";
                                            break;
                                        case 6:
                                            $dia_clase = "Sabado";
                                            break;
                                        case 7:
                                            $dia_clase = "Domingo";
                                            break;
                                        default:
                                            break;
                                    }

                                    switch ($VISIBILIDAD_CLASE) {
                                        case 0:
                                            $visibilidad = "Deshabilitada";
                                            break;
                                        case 1:
                                            $visibilidad = "Habilitada";
                                            break;
                                        default:
                                            break;
                                    }
                                    ?>
                                    <html>
                                        <tr>
                                            <td><a href="../clases/vista_clase_perfil.php?id_clase=<?php echo $ID_CLASE;?>"><?php echo switchClases($TIPO_CLASE);?></a></td>
                                            <td><a href="../profesores/vista_profesor_perfil.php?id_profesor=<?php echo $ID_PROFESOR_CLASE;?>"><?php echo "$NOMBRE_PROFESOR $APELLIDOS_PROFESOR"?></a></td>
                                            <td><?php echo "$dia_clase, ".date("H:i", strtotime($HORA_CLASE));?></td>
                                            <td><?php echo $visibilidad;?></td>
                                            <td><a href="alumnos_php/inscripcion_alumno.php?id_alumno=<?php echo $ID_ALUMNO;?>&id_clase=<?php echo $ID_CLASE;?>&opcion=0">Desinscribir</a></td>
                                            <td>
                                                </html>
                                                <?php
                                                if(isset($ASISTENCIA_INSCRIPCION)){
                                                    switch($ASISTENCIA_INSCRIPCION){
                                                        case 0:
                                                            ?>
                                                            <html>
                                                                <form action="asistencia_php/asistencia.php?id_alumno=<?php echo $ID_ALUMNO;?>&id_clase=<?php echo $ID_CLASE;?>&caso=2" method="post">
                                                                    <label for="si">Asistió</label>
                                                                    <input type="radio" name="asistencia" id="asistencia" value="1" required>
                        
                                                                    <label for="no">No Asistió</label>
                                                                    <input type="radio" name="asistencia" id="asistencia" value="0" checked>
                    
                                                                    <input type="submit" value="Actualizar">
                                                                </form>
                                                            </html>
                                                            <?php
                                                            break;
                                                        case 1:
                                                            ?>
                                                            <html>
                                                                <form action="asistencia_php/asistencia.php?id_alumno=<?php echo $ID_ALUMNO;?>&id_clase=<?php echo $ID_CLASE;?>&caso=1" method="post">
                                                                    <label for="si">Asistió</label>
                                                                    <input type="radio" name="asistencia" id="asistencia" value="1" checked required>
                        
                                                                    <label for="no">No Asistió</label>
                                                                    <input type="radio" name="asistencia" id="asistencia" value="0">
                    
                                                                    <input type="submit" value="Actualizar">
                                                                </form>
                                                            </html>
                                                            <?php
                                                            break;
                                                        default:
                                                            break;
                                                    }
                                                }else{
                                                    ?>
                                                    <html>
                                                        <form action="asistencia_php/asistencia.php?id_alumno=<?php echo $ID_ALUMNO;?>&id_clase=<?php echo $ID_CLASE;?>&caso=0" method="post">
                                                            <label for="si">Asistió</label>
                                                            <input type="radio" name="asistencia" id="asistencia" value="1" required>
                    
                                                            <label for="no">No Asistió</label>
                                                            <input type="radio" name="asistencia" id="asistencia" value="0">
                    
                                                            <input type="submit" value="Actualizar">
                                                        </form>
                                                    </html>
                                                    <?php
                                                }
                                                ?>
                                                <html>
                                            </td>
                                        </tr>
                                    </html>
                                    <?php
                                }
                            }
                        }
                        ?>
                        <html>
                    </tbody>
                </table>
                </html>    
                <?php
            }
            ?>
            <html>
        </div>
    </div>
</body>
</html>
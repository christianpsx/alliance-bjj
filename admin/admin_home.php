<?php
include("../includes/connection.php");
session_start();

$ID = $_SESSION['ID_profesor'];
$NOMBRE = $_SESSION['Nombre'];
$APELLIDOS = $_SESSION['Apellidos'];

$RUT = $_SESSION['Rut'];
$FECHA_N = $_SESSION['Fecha_nacimiento'];

$DIRECCION = $_SESSION['Direccion'];
$MAIL = $_SESSION['Mail'];
$TELEFONO = $_SESSION['Telefono'];

$CINTURON = $_SESSION['Cinturon'];

$PODER = $_SESSION['Poder'];
$ACTIVE = $_SESSION['active'];

if(!isset($ID) OR $PODER == 0 OR $ACTIVE == 0){
    session_destroy();
    header("location: ../index.html");
    exit();
}
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Inicio</title>
    <link rel="stylesheet" href="css/admin.css">
</head>
<body>
    <ul>
        <li class="log"><?php echo "<p>Bienvenido $NOMBRE $APELLIDOS</p>";?></li>
        <li><a href="../profesor/profesor_php/home_profesor.php">Inicio</a></li>
        <li><a href="../profesor/perfil_profesor.php">Perfil</a></li>
        <li><a href="../profesor/horario_profesor.php">Horario</a></li>
        <li><a href="../profesor/vista_clase.php">Clases</a></li>
        <?php
        if($PODER == 1){
        ?>
            </html>
            <li class="active"><a href="#">Administrador</a></li>
            <html>
        <?php
        }
        ?>
        <li><a href="../general/general_php/logout.php">Cerrar sesion</a></li>
    </ul>

    <h1>Configuracion de la pagina</h1>
    <div class="container">

    <div class="alumnos">
        <h2><a href="alumnos/vista_alumnos.php">Alumnos</a></h2>
    </div>

    <div class="profesores">
        <h2><a href="profesores/vista_profesores.php">Profesores</a></h2>
    </div>

    <div class="clases">
        <h2><a href="clases/vista_clases.php">Clases</a></h2>
    </div>
    </div>

    


</body>
</html>
<?php
//Preparamos la conexión:
include("../includes/connection.php");
include_once("../includes/funciones.php");

//Se inicia la sesion del usuario.
session_start();

$ID = $_SESSION['ID_profesor'];
$NOMBRE = $_SESSION['Nombre'];
$APELLIDOS = $_SESSION['Apellidos'];

$RUT = $_SESSION['Rut'];
$FECHA_N = $_SESSION['Fecha_nacimiento'];

$DIRECCION = $_SESSION['Direccion'];
$MAIL = $_SESSION['Mail'];
$TELEFONO = $_SESSION['Telefono'];

$CINTURON = $_SESSION['Cinturon'];

$PODER = $_SESSION['Poder'];
$ACTIVE = $_SESSION['active'];

if(!isset($ID) OR $ACTIVE == 0){
    session_destroy();
    header("location: ../index.html");
    exit();
}
?>

<!DOCTYPE html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" type="text/css" href="profesor_css/crear_clase.css">

    <title>Crear Clase</title>
</head>
<body>
    <ul>
        <li class="log"><?php echo "<p>Bienvenido $NOMBRE $APELLIDOS</p>";?></li>

        <li><a href="profesor_php/home_profesor.php">Inicio</a></li>

        <li><a href="perfil_profesor.php">Perfil</a></li>

        <li><a href="horario_profesor.php">Horario</a></li>

        <li class="active"><a href="#">Clases</a></li>
        
        <li><a href="../general/general_php/logout.php">Cerrar sesion</a></li>
    </ul>

    <div class="container">
        <form action="profesor_php/crear_clase.php" method="post" name="formulario" class="formulario">
            <label for="Cupos"><b>Cupos</b></label>
            <input class="controls" type="number" placeholder="Cupos Disponibles" name="Cupos" id="Cupos" min="1" max="100" required>

            <div class="Dia">
                <label for="Dia"><b>Dia</b></label>
                <select class="select" name="Dia" id="Dia" required>
                    <option value="1" selected>Lunes</option>
                    <option value="2">Martes</option>
                    <option value="3">Miércoles</option>
                    <option value="4">Jueves</option>
                    <option value="5">Viernes</option>
                    <option value="6">Sábado</option>
                    <option value="7">Domingo</option>
                </select>

                <input class = "time" type="time" name="Hora" min="7:00" max="22:00" step="60"/>
            </div>

            <div class="Clase">
                <label for="Clase"><b>Clase</b></label>
                <select class="select" name="Clase" id="Clase" required>
                    </html>
                        <?php
                        for ($i = 1; $i < 30; $i++) { 
                            if(switchClases($i) != "salir"){
                                ?>
                                <option value="<?php echo $i;?>"><?php echo switchClases($i)?></option>
                                <?php
                            }
                        }                        
                        ?>               
                        <html>
                </select>
            </div>
            
            <div class="Cinturon_Grado">
                <label for="Cinturon"><b>Cinturon</b></label>
                <select class="select" name="Cinturon" id="Cinturon" >
                    <option value="1" selected>Ninguno</option>
                    <option value="2">Blanco</option>
                    <option value="3">Azul</option>
                    <option value="4">Morado</option>
                    <option value="5">Cafe</option>
                    <option value="6">Negro</option>
                </select>
    
                <label for="Grado"><b>Grado</b></label>
                <select class="select" name="Grado" id="Grado" >
                    <option value="1" selected>Ninguno</option>
                    <option value="2">I</option>
                    <option value="3">II</option>
                    <option value="4">III</option>
                    <option value="5">IV</option>
                </select>
            </div>
              
            <input type="submit" value="Crear clase" class="button">
        </form>

        <section class="clases">
            <div>
                <h2>Clases creadas:</h2>

                <div class="scroll">
                </html>

                <?php
                $seleccionar_clase = "SELECT * FROM Clase WHERE ID_profesor = '$ID' ORDER BY Dia";
                $peticion_seleccionar_clase = mysqli_query($connect, $seleccionar_clase);

                while($tabla = $peticion_seleccionar_clase->fetch_assoc()){
                    $ID_CLASE = $tabla['ID_clase'];
                    $TIPO_CLASE = $tabla['Tipo_clase'];
                    $CUPOS_CLASE = $tabla['Cupos'];
                    $CINTURON_CLASE = $tabla['Cinturon'];
                    $GRADO_CLASE = $tabla['Grado'];
                    $DIA_CLASE = $tabla['Dia'];
                    $HORA_CLASE = $tabla['Hora'];

                    switch($DIA_CLASE){
                        case 1:
                            $dia_clase = "Lunes";
                            break;
                        case 2:
                            $dia_clase = "Martes";
                            break;
                        case 3:
                            $dia_clase = "Miércoles";
                            break;
                        case 4:
                            $dia_clase = "Jueves";
                            break;
                        case 5:
                            $dia_clase = "Viernes";
                            break;
                        case 6:
                            $dia_clase = "Sabado";
                            break;
                        case 7:
                            $dia_clase = "Domingo";
                            break;
                        default:
                            break;
                    }

                    $consulta_cupos = mysqli_query($connect, "SELECT COUNT(*) AS contador_cupos FROM Inscripcion WHERE ID_clase='$ID_CLASE'");
                    $contador_cupos = mysqli_fetch_array($consulta_cupos);

                    if($contador_cupos['contador_cupos'] >= 0){
                        $cupos_clase = $CUPOS_CLASE - $contador_cupos['contador_cupos'];
                    }

                    ?>

                    <html>
                    <div class="espacio">
                        <label for="tipo_clase" class="anexo">
                            <a href="../clase/Profesor/vista_clase_profesor.php?id_clase=<?php echo $ID_CLASE?>"> <?php echo switchClases($TIPO_CLASE); ?></a> <?php echo ", $dia_clase ".date('H:i', strtotime($HORA_CLASE)).", Cupos disponibles: $cupos_clase"?>
                        </label>
                        
                    </div>
                    </html>
                    
                    <?php
                }
                ?>

                    <html>
                </div>
            </div> 
        </section>


    </div>
    </body>
</html>

                     





    
    
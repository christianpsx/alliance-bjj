<?php
//Conectamos con la base de datos

include("../includes/connection.php");
include_once("../includes/funciones.php");

//Comenzamos sesion y capturamos los datos del profesor
session_start();


$ID = $_SESSION['ID_profesor'];
$NOMBRE = $_SESSION['Nombre'];
$APELLIDOS = $_SESSION['Apellidos'];

$RUT = $_SESSION['Rut'];
$FECHA_N = $_SESSION['Fecha_nacimiento'];

$DIRECCION = $_SESSION['Direccion'];
$MAIL = $_SESSION['Mail'];
$TELEFONO = $_SESSION['Telefono'];

$CINTURON = $_SESSION['Cinturon'];

$PODER = $_SESSION['Poder'];
$ACTIVE = $_SESSION['active'];

$sql = "SELECT Imagen FROM Profesor WHERE ID_profesor = '$ID' AND Imagen != 'NULL'";
$result = $connect->query($sql);
$row = $result->fetch_assoc();

if(!isset($ID) OR $ACTIVE == 0){
    session_destroy();
    header("location: ../index.html");
    exit();
}

switch($CINTURON){
    case 1:
        $cinturon = "Ninguno";
        break;
    case 2:
        $cinturon = "Blanco";
        break;
    case 3:
        $cinturon = "Azul";
        break;
    case 4:
        $cinturon = "Morado";
        break;
    case 5:
        $cinturon = "Marron";
        break;
    case 6: 
        $cinturon = "Negro";
        break;
    default:
        break;
}
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <link rel="stylesheet" type="text/css" href="profesor_css/profe.css">

    <title>Perfil de <?php echo $NOMBRE;?></title>
</head>
<body>
    <ul>
        <li class="log"><?php echo "<p>Bienvenido $NOMBRE $APELLIDOS</p>";?></li>

        <li><a href="profesor_php/home_profesor.php">Inicio</a></li>

        <li class="active"><a href="#">Perfil</a></li>

        <li><a href="horario_profesor.php">Horario</a></li>

        <li><a href="vista_clase.php">Clases</a></li>

        <?php
        if($PODER == 1){
        ?>
            </html>
            <li><a href="../admin/admin_home.php">Administrador</a></li>
            <html>
        <?php
        }
        ?>
        
        <li><a href="../general/general_php/logout.php">Cerrar sesion</a></li>
    </ul>

    <div class="perfil">
        <h1>Perfil de <?php echo "$NOMBRE $APELLIDOS";?></h1>

        <div class="container-user-info">
            <div class="contenedor_imagen">
                <?php if($result->num_rows > 0){ ?>
                    <img class="imagen" src="data:image/jpg;charset=utf8;base64,<?php echo base64_encode($row['Imagen']); ?>" /> 
                <?php }else{ ?>
                    <img src="https://i.ibb.co/3Tyr88g/no-image.png" alt="Foto perfil d" class="imagen">
                <?php } ?>
            </div>

            <div class="user-nombre">
                <p>
                    <?php echo "$NOMBRE $APELLIDOS";?>
                </p>
            </div>

            <div class="direccion_telefono_profesor">
                <p>
                    <?php echo "$DIRECCION";?>
                    <br>
                    <?php echo "$TELEFONO";?>
                </p>
            </div>

            <div class="user-cinturon">
                <p>
                    <?php echo "$cinturon";?>
                </p>
            </div>
        </div>

        <div class="centro">
            <button><a href="configuracion_perfil.php">Configuracion</a></button>
        </div>

        <div class="container_clases">
            <h2>Clases creadas</h2>
            <div class="container_tabla_clases">
            <table border="1">
                    <tr>
                        <th>Nombre Clases</th>
                        <th>Horario</th>
                        <th colspan="2">Estado</th>
                    </tr>

                    <?php
                        $seleccionar_clase = "SELECT * FROM Clase WHERE ID_profesor = '$ID' ORDER BY Dia";
                        $peticion_seleccionar_clase = mysqli_query($connect, $seleccionar_clase);
            
                        while($tabla = $peticion_seleccionar_clase->fetch_assoc()){
                            $ID_CLASE = $tabla['ID_clase'];

                            $TIPO_CLASE = $tabla['Tipo_clase'];

                            $DIA_CLASE = $tabla['Dia'];
                            $HORA_CLASE = $tabla['Hora'];

                            $VISIBILIDAD = $tabla['Visibilidad'];
            
                            switch($DIA_CLASE){
                                case 1:
                                    $dia_clase = "Lunes";
                                    break;
                                case 2:
                                    $dia_clase = "Martes";
                                    break;
                                case 3:
                                    $dia_clase = "Miércoles";
                                    break;
                                case 4:
                                    $dia_clase = "Jueves";
                                    break;
                                case 5:
                                    $dia_clase = "Viernes";
                                    break;
                                case 6:
                                    $dia_clase = "Sabado";
                                    break;
                                case 7:
                                    $dia_clase = "Domingo";
                                    break;
                                default:
                                    break;
                            }

                            switch ($VISIBILIDAD) {
                                case 0:
                                    $visibilidad_clase = "No visible en el horario";
                                    break;
                                case 1:
                                    $visibilidad_clase = "Visible en el horario";
                                    break;
                                default:
                                    break;
                            }
                            ?>
                            </html>
                                <tr>
                                    <td> <a href="../clase/Profesor/vista_clase_profesor.php?id_clase=<?php echo $ID_CLASE;?>"><?php echo switchClases($TIPO_CLASE);?></a></td>
                                    <td> <?php echo $dia_clase.' '.date('H:i', strtotime("$HORA_CLASE"));?></td>
                                    <td> <?php echo $visibilidad_clase;?></td>
                                    <td><a href="../clase/Profesor/vista_configuracion_profesor.php?id_clase=<?php echo $ID_CLASE;?>">Configuracion</a></td>
                                </tr>
                            <html>
                            <?php
                        }
                    ?>
                </table>
            </div>
        </div>
    </div>
    
</body>
</html>
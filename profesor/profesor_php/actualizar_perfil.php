<?php
//Conexion con la base de datos
include("../../includes/connection.php");

//Iniciar sesion del usuario

session_start();

$ID = $_SESSION['ID_profesor'];
$ACTIVE = $_SESSION['active'];

if(!isset($ID) OR $ACTIVE == 0){
    session_destroy();
    header("location: ../../index.html");
    exit();
}

//Recepcion de variables mediante metodo GET

$OPCION = mysqli_real_escape_string($connect, $_GET['opcion']);

switch($OPCION){
    case 1:
        $NOMBRE = mysqli_real_escape_string($connect, $_POST['nombre']);
        $APELLIDOS = mysqli_real_escape_string($connect, $_POST['apellido']);

        $RUT = mysqli_real_escape_string($connect, $_POST['rut']);

        $DIRECCION = mysqli_real_escape_string($connect, $_POST['direccion']);
        $TELEFONO = mysqli_real_escape_string($connect, $_POST['telefono']);
        
        $FECHA_NACIMIENTO = mysqli_real_escape_string($connect, $_POST['fecha']);
        $CINTURON = mysqli_real_escape_string($connect, $_POST['cinturon']);
        
        $actualizar_informacion_profesor = "UPDATE Profesor SET Nombre = '$NOMBRE', Apellidos = '$APELLIDOS', Rut = '$RUT', Fecha_nacimiento = '$FECHA_NACIMIENTO', Telefono = '$TELEFONO', Direccion = '$DIRECCION', Cinturon = '$CINTURON' WHERE ID_profesor = '$ID'";
        
        if ($connect->query($actualizar_informacion_profesor) == TRUE) {
            $consulta_profesor = "SELECT * FROM Profesor WHERE ID_profesor='$ID'";
            $peticion_profesor = mysqli_query($connect, $consulta_profesor);
    
            if($peticion_profesor){
                while($row = $peticion_profesor->fetch_array()){
                    $id = $row['ID_profesor'];

                    $nombre = $row['Nombre'];
                    $apellido = $row['Apellidos'];
                    $rut = $row['Rut'];
                    $direccion = $row['Direccion'];
                    $telefono = $row['Telefono'];

                    $fecha_n = $row['Fecha_nacimiento'];
                    $cinturon = $row['Cinturon'];
                }
            }
            $_SESSION['ID_profesor'] = $id;

            $_SESSION['Nombre'] = $nombre;
            $_SESSION['Apellidos'] = $apellido;
            $_SESSION['Rut'] = $rut;
            $_SESSION['Direccion'] = $direccion;
            $_SESSION['Telefono'] = $telefono;

            $_SESSION['Fecha_nacimiento'] = $fecha_n;    
            $_SESSION['Cinturon'] = $cinturon;
            
            header('Location: ../perfil_profesor.php');
        } else {
            header('Location: ../configuracion_perfil.php');
        }
        break;
    case 2:
        $CONTRASENA = mysqli_real_escape_string($connect, $_POST['psw']);
        $CONTRASENA2 = mysqli_real_escape_string($connect, $_POST['psw-repeat']);
        if($CONTRASENA==$CONTRASENA2){
        
            $actualizar_contrasena_profesor = "UPDATE Profesor SET Contrasena = MD5('".$CONTRASENA."') WHERE ID_profesor='$ID'";
            $peticion_profesor = mysqli_query($connect, $actualizar_contrasena_profesor);
        
            if($peticion_profesor){
                header('Location: ../perfil_profesor.php');
            }else{
                echo '<script type="text/javascript"> alert("No se actualizo la contraseña."); location="../perfil_profesor.php"; </script>';
            }
        }else {
            echo '<script type="text/javascript"> alert("Las contraseñas no son iguales."); location="../configuracion_perfil.php";</script>';
        }
        break;
    case 3:
        $eliminar_profesor = "DELETE FROM Profesor WHERE ID_profesor = '$ID'";
    
        if ($connect->query($eliminar_profesor) === TRUE) {
            session_destroy();
            echo '<script type="text/javascript"> alert("Cuenta eliminada."); location="../../general/login.html"; </script>';
        } else {
            header('Location: ../perfil.php');
        }
        break;
    default:
        break;
}

mysqli_close($connect);
?>
<?php
//Conexion con la base de datos:
include("../../includes/connection.php");

//Comenzamos la sesion:
session_start();

$ID = $_SESSION['ID_profesor'];
$NOMBRE = $_SESSION['Nombre'];
$APELLIDOS = $_SESSION['Apellidos'];

$RUT = $_SESSION['Rut'];
$FECHA_N = $_SESSION['Fecha_nacimiento'];

$DIRECCION = $_SESSION['Direccion'];
$MAIL = $_SESSION['Mail'];
$TELEFONO = $_SESSION['Telefono'];

$CINTURON = $_SESSION['Cinturon'];

$PODER = $_SESSION['Poder'];
$ACTIVE = $_SESSION['active'];

if(!isset($ID) OR $ACTIVE == 0){
    session_destroy();
    header("location: ../../index.html");
    exit();
}
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Inicio</title>

    <link rel="stylesheet" href="../profesor_css/home.css">
    <ul>
        <li class="log"><?php echo "<p>Bienvenido $NOMBRE $APELLIDOS</p>";?></li>

        <li class="active"><a href="#">Inicio</a></li>

        <li><a href="../perfil_profesor.php">Perfil</a></li>

        <li><a href="../horario_profesor.php">Horario</a></li>

        <li><a href="../vista_clase.php">Clases</a></li>
        
        <li><a href="../../general/general_php/logout.php">Cerrar sesion</a></li>
    </ul>
</head>
<body>
    <div>
        <form action="sugerenciasp.php" method="post" class="comentarios"> 
        <div class="espacio">  
        <label for="comentarios"><b>Enviar comentarios</b></label>
            <select name="sugerenciasp" id="sugerenciasp" class="select">
            <option value="Felicitacion" selected>Felicitaciones</option>
            <option value="Sugerencia">Sugerencias</option>
            <option value="Reclamo">Reclamos</option>
            </select>
    </div>

            <textarea  wrap="hard" name="mensaje" id="mensaje" cols="30" rows="7" class="controls" maxlength="255" minlength="1" placeholder="" require></textarea>
            <input type="submit" value="Enviar comentarios" name="comentarios" class="buttons">
        </form>
    </div>
</body>
</html>
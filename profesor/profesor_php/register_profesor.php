<?php
    //Cuando se haga el formulario en action="" pone la ruta de este archivo con method="post"
    include("../../includes/connection.php");

    $nombre = $_POST['nombre'];
    $apellidos = $_POST['apellido'];
    $rut = $_POST['rut'];
    $email = $_POST['email'];
    $contrasena = $_POST['psw'];
    $fecha_nacimiento = $_POST['fecha'];
    $telefono = $_POST['telefono'];
    $direccion = $_POST['direccion'];
    $cinturon = $_POST['cinturon'];

    $nombre = mysqli_real_escape_string($connect, $_POST['nombre']);
    $apellidos = mysqli_real_escape_string($connect, $_POST['apellido']);

    $fecha_nacimiento = mysqli_real_escape_string($connect, $_POST['fecha']);
    $rut = mysqli_real_escape_string($connect, $_POST['rut']);
    
    $email = mysqli_real_escape_string($connect, $_POST['email']);
    $contrasena = mysqli_real_escape_string($connect, $_POST['psw']);
    $contrasena2 = mysqli_real_escape_string($connect, $_POST['psw2']);

    $telefono = mysqli_real_escape_string($connect, $_POST['telefono']);
    $direccion = mysqli_real_escape_string($connect, $_POST['direccion']);
    
    $cinturon = mysqli_real_escape_string($connect, $_POST['cinturon']);


    $validacion_mail_alumno = "SELECT COUNT(*) AS verificador_mail_alumno FROM Alumno WHERE Mail = '$email'";
    $consulta_mail_alumno = mysqli_query($connect, $validacion_mail_alumno);
    $arreglo_mail_alumno = mysqli_fetch_array($consulta_mail_alumno);
    if ($contrasena != $contrasena2) {
        echo '<script type="text/javascript"> alert("Verificar que las contraseñas sean iguales."); location="../register.html"; </script>';

    }elseif($arreglo_mail_alumno['verificador_mail_alumno'] == 1){
        echo '<script type="text/javascript"> alert("El e-mail proporcionado ya ha sido registrado."); location="../register_profesor.html"; </script>';
    }
    else{
        $validacion_mail_profesor = "SELECT COUNT(*) AS verificador_mail_profesor FROM Profesor WHERE Mail = '$email'";
        $consulta_mail_profesor = mysqli_query($connect, $validacion_mail_profesor);
        $arreglo_mail_profesor = mysqli_fetch_array($consulta_mail_profesor);

        if($arreglo_mail_profesor['verificador_mail_profesor'] == 0){
            
            
            $hash = md5(rand(0,1000));
            $sql = "INSERT INTO Profesor (Nombre, Apellidos, Rut, Mail, Contrasena, Fecha_nacimiento, Telefono, Direccion, Cinturon, Hash_profesor) VALUES ('$nombre','$apellidos','$rut','$email', MD5('".$contrasena."'), '$fecha_nacimiento','$telefono','$direccion','$cinturon','$hash')";
    
            if ($connect->query($sql) === TRUE) {

                $to      = $email; //Send email to our user
                $subject = 'Verificación'; //// Give the email a subject 
                $message = '

                Gracias por registrarte como profesor!
                Su cuenta ha sido creada, puede iniciar sesión con su correo y la siguiente contraseña después de haber activado su cuenta pulsando la url de abajo.

                ------------------------
                Password: '.$contrasena.'
                ------------------------

                Haga clic en este enlace para activar su cuenta:
                https://alliancebjj.cl/login/profesor/verifyp.php?email='.$email.'&hash='.$hash.'

                '; // INCLUIR LINK BIEN

                $headers = 'From:noreply@alliance.com' . "\r\n"; // Set from headers
                mail($to, $subject, $message, $headers); // Enviar el email
                header('Location: ../../index.html');
            } else {
                echo "Error: " . $sql . "<br>" . $connect->error;
            }
    
            
        }else{
            echo '<script type="text/javascript"> alert("El e-mail proporcionado ya tiene una cuenta asociada."); location="../register_profesor.html"; </script>';
        }
    }
    
    mysqli_close($connect);
?>
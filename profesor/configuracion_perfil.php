<?php
//Preparamos la conexión:

include("../includes/connection.php");

//Se inicia la sesion del usuario.

session_start();

$ID = $_SESSION['ID_profesor'];
$NOMBRE = $_SESSION['Nombre'];
$APELLIDOS = $_SESSION['Apellidos'];

$RUT = $_SESSION['Rut'];
$FECHA_NACIMIENTO = $_SESSION['Fecha_nacimiento'];

$DIRECCION = $_SESSION['Direccion'];
$MAIL = $_SESSION['Mail'];
$TELEFONO = $_SESSION['Telefono'];

$CINTURON = $_SESSION['Cinturon'];

$PODER = $_SESSION['Poder'];
$ACTIVE = $_SESSION['active'];

if(!isset($ID) OR $ACTIVE == 0){
    session_destroy();
    header("location: ../index.html");
    exit();
}

switch($CINTURON){
    case 1:
        $cinturon = "Ninguno";
        break;
    case 2:
        $cinturon = "Blanco";
        break;
    case 3:
        $cinturon = "Azul";
        break;
    case 4:
        $cinturon = "Morado";
        break;
    case 5:
        $cinturon = "Marron";
        break;
    case 6: 
        $cinturon = "Negro";
        break;
    default:
        break;
}
?>

<!DOCTYPE html>
<html lang="es">
<head>
    

    <link rel="stylesheet" href="profesor_css/config.css">

    <title>Configuracion del perfil</title>
</head>
<body>
    <ul>
		<li class="log"> <?php echo "<p>Bienvenido $NOMBRE $APELLIDOS</p>";?> </li>

		<li><a href="profesor_php/home_profesor.php">Inicio</a></li>

		<li class="active"><a href="perfil_profesor.php">Perfil</a></li>

        <li><a href="horario_profesor.php">Horario</a></li>

		<li><a href="vista_clase.php">Clases</a></li>

		<li><a href="../general/general_php/logout.php">Cerrar sesion</a></li>
	</ul>

    <div class="editar_perfil">
        <div class="centro">
            <h2>Editar perfil</h2>
            
            <form action="profesor_php/actualizar_perfil.php?opcion=1" method="post">
                <div class="nombre">
                    <label for="nombre"><b>Nombres</b></label>
                    <input class="controls" type="text" placeholder="Ingrese sus nombres" name="nombre" id="nombre" value="<?php echo $NOMBRE;?>">
                </div>

                <div class="apellido">
                    <label for="apellido"><b>Apellidos</b></label>
                    <input class="controls" type="text" placeholder="Ingrese su apellido" name="apellido" id="apellido" value="<?php echo $APELLIDOS;?>">
                </div>

                <div class="rut">
                    <label for="rut"><b>RUT</b></label>
                    <input class="controls" type="text" placeholder="Ingrese su RUT, ej: 11.111.111-1" name="rut" id="rut" value="<?php echo $RUT;?>">
                </div>

                <div class="direccion">
                    <label for="direccion"><b>Dirección</b></label>
                    <input class="controls" type="text" placeholder="Ingrese su dirección" name="direccion" id="direccion" value="<?php echo $DIRECCION;?>">
                </div>

                <div class="telefono">
                    <label for="telefono"><b>Teléfono</b></label>
                    <input class="controls" type="text" placeholder="Ingrese su teléfono" name="telefono" id="telefono" value="<?php echo $TELEFONO;?>">
                </div>

                <div class="fecha">
                    <label for="fecha"><b>Fecha de nacimiento</b></label>
                    <input class="controls" type="date" name="fecha" id="fecha" value="<?php echo $FECHA_NACIMIENTO;?>">
                </div>

                <div class="cinturon2">
                    <label for="cinturon"><b>Cinturon</b></label>
                    <select class="cinturon1" name="cinturon" id="cinturon">
                        <option value="<?php echo $CINTURON;?>" selected hidden ><?php echo $cinturon;?></option>
                        <option value="1">Ninguno</option>
                        <option value="2">Blanco</option>
                        <option value="3">Azul</option>
                        <option value="4">Morado</option>
                        <option value="5">Cafe</option>
                        <option value="6">Negro</option>
                    </select>
                </div>

                <div class="enviar">
                    <input type="submit" value="Actualizar perfil" name="actualizar" class="buttons">  
                </div>          
            </form>
        </div>

        <div class="centro">
            <form action="uploadp.php" method="post" enctype="multipart/form-data">
                <label>Cambio de imagen perfil:</label>
                <input type="file" name="image" class="inputfile">
                <input type="submit" name="submit" value="Cambiar">
            </form>
        </div>
    </div>

    <div class="actualizar_contraseña">
        <div class="centro">
            <h2>Actualizar contraseña</h2>
            <form action="profesor_php/actualizar_perfil.php?opcion=2" method="post">
                <div id="cambiar_password">
                <label for="password"><b>Contraseña</b></label>
                <input class="controls" type="password" required="required" alt="strongPass" placeholder="Ingrese su contraseña" name="psw" id="psw">
        
                <label for="password_repeat"><b>Repita la Contraseña</b></label>
                <input class="controls" type="password" required="required" placeholder="Repita su contraseña" name="psw-repeat" id="psw-repeat">
                </div>

                <div class="enviar_password">
                    <input type="submit" value="Actualizar perfil" name="actualizar" class="buttons" id="send_password">  
                </div>         
            </form>

            <h2>Eliminar cuenta</h2>

            <div id="eliminar_container" class="centro" >
                <button id="boton_eliminar" class="buttons" >Eliminar cuenta</button>
            </div>

            <div id="confirmacion_container" class="centro">
                <label for="boton_eliminar_confirmacion" class="centro">¿Esta seguro de que quiere eliminar su cuenta?</label>
                <br>
                <button id="boton_eliminar_confirmacion" class="buttons"><a href="profesor_php/actualizar_perfil.php?opcion=3">Eliminar cuenta</a></button>
                <button id="boton_eliminar_cancelacion" class="buttons">Cancelar</button>
            </div>
        </div>
    </div>
    

    <script>
        document.getElementById("confirmacion_container").style.display = "none";

        document.getElementById("boton_eliminar").onclick = mostrarDiv;
        document.getElementById("boton_eliminar_cancelacion").onclick = MostrarDiv_eliminar;
        
        function mostrarDiv(){
            document.getElementById("eliminar_container").style.display = "none";
            document.getElementById("confirmacion_container").style.display = "block";
        }

        function MostrarDiv_eliminar(){
            document.getElementById("eliminar_container").style.display = "block";
            document.getElementById("confirmacion_container").style.display = "none";
        }

        //Falta verificador de contraseñas
    </script>    
</body>
</html>
<?php 
// Include the database configuration file  
include("../includes/connection.php");
session_start();
$ID = $_SESSION['ID_profesor'];
 
// If file upload form is submitted 
$status = $statusMsg = ''; 
if(isset($_POST["submit"])){ 
    $status = 'error'; 
    if(!empty($_FILES["image"]["name"])) { 
        // Get file info 
        $fileName = basename($_FILES["image"]["name"]); 
        $fileType = pathinfo($fileName, PATHINFO_EXTENSION); 
         
        // Allow certain file formats 
        $allowTypes = array('jpg','png','jpeg','gif'); 
        if(in_array($fileType, $allowTypes)){ 
            $image = $_FILES['image']['tmp_name']; 
            $imgContent = addslashes(file_get_contents($image)); 
         
            // Insert image content into database 
            //$insert = $connect->query("INSERT into images (image, created) VALUES ('$imgContent', NOW())"); 
			$insert = $connect->query("UPDATE Profesor SET Imagen = '$imgContent' WHERE ID_profesor = '$ID'"); 
             
            if($insert){ 
                $status = 'success'; 
                //$statusMsg = "File uploaded successfully."; 
				$statusMsg = '<script type="text/javascript"> alert("Foto actualizada."); location="perfil_profesor.php"; </script>'; 
            }else{ 
                //$statusMsg = "File upload failed, please try again."; 
				$statusMsg = '<script type="text/javascript"> alert("Fallo al subir."); location="perfil_profesor.php"; </script>'; 
            }  
        }else{ 
            //$statusMsg = 'Sorry, only JPG, JPEG, PNG, & GIF files are allowed to upload.'; 
			$statusMsg = '<script type="text/javascript"> alert("Formato incorrecto, solo se acepta JPG, JPEG, PNG y GIF."); location="perfil_profesor.php"; </script>'; 
        } 
    }else{ 
        //$statusMsg = 'Please select an image file to upload.'; 
		$statusMsg = '<script type="text/javascript"> alert("Please select an image file to upload."); location="perfil_profesor.php"; </script>'; 
    } 
} 

// Display status message 
echo $statusMsg; 
?>
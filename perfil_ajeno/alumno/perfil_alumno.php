<?php
// ----------------------ESTO ESTA POR SI ACASO--------------------------------------
include("../../includes/connection.php");

$ID_ALUMNO = mysqli_real_escape_string($connect, $_GET['id_alumno']);

session_start();

$ID = $_SESSION['ID_alumno'];
$ACTIVE = $_SESSION['active'];

$NOMBRE = $_SESSION['Nombre'];
$APELLIDOS = $_SESSION['Apellidos'];


if(!isset($ID) OR $ACTIVE == 0){
    session_destroy();
    header("location: ../../index.html");
    exit();
}

$alumnos = "SELECT * FROM Alumno WHERE ID_alumno='$ID_ALUMNO'";
$peticion_alumnos = mysqli_query($connect, $alumnos);

while($tabla = $peticion_alumnos->fetch_assoc()){
    $NOMBRE_ALUMNO = $tabla['Nombre'];
    $APELLIDO_ALUMNO = $tabla['Apellidos'];

    $RUT_ALUMNO = $tabla['Rut'];
    $FECHA_NACIMIENTO_ALUMNO = $tabla['Fecha_nacimiento'];

    $TELEFONO_ALUMNO = $tabla['Telefono'];
    $DIRECCION_ALUMNO = $tabla['Direccion'];
    $MAIL_ALUMNO = $tabla['Mail'];

    $CINTURON_ALUMNO = $tabla['Cinturon'];
    $GRADO_ALUMNO = $tabla['Grado'];
    $FECHA_GRADO_ALUMNO = $tabla['Fecha_grado'];

    $OBSERVACIONES_ALUMNO = $tabla['Observaciones'];
    $EXPERIENCIA_ALUMNO = $tabla['Experiencia'];

    switch($CINTURON_ALUMNO){
        case 1:
            $cinturon = "Ninguno";
            break;
        case 2:
            $cinturon = "Blanco";
            break;
        case 3:
            $cinturon = "Azul";
            break;
        case 4:
            $cinturon = "Morado";
            break;
        case 5:
            $cinturon = "Cafe";
            break;
        case 6: 
            $cinturon = "Negro";
            break;
        default:
            break;
    }
    
    switch($GRADO_ALUMNO){
        case 1:
            $grado = "Ninguno";
            break;
        case 2:
            $grado = "I";
            break;
        case 3:
            $grado = "II";
            break;
        case 4:
            $grado = "III";
            break;
        case 5: 
            $grado = "IV";
            break;
        default:
            break;
    }
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="../css/alumno.css">

    <title>Perfil de <?php echo "$NOMBRE_ALUMNO $APELLIDO_ALUMNO";?></title>
</head>
<body>
    <ul>
        <li class="log"> <?php echo "<p>Bienvenido $NOMBRE $APELLIDOS</p>";?> </li>
        <li><a href="../../../profesor/profesor_php/home_profesor.php">Inicio</a></li>
        <li><a href="../../../profesor/perfil_profesor.php">Perfil</a></li>
        <li><a href="../../../profesor/horario_profesor.php">Horario</a></li>
        <li class="active"><a href="../../../profesor/vista_clase.php">Clases</a></li>
        <li><a href="../../../general/general_php/logout.php">Cerrar sesion</a></li>
    </ul>
    
    <h1>Perfil de <?php echo "$NOMBRE_ALUMNO $APELLIDO_ALUMNO";?></h1>

    <div class="nombre_alumno">
        <p>
            Nombre: <?php echo "$NOMBRE_ALUMNO $APELLIDO_ALUMNO";?>
        </p>
    </div>

    <div class="cinturon_grado_alumno">
        <p>
            Cinturon: <?php echo $cinturon;?>
            <br>
            Grado: <?php echo $grado;?>
        </p>
    </div>
</body>
</html>